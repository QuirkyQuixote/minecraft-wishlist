Player Mail
===========

Provides an in-server mail system for players.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Entities tagged `wishlist.mail_taker` are post office workers, and will take
named items to add them to a mail pool.

* Mail takers will not pick up unnamed items; instead they will complain to the
  player who dropped the item and emit angry particles.
* Mail takers will not pick up misnamed items; instead they will complain to
  the player who dropped the item and emit angry particles.
* To determine if an item is misnamed, the mail takers search for an entry in
  the `wishlist.playerId` scoreboard that has the same name as the item.
* Items will be stored in the mailroom labelled with the `wishlist.playerId`
  value of the named player.

Every game second, one item will be taken from the mail pool and a player with
a corresponding value of `wishlist.playerId` and who is standing on a solid
block will be searched in the server; if found, an enderfolk will teleport to
that player and give them the item.

If more than one account share the same `wishlist.playerId` value, the mail
office will deliver to the first one that satisfies the conditions.

Overhead
--------

Updates every tick:

* checks every entity in a 4-block radius around each player in search of mail
  takers that collect items.
    * checks every entity in a 2-block radius around mail takers in search of
      items that can be sent as mail.

Updates every second:

* checks the mail list to see if there is mail to be delivered.
    * checks every player to compare their current name to the name of the
      mailed item.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* You Got Mail!: receive mail from another player
* The Backbone of Civilization: send mail to another player
