
# player_mail:leave

playsound minecraft:entity.enderman.teleport neutral @a[distance=..8] ~ ~ ~
tp @s 0 -150 0

# player_mail:load

data modify storage wishlist:args all_chatter append value \
        "Post officers are good with names; they will recognize any typos like that"
data modify storage wishlist:args all_chatter append value \
        "Enderfolk wait until you're on firm ground to deliver you mail, watching…"

function player_mail:tick_1t
function player_mail:tick_1s

# player_mail:messnamed

execute on origin run title @s actionbar { \
        "translate":"player_mail.messnamed", \
        "with":[{"selector":"@e[tag=wishlist.mail_taker,sort=nearest,limit=1]"}] \
        }
tag @s add wishlist.discarded_mail
execute as @e[tag=wishlist.mail_taker,sort=nearest,limit=1] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:angry_villager ~ ~ ~ 0.2 0.2 0.2 1 1 normal

# player_mail:misnamed

$execute on origin run title @s actionbar { \
        "translate":"player_mail.misnamed", \
        "with":[ \
          {"selector":"@e[tag=wishlist.mail_taker,sort=nearest,limit=1]"}, \
          "$(name)" \
          ] \
        }
tag @s add wishlist.discarded_mail
execute as @e[tag=wishlist.mail_taker,sort=nearest,limit=1] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:angry_villager ~ ~ ~ 0.2 0.2 0.2 1 1 normal

# player_mail:receive

scoreboard players set success wishlist.vars 0
execute as @a[tag=!wishlist.hidden,nbt={OnGround:1b}] run function player_mail:receive_1
execute if score success wishlist.vars matches 0 run function player_mail:rotate


# player_mail:receive_1

execute store result score dst wishlist.vars \
        run data get storage wishlist:args mail[0].dst
execute if score dst wishlist.vars = @s wishlist.playerId \
        run function player_mail:receive_2

# player_mail:receive_2

execute at @s \
        rotated ~ 0 \
        run summon minecraft:enderman ^ ^ ^3 { \
          Tags:[wishlist.mail_giver,wishlist.look_alive], \
          CustomName:'"Post Carrier"', \
          NoAI:1b \
        }
playsound minecraft:entity.enderman.teleport neutral @a[distance=..8] ~ ~ ~
tellraw @s { \
        "translate":"wishlist.player_mail.receive_mail", \
        "with":[{"storage":"wishlist:args","nbt":"mail[0].src","interpret":true}], \
        "color":"dark_gray" \
        }
execute at @s \
        rotated ~ 0 \
        run summon minecraft:item ^ ^1.25 ^1 { \
          Tags:[wishlist.mail,wishlist.discarded_mail], \
          Item:{id:"minecraft:stick",Count:1b} \
        }
data modify entity @e[tag=wishlist.mail,sort=nearest,limit=1] Item \
        set from storage wishlist:args mail[0].Item
tag @e[tag=wishlist.mail,distance=..4] remove wishlist.mail
data remove storage wishlist:args mail[0]
scoreboard players set success wishlist.vars 1
advancement grant @s only wishlist:receive_mail

# player_mail:rotate

data modify storage wishlist:args mail append from storage wishlist:args mail[0]
data remove storage wishlist:args mail[0]

# player_mail:send

execute at @s \
        as @e[type=minecraft:item,tag=!wishlist.discarded_mail,distance=..2] \
        run function player_mail:send_1

# player_mail:send_1

execute unless data entity @s Item.components.minecraft:custom_name \
        run return run function player_mail:unnamed


data modify entity @s Tags set value ['"']
data modify entity @s Tags append string entity @s Item.components.minecraft:custom_name 0 1
execute store result score num wishlist.vars if data entity @s Tags[]
data modify entity @s Tags set value []
execute if score num wishlist.vars matches 2 \
        run function player_mail:messnamed


scoreboard players set done wishlist.vars 0
data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.name set string entity @s Item.components.minecraft:custom_name 1 -1
function player_mail:send_2 with storage wishlist:args args
execute if score done wishlist.vars matches 0 \
        run function player_mail:messnamed

# player_mail:send_2

scoreboard players set done wishlist.vars 1
$execute unless score $(name) wishlist.playerId matches 0.. \
        run return run function player_mail:misnamed {name:$(name)}

execute on origin run tag @s add wishlist.match
setblock 0 0 0 minecraft:oak_sign
data modify block 0 0 0 front_text.messages[0] set value '{"selector":"@a[tag=wishlist.match,limit=1]"}'

data modify storage wishlist:args mail append value {}
data modify storage wishlist:args mail[-1].Item set from entity @s Item
data modify storage wishlist:args mail[-1].src set from block 0 0 0 front_text.messages[0]
$execute store result storage wishlist:args mail[-1].dst int 1 \
        run scoreboard players get $(name) wishlist.playerId
execute store result storage wishlist:args mail[-1].time int 1 \
        run time query gametime

setblock 0 0 0 minecraft:air

advancement grant @a[tag=wishlist.match] only wishlist:send_mail

kill @s
particle minecraft:happy_villager ~ ~1 ~ 0.2 0.6 0.2 1 10 normal
tag @a remove wishlist.match


# player_mail:tick_1s

schedule function player_mail:tick_1s 3s

execute as @e[tag=wishlist.mail_giver] at @s run function player_mail:leave

execute if data storage wishlist:args mail[0] run function player_mail:receive

# player_mail:tick_1t

schedule function player_mail:tick_1t 10t

execute at @a \
        as @e[tag=wishlist.mail_taker,distance=..4] \
        run function player_mail:send

# player_mail:unnamed

execute on origin run title @s actionbar { \
        "translate":"player_mail.unnamed", \
        "with":[{"selector":"@e[tag=wishlist.mail_taker,sort=nearest,limit=1]"}] \
        }
tag @s add wishlist.discarded_mail
execute as @e[tag=wishlist.mail_taker,sort=nearest,limit=1] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:angry_villager ~ ~ ~ 0.2 0.2 0.2 1 1 normal
