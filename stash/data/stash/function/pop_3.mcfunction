# pop_3.mcfunction

data modify storage wishlist:args stack append from storage wishlist:args stack[-1].Passengers[0]
function stash:pop_1 with storage wishlist:args stack[-1]
ride @e[tag=wishlist.stash.child,limit=1] mount @s
tag @e remove wishlist.stash.child
data remove storage wishlist:args stack[-1].Passengers[0]
execute if data storage wishlist:args stack[-1].Passengers[0] run function stash:pop_3
