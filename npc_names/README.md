NPC Names
=========

Gives custom names to unnamed villagers, illagers and piglins; from lists
defined at `npc_names/data/npc_names/function/load.mcfunction` that are stored
at the storage `wishlist:args` under the paths `villager_names`,
`illager_names` and `piglin_names`, and can be modified by the user.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Overhead
--------

Updates every second:

* checks every entity in an 8-block radius around each player searching for
  unnamed villagers,
* checks every entity in an 8-block radius around each player searching for
  unnamed pillagers,
* checks every entity in an 8-block radius around each player searching for
  unnamed piglins,
