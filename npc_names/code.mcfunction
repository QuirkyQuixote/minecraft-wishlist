# npc_names:load

data modify storage wishlist:args villager_names set value [ \
"Welham", "Tri", "Faven", "Zibiliath", "Ubty", "Jay", "Triangle", "E", \
"Future", "Willow", "Plume", "Yam", "Eclipse", "Pith", "Bae", "Plothika", \
"Vio", "Mauve", "Yabbit", "Terabithia", "Adair", "Deck", "Ruby", "Sun", "Kap", \
"X", "Pine", "Square", "Umbra", "Moon", "Qumble", "Lime", "Nova", "Plum", \
"Pearl", "Hart", "Pom Pom", "Birch", "Bracket", "Canter", "Sage", "Parsec", \
"Xap", "Curve", "Hum", "Ink", "Honey", "Muni", "Chickory", "Crater", "Sky", \
"Sapphire", "Neutrix", "Tobi", "Goby", "Lumen", "Rub", "Brynn", "Onive", \
"Justice", "Apri", "Tuber", "Lapis", "Flaire", "Helium", "North", "East", \
"Opal", "Ax", "Elm", "Biblo", "Valor", "Easy", "Blu", "Cuprum", "Flair", \
"Borage", "Ash", "Under", "Sam", "Pigeon", "Aspen", "Hypernova", "Canary", \
"Ben", "Kay", "Lev", "Root", "Aphelion", "Kelp", "Wax", "Auxin", "Rowan", \
"Mare", "Loop", "Heart", "Talliamitar", "Idgy", "Juniper", "Milli", "Wick", \
"Pyro", "Trapezoid", "Argyle", "Yorn", "Haven", "Day", "Quint", "Aviar", \
"Avery", "Raine", "Star", "Vapor", "Q", "Kremmy", "Harper", "Hark", "Aster", \
"Ari", "Maple", "Petri", "Phoenix", "Cupcake", "Gibbous", "Bird", "Rune", \
"Lutra", "Pyramid", "Oar", "Berri", "March", "Nine", "Mars", "Carve", "Leen", \
"Cherry", "Yarn", "Noon", "Locust", "Cosmo", "Aeden", "Quasar", "West", "Tree", \
"Berry", "Wane", "Reese", "Lair", "Parallax", "Egel", "Indigo", "Orange", \
"Garnet", "Coron", "Asher", "Jae", "Moo", "Value", "Jey", "Encyclover", "Dyle", \
"Tyad", "Azimuth", "Peridot", "Axe", "Pulsar", "Apple", "Onyx", "Roan", "Book", \
"Hive", "Legion", "Tucket", "Apricot", "Gray", "Time", "Planetoidiath", \
"South", "Yellow", "Checkers", "Nibtucket", "Dendrom", "Lark", "Wothen", \
"Extra", "Chey", "Cranberry", "Luck", "Zenith", "Cadmium", "Squid", "Kirti", \
"Zylith", "Oak", "Hallemutton", "Lane", "Hickory", "Zappy", "Keelan", "Yield", \
"Thyme", "In", "Flare", "Purple", "Usera", "Ram", "Ibringly", "Mercury", \
"Alex", "Riley", "Kapree", "Lyric", "Tayler", "Clover", "Silver", "Amethyst", \
"Perihelion", "Ember", "Yarrow", "Zappi", "Blue", "Probe", "Fig", "Rook", \
"Rin", "Hyperbola", "Quinn", "Lavender", "Zev", "Sphere", "Kilo", "Zap", "Ez", \
"Landish", "Lean", "Wren", "Fur", "Twinkle", "Packett", "Slood", "Kelvin", \
"Puff", "Ice", "Leslie", "Cayenne", "Orbit", "Anvil", "Feather", "Drum", \
"Ovum", "Tuff", "Bouquet", "Stem", "C", "Roam", "Rock", "Roll", "Treble", \
"Piano", "Mastaba", "Mint", "Alyx", "Bass", "Forte", "Axl", "Violet", "Añil", \
"Blurple", "Button", "Sprout", "Spectrum", "Trick", "Tesseract", "Frisk", \
"Jesse", "Francis", "Sparrow", "Jelly", "Candle", "Simplex", "Cotton", "Edge", \
"Stopper", "Dial", "Watch", "Night", "Screen", "Mineam", "Unpetty", "Worth", \
"Vault", "Hinge", "Simmer", "Wooly", "Funlip", "Tulip", "Chestnut", "Trust", \
"Switch", "Pill", "Thorn", "Pipette", "Oblong", "Lid", "Bell", "Toll", \
"Whomst", "Slide", "Shroomhat", "Secant", "Cursor", "Creek", "Butterfingers", \
"Vanilla", "Morningstar", "Testalute", "Inkwell", "Nib", "Staple", "Clasp", \
"Dribble", "Robin", "Ampersand", "Fifty", "Tape", "Beau", "Azulejo", "Roux", \
"Barley", "Eden", "Jordan", "Gaby", "Robbie", "Elmer", "Amari", "Kane", \
"Easton", "Flash", "Arlo", "Burr", "Terrin", "Zane", "Cole", "Nex", "Kai", \
"Lloyd", "Blake", "Camden", "Colby", "Devan", "Kirby", "Squish", "Twist", \
"Saylor", "Harlow", "Aspen", "Beckett", "Darian", "Joey", "Loren", "Maxwell", \
"Brooks", "Pax", "Reese", "Corey", "Zane", "Valen", "Cameron", "Teegan", \
"Jude", "Jace", "Bevel", "Pour", "Slice", "Peyton", "Salem", "Sol", "Kris", \
"Santana", "Bai", "Mica", "Trice", "Ellis", "Koda", "Tater", "Spud", "Starch", \
"Orange", "Carrot", "Borscht", "Pickle", "Beet", "Gluten", "Strawhat", \
"Cornrow", "Grindstone", "Bom Bom", "Max", "Melon", "Litwick", "Echo", "Nurm"] 

data modify storage wishlist:args illager_names set value [ \
"Lyric", "Riley", "Kapree", "Arden", "Taylor", "Doom", "Fir", "Milliner", \
"Axle", "Staple", "Leafsort", "Sweetpea", "Cauldron", "Leigh", "Hardnail", \
"Taiga", "Custard", "Murga", "Rudy", "Scissors", "Brick", "Oshri", "Wolf", \
"Fox", "Paw", "Cobble", "Zimmer", "Cyprus", "Slope", "Blitz", "Petal", \
"Longlash", "Nock", "Quiver", "Fin", "Blank", "Point", "Blunt", "Heath", \
"Calyx", "Corolla", "Whorl", "Mata", "Iris", "Sky", "Cloud", "Tanah", "Ais", \
"Toufan", "Api", "Ali", "Rama", "Bakar", "Rizwan", "Karya", "Walda", "Vaxter", \
"Crow", "Doomberry", "Undertomb", "Doom", "Fir", "Taylor", "Kestrel", "Spook", \
"Dullahan", "Monkeypaw", "Leigh", "Charlie", "Cauldron", "Kendall", \
"Mackenzie", "Yellowroot", "Yellowcap", "Smut", "Morgan"] 

data modify storage wishlist:args piglin_names set value [ \
"Head", "Blade", "Shoulder", "Rib", "Hand", "Loin", "Belly", "Hock", "Trotter", \
"Leg", "Ham", "Rib", "Goldie", "Glitter", "Glint", "Buff", "Shine", "Mignon", \
"Blackeye", "Oogle", "Bullion", "Goldleaf", "Knuckle", "Stem", "Cap", \
"Lamella", "Bolete", "Puffball", "Stinkhorn", "Morel", "Fort", "Mantle", \
"Crust", "Tecton", "Dike", "Sill", "Lacolith", "Pluton", "Batholith", "Mush", \
"Ham", "Brasstack", "Magma"]

data modify storage wishlist:args npc_titles set value [ \
"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "De. ", \
"Div. ", "Ind. ", "M. ", "M*. ", "Mir. ", "Mr. ", "Mre. ", "Ms. ", \
"Msr. ", "Mt. ", "Mm. ", "Mx. ", "Mv. ", "Myr. ", "Mys. ", "Mzr. ", "Nb. ", \
"Pr. ", "Vx. ", "Zr. "]

data modify storage wishlist:args all_chatter append value \
        "Did you know that some Builders don't know that we have names?"
data modify storage wishlist:args all_chatter append value \
        "Yea, Enderfolk don't have names. Because there's only one of them. With millions of bodies"

function npc_names:tick_1s

# npc_names:tick_1s
schedule function npc_names:tick_1s 1s

execute at @a \
        as @e[type=#wishlist:villagers,distance=..8] \
        unless data entity @s CustomName \
        run function npc_names:name_npc {src:"storage wishlist:args villager_names"}

execute at @a \
        as @e[type=#wishlist:illagers,distance=..8] \
        unless data entity @s CustomName \
        run function npc_names:name_npc {src:"storage wishlist:args illager_names"}

execute at @a \
        as @e[type=#wishlist:piglins,distance=..8] \
        unless data entity @s CustomName \
        run function npc_names:name_npc {src:"storage wishlist:args piglin_names"}

# npc_names:name_npc
data modify storage wishlist:args name_npc set value {}
$function wishlist:random_element { \
        src:"$(src)", \
        dst:"storage wishlist:args name_npc.name" \
        }
function wishlist:random_element { \
        src:"storage wishlist:args npc_titles", \
        dst:"storage wishlist:args name_npc.title" \
        }
function npc_names:name_npc_1 with storage wishlist:args name_npc

# npc_names:name_npc_1
$data modify entity @s CustomName set value "\"$(title)$(name)\""

