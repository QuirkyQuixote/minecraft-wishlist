
import json
import os

sizes = {
        "small":[271846,1],
        "medium":[271847,2],
        "large":[271848,3],
        "huge":[271871,4],
        }

brightnesses = {
        "bright":[15,3],
        "soft":[11,2],
        "dim":[7,1],
        }

os.makedirs('data/modern_lights/loot_table', exist_ok=True)
os.makedirs('data/modern_lights/recipe', exist_ok=True)

for s in sizes:
    for b in brightnesses:
        components = {
                "minecraft:custom_name":json.dumps({
                    "translate":f'item.wishlist.{s}_{b}_light',
                    "italic":False
                    }),
                "minecraft:custom_model_data":{"floats":[sizes[s][0]]},
                "minecraft:entity_data":{
                    "id":"minecraft:item_frame",
                    "Tags":[
                        "wishlist.light",
                        f"{s}_{b}"
                        ]
                    }
                }

        ingredients = [
                "minecraft:iron_ingot",
                "minecraft:glass_pane"
                ]

        ingredients += ["minecraft:glowstone_dust"] * brightnesses[b][1]
        ingredients += ["minecraft:prismarine_crystals"] * sizes[s][1]

        with open(f'data/modern_lights/recipe/{s}_{b}_light.json', 'w') as f:
            f.write(json.dumps({
                "type": "minecraft:crafting_shapeless",
                "category": "equipment",
                "group":f"{s}_lights",
                "ingredients": ingredients,
                "result": {
                    "id": "minecraft:item_frame",
                    "count": 1,
                    "components": components
                    }
                }, indent=2))

        with open(f"data/modern_lights/loot_table/{s}_{b}_light.json", 'w') as f:
            f.write(json.dumps({
              "pools": [
                {
                  "rolls": 1,
                  "entries": [
                    {
                      "type": "minecraft:item",
                      "name": "minecraft:item_frame",
                      "functions": [
                        {
                          "function": "minecraft:set_components",
                          "components": components
                          }
                        ]
                      }
                    ]
                  }
                ]
              }, indent=2))


