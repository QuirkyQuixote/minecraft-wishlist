
# modern_lights:load

data modify storage wishlist:args all_chatter append value \
        "Have you seen the new light fixtures?"
data modify storage wishlist:args all_chatter append value \
        "There are some issues with the new light stuff, but damn if they aren't pretty"

data modify storage modern_lights:conf small_bright set value { \
  id: "small_bright", \
  transformation: [2f,0f,0f,-.025f,0f,1f,0f,-.125f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 15, \
  item:{ \
    custom_model_data:{floats:[271846]}, \
    custom_name:'{"translate":"item.wishlist.small_bright_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf medium_bright set value { \
  id: "medium_bright", \
  transformation: [4f,0f,0f,-.05f,0f,2f,0f,-.25f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 15, \
  item:{ \
    custom_model_data:{floats:[271847]}, \
    custom_name:'{"translate":"item.wishlist.medium_bright_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf large_bright set value { \
  id: "large_bright", \
  transformation: [8f,0f,0f,-.1f,0f,4f,0f,-.5f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 15, \
  item:{ \
    custom_model_data:{floats:[271848]}, \
    custom_name:'{"translate":"item.wishlist.large_bright_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf huge_bright set value { \
  id: "huge_bright", \
  transformation: [24f,0f,0f,-.3f,0f,12f,0f,-1.5f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 15, \
  item:{ \
    custom_model_data:{floats:[271871]}, \
    custom_name:'{"translate":"item.wishlist.huge_bright_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf small_soft set value { \
  id: "small_soft", \
  transformation: [2f,0f,0f,-.025f,0f,1f,0f,-.125f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 11, \
  item:{ \
    custom_model_data:{floats:[271846]}, \
    custom_name:'{"translate":"item.wishlist.small_soft_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf medium_soft set value { \
  id: "medium_soft", \
  transformation: [4f,0f,0f,-.05f,0f,2f,0f,-.25f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 11, \
  item:{ \
    custom_model_data:{floats:[271847]}, \
    custom_name:'{"translate":"item.wishlist.medium_soft_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf large_soft set value { \
  id: "large_soft", \
  transformation: [8f,0f,0f,-.1f,0f,4f,0f,-.5f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 11, \
  item:{ \
    custom_model_data:{floats:[271848]}, \
    custom_name:'{"translate":"item.wishlist.large_soft_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf huge_soft set value { \
  id: "huge_soft", \
  transformation: [24f,0f,0f,-.3f,0f,12f,0f,-1.5f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 11, \
  item:{ \
    custom_model_data:{floats:[271871]}, \
    custom_name:'{"translate":"item.wishlist.huge_soft_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf small_dim set value { \
  id: "small_dim", \
  transformation: [2f,0f,0f,-.025f,0f,1f,0f,-.125f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 7, \
  item:{ \
    custom_model_data:{floats:[271846]}, \
    custom_name:'{"translate":"item.wishlist.small_dim_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf medium_dim set value { \
  id: "medium_dim", \
  transformation: [4f,0f,0f,-.05f,0f,2f,0f,-.25f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 7, \
  item:{ \
    custom_model_data:{floats:[271847]}, \
    custom_name:'{"translate":"item.wishlist.medium_dim_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf large_dim set value { \
  id: "large_dim", \
  transformation: [8f,0f,0f,-.1f,0f,4f,0f,-.5f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 7, \
  item:{ \
    custom_model_data:{floats:[271848]}, \
    custom_name:'{"translate":"item.wishlist.large_dim_light","italic":false}', \
  } \
}

data modify storage modern_lights:conf huge_dim set value { \
  id: "huge_dim", \
  transformation: [24f,0f,0f,-.3f,0f,12f,0f,-1.5f,0f,0f,1f,.5f,0f,0f,0f,1f], \
  level: 7, \
  item:{ \
    custom_model_data:{floats:[271871]}, \
    custom_name:'{"translate":"item.wishlist.huge_dim_light","italic":false}', \
  } \
}

function modern_lights:tick

# modern_lights:tick

schedule function modern_lights:tick 1s

execute at @a \
        anchored eyes \
        as @e[tag=wishlist.modern_light,distance=..6] \
        at @s \
        if block ~ ~ ~ #wishlist:empty \
        run function modern_lights:break

# modern_lights:break
fill ^ ^ ^1 ^ ^ ^1 minecraft:air replace minecraft:light
tag @s remove wishlist.modern_light
data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.id set from entity @s Tags[0]
function modern_lights:give with storage wishlist:args args
kill @s

# modern_lights:give
$data modify storage modern_lights:vars args set from storage modern_lights:conf $(id).item
$data modify storage modern_lights:vars args.id set value $(id)
function modern_lights:give_1 with storage modern_lights:vars args

# modern_lights:give_1
$summon minecraft:item ~ ~ ~ { \
  Item: { \
    id:"minecraft:item_frame", \
    count:1, \
    components: { \
      "minecraft:custom_name": '$(custom_name)', \
      "minecraft:custom_model_data":{"floats":[$(custom_model_data)]}, \
      "minecraft:entity_data":{ \
        id:"minecraft:item_frame", \
        Tags:[wishlist.light,$(id)] \
        } \
      } \
    } \
  }

# modern_lights:place
advancement revoke @s only modern_lights:place
execute as @e[type=item_frame,tag=wishlist.light,distance=..8] \
        at @s \
        run function modern_lights:place_1

# modern_lights:place_1
tag @s remove wishlist.light
data modify storage modern_lights:vars args set value {}
data modify storage modern_lights:vars args.id set from entity @s Tags[0]
function modern_lights:place_2 with storage modern_lights:vars args
kill @s

# modern_lights:place_2
data modify storage modern_lights:vars args set value { \
        data: { \
                Tags:[wishlist.modern_light], \
                text:'{"text":" "}', \
                background:-1, \
                brightness:{ \
                        block:15, \
                        sky:15 \
                } \
        } \
}

$data modify storage modern_lights:vars args.data.Tags append value $(id)
data modify storage modern_lights:vars args.data.Rotation set from entity @s Rotation
$data modify storage modern_lights:vars args.data.transformation set from storage modern_lights:conf $(id).transformation
$data modify storage modern_lights:vars args.level set from storage modern_lights:conf $(id).level
function modern_lights:place_3 with storage modern_lights:vars args

# modern_lights:place_3
$summon minecraft:text_display ^ ^ ^-0.5 $(data)
$setblock ~ ~ ~ light[level=$(level)]

