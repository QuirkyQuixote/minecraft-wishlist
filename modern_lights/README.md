Modern Lights
=============

Provides light items that can be placed on a face of an existing block to
create bright spots that suggest LED panels of varying sizes or car lamps; the
lights can be removed by breaking the block they are placed on.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

There are twelve different light items, combining four sizes (small, medium,
large, huge) and three brightnesses (dim, soft, bright).

Small lights are as wide as a fence post, medium lights are as wide as a wall,
large lights are a full block in size, and huge lights are 3x3 blocks in size.

Bright, soft, and dim lights emit a ligth level of 15, 11, and 7 respectively.

### Recipes

All lights are craftable with:

* 1 iron ingot
* 1 glass pane
* 1--3 glowstone dust (determines brightness)
* 1--4 prismarine crystals (determines pane size)

All recipes are shapeless.

All recipes are unlocked when holding prismarine crystals.

### Loot Tables

The following loot tables generate custom light items:

* `modern_lights:small_bright`
* `modern_lights:small_soft`
* `modern_lights:small_dim`
* `modern_lights:medium_bright`
* `modern_lights:medium_soft`
* `modern_lights:medium_dim`
* `modern_lights:large_bright`
* `modern_lights:large_soft`
* `modern_lights:large_dim`
* `modern_lights:huge_bright`
* `modern_lights:huge_soft`
* `modern_lights:huge_dim`

Overhead
--------

Uses one advancements:

* checks for players placing item frames with the nbt data that defines a light
  type.

Updates every second:

* checks every entity in a 6-block radius around each player in search of
  lights whose blocks have been removed to destroy and drop them as items.

Known Issues
------------

The datapack interacts badly with protection mods such as WorldGuard: if you
place a light, the mod will react by removing the item, and placing it back in
your inventory, but the datapack will still register the light as placed, so
the base item will be duped.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:


