#!/bin/python3

# Split code file into separate mcfunctions in the appropriate directories

import sys
import re
import os

fun = re.compile(r"# *([a-zA-Z0-9_]+):([a-zA-Z0-9_]+)")

for a in sys.argv[1:]:
    with open(a, 'r') as src:
        dst = None

        for line in src:
            m = fun.match(line)
            if m:
                path = f'data/{m.group(1)}/function'
                file = f'{path}/{m.group(2)}.mcfunction'
                if not os.path.exists(path):
                    os.makedirs(path)
                dst = open(file, 'w')
                dst.write(line)
            elif dst:
                dst.write(line)
            elif line == '\n':
                continue
            else:
                raise Exception('code before function name',line)


