Parkour
=======

Adds some utilities to make parkour more interesting

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Custom Blocks](../custom_blocks) for custom blocks.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The core of the parkour datapack is the jump function that makes a player jump
as far and high as desired by making them ride a temporary arrow:

```
/function parkour:jump {motion:[<x>,<y>,<z>]}
```

The datapack provides some custom blocks that can trigger this function
automatically: the vertical spring launches the player vertically, and the
diagonal spring launches the player in a 45-degree angle vertically, with the
horizotal direction depending on the orientation of the spring.

Players riding entities will not be launched by springs.

Players can avoid being launched by springs by crouching.

```
/function custom_blocks:give {id:yellow_vertical_spring}
/function custom_blocks:give {id:yellow_diagonal_spring}
/function custom_blocks:give {id:red_vertical_spring}
/function custom_blocks:give {id:red_diagonal_spring}
```

Red springs bounce players further than yellow ones; in general:

* a yellow spring will allow crossing an 8-wide gap or climb a 4-high wall,
* a red spring will allow crossing a 16-wide gap or climb an 8-high wall.

Overhead
--------

Checks every player every tick to see if they are touching a spring

Known Issues
------------

The invisible snowball the player rides has a bad particle texture and they
become visible as magenta and black squares occasionally

The snowball can break items in frames too.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

