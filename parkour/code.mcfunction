# parkour:load
data modify storage parkour:conf diagonal_spring_motion set value [ \
  {motion:[0.0,0.5657,0.5657]}, \
  {motion:[-0.2165,0.5657,0.5226]}, \
  {motion:[-0.4,0.5657,0.4]}, \
  {motion:[-0.5226,0.5657,0.2165]}, \
  {motion:[-0.5657,0.5657,0.0]}, \
  {motion:[-0.5226,0.5657,-0.2165]}, \
  {motion:[-0.4,0.5657,-0.4]}, \
  {motion:[-0.2165,0.5657,-0.5226]}, \
  {motion:[0.0,0.5657,-0.5657]}, \
  {motion:[0.2165,0.5657,-0.5226]}, \
  {motion:[0.4,0.5657,-0.4]}, \
  {motion:[0.5226,0.5657,-0.2165]}, \
  {motion:[0.5657,0.5657,0.0]}, \
  {motion:[0.5226,0.5657,0.2165]}, \
  {motion:[0.4,0.5657,0.4]}, \
  {motion:[0.2165,0.5657,0.5226]}, \
]

data modify storage custom_blocks:conf yellow_vertical_spring set value { \
  interaction: { \
    Tags: [wishlist.spring,wishlist.spring.vertical,wishlist.spring.yellow], \
    width: 1.0, \
    height: 1.0 \
  }, \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
  }, \
  item: { \
    custom_model_data:{floats:[271860]}, \
    custom_name:'{"translate":"item.wishlist.yellow_vertical_spring","italic":false}' \
  }, \
  block: "minecraft:air", \
}

data modify storage custom_blocks:conf yellow_diagonal_spring set value { \
  interaction: { \
    Tags: [wishlist.spring,wishlist.spring.diagonal,wishlist.spring.yellow], \
    width: 1.0, \
    height: 1.0 \
  }, \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
  }, \
  item: { \
    custom_model_data:{floats:[271861]}, \
    custom_name:'{"translate":"item.wishlist.yellow_diagonal_spring","italic":false}' \
  }, \
  block: "minecraft:air", \
}

data modify storage custom_blocks:conf red_vertical_spring set value { \
  interaction: { \
    Tags: [wishlist.spring,wishlist.spring.vertical,wishlist.spring.red], \
    width: 1.0, \
    height: 1.0 \
  }, \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
  }, \
  item: { \
    custom_model_data:{floats:[271862]}, \
    custom_name:'{"translate":"item.wishlist.red_vertical_spring","italic":false}' \
  }, \
  block: "minecraft:air", \
}

data modify storage custom_blocks:conf red_diagonal_spring set value { \
  interaction: { \
    Tags: [wishlist.spring,wishlist.spring.diagonal,wishlist.spring.red], \
    width: 1.0, \
    height: 1.0 \
  }, \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
  }, \
  item: { \
    custom_model_data:{floats:[271863]}, \
    custom_name:'{"translate":"item.wishlist.red_diagonal_spring","italic":false}' \
  }, \
  block: "minecraft:air", \
}

function parkour:tick_1t

# parkour:tick_1t
schedule function parkour:tick_1t 1t
execute as @a[tag=!wishlist.sneaking] \
        unless data entity @s RootVehicle \
        at @s \
        run function parkour:spring

# parkour:diagonal
data modify storage wishlist:args args set value {}
execute on passengers store result score yaw wishlist.vars run data get entity @s Rotation[0]
execute store result storage wishlist:args args.yaw int 0.044444 \
        run scoreboard players add yaw wishlist.vars 12
function parkour:diagonal_1 with storage wishlist:args args 
scoreboard players set go wishlist.vars 1

# parkour:diagonal_1
$data modify storage wishlist:args args set from storage parkour:conf diagonal_spring_motion[$(yaw)]

# parkour:jump
$summon minecraft:snowball ~ ~ ~ { \
  Tags:[wishlist.jump], \
  Motion:$(motion), \
  Silent:1b, \
  Item:{ \
    id: "minecraft:iron_nugget", \
    count: 1, \
    components: { \
      "minecraft:custom_model_data":999999 \
      } \
  } \
}
ride @s mount @e[tag=wishlist.jump,distance=0,limit=1]
playsound parkour:spring.bounce player @a ~ ~ ~

# parkour:spring
scoreboard players set go wishlist.vars 0
execute as @e[tag=wishlist.spring,dx=0,dy=0,dz=0,sort=nearest,limit=1] \
        run function parkour:spring_1
execute if score go wishlist.vars matches 0 run return fail
execute at @e[tag=wishlist.spring,dx=0,dy=0,dz=0,sort=nearest,limit=1] \
        run function parkour:jump with storage wishlist:args args

# parkour:spring_1
execute if entity @s[tag=wishlist.spring.vertical] \
        run function parkour:vertical
execute if entity @s[tag=wishlist.spring.diagonal] \
        run function parkour:diagonal
execute if entity @s[tag=wishlist.spring.yellow] \
        store result storage wishlist:args args.motion[0] double 0.0007 \
        run data get storage wishlist:args args.motion[0] 1000
execute if entity @s[tag=wishlist.spring.yellow] \
        store result storage wishlist:args args.motion[1] double 0.0007 \
        run data get storage wishlist:args args.motion[1] 1000
execute if entity @s[tag=wishlist.spring.yellow] \
        store result storage wishlist:args args.motion[2] double 0.0007 \
        run data get storage wishlist:args args.motion[2] 1000

# parkour:vertical
data modify storage wishlist:args args set value {motion:[0.0,0.8,0.0]}
scoreboard players set go wishlist.vars 1

# parkour:jump2
$execute at @s positioned ~$(x) ~-0.1 ~$(z) run function parkour:jump2_1

# parkour:jump2_1
summon minecraft:breeze_wind_charge ~ ~ ~ {Motion:[0.0,-1.0,0.0]}
summon minecraft:breeze_wind_charge ~ ~ ~ {Motion:[0.0,-1.0,0.0]}
