
import json
import os

colors = {
        "white":["#f9fffe", 271828],
        "light_gray":["#9d9d97", 271829],
        "gray":["#474f52", 271830],
        "black":["#1d1d21", 271831],
        "brown":["#835432", 271832],
        "red":["#b02e26", 271833],
        "orange":["#f9801d", 271834],
        "yellow":["#fed83d", 271835],
        "lime":["#80c71f", 271836],
        "green":["#5e7c16", 271837],
        "cyan":["#169c9c", 271838],
        "light_blue":["#3ab3da", 271839],
        "blue":["#3c44aa", 271840],
        "purple":["#8932b8", 271841],
        "magenta":["#c74ebd", 271842],
        "pink":["#f38baa", 271843],
        }

boldness = {
        "thin":"false",
        "bold":"true",
        }

os.makedirs('data/graffiti/loot_table', exist_ok=True)
os.makedirs('data/graffiti/recipe', exist_ok=True)

for c in colors:
    for b in boldness:
        components = {
                "minecraft:custom_name":json.dumps({
                    "translate":f'item.wishlist.{b}_{c}_marker',
                    "italic":False
                    }),
                "minecraft:custom_model_data":{"floats":[colors[c][1]]},
                "minecraft:custom_data": {
                    "wishlist_marker":{
                        "color":colors[c][0],
                        "bold":boldness[b]
                        }
                    }
                }

        with open(f'data/graffiti/recipe/{b}_{c}_marker.json', 'w') as f:
            f.write(json.dumps({
                "type": "minecraft:crafting_shapeless",
                "category": "equipment",
                "group":f"{b}_markers",
                "ingredients": [
                    "minecraft:iron_ingot",
                    "#minecraft:wool" if b == 'bold' else "#minecraft:wool_carpets",
                    "minecraft:slime_ball",
                    f"minecraft:{c}_dye"
                    ],
                "result": {
                    "id": "minecraft:writable_book",
                    "count": 1,
                    "components": components
                    }
                }, indent=2))

        with open(f"data/graffiti/loot_table/{b}_{c}_marker.json", 'w') as f:
            f.write(json.dumps({
              "pools": [
                {
                  "rolls": 1,
                  "entries": [
                    {
                      "type": "minecraft:item",
                      "name": "minecraft:writable_book",
                      "functions": [
                        {
                          "function": "minecraft:set_components",
                          "components": components
                          }
                        ]
                      }
                    ]
                  }
                ]
              }, indent=2))

