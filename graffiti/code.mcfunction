
# graffiti:erase
advancement revoke @s only graffiti:erase
scoreboard players set steps wishlist.vars 60
execute anchored eyes positioned ^ ^ ^ if function graffiti:erase_1 \
        run advancement grant @s only wishlist:use_eraser

# graffiti:erase_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ minecraft:air run return run function graffiti:erase_2
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run return run function graffiti:erase_1

# graffiti:erase_2
execute if score steps wishlist.vars matches 50.. \
        run return run kill @e[tag=wishlist.graffiti,distance=..0.5,sort=nearest,limit=1]
execute if score steps wishlist.vars matches 40..49 \
        run return run kill @e[tag=wishlist.graffiti,distance=..1,sort=nearest,limit=1]
execute if score steps wishlist.vars matches 30..39 \
        run return run kill @e[tag=wishlist.graffiti,distance=..2,sort=nearest,limit=1]
execute if score steps wishlist.vars matches 20..29 \
        run return run kill @e[tag=wishlist.graffiti,distance=..3,sort=nearest,limit=1]
execute if score steps wishlist.vars matches ..19 \
        run return run kill @e[tag=wishlist.graffiti,distance=..4,sort=nearest,limit=1]
return fail

# graffiti:write
advancement revoke @s only graffiti:write
execute unless data entity @s SelectedItem.components.minecraft:custom_data.wishlist_marker run return fail
execute unless data entity @s SelectedItem.components.minecraft:writable_book_content.pages run return fail
execute store result score len wishlist.vars \
        run data get entity @s SelectedItem.components.minecraft:writable_book_content.pages[0].raw
execute if score len wishlist.vars matches 0 run return fail
scoreboard players set steps wishlist.vars 60
execute anchored eyes positioned ^ ^ ^ run function graffiti:write_1
item modify entity @s weapon.mainhand graffiti:reset

# graffiti:write_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ minecraft:air run return run function graffiti:write_2
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run function graffiti:write_1

# graffiti:write_2
data modify storage wishlist:args args set from entity @s SelectedItem.components.minecraft:custom_data.wishlist_marker
data modify storage wishlist:args args.text set from entity @s SelectedItem.components.minecraft:writable_book_content.pages[0].raw
execute store result score yaw wishlist.vars run data get entity @s Rotation[0] 1000
execute store result storage wishlist:args args.yaw float 0.001 \
        run scoreboard players add yaw wishlist.vars 180000
execute if score steps wishlist.vars matches 50.. \
        run data modify storage wishlist:args args.scale set value 0.5f
execute if score steps wishlist.vars matches 40..49 \
        run data modify storage wishlist:args args.scale set value 1.0f
execute if score steps wishlist.vars matches 30..39 \
        run data modify storage wishlist:args args.scale set value 2.0f
execute if score steps wishlist.vars matches 20..29 \
        run data modify storage wishlist:args args.scale set value 3.0f
execute if score steps wishlist.vars matches ..19 \
        run data modify storage wishlist:args args.scale set value 4.0f

function graffiti:write_3 with storage wishlist:args args
advancement grant @s only wishlist:use_marker

# graffiti:write_3
$execute positioned ~0.1 ~ ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[-90.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~-0.1 ~ ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[90.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~0.1 ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[$(yaw)f,-90.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~-0.1 ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[$(yaw)f,90.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~ ~0.1 \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[0.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~ ~-0.1 \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[180.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
