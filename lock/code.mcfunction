
# lock:chest
execute if data block ~ ~ ~ lock run return run function lock:chest_1
data modify block ~ ~ ~ lock set from entity @s SelectedItem
playsound lock:lock block @a ~ ~ ~
title @s actionbar {"translate":"lock.locked"}



# lock:chest_1
data modify storage wishlist:args lock \
        set from block ~ ~ ~ lock.components.minecraft:custom_data.wishlist_magic.args.code
execute store success score fail wishlist.vars \
        run data modify storage wishlist:args lock \
        set from entity @s SelectedItem.components.minecraft:custom_data.wishlist_magic.args.code
execute unless score fail wishlist.vars matches 0 run return run function lock:locked
data remove block ~ ~ ~ lock
playsound lock:unlock block @a ~ ~ ~
title @s actionbar {"translate":"lock.unlocked"}



# lock:cut
execute unless predicate lock:holding_blank run return fail
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.code int 1 run random value 0..2000000000
function lock:cut_1 with storage wishlist:args args
loot replace entity @s weapon.mainhand loot lock:key
playsound minecraft:block.grindstone.use player @a ~ ~ ~



# lock:cut_1
$data modify storage wishlist:args code set value "$(code)"

# lock:lock
$data modify storage wishlist:args code set value $(code)
scoreboard players set steps wishlist.vars 45
execute anchored eyes positioned ^ ^ ^ run function lock:lock_1

# lock:lock_1
execute if score steps wishlist.vars matches 0 run return fail
execute if block ~ ~ ~ #wishlist:lockable \
        run return run function lock:chest
execute if block ~ ~ ~ #minecraft:trapdoors \
        run return run function lock:lock_2 {height:1}
execute if block ~ ~ ~ #minecraft:doors[half=lower] \
        run return run function lock:lock_2 {height:2}
execute if block ~ ~ ~ #minecraft:doors[half=upper] \
        positioned ~ ~-1 ~ \
        run return run function lock:lock_2 {height:2}
execute unless block ~ ~ ~ #wishlist:non_full run return fail
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run function lock:lock_1

# lock:lock_2
$data modify storage wishlist:args args set value { \
        lock: { \
                Tags:[wishlist.lock], \
                width:1.001, \
                height:$(height).001, \
        } \
}
data modify storage wishlist:args args.lock.Tags append from entity @s SelectedItem.components.minecraft:custom_data.wishlist_magic.args.code
function lock:lock_3 with storage wishlist:args args



# lock:lock_3
$execute align xyz run summon minecraft:interaction ~0.5 ~ ~0.5 $(lock)
playsound lock:lock block @a ~ ~ ~
title @s actionbar {"translate":"lock.locked"}

# lock:locked
playsound minecraft:block.chest.locked block @s ~ ~ ~
title @s actionbar {"translate":"lock.is_locked"}


# lock:punch
advancement revoke @s only lock:punch
playsound minecraft:block.chest.locked block @s ~ ~ ~
title @s actionbar {"translate":"lock.locked"}

# lock:unlock
advancement revoke @s only lock:unlock
data modify storage wishlist:args args set from entity @s SelectedItem.components.minecraft:custom_data.wishlist_magic.args

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        at @s \
        run function lock:unlock_1 with storage wishlist:args args

execute if score done wishlist.vars matches 0 \
        run function lock:locked
execute unless score done wishlist.vars matches 0 \
        run title @s actionbar {"translate":"lock.unlocked"}

# lock:unlock_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
$execute store success score go wishlist.vars run tag @s remove $(code)
execute if score go wishlist.vars matches 0 run return fail
playsound lock:unlock block @a ~ ~ ~
kill @s
scoreboard players set done wishlist.vars 1


