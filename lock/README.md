Lock
====

Adds key items that can lock chests, doors and trapdoors

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for player UI
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

### ![blank] Key Blank

This is an uncut key, that can't open or close items; its only purpose is to
generate the actual key items by using it.

### ![key] Key

When created by using a blank, a random key code is generated and stored in the
key nbt data.

Crouching and using the key in any container, door, or trapdoor will lock it:

* Containers are tagged with `{Lock:<key code>}`, an unused vanilla feature
  that does in fact lock the container.
* For doors and trapdoors an interaction entity is created around them that
  shows and "It's Locked!" message when used.

The locked block can be unloked by using the original key on it.

Overhead
--------

See [Magic](../magic)

Known Issues
------------

The interaction entity around doors and trapdoors takes the entirety of the
block(s) the occupy; that means that a small mob that got into the space will
be covered by it, and become non-interactive itself.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

[blank]: lock/assets/lock/textures/item/lock_blank.png ""
[key]: lock/assets/lock/textures/item/lock_key.png ""
