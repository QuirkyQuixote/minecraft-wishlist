# custom_discs:load
data modify storage custom_blocks:conf gramophone set value { \
  interaction: { \
    Tags: [wishlist.gramophone.empty], \
    width: 1.01, \
    height: 1.01 \
  }, \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,-0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
  }, \
  item: { \
        custom_model_data:{floats:[271864]}, \
        custom_name:'{"translate":"item.wishlist.gramophone","italic":false}' \
  }, \
  block: "minecraft:barrier" \
}

