Custom Discs
============

Provides some additional music discs for the game, and a custom decorative
gramophone block.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Custom Blocks](../custom_blocks) for custom blocks
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The datapack provides loot tables to obtain the custom items

* `custom_discs:music_disc_mindless_marionette`
* `custom_discs:music_disc_rusty_rain`
* `custom_discs:music_disc_woodland_whispers`
* `custom_discs:jukebox`

Overhead
--------

Custom discs have no overhead beyond what vanilla minecraft requires

The gramophone uses [Custom Blocks](../custom_blocks) for placing/removing.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

