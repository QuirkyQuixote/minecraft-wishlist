# random_tick:load
scoreboard players set 16 wishlist.vars 16
scoreboard players set 256 wishlist.vars 256
function random_tick:tick

# random_tick:tick
execute as @a at @s run function random_tick:tick_1
schedule function random_tick:tick 1t

# random_tick:tick_1
execute store result score x wishlist.vars run data get entity @s Pos[0] 0.0625
execute store result score z wishlist.vars run data get entity @s Pos[2] 0.0625
execute summon minecraft:marker run function random_tick:tick_2

# random_tick:tick_2
execute store result score r wishlist.vars run random value 0..2000000000
scoreboard players operation r wishlist.vars /= 256 wishlist.vars
scoreboard players operation r wishlist.vars %= 16 wishlist.vars
scoreboard players remove x wishlist.vars 8
scoreboard players operation x wishlist.vars += r wishlist.vars
execute store result entity @s Pos[0] double 16 run scoreboard players get x wishlist.vars

execute store result score r wishlist.vars run random value 0..2000000000
scoreboard players operation r wishlist.vars /= 256 wishlist.vars
scoreboard players operation r wishlist.vars %= 16 wishlist.vars
scoreboard players remove z wishlist.vars 8
scoreboard players operation z wishlist.vars += r wishlist.vars
execute store result entity @s Pos[2] double 16 run scoreboard players get z wishlist.vars

execute at @s positioned ~ ~-128 ~ run function #wishlist:random_tick
kill @s

# random_tick:show
#particle minecraft:flash ~8 ~128 ~8 0 0 0 1 1 force @a[gamemode=creative]

