Random Tick
===========

Ticks one loaded chunk per player per tick, executing all functions tagged
`#wishlist:random_tick`.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.

Usage
-----

Any function tagged `#wishlist:random_tick` is called once per tick with the
position of the north-bottom-west corner of a random chunk. Here's an example
of one such function that targets every entity in that chunk:

```
execute as @e[dx=16,dy=256,dz=16]
        at @s
        run <your code here>
```

Overhead
--------

Updates every tick

* checks every player and chooses a random loaded chunk around them to tick

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

