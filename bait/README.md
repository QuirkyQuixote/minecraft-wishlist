Bait
====

Allows creating points of interest that will summon NPCs with *very* simple AI
when a player is close, and unload when the player is not close anymore.

This datapack is similar to [Multitudes](../multitudes), with some differences:

* Bait stores all POI information in storage; entities are only created when
  the actual NPCs are spawned.
* Bait stores all positions as integers so NPCs are spawned aligned to the
  grid.
* With Bait, playing in peaceful gamemode or otherwise blocking the spawning of
  certain entities will not erode the baits placed.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for UI
* Requires [Wishlist Resources](../resources) for media and text translations.
* Recommends [Chatterbox](../chatterbox) to enable NPC chatter.
* Recommends [NPC Names](../npc_names) to autoname NPCs.

Usage
-----

The bait datapack provides two new items for the magic datapack.

Holding any of these on the main hand will show the positions of baits in a
3x1x3 box of chunks centered in the player as particle effects:

* baits owned by the player are colored golden
* baits owned by other players are colored teal

### ![wand] Place Bait

* Blaces an NPC bait where NPCs may spawn,
* Can be obtained with the loot table `bait:place_bait`.

### ![wand] Remove Bait

* Removes all baits placed by the same player in that block.
* Can be obtained with the loot table `bait:remove_bait`.

Overhead
--------

Every second checks if any chunk in a 3x3x3 chunk box around each player
contains unspawned roamers, and if so, spawns them.

Every second checks if any chunk in a 3x1x3 chunk box around each player
holding one of the wands contains baits, and if so spawns a particle for each
one.

Every second checks the chunks in the loaded list and if no player is close to
one, roamers spawned in them are removed and the chunk removed from the loaded
list.

Teleporting away from chunks holding roamers or otherwise moving from them at
*very* high speeds will result in them being unloaded by the game before the
datapack can do so; in those situations the datapack will temporarily forceload
the chunk, remove the roamers, and deforceload it.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

[wand]: magic/assets/magic/textures/item/wand.png ""
