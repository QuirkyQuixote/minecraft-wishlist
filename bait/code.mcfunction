# bait:load
execute unless data storage bait:conf chunks \
        run data modify storage bait:conf chunks \
        set from storage wishlist:bait chunks
execute unless data storage bait:conf loaded \
        run data modify storage bait:conf loaded \
        set value []
function bait:tick_1s

# bait:tick_1s
schedule function bait:tick_1s 1s
data modify storage wishlist:args loaded set from storage bait:conf loaded
data modify storage bait:conf loaded set value []
function bait:remove_mobs
execute as @a[gamemode=!spectator] at @s run function bait:place_mobs
execute as @a[predicate=bait:holding_wand] at @s run function bait:show_bait

# bait:random_tick

# bait:chunk_args
# Construct the chunk identifier from an entity's position
data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.dimension set from entity @p Dimension
execute store result storage wishlist:args args.x int 1 run data get entity @s Pos[0] 0.0625
execute store result storage wishlist:args args.y int 1 run data get entity @s Pos[1] 0.0625
execute store result storage wishlist:args args.z int 1 run data get entity @s Pos[2] 0.0625
function bait:chunk_args_1 with storage wishlist:args args

# bait:chunk_args_offset
# Construct the chunk identifier from an entity's position plus (x,y,z)
data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.dimension set from entity @p Dimension

execute store result score a wishlist.vars run data get entity @s Pos[0] 0.0625
$scoreboard players set b wishlist.vars $(x)
execute store result storage wishlist:args args.x int 1 \
        run scoreboard players operation a wishlist.vars += b wishlist.vars

execute store result score a wishlist.vars run data get entity @s Pos[1] 0.0625
$scoreboard players set b wishlist.vars $(y)
execute store result storage wishlist:args args.y int 1 \
        run scoreboard players operation a wishlist.vars += b wishlist.vars

execute store result score a wishlist.vars run data get entity @s Pos[2] 0.0625
$scoreboard players set b wishlist.vars $(z)
execute store result storage wishlist:args args.z int 1 \
        run scoreboard players operation a wishlist.vars += b wishlist.vars

function bait:chunk_args_1 with storage wishlist:args args

# bait:raycast_args
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.player int 1 \
        run scoreboard players get @s wishlist.playerId
data modify storage wishlist:args args.x set from storage wishlist:raycast x0
data modify storage wishlist:args args.y set from storage wishlist:raycast y0
data modify storage wishlist:args args.z set from storage wishlist:raycast z0
data modify storage wishlist:args arg2 set value {}
data modify storage wishlist:args arg2.dimension set from entity @p Dimension
execute store result storage wishlist:args arg2.x int 1 run data get storage wishlist:args args.x 0.0625
execute store result storage wishlist:args arg2.y int 1 run data get storage wishlist:args args.y 0.0625
execute store result storage wishlist:args arg2.z int 1 run data get storage wishlist:args args.z 0.0625
function bait:chunk_args_1 with storage wishlist:args arg2

# bait:chunk_args_1
$data modify storage wishlist:args args.chunk \
        set value "$(dimension).$(x).$(y).$(z)"

# bait:show_bait
function bait:chunk_args_offset {x:-1,y:0,z:-1}
function bait:show_bait_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:0,z:0}
function bait:show_bait_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:0,z:1}
function bait:show_bait_1 with storage wishlist:args args

function bait:chunk_args_offset {x:0,y:0,z:-1}
function bait:show_bait_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:0,z:0}
function bait:show_bait_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:0,z:1}
function bait:show_bait_1 with storage wishlist:args args

function bait:chunk_args_offset {x:1,y:0,z:-1}
function bait:show_bait_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:0,z:0}
function bait:show_bait_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:0,z:1}
function bait:show_bait_1 with storage wishlist:args args

# bait:show_bait_1
$execute unless data storage bait:conf chunks."$(chunk)" run return fail
$data modify storage wishlist:args bait set from storage bait:conf chunks."$(chunk)"
function bait:show_bait_2

# bait:show_bait_2
execute unless data storage wishlist:args bait[0] run return 1
function bait:show_bait_3 with storage wishlist:args bait[0]
data remove storage wishlist:args bait[0]
function bait:show_bait_2

# bait:show_bait_3
$execute if score @s wishlist.playerId matches $(player) \
        run particle minecraft:dust{color:[1f,0.7f,0.4f],scale:1f} \
                $(x) $(y).2 $(z) 0 0 0 0.1 1 normal @s
$execute unless score @s wishlist.playerId matches $(player) \
        run particle minecraft:dust{color:[0.4f,1f,1f],scale:1f} \
                $(x) $(y).2 $(z) 0 0 0 0.1 1 normal @s

# bait:place_bait
# Place bait before the first solid block seen from the executor's eyes
execute unless function wishlist:raycast run return fail
function bait:raycast_args
function bait:place_bait_1 with storage wishlist:args args

# bait:place_bait_1
$data modify storage bait:conf chunks."$(chunk)" append value \
        { x:$(x),y:$(y),z:$(z),player:$(player) }
$particle minecraft:dust{color:[1f,0.7f,0.4f],scale:1f} \
        $(x) $(y).2 $(z) 0.1 0.1 0.1 0.1 5 normal @s
title @s actionbar { "translate":"bait.placed" }

# bait:remove_bait
# Remove bait before the first solid block seen from the executor's eyes
execute unless function wishlist:raycast run return fail
function bait:raycast_args
function bait:remove_bait_1 with storage wishlist:args args

# bait:remove_bait_1
scoreboard players set done wishlist.vars 0
$execute store success score done wishlist.vars \
        run data remove storage bait:conf chunks."$(chunk)"[{x:$(x),y:$(y),z:$(z),player:$(player)}]
$execute unless data storage bait:conf chunks."$(chunk)"[0] \
        run data remove storage bait:conf chunks."$(chunk)"
execute if score done wishlist.vars matches 0 \
        run return run title @s actionbar { "translate":"bait.not_removed" }
$particle minecraft:dust{color:[1f,0.7f,0.4f],scale:1f} \
        $(x) $(y).2 $(z) 0.1 0.1 0.1 0.1 5 normal @s
title @s actionbar { "translate":"bait.removed" }

# bait:place_mobs
# Query chunks in a 3x3x3 box around the executing entity;
# if any of them are unloaded, load them and roll some mobs.
function bait:chunk_args_offset {x:-1,y:-1,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:-1,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:-1,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:0,y:-1,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:-1,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:-1,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:1,y:-1,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:-1,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:-1,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:-1,y:0,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:0,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:0,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:0,y:0,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:0,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:0,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:1,y:0,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:0,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:0,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:-1,y:1,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:1,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:-1,y:1,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:0,y:1,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:1,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:0,y:1,z:1}
function bait:place_mobs_1 with storage wishlist:args args

function bait:chunk_args_offset {x:1,y:1,z:-1}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:1,z:0}
function bait:place_mobs_1 with storage wishlist:args args
function bait:chunk_args_offset {x:1,y:1,z:1}
function bait:place_mobs_1 with storage wishlist:args args

# bait:place_mobs_1
$execute unless data storage bait:conf chunks."$(chunk)" run return fail
$execute if data storage bait:conf loaded[{chunk:"$(chunk)"}] run return fail
execute store result storage wishlist:args args.x int 1 run data get storage wishlist:args args.x 16
execute store result storage wishlist:args args.y int 1 run data get storage wishlist:args args.y 16
execute store result storage wishlist:args args.z int 1 run data get storage wishlist:args args.z 16
data modify storage bait:conf loaded append from storage wishlist:args args
$data modify storage wishlist:args bait set from storage bait:conf chunks."$(chunk)"
function bait:place_mobs_2

execute store result score n wishlist.vars if entity @e[tag=wishlist.roamer]
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[bait] +%s (%s roamers)", \
        "with":[ \
                "$(chunk)", \
                {"score":{"name":"n","objective":"wishlist.vars"}} \
                ], \
        "color":"green" \
        }

# bait:place_mobs_2
execute unless data storage wishlist:args bait[0] run return 1
function bait:place_mobs_3 with storage wishlist:args bait[0]
data remove storage wishlist:args bait[0]
function bait:place_mobs_2

# bait:place_mobs_3
$execute positioned $(x) $(y) $(z) run function bait:place_mobs_4

# bait:place_mobs_4

# * If the block where the roamer spawn is not empty they can be moved up
# * If the block under the roamer is not full they can be moved down
# * Otherwise, spawn the roamer at the exact coordinates
# * The list of blocks that are considered for adjustment:
#   * bottom stairs
#   * bottom slabs
#   * bottom closed trapdoors
#   * dirt path
#   * campfires

execute if block ~ ~ ~ #minecraft:stairs[half=bottom] \
        positioned ~ ~0.5 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~ ~ #minecraft:slabs[type=bottom] \
        positioned ~ ~0.5 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~ ~ #minecraft:trapdoors[half=bottom,open=false] \
        positioned ~ ~0.1875 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~ ~ minecraft:dirt_path \
        positioned ~ ~0.9375 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~ ~ #minecraft:campfires \
        positioned ~ ~0.4375 ~ \
        run return run function bait:place_mobs_5
execute unless block ~ ~ ~ #wishlist:non_full run return fail

execute if block ~ ~-1 ~ #minecraft:stairs[half=bottom] \
        positioned ~ ~-0.5 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~-1 ~ #minecraft:slabs[type=bottom] \
        positioned ~ ~-0.5 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~-1 ~ #minecraft:trapdoors[half=bottom,open=false] \
        positioned ~ ~-0.8125 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~-1 ~ minecraft:dirt_path \
        positioned ~ ~-0.0625 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~-1 ~ #minecraft:campfires \
        positioned ~ ~-0.5625 ~ \
        run return run function bait:place_mobs_5
execute if block ~ ~-1 ~ #wishlist:non_full run return fail

return run function bait:place_mobs_5

# bait:place_mobs_5
execute if entity @e[dx=0,dy=0,dz=0] run return fail

data modify storage wishlist:args args set value { \
  data:{ \
    NoAI:1b, \
    Invulnerable:1b, \
    Tags:[wishlist.roamer,wishlist.look_alive,wishlist.chatterbox], \
    Rotation:[0f,0f], \
    ArmorItems:[ \
      {}, \
      {}, \
      {}, \
      { \
        id:"minecraft:stone_button", \
        count:1, \
        components: { \
          "minecraft:custom_model_data":{"floats":[999999]} \
          } \
        } \
      ], \
    ArmorDropChances:[0f,0f,0f,0f] \
    } \
  }

execute store result storage wishlist:args args.data.Rotation[0] float 1 run random value 0..359
execute store result score type wishlist.vars run random value 0..255

execute if score type wishlist.vars matches 0..3 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:none"},\
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 4..7 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:armorer"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 8..11 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:butcher"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 12..15 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:cartographer"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 16..19 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:cleric"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 20..23 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:farmer"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 24..27 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:fisherman"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 28..31 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:fletcher"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 32..35 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:leatherworker"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 36..39 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:librarian"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 40..43 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:nitwit"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 44..47 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:mason"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 48..51 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:shepherd"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 52..55 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:toolsmith"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 56..59 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:weaponsmith"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 60..63 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:wandering_trader", \
                data:{ \
                        VillagerData:{profession:"minecraft:weaponsmith"}, \
                        Offers:{Recipes:[]} \
                        } \
                }

execute if score type wishlist.vars matches 64..67 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:villager", \
                data:{ \
                        VillagerData:{profession:"minecraft:none"}, \
                        Age:-2000000000 \
                        } \
                }

execute if score type wishlist.vars matches 68..71 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:pillager", \
                data:{ \
                        PersistenceRequired:1b, \
                        CanJoinRaids:0b, \
                        } \
                }

execute if score type wishlist.vars matches 72..75 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:vindicator", \
                data:{ \
                        PersistenceRequired:1b, \
                        CanJoinRaids:0b, \
                        } \
                }

execute if score type wishlist.vars matches 76..79 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:evoker", \
                data:{ \
                        PersistenceRequired:1b, \
                        CanJoinRaids:0b, \
                        } \
                }

execute if score type wishlist.vars matches 80..83 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:witch", \
                data:{ \
                        PersistenceRequired:1b, \
                        CanJoinRaids:0b, \
                        } \
                }

execute if score type wishlist.vars matches 84..87 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:piglin", \
                data:{ \
                        PersistenceRequired:1b, \
                        IsImmuneToZombification:1b, \
                        } \
                }

execute if score type wishlist.vars matches 88..91 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:piglin", \
                data:{ \
                        PersistenceRequired:1b, \
                        IsImmuneToZombification:1b, \
                        IsBaby:1b, \
                        } \
                }

execute if score type wishlist.vars matches 92..95 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:piglin_brute", \
                data:{ \
                        PersistenceRequired:1b, \
                        IsImmuneToZombification:1b, \
                        } \
                }

execute if score type wishlist.vars matches 96..99 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:enderman", \
                data:{ \
                        PersistenceRequired:1b, \
                        }, \
                chatter: "…" \
                }

execute if score type wishlist.vars matches 100..103 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:wolf", \
                data:{ \
                        variant:"minecraft:pale", \
                        }, \
                chatter: "…" \
                }

execute if score type wishlist.vars matches 104..106 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:chicken", \
                chatter: "…" \
                }

execute if score type wishlist.vars matches 107 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:chicken", \
                chatter: "…", \
                data:{ \
                  CustomName:"'Zombie-Sized Chicken'", \
                  attributes:[ \
                    {id:"minecraft:scale",base:2.0d} \
                    ] \
                  } \
                }

execute if score type wishlist.vars matches 108..111 \
        run data modify storage wishlist:args args merge value { \
          id:"minecraft:villager", \
          data:{ \
            VillagerData:{profession:"minecraft:none"},\
            Offers:{Recipes:[]}, \
            ArmorItems:[ \
              {}, \
              {}, \
              {}, \
              { \
                id:"minecraft:stone_button", \
                count:1, \
                components: { \
                  "minecraft:custom_model_data":{"floats":[271828]} \
                  } \
                } \
              ] \
            } \
          }

execute if score type wishlist.vars matches 112..115 \
        run data modify storage wishlist:args args merge value { \
          id:"minecraft:villager", \
          data:{ \
            VillagerData:{profession:"minecraft:none"},\
            Offers:{Recipes:[]}, \
            ArmorItems:[ \
              {}, \
              {}, \
              {}, \
              { \
                id:"minecraft:stone_button", \
                count:1, \
                components: { \
                  "minecraft:custom_model_data":{"floats":[271829]} \
                  } \
                } \
              ] \
            } \
          }

execute if score type wishlist.vars matches 116..119 \
        run data modify storage wishlist:args args merge value { \
          id:"minecraft:villager", \
          data:{ \
            VillagerData:{profession:"minecraft:none"},\
            Offers:{Recipes:[]}, \
            ArmorItems:[ \
              {}, \
              {}, \
              {}, \
              { \
                id:"minecraft:stone_button", \
                count:1, \
                components:{ \
                  "minecraft:custom_model_data":{"floats":[271830]} \
                  } \
                } \
              ] \
            } \
          }

execute if score type wishlist.vars matches 120 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:pillager", \
                data:{ \
                  PersistenceRequired:1b, \
                  CanJoinRaids:0b, \
                  attributes:[ \
                    {id:"minecraft:scale",base:0.5d} \
                    ] \
                  } \
                }

execute if score type wishlist.vars matches 121 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:vindicator", \
                data:{ \
                  PersistenceRequired:1b, \
                  CanJoinRaids:0b, \
                  attributes:[ \
                    {id:"minecraft:scale",base:0.5d} \
                    ] \
                  } \
                }

execute if score type wishlist.vars matches 122 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:evoker", \
                data:{ \
                  PersistenceRequired:1b, \
                  CanJoinRaids:0b, \
                  attributes:[ \
                    {id:"minecraft:scale",base:0.5d} \
                    ] \
                  } \
                }

execute if score type wishlist.vars matches 123 \
        run data modify storage wishlist:args args merge value { \
                id:"minecraft:witch", \
                data:{ \
                  PersistenceRequired:1b, \
                  CanJoinRaids:0b, \
                  attributes:[ \
                    {id:"minecraft:scale",base:0.5d} \
                    ] \
                  } \
                }

execute if score type wishlist.vars matches 124.. run return fail

execute unless data storage wishlist:args args.chatter \
        run function wishlist:random_element { \
                dst:"storage wishlist:args args.chatter", \
                src:"storage wishlist:args all_chatter" \
        }

execute store result score t1 wishlist.vars run random value 160..320
scoreboard players operation t1 wishlist.vars += t0 wishlist.vars
execute store result storage wishlist:args \
        args.data.ArmorItems[3].components.minecraft:custom_data.focus_timeout int 1 \
        run scoreboard players get t1 wishlist.vars

execute if data storage wishlist:args args.data.VillagerData run function bait:place_mobs_8
execute if data storage wishlist:args args.data.variant run function bait:place_mobs_9
function bait:place_mobs_6 with storage wishlist:args args
function bait:place_mobs_7 with storage wishlist:args args

# bait:place_mobs_6
$data modify storage wishlist:args \
        args.data.ArmorItems[3].components.minecraft:custom_data.chatter \
        set value "\"$(chatter)\""

# bait:place_mobs_7
$summon $(id) ~ ~ ~ $(data)

# bait:place_mobs_8
execute store result score rand wishlist.vars run random value 0..15
execute if score rand wishlist.vars matches 0 \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:desert"
execute if score rand wishlist.vars matches 1 \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:jungle"
execute if score rand wishlist.vars matches 2 \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:savanna"
execute if score rand wishlist.vars matches 3 \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:snow"
execute if score rand wishlist.vars matches 4 \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:swamp"
execute if score rand wishlist.vars matches 5 \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:taiga"
execute if score rand wishlist.vars matches 6 \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:plains"
execute if biome ~ ~ ~ #wishlist:spawns_desert_villagers \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:desert"
execute if biome ~ ~ ~ #wishlist:spawns_jungle_villagers \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:jungle"
execute if biome ~ ~ ~ #wishlist:spawns_savanna_villagers \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:savanna"
execute if biome ~ ~ ~ #wishlist:spawns_snow_villagers \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:snow"
execute if biome ~ ~ ~ #wishlist:spawns_swamp_villagers \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:swamp"
execute if biome ~ ~ ~ #wishlist:spawns_taiga_villagers \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:taiga"
execute if biome ~ ~ ~ #wishlist:spawns_plains_villagers \
        run return run data modify storage wishlist:args args.data.VillagerData.type \
        set value "minecraft:plains"

# bait:place_mobs_9
execute store result score rand wishlist.vars run random value 0..8
execute if score rand wishlist.vars matches 0 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:ashen"
execute if score rand wishlist.vars matches 1 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:black"
execute if score rand wishlist.vars matches 2 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:chestnut"
execute if score rand wishlist.vars matches 3 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:pale"
execute if score rand wishlist.vars matches 4 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:rusty"
execute if score rand wishlist.vars matches 5 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:snowy"
execute if score rand wishlist.vars matches 6 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:spotted"
execute if score rand wishlist.vars matches 7 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:stripped"
execute if score rand wishlist.vars matches 8 \
        run return run data modify storage wishlist:args args.data.variant \
        set value "minecraft:woods"

# bait:remove_mobs
# Iterate the loaded chunk list; if one of them has no active player close
# enough (but is still loaded by the game), dismiss all mobs and remove from
# the loaded list.
execute unless data storage wishlist:args loaded[0] run return fail
scoreboard players set done wishlist.vars 0
function bait:remove_mobs_1 with storage wishlist:args loaded[0]
execute if score done wishlist.vars matches 0 \
        run data modify storage bait:conf loaded \
        append from storage wishlist:args loaded[0]
data remove storage wishlist:args loaded[0]
function bait:remove_mobs

# bait:remove_mobs_1
$execute in $(dimension) \
        positioned $(x) $(y) $(z) \
        run function bait:remove_mobs_2

# bait:remove_mobs_2
execute unless loaded ~ ~ ~ run return run function bait:remove_mobs_3
execute positioned ~8 ~8 ~8 if entity @a[gamemode=!spectator,distance=..48] run return fail
execute as @e[tag=wishlist.roamer,dx=16,dy=16,dz=16] run function wishlist:dismiss
scoreboard players set done wishlist.vars 1

execute store result score n wishlist.vars if entity @e[tag=wishlist.roamer]
tellraw @a[tag=wishlist.debug] { \
        "translate":"[bait] -%s (%s roamers)", \
        "with":[ \
                {"storage":"wishlist:args","nbt":"loaded[0].chunk"}, \
                {"score":{"name":"n","objective":"wishlist.vars"}} \
                ], \
        "color":"red" \
        }

# bait:remove_mobs_3
forceload add ~ ~
execute as @e[tag=wishlist.roamer,dx=16,dy=16,dz=16] run function wishlist:dismiss
forceload remove ~ ~
scoreboard players set done wishlist.vars 1

execute store result score n wishlist.vars if entity @e[tag=wishlist.roamer]
tellraw @a[tag=wishlist.debug] { \
        "translate":"[bait] -%s (%s roamers) F!", \
        "with":[ \
                {"storage":"wishlist:args","nbt":"loaded[0].chunk"}, \
                {"score":{"name":"n","objective":"wishlist.vars"}} \
                ], \
        "color":"red" \
        }

