Noise Box
=========

Adds a noise box item that can be toggled ![noise_box_on] open and
![noise_box_off] close; if carried while open, it tags the player with
`wishlist.hidden`.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for player UI
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

A ![noise_box] noise_box can be aquired in two ways:

* By crafting the `noise_box:noise_box` recipe, that requires eight copper
  ingots surrounding a nether star.
* Through the `noise_box:noise_box` loot table.

Holding and using the noise box toggles it between its open and close state.

If a player has it open in their inventory, they are tagged `wishlist.hidden`.

The hidden tag interacts with a number of other datapacks:

* players will not be highlighted by an aura lens,
* skip to player tickets will not work,
* mail will not be delivered to hidden players,
  * this includes money delivered by the universal basic income datapack.

Overhead
--------

Checks every player every second to see if they have an open noise box in their
inventory

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Apotropaic Device: Open a noise box

[noise_box_off]: noise_box/assets/noise_box/textures/item/noise_box_off.png ""
[noise_box_on]: noise_box/assets/noise_box/textures/item/noise_box_on.png ""
