# noise_box:load
data modify storage wishlist:args all_chatter append value \
        "The Noise Box is an awesome tool, but it can prevent your own tools from working"

function noise_box:tick

# noise_box:tick
schedule function noise_box:tick 1s
tag @a remove wishlist.hidden
tag @a[predicate=noise_box:holding_noise_box] add wishlist.hidden

# noise_box:toggle
item modify entity @s weapon.mainhand noise_box:toggle
playsound noise_box:toggle player @s
advancement grant @s only wishlist:use_noise_box
