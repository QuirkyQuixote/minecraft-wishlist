
# popular_displays:display

data modify storage wishlist:args tags set value { \
        Tags:[wishlist.popular_displays], \
        item_display:"fixed", \
        view_range:0.5f, \
        transformation:{ \
          scale:[0.5f,0.5f,0.5f] \
        } \
      }

data modify storage wishlist:args tags.item set from entity @s Item

execute align xyz \
        positioned ~0.5 ~0.25 ~0.5 \
        summon minecraft:item_display \
        run data modify entity @s {} merge from storage wishlist:args tags

kill @s

advancement grant @a[predicate=popular_displays:hold_displayer,distance=..4] \
        only wishlist:create_item_display

# popular_displays:drop_display

data modify storage wishlist:args tags set from entity @s {}

execute at @s run summon minecraft:item ~ ~ ~ { \
        Item:{id:"minecraft:stick",Count:1b}, \
        Tags:[wishlist.popular_displays.drop] \
        }

data modify entity @e[tag=wishlist.popular_displays.drop,limit=1] Item \
        set from storage wishlist:args tags.item

tag @e[tag=wishlist.popular_displays.drop,limit=1] remove wishlist.popular_displays.drop
kill @s

# popular_displays:load

scoreboard objectives add wishlist.popular_displays.trigger trigger

data modify storage wishlist:args all_chatter append value \
        "If you get your hands on a Display Book you'll be able to do some insane decoration"
data modify storage wishlist:args all_chatter append value \
        "Remember to lock your displays so people won't accidentally drop them!"

function popular_displays:tick

# popular_displays:lock

tag @s remove wishlist.popular_displays
tag @s add wishlist.popular_displays.locked
particle minecraft:instant_effect ~ ~ ~ 0.1 0.1 0.1 0.005 10
playsound minecraft:block.wooden_trapdoor.close block @a ~ ~ ~

# popular_displays:move_1
execute if score facing wishlist.vars matches 0 \
        run tp @s ~0.0625 ~ ~
execute if score facing wishlist.vars matches 1 \
        run tp @s ~-0.0625 ~ ~
execute if score facing wishlist.vars matches 2 \
        run tp @s ~ ~0.0625 ~
execute if score facing wishlist.vars matches 3 \
        run tp @s ~ ~-0.0625 ~
execute if score facing wishlist.vars matches 4 \
        run tp @s ~ ~ ~0.0625
execute if score facing wishlist.vars matches 5 \
        run tp @s ~ ~ ~-0.0625


# popular_displays:move_16
execute if score facing wishlist.vars matches 0 \
        run tp @s ~1 ~ ~
execute if score facing wishlist.vars matches 1 \
        run tp @s ~-1 ~ ~
execute if score facing wishlist.vars matches 2 \
        run tp @s ~ ~1 ~
execute if score facing wishlist.vars matches 3 \
        run tp @s ~ ~-1 ~
execute if score facing wishlist.vars matches 4 \
        run tp @s ~ ~ ~1
execute if score facing wishlist.vars matches 5 \
        run tp @s ~ ~ ~-1

# popular_displays:move_2
execute if score facing wishlist.vars matches 0 \
        run tp @s ~0.125 ~ ~
execute if score facing wishlist.vars matches 1 \
        run tp @s ~-0.125 ~ ~
execute if score facing wishlist.vars matches 2 \
        run tp @s ~ ~0.125 ~
execute if score facing wishlist.vars matches 3 \
        run tp @s ~ ~-0.125 ~
execute if score facing wishlist.vars matches 4 \
        run tp @s ~ ~ ~0.125
execute if score facing wishlist.vars matches 5 \
        run tp @s ~ ~ ~-0.125


# popular_displays:move_4
execute if score facing wishlist.vars matches 0 \
        run tp @s ~0.25 ~ ~
execute if score facing wishlist.vars matches 1 \
        run tp @s ~-0.25 ~ ~
execute if score facing wishlist.vars matches 2 \
        run tp @s ~ ~0.25 ~
execute if score facing wishlist.vars matches 3 \
        run tp @s ~ ~-0.25 ~
execute if score facing wishlist.vars matches 4 \
        run tp @s ~ ~ ~0.25
execute if score facing wishlist.vars matches 5 \
        run tp @s ~ ~ ~-0.25


# popular_displays:move_8
execute if score facing wishlist.vars matches 0 \
        run tp @s ~0.5 ~ ~
execute if score facing wishlist.vars matches 1 \
        run tp @s ~-0.5 ~ ~
execute if score facing wishlist.vars matches 2 \
        run tp @s ~ ~0.5 ~
execute if score facing wishlist.vars matches 3 \
        run tp @s ~ ~-0.5 ~
execute if score facing wishlist.vars matches 4 \
        run tp @s ~ ~ ~0.5
execute if score facing wishlist.vars matches 5 \
        run tp @s ~ ~ ~-0.5


# popular_displays:tick

schedule function popular_displays:tick 1s

execute at @a[predicate=popular_displays:hold_displayer] \
        run function popular_displays:tick_player

execute as @a[scores={wishlist.popular_displays.trigger=1..}] \
        at @s \
        run function popular_displays:trigger

scoreboard players enable @a wishlist.popular_displays.trigger
scoreboard players set @a wishlist.popular_displays.trigger 0

execute at @a \
        as @e[type=minecraft:item_frame,distance=..8,nbt={Invisible:1b}] \
        unless data entity @s Item \
        run function popular_displays:pop_item_frame

execute at @a \
        as @e[type=minecraft:glow_item_frame,distance=..8,nbt={Invisible:1b}] \
        unless data entity @s Item \
        run function popular_displays:pop_glow_item_frame

# popular_displays:pop_item_frame
execute at @s run summon minecraft:item ~ ~ ~ {Item:{id:item_frame,count:1}}
kill @s

# popular_displays:pop_glow_item_frame
execute at @s run summon minecraft:item ~ ~ ~ {Item:{id:glow_item_frame,count:1}}
kill @s

# popular_displays:tick_player


execute as @e[type=item,distance=..4,predicate=popular_displays:on_ground] \
        at @s \
        run function popular_displays:display


# popular_displays:trigger

scoreboard players operation facing wishlist.vars = @s wishlist.facing

execute if score @s wishlist.popular_displays.trigger matches 1 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run function popular_displays:drop_display
execute if score @s wishlist.popular_displays.trigger matches 2 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run function popular_displays:drop_display
execute if score @s wishlist.popular_displays.trigger matches 3 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run function popular_displays:drop_display
execute if score @s wishlist.popular_displays.trigger matches 4 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run function popular_displays:drop_display
execute if score @s wishlist.popular_displays.trigger matches 5 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run function popular_displays:drop_display
execute if score @s wishlist.popular_displays.trigger matches 6 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run function popular_displays:drop_display

execute if score @s wishlist.popular_displays.trigger matches 11 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {"billboard":"fixed"}
execute if score @s wishlist.popular_displays.trigger matches 12 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data merge entity @s {"billboard":"fixed"}
execute if score @s wishlist.popular_displays.trigger matches 13 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data merge entity @s {"billboard":"fixed"}
execute if score @s wishlist.popular_displays.trigger matches 14 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data merge entity @s {"billboard":"fixed"}
execute if score @s wishlist.popular_displays.trigger matches 15 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data merge entity @s {"billboard":"fixed"}
execute if score @s wishlist.popular_displays.trigger matches 16 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data merge entity @s {"billboard":"fixed"}

execute if score @s wishlist.popular_displays.trigger matches 21 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {"billboard":"horizontal"}
execute if score @s wishlist.popular_displays.trigger matches 22 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data merge entity @s {"billboard":"horizontal"}
execute if score @s wishlist.popular_displays.trigger matches 23 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data merge entity @s {"billboard":"horizontal"}
execute if score @s wishlist.popular_displays.trigger matches 24 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data merge entity @s {"billboard":"horizontal"}
execute if score @s wishlist.popular_displays.trigger matches 25 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data merge entity @s {"billboard":"horizontal"}
execute if score @s wishlist.popular_displays.trigger matches 26 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data merge entity @s {"billboard":"horizontal"}

execute if score @s wishlist.popular_displays.trigger matches 31 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {"billboard":"center"}
execute if score @s wishlist.popular_displays.trigger matches 32 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data merge entity @s {"billboard":"center"}
execute if score @s wishlist.popular_displays.trigger matches 33 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data merge entity @s {"billboard":"center"}
execute if score @s wishlist.popular_displays.trigger matches 34 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data merge entity @s {"billboard":"center"}
execute if score @s wishlist.popular_displays.trigger matches 35 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data merge entity @s {"billboard":"center"}
execute if score @s wishlist.popular_displays.trigger matches 36 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data merge entity @s {"billboard":"center"}

execute if score @s wishlist.popular_displays.trigger matches 41 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data modify entity @s brightness set value {"block":0b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 42 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data modify entity @s brightness set value {"block":0b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 43 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data modify entity @s brightness set value {"block":0b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 44 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data modify entity @s brightness set value {"block":0b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 45 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data modify entity @s brightness set value {"block":0b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 46 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data modify entity @s brightness set value {"block":0b,"sky":0b}

execute if score @s wishlist.popular_displays.trigger matches 51 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data modify entity @s brightness set value {"block":7b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 52 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data modify entity @s brightness set value {"block":7b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 53 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data modify entity @s brightness set value {"block":7b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 54 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data modify entity @s brightness set value {"block":7b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 55 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data modify entity @s brightness set value {"block":7b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 56 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data modify entity @s brightness set value {"block":7b,"sky":0b}

execute if score @s wishlist.popular_displays.trigger matches 61 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data modify entity @s brightness set value {"block":11b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 62 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data modify entity @s brightness set value {"block":11b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 63 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data modify entity @s brightness set value {"block":11b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 64 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data modify entity @s brightness set value {"block":11b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 65 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data modify entity @s brightness set value {"block":11b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 66 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data modify entity @s brightness set value {"block":11b,"sky":0b}

execute if score @s wishlist.popular_displays.trigger matches 71 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data modify entity @s brightness set value {"block":15b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 72 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data modify entity @s brightness set value {"block":15b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 73 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data modify entity @s brightness set value {"block":15b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 74 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data modify entity @s brightness set value {"block":15b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 75 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data modify entity @s brightness set value {"block":15b,"sky":0b}
execute if score @s wishlist.popular_displays.trigger matches 76 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data modify entity @s brightness set value {"block":15b,"sky":0b}

execute if score @s wishlist.popular_displays.trigger matches 81 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data remove entity @s brightness
execute if score @s wishlist.popular_displays.trigger matches 82 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data remove entity @s brightness
execute if score @s wishlist.popular_displays.trigger matches 83 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data remove entity @s brightness
execute if score @s wishlist.popular_displays.trigger matches 84 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data remove entity @s brightness
execute if score @s wishlist.popular_displays.trigger matches 85 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data remove entity @s brightness
execute if score @s wishlist.popular_displays.trigger matches 86 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data remove entity @s brightness

execute if score @s wishlist.popular_displays.trigger matches 91 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data modify entity @s Glowing set value 1b
execute if score @s wishlist.popular_displays.trigger matches 92 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data modify entity @s Glowing set value 1b
execute if score @s wishlist.popular_displays.trigger matches 93 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data modify entity @s Glowing set value 1b
execute if score @s wishlist.popular_displays.trigger matches 94 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data modify entity @s Glowing set value 1b
execute if score @s wishlist.popular_displays.trigger matches 95 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data modify entity @s Glowing set value 1b
execute if score @s wishlist.popular_displays.trigger matches 96 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data modify entity @s Glowing set value 1b

execute if score @s wishlist.popular_displays.trigger matches 101 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        run data modify entity @s Glowing set value 0b
execute if score @s wishlist.popular_displays.trigger matches 102 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        run data modify entity @s Glowing set value 0b
execute if score @s wishlist.popular_displays.trigger matches 103 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        run data modify entity @s Glowing set value 0b
execute if score @s wishlist.popular_displays.trigger matches 104 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        run data modify entity @s Glowing set value 0b
execute if score @s wishlist.popular_displays.trigger matches 105 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        run data modify entity @s Glowing set value 0b
execute if score @s wishlist.popular_displays.trigger matches 106 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        run data modify entity @s Glowing set value 0b

execute if score @s wishlist.popular_displays.trigger matches 111 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 112 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 113 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 114 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 115 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 116 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~0.0625 ~ ~

execute if score @s wishlist.popular_displays.trigger matches 121 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~-0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 122 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~-0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 123 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~-0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 124 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~-0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 125 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~-0.0625 ~ ~
execute if score @s wishlist.popular_displays.trigger matches 126 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~-0.0625 ~ ~

execute if score @s wishlist.popular_displays.trigger matches 131 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 132 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 133 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 134 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 135 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 136 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~0.0625 ~

execute if score @s wishlist.popular_displays.trigger matches 141 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~-0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 142 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~-0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 143 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~-0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 144 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~-0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 145 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~-0.0625 ~
execute if score @s wishlist.popular_displays.trigger matches 146 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~-0.0625 ~

execute if score @s wishlist.popular_displays.trigger matches 151 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~ ~0.0625
execute if score @s wishlist.popular_displays.trigger matches 152 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~ ~0.0625
execute if score @s wishlist.popular_displays.trigger matches 153 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~ ~0.0625
execute if score @s wishlist.popular_displays.trigger matches 154 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~ ~0.0625
execute if score @s wishlist.popular_displays.trigger matches 155 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~ ~0.0625
execute if score @s wishlist.popular_displays.trigger matches 156 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~ ~0.0625

execute if score @s wishlist.popular_displays.trigger matches 161 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~ ~-0.0625
execute if score @s wishlist.popular_displays.trigger matches 162 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~ ~-0.0625
execute if score @s wishlist.popular_displays.trigger matches 163 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~ ~-0.0625
execute if score @s wishlist.popular_displays.trigger matches 164 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~ ~-0.0625
execute if score @s wishlist.popular_displays.trigger matches 165 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~ ~-0.0625
execute if score @s wishlist.popular_displays.trigger matches 166 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~ ~-0.0625

execute if score @s wishlist.popular_displays.trigger matches 171 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~ ~ ~22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 172 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~ ~ ~22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 173 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~ ~ ~22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 174 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~ ~ ~22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 175 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~ ~ ~22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 176 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~ ~ ~22.5 ~

execute if score @s wishlist.popular_displays.trigger matches 181 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~ ~ ~-22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 182 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~ ~ ~-22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 183 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~ ~ ~-22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 184 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~ ~ ~-22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 185 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~ ~ ~-22.5 ~
execute if score @s wishlist.popular_displays.trigger matches 186 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~ ~ ~-22.5 ~

execute if score @s wishlist.popular_displays.trigger matches 191 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~ ~ ~ ~22.5
execute if score @s wishlist.popular_displays.trigger matches 192 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~ ~ ~ ~22.5
execute if score @s wishlist.popular_displays.trigger matches 193 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~ ~ ~ ~22.5
execute if score @s wishlist.popular_displays.trigger matches 194 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~ ~ ~ ~22.5
execute if score @s wishlist.popular_displays.trigger matches 195 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~ ~ ~ ~22.5
execute if score @s wishlist.popular_displays.trigger matches 196 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~ ~ ~ ~22.5

execute if score @s wishlist.popular_displays.trigger matches 201 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~ ~ ~ ~-22.5
execute if score @s wishlist.popular_displays.trigger matches 202 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~ ~ ~ ~-22.5
execute if score @s wishlist.popular_displays.trigger matches 203 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~ ~ ~ ~-22.5
execute if score @s wishlist.popular_displays.trigger matches 204 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~ ~ ~ ~-22.5
execute if score @s wishlist.popular_displays.trigger matches 205 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~ ~ ~ ~-22.5
execute if score @s wishlist.popular_displays.trigger matches 206 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~ ~ ~ ~-22.5

execute if score @s wishlist.popular_displays.trigger matches 211 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        align xyz \
        run tp @s ~0.5 ~0.5 ~0.5
execute if score @s wishlist.popular_displays.trigger matches 212 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        align xyz \
        run tp @s ~0.5 ~0.5 ~0.5
execute if score @s wishlist.popular_displays.trigger matches 213 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        align xyz \
        run tp @s ~0.5 ~0.5 ~0.5
execute if score @s wishlist.popular_displays.trigger matches 214 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        align xyz \
        run tp @s ~0.5 ~0.5 ~0.5
execute if score @s wishlist.popular_displays.trigger matches 215 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        align xyz \
        run tp @s ~0.5 ~0.5 ~0.5
execute if score @s wishlist.popular_displays.trigger matches 216 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        align xyz \
        run tp @s ~0.5 ~0.5 ~0.5

execute if score @s wishlist.popular_displays.trigger matches 221 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run tp @s ~ ~ ~ 0 0
execute if score @s wishlist.popular_displays.trigger matches 222 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run tp @s ~ ~ ~ 0 0
execute if score @s wishlist.popular_displays.trigger matches 223 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run tp @s ~ ~ ~ 0 0
execute if score @s wishlist.popular_displays.trigger matches 224 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run tp @s ~ ~ ~ 0 0
execute if score @s wishlist.popular_displays.trigger matches 225 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run tp @s ~ ~ ~ 0 0
execute if score @s wishlist.popular_displays.trigger matches 226 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run tp @s ~ ~ ~ 0 0

execute if score @s wishlist.popular_displays.trigger matches 231 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run function popular_displays:lock
execute if score @s wishlist.popular_displays.trigger matches 232 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run function popular_displays:lock
execute if score @s wishlist.popular_displays.trigger matches 233 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run function popular_displays:lock
execute if score @s wishlist.popular_displays.trigger matches 234 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run function popular_displays:lock
execute if score @s wishlist.popular_displays.trigger matches 235 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run function popular_displays:lock
execute if score @s wishlist.popular_displays.trigger matches 236 \
        as @e[tag=wishlist.popular_displays,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run function popular_displays:lock

execute if score @s wishlist.popular_displays.trigger matches 241 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays.locked,distance=..8,sort=nearest,limit=1] \
        at @s \
        run function popular_displays:unlock
execute if score @s wishlist.popular_displays.trigger matches 242 \
        as @e[tag=wishlist.popular_displays.locked,distance=..1] \
        at @s \
        run function popular_displays:unlock
execute if score @s wishlist.popular_displays.trigger matches 243 \
        as @e[tag=wishlist.popular_displays.locked,distance=..2] \
        at @s \
        run function popular_displays:unlock
execute if score @s wishlist.popular_displays.trigger matches 244 \
        as @e[tag=wishlist.popular_displays.locked,distance=..4] \
        at @s \
        run function popular_displays:unlock
execute if score @s wishlist.popular_displays.trigger matches 245 \
        as @e[tag=wishlist.popular_displays.locked,distance=..8] \
        at @s \
        run function popular_displays:unlock
execute if score @s wishlist.popular_displays.trigger matches 246 \
        as @e[tag=wishlist.popular_displays.locked,distance=..16,nbt={Glowing:1b}] \
        at @s \
        run function popular_displays:unlock

execute if score @s wishlist.popular_displays.trigger matches 251 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run function popular_displays:move_1
execute if score @s wishlist.popular_displays.trigger matches 252 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run function popular_displays:move_1
execute if score @s wishlist.popular_displays.trigger matches 253 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run function popular_displays:move_1
execute if score @s wishlist.popular_displays.trigger matches 254 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run function popular_displays:move_1
execute if score @s wishlist.popular_displays.trigger matches 255 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run function popular_displays:move_1
execute if score @s wishlist.popular_displays.trigger matches 256 \
        as @e[tag=wishlist.popular_displays,distance=..16] \
        at @s \
        run function popular_displays:move_1

execute if score @s wishlist.popular_displays.trigger matches 261 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 262 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 263 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 264 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 265 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 266 \
        as @e[tag=wishlist.popular_displays,distance=..16] \
        at @s \
        run function popular_displays:move_4

execute if score @s wishlist.popular_displays.trigger matches 271 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 272 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 273 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 274 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 275 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run function popular_displays:move_4
execute if score @s wishlist.popular_displays.trigger matches 276 \
        as @e[tag=wishlist.popular_displays,distance=..16] \
        at @s \
        run function popular_displays:move_4

execute if score @s wishlist.popular_displays.trigger matches 281 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run function popular_displays:move_8
execute if score @s wishlist.popular_displays.trigger matches 282 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run function popular_displays:move_8
execute if score @s wishlist.popular_displays.trigger matches 283 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run function popular_displays:move_8
execute if score @s wishlist.popular_displays.trigger matches 284 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run function popular_displays:move_8
execute if score @s wishlist.popular_displays.trigger matches 285 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run function popular_displays:move_8
execute if score @s wishlist.popular_displays.trigger matches 286 \
        as @e[tag=wishlist.popular_displays,distance=..16] \
        at @s \
        run function popular_displays:move_8

execute if score @s wishlist.popular_displays.trigger matches 291 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[tag=wishlist.popular_displays,distance=..8,sort=nearest,limit=1] \
        at @s \
        run function popular_displays:move_16
execute if score @s wishlist.popular_displays.trigger matches 292 \
        as @e[tag=wishlist.popular_displays,distance=..1] \
        at @s \
        run function popular_displays:move_16
execute if score @s wishlist.popular_displays.trigger matches 293 \
        as @e[tag=wishlist.popular_displays,distance=..2] \
        at @s \
        run function popular_displays:move_16
execute if score @s wishlist.popular_displays.trigger matches 294 \
        as @e[tag=wishlist.popular_displays,distance=..4] \
        at @s \
        run function popular_displays:move_16
execute if score @s wishlist.popular_displays.trigger matches 295 \
        as @e[tag=wishlist.popular_displays,distance=..8] \
        at @s \
        run function popular_displays:move_16
execute if score @s wishlist.popular_displays.trigger matches 296 \
        as @e[tag=wishlist.popular_displays,distance=..16] \
        at @s \
        run function popular_displays:move_16

execute if score @s wishlist.popular_displays.trigger matches 301 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[type=#wishlist:item_frames,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {Invisible:1b}
execute if score @s wishlist.popular_displays.trigger matches 302 \
        as @e[type=#wishlist:item_frames,distance=..1] \
        run data merge entity @s {Invisible:1b}
execute if score @s wishlist.popular_displays.trigger matches 303 \
        as @e[type=#wishlist:item_frames,distance=..2] \
        run data merge entity @s {Invisible:1b}
execute if score @s wishlist.popular_displays.trigger matches 304 \
        as @e[type=#wishlist:item_frames,distance=..4] \
        run data merge entity @s {Invisible:1b}
execute if score @s wishlist.popular_displays.trigger matches 305 \
        as @e[type=#wishlist:item_frames,distance=..8] \
        run data merge entity @s {Invisible:1b}
execute if score @s wishlist.popular_displays.trigger matches 306 \
        as @e[type=#wishlist:item_frames,distance=..16] \
        run data merge entity @s {Invisible:1b}

execute if score @s wishlist.popular_displays.trigger matches 311 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[type=#wishlist:item_frames,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {Invisible:0b}
execute if score @s wishlist.popular_displays.trigger matches 312 \
        as @e[type=#wishlist:item_frames,distance=..1] \
        run data merge entity @s {Invisible:0b}
execute if score @s wishlist.popular_displays.trigger matches 313 \
        as @e[type=#wishlist:item_frames,distance=..2] \
        run data merge entity @s {Invisible:0b}
execute if score @s wishlist.popular_displays.trigger matches 314 \
        as @e[type=#wishlist:item_frames,distance=..4] \
        run data merge entity @s {Invisible:0b}
execute if score @s wishlist.popular_displays.trigger matches 315 \
        as @e[type=#wishlist:item_frames,distance=..8] \
        run data merge entity @s {Invisible:0b}
execute if score @s wishlist.popular_displays.trigger matches 316 \
        as @e[type=#wishlist:item_frames,distance=..16] \
        run data merge entity @s {Invisible:0b}

# popular_displays:unlock

tag @s remove wishlist.popular_displays.locked
tag @s add wishlist.popular_displays
particle minecraft:instant_effect ~ ~ ~ 0.1 0.1 0.1 0.005 10
playsound minecraft:block.wooden_trapdoor.close block @a ~ ~ ~
