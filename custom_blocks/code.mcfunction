# custom_blocks:load

data modify storage custom_blocks:conf black_pawn set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271833]}, \
    "custom_name":'{"translate":"item.wishlist.black_pawn","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf black_knight set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271834]}, \
    "custom_name":'{"translate":"item.wishlist.black_knight","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf black_bishop set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271835]}, \
    "custom_name":'{"translate":"item.wishlist.black_bishop","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf black_rook set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271836]}, \
    "custom_name":'{"translate":"item.wishlist.black_rook","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf black_queen set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271837]}, \
    "custom_name":'{"translate":"item.wishlist.black_queen","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf black_king set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271838]}, \
    "custom_name":'{"translate":"item.wishlist.black_king","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf white_pawn set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271839]}, \
    "custom_name":'{"translate":"item.wishlist.white_pawn","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf white_knight set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271840]}, \
    "custom_name":'{"translate":"item.wishlist.white_knight","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf white_bishop set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271841]}, \
    "custom_name":'{"translate":"item.wishlist.white_bishop","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf white_rook set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271842]}, \
    "custom_name":'{"translate":"item.wishlist.white_rook","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf white_queen set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271843]}, \
    "custom_name":'{"translate":"item.wishlist.white_queen","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

data modify storage custom_blocks:conf white_king set value { \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
    billboard: "vertical", \
    }, \
  item: { \
    custom_model_data:{floats:[271844]}, \
    "custom_name":'{"translate":"item.wishlist.white_king","italic":false}' \
    }, \
  block: "minecraft:air" \
  }

# custom_blocks:attack
advancement revoke @s only custom_blocks:attack
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=minecraft:interaction,tag=wishlist.custom_block,distance=..4] \
        if data entity @s attack \
        at @s \
        run function custom_blocks:attack_1


# custom_blocks:attack_1
execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run data modify storage custom_blocks:vars item set from entity @s item
function custom_blocks:attack_2 with storage custom_blocks:vars item
function custom_blocks:attack_3 with storage custom_blocks:vars item.components.minecraft:custom_data
function custom_blocks:give with storage custom_blocks:vars item.components.minecraft:custom_data
execute on passengers run kill @s
kill @s

# custom_blocks:attack_2
$particle minecraft:item{ \
        item:{ \
                id:"minecraft:item_frame", \
                components:$(components) \
                } \
        } \
        ~ ~ ~ 0.3 0.3 0.3 0.05 40

# custom_blocks:attack_3
$function custom_blocks:attack_4 with storage custom_blocks:conf $(id)

# custom_blocks:attack_4
$execute if block ~ ~ ~ $(block) run setblock ~ ~ ~ minecraft:air

# custom_blocks:give
$data modify storage custom_blocks:vars args set from storage custom_blocks:conf $(id).item
$data modify storage custom_blocks:vars args.id set value $(id)
function custom_blocks:give_1 with storage custom_blocks:vars args

# custom_blocks:give_1
$summon minecraft:item ~ ~ ~ { \
  Item: { \
    id:"minecraft:item_frame", \
    count:1, \
    components: { \
      "minecraft:custom_name": '$(custom_name)', \
      "minecraft:custom_model_data":$(custom_model_data), \
      "minecraft:entity_data":{ \
        id:"minecraft:item_frame", \
        Tags:[wishlist.block,$(id)] \
        } \
      } \
    } \
  }

# custom_blocks:place
advancement revoke @s only custom_blocks:place
scoreboard players operation yaw wishlist.vars = @s wishlist.yaw
execute as @e[type=item_frame,tag=wishlist.block,distance=..8] \
        at @s \
        align xyz \
        positioned ~0.5 ~ ~0.5 \
        run function custom_blocks:place_1

# custom_blocks:place_1
tag @s remove wishlist.block
data modify storage custom_blocks:vars args set value {}
data modify storage custom_blocks:vars args.id set from entity @s Tags[0]
execute unless block ~ ~ ~ #minecraft:replaceable \
        run function custom_blocks:give with storage custom_blocks:vars args
execute if block ~ ~ ~ #minecraft:replaceable \
        run function custom_blocks:place_2 with storage custom_blocks:vars args
kill @s[type=item_frame]

# custom_blocks:place_2

# Note: the rotation stuff is done to make the custom blocks align to the usual
# minecraft rotation scheme for blocks, that divides a full circumference in 16
# arcs but also to rotate the block 180 from the rotation of the player who
# placed it; the full equation is:
#
# r_block = floor((r_player + 180 + 11.25) / 22.5) * 22.5
#
# but we round 11.25 to 11 because we can't do floating-point math on scores

data modify storage custom_blocks:vars block set value { \
  data: { \
    Passengers: [{ \
      id:"minecraft:item_display", \
      Rotation:[0f,0f], \
      item: { \
        id:"minecraft:item_frame", \
        count:1, \
      } \
    }] \
  } \
}

$data modify storage custom_blocks:vars block.data \
        merge from storage custom_blocks:conf $(id).interaction
$data modify storage custom_blocks:vars block.data.Passengers[0] \
        merge from storage custom_blocks:conf $(id).display
$data modify storage custom_blocks:vars block.data.Passengers[0].item.components.minecraft:custom_model_data \
        set from storage custom_blocks:conf $(id).item.custom_model_data
$data modify storage custom_blocks:vars block.data.Passengers[0].item.components.minecraft:custom_data.id \
        set value $(id)
execute store result storage custom_blocks:vars block.data.Passengers[0].Rotation[0] float 0.044444 \
        run scoreboard players add yaw wishlist.vars 191
execute store result storage custom_blocks:vars block.data.Passengers[0].Rotation[0] float 22.5 \
        run data get storage custom_blocks:vars block.data.Passengers[0].Rotation[0]
data modify storage custom_blocks:vars block.data.Tags append value wishlist.custom_block
$data modify storage custom_blocks:vars block.block set from storage custom_blocks:conf $(id).block
function custom_blocks:place_3 with storage custom_blocks:vars block

# custom_blocks:place_3
$summon minecraft:interaction ~ ~ ~ $(data)
$setblock ~ ~ ~ $(block) destroy

