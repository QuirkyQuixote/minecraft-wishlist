Custom Blocks
=============

Framework to implement arbitrary interative custom blocks.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.

Usage
-----

A custom block is composed of two entities: an interaction entity that's used
for the players to use it, and a display entity that is used to provide a
visual cue. The display entity always holds an item frame with custom model
data, that is the same as the item used to represent it in the inventory.

Custom blocks may also have an actual minecraft block hidden by the display
entity.

Custom blocks are configured by adding one entry to the
`wishlist:custom_blocks` storage, with the custom block name, such as:

```
data modify storage wishlist:custom_blocks black_pawn set value { \
    interaction: {}, \
    display: { \
        transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
        billboard: "vertical", \
    }, \
    item: { \
        custom_model_data:271833, \
        custom_name:{"text":"Black Pawn","italic":false} \
    }, \
    block: "minecraft:barrier" \
}
```

The `interaction`, `display` and `item` objects are merged with the interaction
entity, item display entity, and the item held by the display entity *or* the
item as displayed in the inventory.

`block` determines what kind of block will be placed to provide collision or
other behavior: `minecraft:barrier` is good for a collidable full block,
whereas `minecraft:air` is good for small blocks that should not be collided
with; any block place will be replaced with air when the custom block is
removed.

If you use a full block, you should set the `width` and `height` fields of
the `interaction` object to values greater than `1`, even if by a very small
margin; otherwise, the barrier will prevent players from interacting (and thus
removing) the block.

Custom blocks have minimal durability and will break in one hit, just like
scaffolding, honey blocks, or slime blocks.

The datapack provides a function that can be called to give a player one
instance of any block configured:

```
/function custom_blocks:give {id:<block name>}
```

Overhead
--------

Uses an advancement to detect if a player places a custom block; if so, a
function is called to construct it and remove the placed item frame.

Uses an advancement to detect if a player hits a custom block; if so, a
function is called to destroy it and drop an appropriate block item.

Known Issues
------------

Custom blocks can be placed in any place where an item frame can be placed.
This doesn't *exactly* correspond to all possible places where a block can be
placed, but it's quite similar to it.

In particular, you *can* place a block in the hole left by a slab; in that
case the slab will break and drop as an item.

If the block is declared solid and a barrier block spawned, you should set
the `width` and `height` fields of the `interaction` object to values greater
than `1`, even if it is by a very small margin; otherwise the barrier block
will prevent players from interacting with the interaction entity, and it will
become and unremovable object.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:



