# better_nicks:load
scoreboard objectives add wishlist.better_nicks.trigger trigger
data modify storage wishlist:args all_chatter append value \
        "You're a builder? I heard you people could have colored names"
data modify storage wishlist:args all_chatter append value \
        "You can set up your pronouns for people to check with a book"
function better_nicks:tick_1s

# better_nicks:tick_1s
schedule function better_nicks:tick_1s 1s

execute as @a[scores={wishlist.better_nicks.trigger=1..}] \
        at @s \
        run function better_nicks:trigger

scoreboard players enable @a wishlist.better_nicks.trigger
scoreboard players set @a wishlist.better_nicks.trigger 0

# better_nicks:trigger
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.team int 1 \
        run scoreboard players get @s wishlist.playerId
function better_nicks:trigger_1 with storage wishlist:args args

# better_nicks:trigger_1
$title @s actionbar "Editing team $(team)"

$team add $(team)
$team join $(team) @s

$execute if score @s wishlist.better_nicks.trigger matches 1 \
        run function better_nicks:color {team:$(team),color:"black"}
$execute if score @s wishlist.better_nicks.trigger matches 2 \
        run function better_nicks:color {team:$(team),color:"dark_purple"}
$execute if score @s wishlist.better_nicks.trigger matches 3 \
        run function better_nicks:color {team:$(team),color:"dark_blue"}
$execute if score @s wishlist.better_nicks.trigger matches 4 \
        run function better_nicks:color {team:$(team),color:"dark_aqua"}
$execute if score @s wishlist.better_nicks.trigger matches 5 \
        run function better_nicks:color {team:$(team),color:"dark_green"}
$execute if score @s wishlist.better_nicks.trigger matches 6 \
        run function better_nicks:color {team:$(team),color:"gold"}
$execute if score @s wishlist.better_nicks.trigger matches 7 \
        run function better_nicks:color {team:$(team),color:"dark_red"}
$execute if score @s wishlist.better_nicks.trigger matches 8 \
        run function better_nicks:color {team:$(team),color:"gray"}
$execute if score @s wishlist.better_nicks.trigger matches 9 \
        run function better_nicks:color {team:$(team),color:"dark_gray"}
$execute if score @s wishlist.better_nicks.trigger matches 10 \
        run function better_nicks:color {team:$(team),color:"light_purple"}
$execute if score @s wishlist.better_nicks.trigger matches 11 \
        run function better_nicks:color {team:$(team),color:"blue"}
$execute if score @s wishlist.better_nicks.trigger matches 12 \
        run function better_nicks:color {team:$(team),color:"aqua"}
$execute if score @s wishlist.better_nicks.trigger matches 13 \
        run function better_nicks:color {team:$(team),color:"green"}
$execute if score @s wishlist.better_nicks.trigger matches 14 \
        run function better_nicks:color {team:$(team),color:"yellow"}
$execute if score @s wishlist.better_nicks.trigger matches 15 \
        run function better_nicks:color {team:$(team),color:"red"}
$execute if score @s wishlist.better_nicks.trigger matches 16 \
        run function better_nicks:color {team:$(team),color:"white"}

$execute if score @s wishlist.better_nicks.trigger matches 20 \
        run function better_nicks:suffix {team:$(team),suffix:"(any/all)"}
$execute if score @s wishlist.better_nicks.trigger matches 21 \
        run function better_nicks:suffix {team:$(team),suffix:"(ask me)"}
$execute if score @s wishlist.better_nicks.trigger matches 22 \
        run function better_nicks:suffix {team:$(team),suffix:"(use my name)"}

$execute if score @s wishlist.better_nicks.trigger matches 100 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/them)"}
$execute if score @s wishlist.better_nicks.trigger matches 101 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/she)"}
$execute if score @s wishlist.better_nicks.trigger matches 102 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/he)"}
$execute if score @s wishlist.better_nicks.trigger matches 103 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/ae)"}
$execute if score @s wishlist.better_nicks.trigger matches 104 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/ey)"}
$execute if score @s wishlist.better_nicks.trigger matches 105 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/fae)"}
$execute if score @s wishlist.better_nicks.trigger matches 106 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/xe)"}
$execute if score @s wishlist.better_nicks.trigger matches 107 \
        run function better_nicks:suffix {team:$(team),suffix:"(they/ze)"}

$execute if score @s wishlist.better_nicks.trigger matches 110 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/they)"}
$execute if score @s wishlist.better_nicks.trigger matches 111 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/her)"}
$execute if score @s wishlist.better_nicks.trigger matches 112 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/he)"}
$execute if score @s wishlist.better_nicks.trigger matches 113 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/ae)"}
$execute if score @s wishlist.better_nicks.trigger matches 114 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/ey)"}
$execute if score @s wishlist.better_nicks.trigger matches 115 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/fae)"}
$execute if score @s wishlist.better_nicks.trigger matches 116 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/xe)"}
$execute if score @s wishlist.better_nicks.trigger matches 117 \
        run function better_nicks:suffix {team:$(team),suffix:"(she/ze)"}

$execute if score @s wishlist.better_nicks.trigger matches 120 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/they)"}
$execute if score @s wishlist.better_nicks.trigger matches 121 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/she)"}
$execute if score @s wishlist.better_nicks.trigger matches 122 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/him)"}
$execute if score @s wishlist.better_nicks.trigger matches 123 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/ae)"}
$execute if score @s wishlist.better_nicks.trigger matches 124 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/ey)"}
$execute if score @s wishlist.better_nicks.trigger matches 125 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/fae)"}
$execute if score @s wishlist.better_nicks.trigger matches 126 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/xe)"}
$execute if score @s wishlist.better_nicks.trigger matches 127 \
        run function better_nicks:suffix {team:$(team),suffix:"(he/ze)"}

$execute if score @s wishlist.better_nicks.trigger matches 130 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/they)"}
$execute if score @s wishlist.better_nicks.trigger matches 131 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/she)"}
$execute if score @s wishlist.better_nicks.trigger matches 132 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/he)"}
$execute if score @s wishlist.better_nicks.trigger matches 133 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/aer)"}
$execute if score @s wishlist.better_nicks.trigger matches 134 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/ey)"}
$execute if score @s wishlist.better_nicks.trigger matches 135 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/fae)"}
$execute if score @s wishlist.better_nicks.trigger matches 136 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/xe)"}
$execute if score @s wishlist.better_nicks.trigger matches 137 \
        run function better_nicks:suffix {team:$(team),suffix:"(ae/ze)"}

$execute if score @s wishlist.better_nicks.trigger matches 140 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/they)"}
$execute if score @s wishlist.better_nicks.trigger matches 141 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/she)"}
$execute if score @s wishlist.better_nicks.trigger matches 142 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/he)"}
$execute if score @s wishlist.better_nicks.trigger matches 143 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/ae)"}
$execute if score @s wishlist.better_nicks.trigger matches 144 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/em)"}
$execute if score @s wishlist.better_nicks.trigger matches 145 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/fae)"}
$execute if score @s wishlist.better_nicks.trigger matches 146 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/xe)"}
$execute if score @s wishlist.better_nicks.trigger matches 147 \
        run function better_nicks:suffix {team:$(team),suffix:"(ey/ze)"}

$execute if score @s wishlist.better_nicks.trigger matches 150 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/they)"}
$execute if score @s wishlist.better_nicks.trigger matches 151 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/she)"}
$execute if score @s wishlist.better_nicks.trigger matches 152 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/he)"}
$execute if score @s wishlist.better_nicks.trigger matches 153 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/ae)"}
$execute if score @s wishlist.better_nicks.trigger matches 154 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/ey)"}
$execute if score @s wishlist.better_nicks.trigger matches 155 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/faer)"}
$execute if score @s wishlist.better_nicks.trigger matches 156 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/xe)"}
$execute if score @s wishlist.better_nicks.trigger matches 157 \
        run function better_nicks:suffix {team:$(team),suffix:"(fae/ze)"}

$execute if score @s wishlist.better_nicks.trigger matches 160 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/they)"}
$execute if score @s wishlist.better_nicks.trigger matches 161 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/she)"}
$execute if score @s wishlist.better_nicks.trigger matches 162 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/he)"}
$execute if score @s wishlist.better_nicks.trigger matches 163 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/ae)"}
$execute if score @s wishlist.better_nicks.trigger matches 164 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/ey)"}
$execute if score @s wishlist.better_nicks.trigger matches 165 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/fae)"}
$execute if score @s wishlist.better_nicks.trigger matches 166 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/xem)"}
$execute if score @s wishlist.better_nicks.trigger matches 167 \
        run function better_nicks:suffix {team:$(team),suffix:"(xe/ze)"}

$execute if score @s wishlist.better_nicks.trigger matches 170 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/they)"}
$execute if score @s wishlist.better_nicks.trigger matches 171 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/she)"}
$execute if score @s wishlist.better_nicks.trigger matches 172 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/he)"}
$execute if score @s wishlist.better_nicks.trigger matches 173 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/ae)"}
$execute if score @s wishlist.better_nicks.trigger matches 174 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/ey)"}
$execute if score @s wishlist.better_nicks.trigger matches 175 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/fae)"}
$execute if score @s wishlist.better_nicks.trigger matches 176 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/xe)"}
$execute if score @s wishlist.better_nicks.trigger matches 177 \
        run function better_nicks:suffix {team:$(team),suffix:"(ze/hir)"}

# better_nicks:color
$team modify $(team) color $(color)

# better_nicks:suffix
$team modify $(team) suffix {"text":" $(suffix)","color":"gray"}
