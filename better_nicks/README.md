Better Nicks
============

Adds the ability to customize nicks through the team system

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.

Usage
-----

The user interface book can be obtained with the `better_nicks:book` loot
table.

The book provides an option to select any one of sixteen colors to color a
player's nick

The book provides a choice of pronoun tags of which you can get two at once
between: they/them, she/her, he/him, ae/aer, ey/em, fae/faer, xe/xem,
ze/hir/zir; separate options are provided for "any pronoun allowed", "ask me
about my pronouns" and "don't use pronouns, only my name".

Overhead
--------

Clicking on an option on the book triggers a scoreboard whose value is checked
every second to see which action (if any) the player has chosen.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

