Quirky's Minecraft Wishlist
===========================

A collection of datapacks for Minecraft Java that change the game to implement
some of the things I'd like to see in it.

The code of Quirky's Minecraft Wishlist is published under the
[Affero General Public License 3.0 or later](https://www.gnu.org/licenses/agpl-3.0.en.html).

Unless specified otherwise, the [resource pack](resources) is published under
[Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/); see [resources](resources/README.md) for details.

Installation
------------

If you don't want to bother with processing the files and making the datapacks
themselves, a [zipped
release](https://gitlab.com/QuirkyQuixote/minecraft-wishlist/-/jobs/artifacts/main/download?job=build-job)
should be available at [my
gitlab](https://gitlab.com/QuirkyQuixote/minecraft-wishlist/).

You can unzip it and copy the desired datapacks to your local world, server, or
client; if you do that you need not read any further in this section.

The intended way to generate the datapacks is to use the Makefile scripts
provided with the repo:

```shell
make
```

For this you will require:

* A UNIX shell (I used zsh myself)
* GNU Make
* Python3
* zip

The specific handling required:

* Some datapacks will generate both JSON and mcfunction files from python code
  that may or may not use other files as input, including:
  * datapacks that must generate large amounts of loot tables, functions,
    predicates, etc. that only vary in small details.
  * any datapack that generates a book as loot table will use
    [book.py](book.py) to parse the JSON pages and generate the appropriate
    loot table.

* Many datapacks have all their mcfunction code in a single file that is parsed
  by the [pp.py](pp.py) script to generate the actual mcfunction files in their
  right directories.

Contents
--------

The core datapack for the project is:

* [Wishlist Core](wishlist): provides a number of utilities that are used by
  many of the other datapacks.

There's also a resource pack:

* [Wishlist Resources](resources): can be installed on the clients (manually or
  by setting it as server resource pack) to add custom media that can be used
  by the datapacks. Highly recommended since it also has the translation
  strings for many messages used by different datapacks.

A number of datapacks are mostly libraries that add a framework for actual
features to be built over them:

* [Custom Blocks](custom_blocks): framework to implement arbitrary custom
  blocks.
* [Entity Profiling](entity_profiling): provides tools to find
  entities that may be lagging the server.
* [Extreme Biomes](extreme_biomes): provides some biomes where environmental
  conditions are hazardous without proper protection.
* [Look Alive](look_alive): makes entities tagged NoAI turn and look around in
  response to their surroundings.
* [Looping Music](looping_music): adds a system to allow playing of looping
  music that overrides the vanilla minecraft music while it's playing.
* [Magic](magic): provides a framework to create special tools that can have
  arbitrary effects.
* [Player Bound](player_bound): unloads entities way before they reach despawn
  distance from a player; useful for entities that are only required for player
  interaction, and have no behavior otherwise.
* [Random Tick](random_tick): ticks one loaded chunk per player per tick,
  executing all functions tagged `#wishlist:random_tick`.
* [Reload](reload): provides a `wishlist:reload` function to reload all
  datapacks.
* [Stash](stash): low-level utility functions to serialize and deserialize
  non-player entities.
* [Summon](summon): provides an alternative to spawn eggs that will not allow
  to change spawners and allows the summoner to dismiss their summoned
  entities.
* [Upgrade](upgrade): tries to keep custom items up-to-date in the face of
  version changes both from the vanilla game and the datapacks.
* [World Ocean](world_ocean): provides a custom endless ocean dimension
* [World Sketch](world_sketch): provides a custom flatland dimension
* [World Sky](world_sky): provides a custom empty dimension

The actual feature libraries are:

* ![astrolabe] [Astrolabe](astrolabe): provides a custom item that
  shows the player coordinates, facing direction, and region name while held.
* ![aura_lens] [Aura Lens](aura_lens): provides a custom item that
  highlights surrounding mobs when held, and gives hints to their status.
* ![contract] [Bait](bait): items to create points of interest that will
  attract random NPCs with very simple AI.
* ![copper_shovel] [Copper Tools](copper_tools): recipes and loot tables to
  generate copper-tier tools.
* ![speech] [Better Gossip](better_gossip): provides a server-wide reputation score for
  each player according to both villagers and illagers; the villager reputation
  is linked to the villager gossip system.
* ![book] [Better Nicks](better_nicks): gives some degree of customizability to the
  player names.
* ![speech] [Chatterbox](chatterbox): Provide more ways to interact with mobs
  of all types, beyond the default hit/feed/trade.
* ![cushion] [Cushion](cushion): item that allows players to sit on blocks.
* ![music_disc] [Custom Discs](custom_discs): provides additional music discs in
  the game (as opposed to changing the tracks associated with the existing
  ones).
* [Custom Suits](custom_suits): special suits that must we worn whole
  to give bonuses.
* ![big_key] [Dungeons](dungeons): provides a set of tools to build Zelda-like dungeons
  that reset after the players get out.
* [End Above](end_above): teleports to the Overworld sky players that
  fall to The End void.
* [Free Maps](free_maps): allows copying maps from item frames by using a blank
  map on them.
* ![white_marker] [Graffiti](graffiti): provides marker objects that allow writing on walls,
  ceilings, and floors; and a eraser object to remove it.
* ![contract] [Hired Traders](hired_traders): villagers that will sell a player's
  stuff and give them the earned money.
* ![lock_key] [Lock](lock): provides a key item that can be used to lock and unlock
  containers, doors and trapdoors.
* [Millinery](millinery): provides NPCs that allow players to wear any item as
  a silly hat.
* ![small_light] [Modern Lights](modern_lights): provides custom lights that can be placed on
  any side of an existing block.
* ![noise_box] [Noise Box](noise_box): provides a custom item that can hide the
  player from effects from other datapacks.
* ![speech] [NPC Names](npc_names): names unnamed villagers, illagers and piglins; names
  are randomly generated from pieces taken from customizable lists.
* [Pillager Bartering](pillager_bartering): unarmed pillagers will
  barter for emeralds.
* ![envelope] [Player Mail](player_mail): provides NPCs that allow players to send any
  object to each other.
* ![book] [Popular Displays](popular_displays): tools to create item or text display
  entities from items, and back.
* ![contract] [Server Traders](server_traders): allows creating villagers with custom
  trades that will not run out.
* ![ticket] [Skip](skip): provides custom items that can be used to teleport players
  across the world in different ways.
* ![sledgehammer] [Sledgehammer](sledgehammer): craftable set of sledgehammer
  tools.
* ![stiletto] [Stiletto](stiletto): craftable set of stiletto weapons.
* ![contract] [Stock Traders](stock_traders): allows creating villagers with that trade
  between a currency and another material, with the price depending on how much
  they have in their inventory
* ![quip] [Universal Basic Income](universal_basic_income): gives players some spending
  money periodically.

[astrolabe]: astrolabe/assets/astrolabe/textures/item/astrolabe.png ""
[aura_lens]: aura_lens/assets/aura_lens/textures/item/aura_lens.png ""
[music_disc]: custom_discs/assets/custom_discs/textures/item/music_disc_rusty_rain.png ""
[big_key]: dungeons/assets/dungeons/textures/item/big_key.png ""
[noise_box]: noise_box/assets/noise_box/textures/item/noise_box_on.png ""
[small_light]: modern_lights/assets/modern_lights/textures/item/small_light.png ""
[lock_key]: lock/assets/lock/textures/item/lock_key.png ""
[white_marker]: graffiti/assets/graffiti/textures/item/white_marker.png ""
[ticket]: magic/assets/magic/textures/item/ticket.png ""
[quip]: grabeltone/assets/grabeltone/textures/item/coin.png ""
[envelope]: grabeltone/assets/grabeltone/textures/item/envelope.png ""
[book]: wishlist/assets/wishlist/textures/item/modern_book_yellow.png ""
[contract]: summon/assets/summon/textures/item/contract.png ""
[speech]: wishlist/assets/wishlist/textures/item/speech.png ""
[cushion]: cushion/assets/cushion/textures/item/cushion.png ""
[sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/wooden_sledgehammer.png ""
[stiletto]: stiletto/assets/stiletto/textures/item/iron_stiletto.png ""
[copper_shovel]: copper_tools/assets/copper_tools/textures/item/copper_shovel.png ""
