Upgrade
=======

Upgrades items held by players and custom blocks near to them to newer versions
when possible.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.

Usage
-----

As time goes on some of the data in the different custom items from wishlist
has changed; this datapack checks periodically the items wielded by players to
see if they should be upgraded to a newer version.

In general terms, the datapack tries to identify what item something is from a
combination of its item id and its custom model data, on the basis that items
that *look* a certain way are expected by players to be those actual items.

The following items will upgrade when a player holds them:

* Astrolabe is regenerated from `astrolabe:astrolabe` loot table
* Aura Lens is regenerated from `aura_lens:aura_lens` loot table
* Noise Box is regenerated from `noise_box:noise_box` loot table
* All items from from [Magic](../magic) are upgraded from the pre-flattening
  version to the current one
  * This includes flattening items bound inside a grimoire
  * But similar items from [Magic Tools](../magic_tools) are not
* All markers are upgraded to the current version from [Graffiti](../graffiti)
* Keys from [Lockable Chests](../lockable_chests) are upgraded to key from
  [Lock](../lock) with the same combination, so they can open the same chests
  that were locked with the original.

The following custom blocks will upgrade when a player comes close:

* Map hall cores that have no radius and zoom info are tagged with a radius of
  16 blocks and a zoom level of 2.
* Radios from before the [Custom Blocks](../custom_blocks) datapack become
  their custom block variants.
* Custom blocks from [Cooking Stations](../cooking_stations) become the closer
  equivalent from [Better Recipes](../better_recipes)
* Text placed with the [Magic Markers](../magic_markers) datapack will be
  retagged to be managed with the [Graffiti](../graffiti) datapack instead.

Overhead
--------

Checks the item held by each player each second

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

