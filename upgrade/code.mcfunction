# upgrade:load
function upgrade:tick

# upgrade:tick
schedule function upgrade:tick 3s

execute as @a at @s run function upgrade:upgrade
execute at @a as @e[distance=..8] run function upgrade:entity

# upgrade:random_tick
execute as @e[dx=16,dy=256,dz=16] run function upgrade:entity

# upgrade:upgrade
execute if predicate upgrade:aura_lens \
        run loot replace entity @s weapon.mainhand loot aura_lens:aura_lens

execute if predicate upgrade:astrolabe \
        run loot replace entity @s weapon.mainhand loot astrolabe:astrolabe

execute if predicate upgrade:noise_box \
        run loot replace entity @s weapon.mainhand loot noise_box:noise_box

execute if predicate upgrade:chest_key run function upgrade:chest_key


# upgrade:entity
execute as @s[tag=wishlist.globe] \
        on passengers \
        unless data entity @s item.components.minecraft:custom_data.wishlist_globe \
        run data modify entity @s item.components.minecraft:custom_data.wishlist_globe set value {radius:16,zoom:2}

execute as @s[tag=wishlist.better_gossip.radio,tag=!wishlist.custom_block] \
        run function upgrade:radio

execute as @s[tag=wishlist.cooking_stations] at @s \
        run function upgrade:cooking_stations

execute as @s[tag=wishlist.magic_markers.dry] \
        run data modify entity @s Tags set value [wishlist.graffiti]

execute as @s[tag=wishlist.hired_trader] \
        run function upgrade:hired_trader

execute as @s[type=!marker,tag=wishlist.multitudes.npc] \
        run function upgrade:multitudes
execute as @s[type=marker,tag=wishlist.multitudes.npc] \
        run function upgrade:multitudes_marker
execute as @s[tag=wishlist.multitudes.bait] \
        run function upgrade:multitudes_marker

execute as @s[tag=wishlist.popular_displays] \
        run data modify entity @s item_display set value "fixed"

execute as @s[tag=wishlist.custom_block] \
        run function upgrade:custom_block

# upgrade:multitudes
tag @s remove wishlist.multitudes.npc
tag @s add wishlist.roamer
data modify storage wishlist:args bait set value {}
data modify storage wishlist:args bait.player set from entity @s ArmorItems[3].components.minecraft:custom_data.employer
execute at @s summon minecraft:marker run function bait:place_bait_2
tellraw @a[tag=wishlist.debug] {"text":"[upgrade] multitudes > bait","color":"yellow"}

# upgrade:multitudes_marker
data modify storage wishlist:args bait set value {}
data modify storage wishlist:args bait.player set from entity @s data.employer
function bait:place_bait_2
tellraw @a[tag=wishlist.debug] {"text":"[upgrade] multitudes > bait","color":"yellow"}

# upgrade:chest_key
data modify storage wishlist:args code set from entity @s SelectedItem.components.minecraft:custom_data.wishlist.combination
loot replace entity @s weapon.mainhand loot lock:key



# upgrade:cooking_stations
execute on passengers if entity @s[tag=wishlist.cooking_stations.chop] \
        run function custom_blocks:place_1 {id:sink}
execute on passengers if entity @s[tag=wishlist.cooking_stations.oven] \
        run function custom_blocks:place_1 {id:oven}
execute on passengers if entity @s[tag=wishlist.cooking_stations.plate] \
        run function custom_blocks:place_1 {id:cooktop}
execute on passengers if entity @s[tag=wishlist.cooking_stations.press] \
        run function custom_blocks:place_1 {id:oven}
execute on passengers if entity @s[tag=wishlist.cooking_stations.rest] \
        run function custom_blocks:place_1 {id:rack}
execute on passengers if entity @s[tag=wishlist.cooking_stations.ripen] \
        run function custom_blocks:place_1 {id:cask}

function wishlist:dismiss



# upgrade:radio
execute at @s run function custom_blocks:place_1 {id:radio}
function wishlist:dismiss


# upgrade:custom_block
execute on passengers \
        unless data entity @s item.components.minecraft:custom_data.id \
        run data modify entity @s item.components.minecraft:custom_data.id \
                set from entity @s item.components.minecraft:entity_data.Item.tag.wishlist.custom_block.id

# upgrade:hired_trader
execute unless data entity @s ArmorItems[3].components.minecraft:custom_model_data \
        run data modify entity @s ArmorItems[3].components.minecraft:custom_model_data set value 271828

execute unless data entity @s HandItems[1].components.minecraft:custom_data.extra run return fail
data modify storage wishlist:args extra set from entity @s HandItems[1].components.minecraft:custom_data.extra
data modify storage wishlist:args trades set from entity @s Offers.Recipes
data modify storage wishlist:args stock set value []
function upgrade:hired_trader_1
data modify entity @s ArmorItems[3].components.minecraft:custom_data.stock set from storage wishlist:args stock
item replace entity @s weapon.offhand with minecraft:air

# upgrade:hired_trader_1
execute unless data storage wishlist:args extra[0] run return 0
execute store result score stock wishlist.vars run data get storage wishlist:args extra[0].stock
data modify storage wishlist:args temp set from storage wishlist:args trades[0].sell
function upgrade:hired_trader_2
execute store result score stock wishlist.vars run data get storage wishlist:args extra[0].earna
data modify storage wishlist:args temp set from storage wishlist:args trades[0].buy
function upgrade:hired_trader_2
execute store result score stock wishlist.vars run data get storage wishlist:args extra[0].earnb
data modify storage wishlist:args temp set from storage wishlist:args trades[0].buyB
function upgrade:hired_trader_2
data remove storage wishlist:args extra[0]
data remove storage wishlist:args trades[0]
function upgrade:hired_trader_1

# upgrade:hired_trader_2
execute if score stock wishlist.vars matches 0 run return fail
data modify storage wishlist:args stock append from storage wishlist:args temp
execute store result storage wishlist:args stock[-1].count int 1 \
        run scoreboard players get stock wishlist.vars
