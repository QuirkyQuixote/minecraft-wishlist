
include config.mk

packs += astrolabe
packs += aura_lens
packs += bait
packs += better_gossip
packs += better_nicks
packs += better_recipes
packs += chatterbox
packs += copper_tools
packs += cushion
packs += custom_blocks
packs += custom_discs
packs += custom_suits
packs += dungeons
packs += end_above
packs += entity_profiling
packs += extreme_biomes
packs += free_maps
packs += graffiti
packs += hired_traders
packs += lock
packs += looping_music
packs += look_alive
packs += magic
packs += millinery
packs += modern_lights
packs += noise_box
packs += npc_names
packs += parkour
packs += player_bound
packs += player_mail
packs += pillager_bartering
packs += popular_displays
packs += random_tick
packs += reload
packs += server_traders
packs += skip
packs += sledgehammer
packs += stiletto
packs += stash
packs += stock_traders
packs += summon
packs += universal_basic_income
packs += upgrade
packs += wishlist
packs += world_ocean
packs += world_sketch
packs += world_sky
packs += grabeltone
packs += traaa

.PHONY: all
all: data resources

.PHONY: clean
clean: clean-data clean-resources

.PHONY: data
data:
	for i in $(packs); do make -C $$i; done

.PHONY: clean-data
clean-data:
	for i in $(packs); do make -C $$i clean; done

.PHONY: resources
resources:
	$(RM) resources.zip
	cd resources && zip -rq ../resources.zip pack.mcmeta pack.png && cd ..
	for i in $(packs); do \
		[ -d $$i/assets ] && cd $$i && zip -grq ../resources.zip assets && cd .. ;true; \
	done

.PHONY: clean-resouces
clean-resources:
	$(RM) resources.zip


