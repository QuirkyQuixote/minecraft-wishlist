Stiletto
============

Adds a craftable set of stiletto tools.

Stilettos do trash damage when used normally, but can be used to backstab a mob
for massive damage instead.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Provides the following new items:

### ![wooden_stiletto] Wooden Stiletto

* crafted from any planks and one stick
* recipe unlocked when holding a stick

### ![stone_stiletto] Stone Stiletto

* crafted from any stone-tier block and one stick
* recipe unlocked when holding cobblestone

### ![iron_stiletto] Iron Stiletto

* crafted from an iron ingot and one stick
* recipe unlocked when holding iron ingot

### ![golden_stiletto] Golden Stiletto

* crafted from a gold ingot and one stick
* recipe unlocked when holding gold ingot

### ![diamond_stiletto] Diamond Stiletto

* crafted from a diamond and one stick
* recipe unlocked when holding diamond

### ![netherite_stiletto] Netherite Stiletto

* crafted from a netherite ingot and one stick
* recipe unlocked when holding netherite ingot

### ![copper_stiletto] Copper Stiletto

* crafted from a copper ingot and one stick
* recipe unlocked when holding copper ingot

### Backstab

When using a stiletto, the player gathers strength for one second; during
this time the player has a slowness VI effect applied and can't move. If they
are hit at this point, the action is cancelled; otherwise the attack is
unleashed on the entity they are looking at.

The backstab requires the entity to be looking away from the attacking player;
if not, it will fail.

Using the stiletto in this way depletes its durability.
* Stilettos support the unbreaking enchantment and it will protect its
  durability as usual
  * unbreaking 1 has a 50% chance of protecting the tool
  * unbreaking 2 has a 67% chance of protecting the tool
  * unbreaking 3 has a 75% chance of protecting the tool

Overhead
--------

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

[wooden_stiletto]: stiletto/assets/stiletto/textures/item/wooden_stiletto.png ""
[stone_stiletto]: stiletto/assets/stiletto/textures/item/stone_stiletto.png ""
[iron_stiletto]: stiletto/assets/stiletto/textures/item/iron_stiletto.png ""
[golden_stiletto]: stiletto/assets/stiletto/textures/item/golden_stiletto.png ""
[diamond_stiletto]: stiletto/assets/stiletto/textures/item/diamond_stiletto.png ""
[netherite_stiletto]: stiletto/assets/stiletto/textures/item/netherite_stiletto.png ""
[copper_stiletto]: stiletto/assets/stiletto/textures/item/copper_stiletto.png ""
