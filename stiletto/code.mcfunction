
# stiletto:load
scoreboard objectives add stiletto.countdown dummy

data modify storage stiletto:conf tier set value { \
        wooden: {base:wooden, damage:21.0}, \
        stone: {base:stone, damage:27.0}, \
        iron: {base:iron, damage:27.0}, \
        gold: {base:gold, damage:21.0}, \
        diamond: {base:diamond, damage:27.0}, \
        netherite: {base:netherite, damage:30.0}, \
        copper: {base:iron, damage:27.0} \
}

function stiletto:tick

# stiletto:use
execute if score @s stiletto.countdown matches 1.. run return fail
execute unless predicate stiletto:holding_stiletto run return fail
effect give @s minecraft:slowness 1 10 true
scoreboard players set @s stiletto.countdown 20

# stiletto:interrupt
advancement revoke @s only stiletto:interrupt
execute unless score @s stiletto.countdown matches 0.. run return fail
title @s actionbar {"translate":"stiletto.interrupted"}
scoreboard players reset @s stiletto.countdown

# stiletto:tick
schedule function stiletto:tick 1t
scoreboard players remove @a[scores={stiletto.countdown=1..}] stiletto.countdown 1
execute as @a[scores={stiletto.countdown=0}] run function stiletto:tick_1

# stiletto:tick_1
scoreboard players reset @s stiletto.countdown
execute unless predicate stiletto:holding_stiletto run return fail
execute store result score yaw wishlist.vars run data get entity @s Rotation[0]
function stiletto:get_tier with entity @s SelectedItem.components.minecraft:custom_data
tag @s add wishlist.match
execute at @s run function stiletto:raycast
function wishlist:damage_held_item
tag @s remove wishlist.match

# stiletto:raycast
scoreboard players set steps wishlist.vars 45
execute anchored eyes positioned ^ ^ ^ run return run function stiletto:raycast_1

# stiletto:raycast_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ #wishlist:non_full run return fail
execute as @e[tag=!wishlist.match,dx=0,dy=0,dz=0] \
        positioned ~-1 ~-1 ~-1 \
        if entity @s[dx=0,dy=0,dz=0] \
        positioned ~1 ~1 ~1 \
        run return run function stiletto:backstab
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^.1 run return run function stiletto:raycast_1

# stiletto:get_tier
$data modify storage stiletto:vars tier set from storage stiletto:conf tier.$(wishlist_tier)

# stiletto:backstab
scoreboard players set 360 wishlist.vars 360
execute store result score dif wishlist.vars run data get entity @s Rotation[0]
scoreboard players operation dif wishlist.vars -= yaw wishlist.vars
scoreboard players operation dif wishlist.vars %= 360 wishlist.vars
execute if score dif wishlist.vars matches 90..270 run return fail
particle minecraft:crit ~ ~ ~ 0.1 0.1 0.1 0.5 10 normal
function stiletto:damage with storage stiletto:vars tier 
return 1

# stiletto:damage
$damage @s $(damage) minecraft:generic by @a[tag=wishlist.match,limit=1]
