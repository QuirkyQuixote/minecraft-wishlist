#!/bin/python3

import json
import os

items = [
        {
            "name":"wooden_stiletto",
            "tier":"wooden",
            "ingredients": [
                "#minecraft:planks",
                "minecraft:stick"
                ],
            "max_uses":59,
            "model":14663501
            },
        {
            "name":"stone_stiletto",
            "tier":"stone",
            "ingredients": [
                "#minecraft:stone_tool_materials",
                "minecraft:stick"
                ],
            "max_uses":131,
            "model":14663502
            },
        {
            "name":"iron_stiletto",
            "tier":"iron",
            "ingredients": [
                "minecraft:iron_ingot",
                "minecraft:stick"
                ],
            "max_uses":250,
            "model":14663503
            },
        {
            "name":"golden_stiletto",
            "tier":"golden",
            "ingredients": [
                "minecraft:gold_ingot",
                "minecraft:stick"
                ],
            "max_uses":32,
            "model":14663504
            },
        {
            "name":"diamond_stiletto",
            "tier":"diamond",
            "ingredients": [
                "minecraft:diamond",
                "minecraft:stick"
                ],
            "max_uses":1561,
            "model":14663505
            },
        {
            "name":"netherite_stiletto",
            "tier":"netherite",
            "ingredients": [
                "minecraft:netherite_ingot",
                "minecraft:stick"
                ],
            "max_uses":2031,
            "model":14663506
            },
        {
            "name":"copper_stiletto",
            "tier":"copper",
            "ingredients": [
                "minecraft:copper_ingot",
                "minecraft:stick"
                ],
            "max_uses":200,
            "model":14663507
            },
        ]

os.makedirs('data/stiletto/loot_table', exist_ok=True)
os.makedirs('data/stiletto/recipe', exist_ok=True)

for i in items:
    components = {
            "minecraft:custom_name": json.dumps({
                "translate":f"item.wishlist.{i['name']}",
                "italic":False
                }),
            "minecraft:custom_model_data": {"floats":[i['model']]},
            "minecraft:custom_data": {
                "wishlist_stiletto":True,
                "wishlist_tier":i['tier']
                },
            "minecraft:max_damage": i['max_uses']
            }

    if 'ingredients' in i:
        with open(f'data/stiletto/recipe/{i["name"]}.json', 'w') as f:
            f.write(json.dumps({
                "type": "minecraft:crafting_shapeless",
                "category": "equipment",
                "ingredients": i['ingredients'],
                "result": {
                    "id": "minecraft:carrot_on_a_stick",
                    "count": 1,
                    "components": components
                    }
                }, indent=2))

    with open(f'data/stiletto/loot_table/{i["name"]}.json', 'w') as f:
        f.write(json.dumps({
            "pools": [
                {
                    "rolls": 1,
                    "entries": [
                        {
                            "type": "minecraft:item",
                            "name": f"carrot_on_a_stick",
                            "functions": [
                                {
                                    "function": "minecraft:set_components",
                                    "components": components
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }, indent=2))
