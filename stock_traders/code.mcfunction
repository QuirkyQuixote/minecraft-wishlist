
# stock_traders:load

scoreboard players set price_curve wishlist.vars 100
scoreboard players set price_mult wishlist.vars 1000

data modify storage wishlist:args all_chatter append value \
        "A server-wide storage of stuff, with prices changing according to the stock…"
data modify storage wishlist:args all_chatter append value \
        "So, are you contributing to the Stock?"

data modify storage wishlist:args empty_recipe set value { \
  maxUses:2147483647, \
  xp:0, \
  rewardExp:0b, \
  buy:{}, \
  buyB:{}, \
  sell:{}, \
  "Paper.IgnoreDiscounts":1b \
  }

# stock_traders:tick
execute at @a \
        as @e[type=#wishlist:trader,tag=wishlist.stock_trader.update,distance=..6] \
        run function stock_traders:update_trader

# stock_traders:reset_trader
data modify entity @s NoAI set value 1b
tag @s add wishlist.stock_trader

function wishlist:recall

data modify storage wishlist:args memories.buy set from block ~ ~-1 ~ Items[{Slot:0b}]
data remove storage wishlist:args memories.buy.Slot
data modify storage wishlist:args memories.sell set from block ~ ~-1 ~ Items[{Slot:18b}]
data remove storage wishlist:args memories.sell.Slot
data modify storage wishlist:args memories.stock set value 0

data modify storage wishlist:args recipes set value []

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy merge from storage wishlist:args memories.buy
data modify storage wishlist:args recipe.sell merge from storage wishlist:args memories.sell
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy merge from storage wishlist:args memories.sell
data modify storage wishlist:args recipe.sell merge from storage wishlist:args memories.buy
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify entity @s Offers.Recipes set from storage wishlist:args recipes

function wishlist:memorize

particle minecraft:happy_villager ~ ~1 ~ 0.2 1 0.2 0.5 10

function stock_traders:update_trader

# stock_traders:trade_with_stock
tag @e[type=#wishlist:trader,tag=wishlist.stock_trader,distance=..6] \
        add wishlist.stock_trader.update
advancement revoke @s only stock_traders:trade_with_stock
schedule function stock_traders:tick 1t

# stock_traders:update_trader
function wishlist:recall
data modify storage wishlist:args recipes set from entity @s Offers.Recipes

execute store result score stock wishlist.vars \
        run data get storage wishlist:args memories.stock
execute store result score buyCount wishlist.vars \
        run data get storage wishlist:args memories.buy.count
execute store result score sellCount wishlist.vars \
        run data get storage wishlist:args memories.sell.count
execute store result score price wishlist.vars \
        run data get storage wishlist:args recipes[0].sell.count

scoreboard players set priceCurrency wishlist.vars 0
execute store result score priceCurrency wishlist.vars \
        run data get storage wishlist:args \
                memories.sell.components.minecraft:custom_data.wishlist_currency
scoreboard players operation priceCurrency wishlist.vars *= price wishlist.vars

execute store result score uses wishlist.vars \
        run data get storage wishlist:args recipes[0].uses
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= buyCount wishlist.vars
scoreboard players operation stock wishlist.vars += tmp wishlist.vars
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= priceCurrency wishlist.vars
scoreboard players operation vault wishlist.vars -= tmp wishlist.vars

execute store result score uses wishlist.vars \
        run data get storage wishlist:args recipes[1].uses
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= buyCount wishlist.vars
scoreboard players operation stock wishlist.vars -= tmp wishlist.vars
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= priceCurrency wishlist.vars
scoreboard players operation vault wishlist.vars += tmp wishlist.vars

scoreboard players operation price wishlist.vars = sellCount wishlist.vars
scoreboard players operation price wishlist.vars *= price_mult wishlist.vars
execute store result score tmp wishlist.vars \
        run data get storage wishlist:args memories.stock
scoreboard players operation tmp wishlist.vars *= price_mult wishlist.vars
scoreboard players operation tmp wishlist.vars /= price_curve wishlist.vars
scoreboard players operation tmp wishlist.vars += price_mult wishlist.vars
scoreboard players operation price wishlist.vars /= tmp wishlist.vars

scoreboard players set clampedPrice wishlist.vars 1
scoreboard players operation clampedPrice wishlist.vars > price wishlist.vars

execute store result storage wishlist:args recipes[0].sell.count int 1 \
        run scoreboard players get clampedPrice wishlist.vars
execute store result storage wishlist:args recipes[1].buy.count int 1 \
        run scoreboard players get clampedPrice wishlist.vars

data modify storage wishlist:args recipes[0].uses set value 0
data modify storage wishlist:args recipes[1].uses set value 0

data modify storage wishlist:args recipes[0].maxUses set value 1
execute if score vault wishlist.vars < clampedPrice wishlist.vars \
        run data modify storage wishlist:args recipes[0].maxUses set value 0

data modify storage wishlist:args recipes[1].maxUses set value 1
execute if score stock wishlist.vars < buyCount wishlist.vars \
        run data modify storage wishlist:args recipes[1].maxUses set value 0

execute store result storage wishlist:args memories.stock int 1 \
        run scoreboard players get stock wishlist.vars

function wishlist:memorize
data modify entity @s Offers.Recipes set from storage wishlist:args recipes
tag @s remove wishlist.stock_trader.update

