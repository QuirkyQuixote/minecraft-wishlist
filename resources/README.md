Quirky's Wishlist Resources
===========================

Resource pack for Quirky's Minecraft Wishlist datapacks

The following media appears thanks to their respective creators, under their
own licensing terms:

* [sounds/radio/place1.ogg](resources/assets/wishlist/sounds/radio/place1.ogg):
  ["Flicker"](https://freesound.org/people/kwahmah_02/sounds/252012/)
  by [kwahmah_02 of freesound.org](https://freesound.org/people/kwahmah_02/),
  published under [Attribution 3.0 Unported (CC BY 3.0)](http://creativecommons.org/licenses/by/3.0/)
* [sounds/radio/place2.ogg](resources/assets/wishlist/sounds/radio/place2.ogg):
  ["Noiseburst"](https://freesound.org/people/theplax/sounds/620517/)
  by [theplax of freesound.org](https://freesound.org/people/theplax/),
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
* [sounds/radio/place3.ogg](resources/assets/wishlist/sounds/radio/place3.ogg):
  ["Radio Jp Old Record Sound"](https://freesound.org/people/AlienXXX/sounds/257718/)
  by [AlienXXX of freesound.org](https://freesound.org/people/AlienXXX/),
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
* [sounds/radio/break1.ogg](resources/assets/wishlist/sounds/radio/break1.ogg):
  ["Radio Sign Off/Squelch"](https://freesound.org/people/JovianSounds/sounds/524205/)
  by [JovianSounds of freesound.org](https://freesound.org/people/JovianSounds/),
  published under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
* [sounds/radio/break2.ogg](resources/assets/wishlist/sounds/radio/break2.ogg):
  ["Snare Radio Click 2"](https://freesound.org/people/FlyinEye/sounds/160221/)
  by [FlyinEye of freesound.org](https://freesound.org/people/FlyinEye/),
  published under [Attribution 3.0 Unported (CC BY 3.0)](http://creativecommons.org/licenses/by/3.0/)
* [sounds/radio/break3.ogg](resources/assets/wishlist/sounds/radio/break3.ogg):
  ["Snare Radio Click 1"](https://freesound.org/people/FlyinEye/sounds/160222/)
  by [FlyinEye of freesound.org](https://freesound.org/people/FlyinEye/),
  published under [Attribution 3.0 Unported (CC BY 3.0)](http://creativecommons.org/licenses/by/3.0/)
* [sounds/radio/voices1.ogg](resources/assets/wishlist/sounds/radio/voices1.ogg),
  [sounds/radio/voices2.ogg](resources/assets/wishlist/sounds/radio/voices2.ogg),
  [sounds/radio/voices3.ogg](resources/assets/wishlist/sounds/radio/voices3.ogg):
  ["Mumbled Voices"](https://freesound.org/people/shortwave_ghosts/sounds/415294/)
  by [shortwave_ghosts of freesound.org](https://freesound.org/people/shortwave_ghosts/),
  published under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
* [sounds/cash_register/place1.ogg](resources/assets/wishlist/sounds/cash_register/place1.ogg):
  ["Cash Register Purchase"](https://freesound.org/people/Zott820/sounds/209578/)
  by [Zott820 of freesound.org](https://freesound.org/people/Zott820/),
  published under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
* [sounds/cash_register/place2.ogg](resources/assets/wishlist/sounds/cash_register/place2.ogg):
  ["Cash Register"](https://freesound.org/people/kiddpark/sounds/201159/)
  by [Zott820 of freesound.org](https://freesound.org/people/kiddpark/),
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
* [sounds/cash_register/break1.ogg](resources/assets/wishlist/sounds/cash_register/break1.ogg):
  ["Coho - Shutting Cash Register"](https://freesound.org/people/atha89/sounds/79068/)
  by [atha89 of freesound.org](https://freesound.org/people/atha89/),
  published under [Attribution 3.0 Unported (CC BY 3.0)](http://creativecommons.org/licenses/by/3.0/)
* [sounds/cash_register/break2.ogg](resources/assets/wishlist/sounds/cash_register/break2.ogg):
  ["Close_3.WAV"](https://freesound.org/people/kjackson/sounds/32611/)
  by [kjackson of freesound.org](https://freesound.org/people/kjackson/),
  published under [Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)](http://creativecommons.org/licenses/by-nc/3.0/)
* [sounds/noise_box/toggle1](resources/assets/wishlist/sounds/noise_box/toggle1.ogg),
  [sounds/noise_box/toggle2](resources/assets/wishlist/sounds/noise_box/toggle2.ogg),
  [sounds/noise_box/toggle3](resources/assets/wishlist/sounds/noise_box/toggle3.ogg):
  ["opening closing jewelry box"](https://freesound.org/people/buzzatsea/sounds/474173/)
  by [buzzatsea of freesound.org](https://freesound.org/people/buzzatsea/),
  published under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
* [sounds/random/click.ogg](resources/assets/minecraft/sounds/random/click.ogg):
  ["Large Fan"](https://freesound.org/people/MrLindstrom/sounds/543964/)
  by [MrLindstrom of freesound.org](nhttps://freesound.org/people/MrLindstrom/),
  published under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
* [textures/item/echoing_blade.png](resources/assets/grabeltone/textures/item/echoing_blade.png)
  by EssenseOfLuna
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
* [textures/item/axecutioner_axe.png](resources/assets/grabeltone/textures/item/axecutioner_axe.png)
  by EssenseOfLuna
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
* [models/item/collar.json](resources/assets/wishlist/models/item/collar.json)
  by [Emma of steamcommunity.com](https://steamcommunity.com/id/966829697ew80777878d/)
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
* [textures/item/kitkat.png](resources/assets/wishlist/textures/item/kitkat.json)
  by Omori of @andromedasys at discord
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
* [sounds/spring/bounce1](resources/assets/wishlist/sounds/spring/bounce1.ogg),
  [sounds/spring/bounce1](resources/assets/wishlist/sounds/spring/bounce2.ogg),
  [sounds/spring/bounce1](resources/assets/wishlist/sounds/spring/bounce3.ogg),
  [sounds/spring/bounce1](resources/assets/wishlist/sounds/spring/bounce4.ogg):
  ["boing sounds"](https://freesound.org/people/Johnny2810/sounds/577061/)
  by [Johnny2810 of freesound.org](https://freesound.org/people/Johnny2810/)
  published under [Creative Commons 0](http://creativecommons.org/publicdomain/zero/1.0/)
* [sounds/lock/lock1](resources/assets/wishlist/sounds/lock/lock1.ogg),
  [sounds/lock/lock2](resources/assets/wishlist/sounds/lock/lock2.ogg),
  [sounds/lock/lock3](resources/assets/wishlist/sounds/lock/lock3.ogg),
  [sounds/lock/lock4](resources/assets/wishlist/sounds/lock/lock4.ogg),
  [sounds/lock/unlock1](resources/assets/wishlist/sounds/lock/unlock1.ogg),
  [sounds/lock/unlock2](resources/assets/wishlist/sounds/lock/unlock2.ogg),
  [sounds/lock/unlock3](resources/assets/wishlist/sounds/lock/unlock3.ogg),
  [sounds/lock/unlock4](resources/assets/wishlist/sounds/lock/unlock4.ogg):
  ["Locking Unlocking Door 1 Para"](https://freesound.org/people/Paradoxxxical/sounds/679250/)
  by [Paradoxxxical of freesound.org](https://freesound.org/s/679250/)
  published under [Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/)

Translations of the datapacks:

* Hebrew (Israel) translation by Xn0AK0xn
* Hebrew (Israel) translation by [Rose](https://stigstille.uk) (@stigstille at discord)

