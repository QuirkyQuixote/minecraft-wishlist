Sledgehammer
============

Adds a craftable set of sledgehammer tools.

Despite their look, sledgehammers do horrible damage with a regular attack, and
mine *very* slowly, but when used as objects, they allow the player to perform
a charged attack that hits several entities and breaks several blocks at once.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Provides the following new items:

### ![wooden_sledgehammer] Wooden Sledgehammer

* crafted from any log and two sticks
* recipe unlocked when holding a stick
* deals 10.5 damage to mobs
* 59 uses before breaking

### ![stone_sledgehammer] Stone Sledgehammer

* crafted from an stone block and two sticks
* recipe unlocked when holding a stone block
* deals 13.5 damage to mobs
* 131 uses before breaking

### ![iron_sledgehammer] Iron Sledgehammer

* crafted from an iron block and two sticks
* recipe unlocked when holding an iron block
* deals 13.5 damage to mobs
* 250 uses before breaking

### ![golden_sledgehammer] Golden Sledgehammer

* crafted from a gold block and two sticks
* recipe unlocked when holding a gold block
* deals 10.5 damage to mobs
* 32 uses before breaking

### ![diamond_sledgehammer] Diamond Sledgehammer

* crafted from a diamond block and two sticks
* recipe unlocked when holding a diamond block
* deals 13.5 damage to mobs
* 1561 uses before breaking

### ![netherite_sledgehammer] Netherite Sledgehammer

* crafted from a netherite block and two sticks
* recipe unlocked when holding a netherite block
* deals 15 damage to mobs
* 2031 uses before breaking

### ![copper_sledgehammer] Copper Sledgehammer

* crafted from a copper block and two sticks
* recipe unlocked when holding a copper block
* deals 13.5 damage to mobs
* 201 uses before breaking

### Hammer Smash

When using a sledgehammer, the player gathers strength for one second; during
this time the player has a slowness VI effect applied and can't move. If they
are hit at this point, the action is cancelled; otherwise the attack is
unleashed on the block or entity they are looking at.

Blocks in a 3x3x3 cube centered one block above the target will be destroyed.

* Exceptions to this are unbreakable blocks, fluids and portals.
* Every destroyed block drops what it would have dropped if mined with a
  pickaxe of the same material.
* This effect does not work if the player is in adventure mode.

Entities close to the impact point will be dealth an amount of damage that
depends on the sledgehammer material.

Using the sledgehammer in this way depletes its durability.
* Sledgehammers support the unbreaking enchantment and it will protect its
  durability as usual
  * unbreaking 1 has a 50% chance of protecting the tool
  * unbreaking 2 has a 67% chance of protecting the tool
  * unbreaking 3 has a 75% chance of protecting the tool

Overhead
--------

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

[wooden_sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/wooden_sledgehammer.png ""
[stone_sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/stone_sledgehammer.png ""
[iron_sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/iron_sledgehammer.png ""
[golden_sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/golden_sledgehammer.png ""
[diamond_sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/diamond_sledgehammer.png ""
[netherite_sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/netherite_sledgehammer.png ""
[copper_sledgehammer]: sledgehammer/assets/sledgehammer/textures/item/copper_sledgehammer.png ""
