
# sledgehammer:load
scoreboard objectives add sledgehammer.countdown dummy

data modify storage sledgehammer:conf tier set value { \
        wooden: {base:wooden, damage:7.5}, \
        stone: {base:stone, damage:9.0}, \
        iron: {base:iron, damage:10.5}, \
        gold: {base:gold, damage:7.5}, \
        diamond: {base:diamond, damage:12.0}, \
        netherite: {base:netherite, damage:13.5}, \
        copper: {base:iron, damage:10.5} \
}

function sledgehammer:tick

# sledgehammer:use
execute if score @s sledgehammer.countdown matches 1.. run return fail
execute unless predicate sledgehammer:holding_sledgehammer run return fail
effect give @s minecraft:slowness 1 10 true
scoreboard players set @s sledgehammer.countdown 20

# sledgehammer:interrupt
advancement revoke @s only sledgehammer:interrupt
execute unless score @s sledgehammer.countdown matches 0.. run return fail
title @s actionbar {"translate":"sledgehammer.interrupted"}
scoreboard players reset @s sledgehammer.countdown

# sledgehammer:tick
schedule function sledgehammer:tick 1t
scoreboard players remove @a[scores={sledgehammer.countdown=1..}] sledgehammer.countdown 1
execute as @a[scores={sledgehammer.countdown=0}] run function sledgehammer:tick_1

# sledgehammer:tick_1
scoreboard players reset @s sledgehammer.countdown
execute unless predicate sledgehammer:holding_sledgehammer run return fail
tag @s add wishlist.match
execute at @s \
        if function sledgehammer:raycast \
        run function sledgehammer:tick_2 with storage sledgehammer:vars raycast
tag @s remove wishlist.match

# sledgehammer:tick_2
$execute positioned $(x) $(y) $(z) run function sledgehammer:tick_3

# sledgehammer:tick_3
function sledgehammer:tick_4 with entity @s SelectedItem.components.minecraft:custom_data
execute if entity @s[gamemode=!adventure] \
        positioned ~-1 ~-1 ~-1 \
        run function sledgehammer:mine
playsound minecraft:entity.player.attack.crit player @a ~ ~ ~
particle minecraft:crit ~ ~ ~ 1 1 1 0.1 40 normal @a
function sledgehammer:attack with storage sledgehammer:vars tier
function wishlist:damage_held_item

# sledgehammer:tick_4
$data modify storage sledgehammer:vars tier set from storage sledgehammer:conf tier.$(wishlist_tier)

# sledgehammer:mine
execute positioned ~0 ~0 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~0 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~0 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~1 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~1 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~1 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~2 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~2 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~0 ~2 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier

execute positioned ~1 ~0 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~0 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~0 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~1 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~1 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~1 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~2 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~2 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~1 ~2 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier

execute positioned ~2 ~0 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~0 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~0 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~1 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~1 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~1 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~2 ~0 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~2 ~1 run function sledgehammer:mine_1 with storage sledgehammer:vars tier
execute positioned ~2 ~2 ~2 run function sledgehammer:mine_1 with storage sledgehammer:vars tier

# sledgehammer:mine_1
execute if block ~ ~ ~ #minecraft:dragon_immune run return fail
execute if block ~ ~ ~ minecraft:water run return fail
execute if block ~ ~ ~ minecraft:lava run return fail
$execute unless block ~ ~ ~ #minecraft:incorrect_for_$(base)_tool \
        run setblock ~ ~ ~ minecraft:air destroy
setblock ~ ~ ~ minecraft:air

# sledgehammer:attack
$execute as @e[tag=!wishlist.match,type=!minecraft:item,dx=2,dy=2,dz=2] \
        run damage @s $(damage) minecraft:player_explosion by @a[tag=wishlist.match,limit=1]

# sledgehammer:raycast
scoreboard players set steps wishlist.vars 45
execute anchored eyes positioned ^ ^ ^ run return run function sledgehammer:raycast_1

# sledgehammer:raycast_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ #wishlist:non_full \
        summon minecraft:marker \
        run return run function sledgehammer:raycast_2
execute as @e[tag=!wishlist.match,dx=0,dy=0,dz=0] \
        positioned ~-1 ~-1 ~-1 \
        if entity @s[dx=0,dy=0,dz=0] \
        positioned ~1 ~1 ~1 \
        summon minecraft:marker \
        run return run function sledgehammer:raycast_2
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^.1 run return run function sledgehammer:raycast_1

# sledgehammer:raycast_2
data modify storage sledgehammer:vars raycast set value {}
data modify storage sledgehammer:vars raycast.x set from entity @s Pos[0]
data modify storage sledgehammer:vars raycast.y set from entity @s Pos[1]
data modify storage sledgehammer:vars raycast.z set from entity @s Pos[2]
kill @s
return 1

