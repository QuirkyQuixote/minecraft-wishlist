#!/bin/python3

import json
import os

items = [
        {
            "type":"wooden",
            "ingredients": {
                "1":"#minecraft:logs",
                "2": "minecraft:stick"
                },
            "max_uses":59,
            "model":14113501
            },
        {
            "type":"stone",
            "ingredients": {
                "1": "minecraft:stone",
                "2": "minecraft:stick"
                },
            "max_uses":131,
            "model":14113502
            },
        {
            "type":"iron",
            "ingredients": {
                "1": "minecraft:iron_block",
                "2": "minecraft:stick"
                },
            "max_uses":250,
            "model":14113503
            },
        {
            "type":"golden",
            "ingredients": {
                "1": "minecraft:gold_block",
                "2": "minecraft:stick"
                },
            "max_uses":32,
            "model":14113504
            },
        {
            "type":"diamond",
            "ingredients": {
                "1": "minecraft:diamond_block",
                "2": "minecraft:stick"
                },
            "max_uses":1561,
            "model":14113505
            },
        {
            "type":"netherite",
            "ingredients": {
                "1": "minecraft:netherite_block",
                "2": "minecraft:stick"
                },
            "max_uses":2031,
            "model":14113506
            },
        {
            "type":"copper",
            "ingredients": {
                "1": "minecraft:copper_block",
                "2": "minecraft:stick"
                },
            "max_uses":201,
            "model":14113507
            },
        ]

os.makedirs('data/sledgehammer/loot_table', exist_ok=True)
os.makedirs('data/sledgehammer/recipe', exist_ok=True)

for i in items:
    components = {
            "minecraft:custom_name": json.dumps({
                "translate":f"item.wishlist.{i['type']}_sledgehammer",
                "italic":False
                }),
            "minecraft:custom_model_data": {"floats":[i['model']]},
            "minecraft:custom_data": {
                "wishlist_sledgehammer":True,
                "wishlist_tier":i['type']
                },
            "minecraft:max_damage": i['max_uses']
            }

    if 'ingredients' in i:
        with open(f'data/sledgehammer/recipe/{i["type"]}_sledgehammer.json', 'w') as f:
            f.write(json.dumps({
                "type": "minecraft:crafting_shaped",
                "category": "equipment",
                "pattern": [
                    " 1 ",
                    " 2 ",
                    " 2 "
                    ],
                "key": i['ingredients'],
                "result": {
                    "id": "minecraft:carrot_on_a_stick",
                    "count": 1,
                    "components": components
                    }
                }, indent=2))

    with open(f'data/sledgehammer/loot_table/{i["type"]}_sledgehammer.json', 'w') as f:
        f.write(json.dumps({
            "pools": [
                {
                    "rolls": 1,
                    "entries": [
                        {
                            "type": "minecraft:item",
                            "name": "minecraft:carrot_on_a_stick",
                            "functions": [
                                {
                                    "function": "minecraft:set_components",
                                    "components": components
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }, indent=2))

