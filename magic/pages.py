
import os
import json

os.makedirs('data/magic/function', exist_ok=True)

def page(number, begin, end):
    f.write("$data modify storage wishlist:args pages append value {raw:'[")
    for i in range(begin,end):
        if i != begin:
            f.write(',{"text":"\\\\n"},')
        f.write(f'{{"text":"","extra":[$(name{i})],"clickEvent":{{"action":"run_command","value":"/trigger wishlist.grimoire set {i}"}},"hoverEvent":{{"action":"show_text","contents":$(lore{i})}}}}')
    f.write("]'}\n\n")

with open("data/magic/function/rewrite_1.mcfunction",'w') as f:
    f.write('data modify storage wishlist:args pages set value []\n\n')
    page(1, 0, 9)
    page(2, 9, 18)
    page(3, 18, 27)
    f.write('data modify entity @s Item.components.minecraft:written_book_content.pages set from storage wishlist:args pages')
