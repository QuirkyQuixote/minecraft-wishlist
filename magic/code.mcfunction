
# magic:load
scoreboard objectives add wishlist.grimoire trigger

data modify storage wishlist:args all_chatter append value \
        "You have to tear Tickets to use them so they're only good once"
data modify storage wishlist:args all_chatter append value \
        "Do you have a Magic Wand? those things are pretty useful"
data modify storage wishlist:args all_chatter append value \
        "A Magic Book can hold up to twenty-seven magics, I guess they couldn't fit more"
data modify storage wishlist:args all_chatter append value \
        "I love Baubles: they're so fragile, but that's part of the charm"

function magic:pages
function magic:tick_1s
function magic:tick_1t

# magic:tick_1s
schedule function magic:tick_1s 1s

execute at @a \
        as @e[type=minecraft:item,distance=..4] \
        if data entity @s Item.components.minecraft:custom_data.wishlist_grimoire \
        at @s \
        run function magic:book

execute at @a \
        as @e[type=minecraft:item,distance=..4] \
        if data entity @s Item.components.minecraft:custom_data.wishlist_bauble \
        at @s \
        run function magic:bauble

# magic:tick_1t
schedule function magic:tick_1t 1t

execute as @a[gamemode=!spectator,scores={wishlist.grimoire=0..}] \
        run function magic:use_book
scoreboard players enable @a wishlist.grimoire
scoreboard players set @a wishlist.grimoire -1

# magic:bauble
data modify storage wishlist:args bauble set value {}
scoreboard players set done wishlist.vars 0
execute as @e[type=item,distance=..1] \
        if data entity @s Item.components.minecraft:custom_data.wishlist_magic \
        unless data entity @s Item.components.minecraft:custom_data.wishlist_bauble \
        run function magic:bauble_1
execute if score done wishlist.vars matches 0 run return fail
data remove entity @s Item.components.minecraft:custom_name
data remove entity @s Item.components.minecraft:lore
data remove entity @s Item.components.minecraft:custom_data.wishlist.magic
data modify entity @s Item.components merge from storage wishlist:args bauble
playsound minecraft:block.anvil.use player @a ~ ~ ~

# magic:bauble_1
data modify storage wishlist:args bauble.minecraft:custom_name \
        merge from entity @s Item.components.minecraft:custom_name
data modify storage wishlist:args bauble.minecraft:lore \
        merge from entity @s Item.components.minecraft:lore
data modify storage wishlist:args bauble.minecraft:custom_data.wishlist_magic \
        merge from entity @s Item.components.minecraft:custom_data.wishlist_magic
kill @s
scoreboard players add done wishlist.vars 1

# magic:book
execute if block ~ ~ ~ minecraft:chest run function magic:edit

# magic:edit
execute if entity @s[tag=wishlist.rebinded] run return fail
execute unless entity @s[nbt={Item:{count:1}}] \
        on origin \
        run return run title @s actionbar {"translate":"magic.too_many_books"}

data modify storage wishlist:args items set from entity @s \
        Item.components.minecraft:custom_data.wishlist_grimoire
data modify entity @s Item.components.minecraft:custom_data.wishlist_grimoire \
        set from block ~ ~ ~ Items
data modify block ~ ~ ~ Items set from storage wishlist:args items
function magic:rewrite

tag @s add wishlist.rebinded

playsound minecraft:block.anvil.use player @a ~ ~ ~
execute on origin run title @s actionbar {"translate":"magic.rebind"}
execute if data entity @s Item.components.minecraft:custom_data.wishlist_grimoire[0] \
        on origin \
        run advancement grant @s only wishlist:use_grimoire

# magic:mainhand_bauble
advancement revoke @s only magic:mainhand_bauble
function magic:use_magic_1 with entity @s \
        SelectedItem.components.minecraft:custom_data.wishlist_magic
item replace entity @s weapon.mainhand with air
title @s actionbar {"translate":"magic.bauble_broke"}

# magic:offhand_bauble
advancement revoke @s only magic:offhand_bauble
function magic:use_magic_1 with entity @s \
        Inventory[{Slot:-106b}].components.minecraft:custom_data.wishlist_magic
item replace entity @s weapon.offhand with air
title @s actionbar {"translate":"magic.bauble_broke"}

# magic:rewrite
data modify storage wishlist:args items set from entity @s \
        Item.components.minecraft:custom_data.wishlist_grimoire

data modify storage wishlist:args args set value { \
        name0:'{"text":""}', name1:'{"text":""}', name2:'{"text":""}', \
        name3:'{"text":""}', name4:'{"text":""}', name5:'{"text":""}', \
        name6:'{"text":""}', name7:'{"text":""}', name8:'{"text":""}', \
        name9:'{"text":""}', name10:'{"text":""}', name11:'{"text":""}', \
        name12:'{"text":""}', name13:'{"text":""}', name14:'{"text":""}', \
        name15:'{"text":""}', name16:'{"text":""}', name17:'{"text":""}', \
        name18:'{"text":""}', name19:'{"text":""}', name20:'{"text":""}', \
        name21:'{"text":""}', name22:'{"text":""}', name23:'{"text":""}', \
        name24:'{"text":""}', name25:'{"text":""}', name26:'{"text":""}', \
        lore0:'{"text":""}', lore1:'{"text":""}', lore2:'{"text":""}', \
        lore3:'{"text":""}', lore4:'{"text":""}', lore5:'{"text":""}', \
        lore6:'{"text":""}', lore7:'{"text":""}', lore8:'{"text":""}', \
        lore9:'{"text":""}', lore10:'{"text":""}', lore11:'{"text":""}', \
        lore12:'{"text":""}', lore13:'{"text":""}', lore14:'{"text":""}', \
        lore15:'{"text":""}', lore16:'{"text":""}', lore17:'{"text":""}', \
        lore18:'{"text":""}', lore19:'{"text":""}', lore20:'{"text":""}', \
        lore21:'{"text":""}', lore22:'{"text":""}', lore23:'{"text":""}', \
        lore24:'{"text":""}', lore25:'{"text":""}', lore26:'{"text":""}', \
        }

data modify storage wishlist:args args.name0 set from storage \
        wishlist:args items[{Slot:0b}].components.minecraft:custom_name
data modify storage wishlist:args args.name1 set from storage \
        wishlist:args items[{Slot:1b}].components.minecraft:custom_name
data modify storage wishlist:args args.name2 set from storage \
        wishlist:args items[{Slot:2b}].components.minecraft:custom_name
data modify storage wishlist:args args.name3 set from storage \
        wishlist:args items[{Slot:3b}].components.minecraft:custom_name
data modify storage wishlist:args args.name4 set from storage \
        wishlist:args items[{Slot:4b}].components.minecraft:custom_name
data modify storage wishlist:args args.name5 set from storage \
        wishlist:args items[{Slot:5b}].components.minecraft:custom_name
data modify storage wishlist:args args.name6 set from storage \
        wishlist:args items[{Slot:6b}].components.minecraft:custom_name
data modify storage wishlist:args args.name7 set from storage \
        wishlist:args items[{Slot:7b}].components.minecraft:custom_name
data modify storage wishlist:args args.name8 set from storage \
        wishlist:args items[{Slot:8b}].components.minecraft:custom_name

data modify storage wishlist:args args.name9 set from storage \
        wishlist:args items[{Slot:9b}].components.minecraft:custom_name
data modify storage wishlist:args args.name10 set from storage \
        wishlist:args items[{Slot:10b}].components.minecraft:custom_name
data modify storage wishlist:args args.name11 set from storage \
        wishlist:args items[{Slot:11b}].components.minecraft:custom_name
data modify storage wishlist:args args.name12 set from storage \
        wishlist:args items[{Slot:12b}].components.minecraft:custom_name
data modify storage wishlist:args args.name13 set from storage \
        wishlist:args items[{Slot:13b}].components.minecraft:custom_name
data modify storage wishlist:args args.name14 set from storage \
        wishlist:args items[{Slot:14b}].components.minecraft:custom_name
data modify storage wishlist:args args.name15 set from storage \
        wishlist:args items[{Slot:15b}].components.minecraft:custom_name
data modify storage wishlist:args args.name16 set from storage \
        wishlist:args items[{Slot:16b}].components.minecraft:custom_name
data modify storage wishlist:args args.name17 set from storage \
        wishlist:args items[{Slot:17b}].components.minecraft:custom_name

data modify storage wishlist:args args.name18 set from storage \
        wishlist:args items[{Slot:18b}].components.minecraft:custom_name
data modify storage wishlist:args args.name19 set from storage \
        wishlist:args items[{Slot:19b}].components.minecraft:custom_name
data modify storage wishlist:args args.name20 set from storage \
        wishlist:args items[{Slot:20b}].components.minecraft:custom_name
data modify storage wishlist:args args.name21 set from storage \
        wishlist:args items[{Slot:21b}].components.minecraft:custom_name
data modify storage wishlist:args args.name22 set from storage \
        wishlist:args items[{Slot:22b}].components.minecraft:custom_name
data modify storage wishlist:args args.name23 set from storage \
        wishlist:args items[{Slot:23b}].components.minecraft:custom_name
data modify storage wishlist:args args.name24 set from storage \
        wishlist:args items[{Slot:24b}].components.minecraft:custom_name
data modify storage wishlist:args args.name25 set from storage \
        wishlist:args items[{Slot:25b}].components.minecraft:custom_name
data modify storage wishlist:args args.name26 set from storage \
        wishlist:args items[{Slot:26b}].components.minecraft:custom_name

data modify storage wishlist:args args.lore0 set from storage \
        wishlist:args items[{Slot:0b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore1 set from storage \
        wishlist:args items[{Slot:1b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore2 set from storage \
        wishlist:args items[{Slot:2b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore3 set from storage \
        wishlist:args items[{Slot:3b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore4 set from storage \
        wishlist:args items[{Slot:4b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore5 set from storage \
        wishlist:args items[{Slot:5b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore6 set from storage \
        wishlist:args items[{Slot:6b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore7 set from storage \
        wishlist:args items[{Slot:7b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore8 set from storage \
        wishlist:args items[{Slot:8b}].components.minecraft:lore[0]

data modify storage wishlist:args args.lore9 set from storage \
        wishlist:args items[{Slot:9b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore10 set from storage \
        wishlist:args items[{Slot:10b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore11 set from storage \
        wishlist:args items[{Slot:11b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore12 set from storage \
        wishlist:args items[{Slot:12b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore13 set from storage \
        wishlist:args items[{Slot:13b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore14 set from storage \
        wishlist:args items[{Slot:14b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore15 set from storage \
        wishlist:args items[{Slot:15b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore16 set from storage \
        wishlist:args items[{Slot:16b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore17 set from storage \
        wishlist:args items[{Slot:17b}].components.minecraft:lore[0]

data modify storage wishlist:args args.lore18 set from storage \
        wishlist:args items[{Slot:18b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore19 set from storage \
        wishlist:args items[{Slot:19b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore20 set from storage \
        wishlist:args items[{Slot:20b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore21 set from storage \
        wishlist:args items[{Slot:21b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore22 set from storage \
        wishlist:args items[{Slot:22b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore23 set from storage \
        wishlist:args items[{Slot:23b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore24 set from storage \
        wishlist:args items[{Slot:24b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore25 set from storage \
        wishlist:args items[{Slot:25b}].components.minecraft:lore[0]
data modify storage wishlist:args args.lore26 set from storage \
        wishlist:args items[{Slot:26b}].components.minecraft:lore[0]

function magic:rewrite_1 with storage wishlist:args args

# magic:use_book
execute unless data entity @s SelectedItem.components.minecraft:custom_data.wishlist_grimoire \
        run return fail
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.grimoire
execute at @s run function magic:use_book_1 with storage wishlist:args args

# magic:use_book_1
$function magic:use_magic_1 with entity @s SelectedItem.components.minecraft:custom_data.wishlist_grimoire[{Slot:$(id)b}].components.minecraft:custom_data.wishlist_magic

# magic:use_magic
function magic:use_magic_1 with entity @s SelectedItem.components.minecraft:custom_data.wishlist_magic
execute unless entity @s[gamemode=creative] \
        if data entity @s SelectedItem.components.minecraft:custom_data.wishlist_one_use \
        run item modify entity @s weapon.mainhand wishlist:consume_one

# magic:use_magic_1
$function $(id) $(args)
