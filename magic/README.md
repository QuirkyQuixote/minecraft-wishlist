Magic
=====

Provides a framework to build tools that allow triggering arbitrary effects

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

There are four special items provided by the datapack:

### ![ticket] Ticket

* triggered by using
* will break after one use
* no default way to create these is provided in this datapack
  * other datapacks provide specific types

### ![wand] Wand

* triggered by using
* will not break and can be reused
* no default way to create these is provided in this datapack
  * other datapacks provide specific types

### ![bauble] Bauble

* triggered by either using or getting hurt
* will break after one use
* created with the `magic:bauble` loot table

The effect triggered by the ![bauble] bauble can be changed by dropping it
alongside a ![ticket] ticket or ![wand] wand; the second item will be destroyed
and the effect will be copied, overwriting any previous effects it may have
had.

### ![grimoire] Grimoire

* can hold up to twenty-seven effects and cast them at will
* obtained with the `magic:book` loot table

The ![grimoire] grimoire can be edited by dropping it on an chest; by doing so
the grimoire's and chest's inventories will be swapped. any ![ticket] tickets,
![wand] wands, and ![bauble] baubles bound to the grimoire will then appear in
its first three pages, and they can be used by clicking on their names.

![ticket] Tickets and ![bauble] baubles bound into a book will not break when
used.

The lore of items bound to the book will pop as text when the cursor hover over
the clickable name.

Overhead
--------

Checks every player every frame to see if they used a carrot on a stick

Checks every item in a 4-block distance of every player every second to see if
it is a book that should be updated

Known Issues
------------

Any magic item that replaces the held tool when cast should either have a
safety check in the called function or it will destroy the book being held by
the player.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Bound Pages and Spells: bind at least one item in a grimoire

[wand]: magic/assets/magic/textures/item/wand.png ""
[ticket]: magic/assets/magic/textures/item/ticket.png ""
[bauble]: magic/assets/magic/textures/item/bauble.png ""
[grimoire]: magic/assets/magic/textures/item/book.png ""
