
# cushion:load
function cushion:tick_1s
function cushion:tick_1t

# cushion:tick_1s
schedule function cushion:tick_1s 1s
execute at @a run function cushion:prune

# cushion:tick_1t
schedule function cushion:tick_1t 1t
scoreboard players operation sit_players_0 wishlist.vars = sit_players_1 wishlist.vars
scoreboard players set sit_players_1 wishlist.vars 0
execute as @a on vehicle run scoreboard players add sit_players_1 wishlist.vars 1
execute unless score sit_players_0 wishlist.vars = sit_players_1 wishlist.vars \
        at @a run function cushion:prune

# cushion:block
execute on vehicle run return fail
execute unless predicate cushion:holding_cushion run return fail
execute if entity @s[tag=wishlist.sneaking] run return fail
execute unless function wishlist:raycast run return fail
function cushion:block_1 with storage wishlist:raycast {}

# cushion:block_1
$execute if block $(x1) $(y1) $(z1) minecraft:decorated_pot \
        run return fail
$execute if block $(x1) $(y1) $(z1) #minecraft:stairs[half=bottom] \
        positioned $(x1) $(y1).5 $(z1) run return run function cushion:block_2
$execute if block $(x1) $(y1) $(z1) #minecraft:slabs[type=bottom] \
        positioned $(x1) $(y1).5 $(z1) run return run function cushion:block_2
$execute if block $(x1) $(y1) $(z1) #cushion:custom \
        positioned $(x1) $(y1).5 $(z1) run return run function cushion:block_2
$execute positioned $(x0) $(y0) $(z0) run return run function cushion:block_2

# cushion:block_2
execute if block ~ ~-0.25 ~ #wishlist:non_full run return fail
execute unless block ~ ~0.75 ~ #wishlist:non_full run return fail
execute unless block ~ ~1.75 ~ #wishlist:non_full run return fail
tag @s add wishlist.match
execute summon minecraft:interaction run function cushion:sit
tag @s remove wishlist.match

# cushion:entity
advancement revoke @s only cushion:use_cushion_on_entity
execute on vehicle run return fail
execute unless predicate cushion:holding_cushion run return fail
execute if entity @s[tag=wishlist.sneaking] run return fail
tag @s add wishlist.match

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        at @s \
        if data entity @s interaction \
        store success score done wishlist.vars \
        run function cushion:entity_1

execute if score done wishlist.vars matches 0 run title @s actionbar {"translate":"cushion.no_space"}
tag @s remove wishlist.match

# cushion:entity_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute unless block ~ ~1 ~ #wishlist:non_full run return fail
execute unless block ~ ~2 ~ #wishlist:non_full run return fail
data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.height set from entity @s height
execute at @s run function cushion:entity_2 with storage wishlist:args args

# cushion:entity_2
$execute positioned ~ ~$(height) ~ summon minecraft:interaction run function cushion:sit

# cushion:prune
execute as @e[tag=wishlist.cushion,distance=..4] run function cushion:prune_1

# cushion:prune_1
execute on passengers run return fail
kill @s

# cushion:sit
tellraw @a[tag=wishlist.debug] {"text":"[cushion] sit","color":"yellow"}
data merge entity @s {Tags:[wishlist.cushion],width:0.5,height:0}
ride @a[tag=wishlist.match,limit=1] mount @s
return 1

