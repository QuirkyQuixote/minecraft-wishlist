Cushion
=======

Adds a ![cushion] cushion item that allows players to sit on blocks and
interaction entities

Compare with [Sittable Blocks](../sittable_blocks):

* Cushion allows to sit on interactions.
* Cushion triggers interactive blocks when sitting on them.
* Cushion will sit the player on the block they click.
* Cushion does require the use of an item taking one inventory slot.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for UI
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

A ![cushion] cushion can be aquired in two ways:

* By crafting the `cushion:cushion` recipe, that four wool carpet pieces of any
  colors in the corners of the grid, and five feathers filling the rest of it.
* Through the `cushion:cushion` loot table.

For most blocks:

* clicking on top will sit you on top,
* clicking on the bottom will fail,
* clicking on the side will try to sit you on a block on that side.

For bottom stairs, bottom slabs, any block in the `cushion:custom` tag, and
interaction entities:

* clicking anywhere will sit you on it as if it was a chair

When sitting the cushion will be consumed.

Standing up will spawn a new cushion item for the player to pick up.

Overhead
--------

Uses [Magic](../magic) to trigger the cushion item.

Checks entities in a small radius around every player every second in search of
unoccupied cushions that can be respawned.

Known Issues
------------

You can't sit by clicking on a pot; it is artificially blocked so that players
will not sit *and* stash the cushion inside duping the cushion.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

[cushion]: cushion/assets/cushion/textures/item/cushion.png ""
