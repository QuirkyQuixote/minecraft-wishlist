Astrolabe
=========

Adds an ![astrolabe] astrolabe item that shows current coordinates while being held.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

An ![astrolabe] astrolabe can be aquired in two ways:

* By crafting the `astrolabe:astrolabe` recipe, that requires four copper ingots
  surrounding a globe banner pattern.
* Through the `astrolabe:astrolabe` loot table.

Holding the ![astrolabe] astrolabe item in any hand shows the current coordinates, facing
direction, and name of the current chunk and region in the action bar:

```
<dimension name> <x> <y> <z> <facing direction> <chunk name> <region name>
```

Overhead
--------

Updates players holding an astrolabe every tick.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Star Taker: hold an astrolabe on either hand

[astrolabe]: astrolabe/assets/astrolabe/textures/item/astrolabe.png ""
