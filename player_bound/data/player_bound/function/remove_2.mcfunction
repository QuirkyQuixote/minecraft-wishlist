# remove_2.mcfunction

data modify entity @s data set from storage wishlist:args stash
tag @s add wishlist.player_bound.stash

execute store result score n wishlist.vars if entity @e[tag=wishlist.player_bound]
tellraw @a[tag=wishlist.debug] { \
        "translate":"[bound] -1 (%s active)", \
        "with":[{"score":{"name":"n","objective":"wishlist.vars"}}], \
        "color":"red" \
        }

