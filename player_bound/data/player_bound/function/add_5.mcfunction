# add_5.mcfunction

data modify storage wishlist:args stash set from entity @s data
function stash:pop
kill @s

execute store result score n wishlist.vars if entity @e[tag=wishlist.player_bound]
tellraw @a[tag=wishlist.debug] { \
        "translate":"[bound] +1 (%s active)", \
        "with":[{"score":{"name":"n","objective":"wishlist.vars"}}], \
        "color":"green" \
        }

