Player Bound
============

Provides a mechanic that dismisses certain entities way below the server
simulation distance, and brings them back when one player is basically on top
of them. Useful for optimizing areas with large amounts of NPCs.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Random Tick](../random_tick) for update optimizations.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Entities tagged `wishlist.player_bound` will be "stashed" inside a marker when
no player remains inside a 48-block sphere centered on them.

Markers will become the former entities when a player enters a 32-block sphere
centered on them.

Passengers will be stashed alongside their vehicle; only the vehicle should be
tagged `wishlist.player_bound`; otherwise, the passengers will be duplicated.

Overhead
--------

Updates every three seconds:

* checks every entity in a 32-block radius around each player in search of npc
  locations to be populated.

Randomly updates one chunk per tick:

* checks every entity in the chunk in search of NPCs outside the range of an
  existing player to remove them.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:
