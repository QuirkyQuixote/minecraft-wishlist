
# better_recipes:load
scoreboard objectives add wishlist.load dummy
scoreboard objectives add wishlist.doneTick dummy
scoreboard objectives add wishlist.messTick dummy

data modify storage custom_blocks:conf oven set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.oven], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,-0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271850]}, \
              custom_name:'{"translate":"block.wishlist.oven","italic":false}' \
        }, \
        block: "minecraft:barrier" \
}

data modify storage custom_blocks:conf cooktop set value { \
        interaction: { \
                Tags: [wishlist.cooktop], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,-0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271851]}, \
              custom_name:'{"translate":"block.wishlist.cooktop","italic":false}' \
        }, \
        block: "minecraft:barrier" \
}

data modify storage custom_blocks:conf press set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.press], \
                width: 0.5, \
                height: 0.5 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.0f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271852]}, \
              custom_name:'{"translate":"block.wishlist.press","italic":false}' \
        }, \
        block: "minecraft:air" \
}

data modify storage custom_blocks:conf rack set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.rack], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,-0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271853]}, \
              custom_name:'{"translate":"block.wishlist.rack","italic":false}' \
        }, \
        block: "minecraft:barrier" \
}

data modify storage custom_blocks:conf sink set value { \
        interaction: { \
                Tags: [wishlist.sink], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,-0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271856]}, \
              custom_name:'{"translate":"block.wishlist.sink","italic":false}' \
        }, \
        block: "minecraft:barrier" \
}

data modify storage custom_blocks:conf trash set value { \
        interaction: { \
                Tags: [wishlist.trash], \
                width: 0.75, \
                height: 0.875 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,-0.375f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271857]}, \
              custom_name:'{"translate":"block.wishlist.trash","italic":false}' \
        }, \
        block: "minecraft:air" \
}

data modify storage custom_blocks:conf cask set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.cask], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,-0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271859]}, \
              custom_name:'{"translate":"block.wishlist.cask","italic":false}' \
        }, \
        block: "minecraft:barrier" \
}

data modify storage custom_blocks:conf still set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.still], \
                width: 0.875, \
                height: 0.875 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,-0.375f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              custom_model_data:{floats:[271849]}, \
              custom_name:'{"translate":"block.wishlist.still","italic":false}' \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:cooktop_variants pan set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.pan], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                item: { components: {"minecraft:custom_model_data":271854 }} \
        } \
}

data modify storage wishlist:cooktop_variants pot set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.pot], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                item: { components: {"minecraft:custom_model_data":271855 }} \
        } \
}

data modify storage wishlist:cooktop_variants steamer set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.steamer], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                item: {components: { "minecraft:custom_model_data":271858 }} \
        } \
}

function better_recipes:transitions
function better_recipes:results
function better_recipes:tick_1s

# better_recipes:tick_1s
schedule function better_recipes:tick_1s 1s
execute at @a \
        as @e[tag=wishlist.kitchen,distance=..8] \
        at @s \
        on passengers \
        run function better_recipes:update

# better_recipes:update
execute unless score @s wishlist.load matches 1.. run return fail
execute store result score now wishlist.vars run time query gametime
execute if score now wishlist.vars < @s wishlist.doneTick \
        run return run particle minecraft:smoke ~ ~0.8 ~ 0.1 0.1 0.1 0 5
execute if score now wishlist.vars < @s wishlist.messTick \
        run return run particle minecraft:poof ~ ~0.8 ~ 0.1 0.1 0.1 0 10
particle minecraft:large_smoke ~ ~0.8 ~ 0.1 0.1 0.1 0 5

# better_recipes:cooktop
advancement revoke @s only better_recipes:cooktop
data modify storage wishlist:args args set from entity @s SelectedItem

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function better_recipes:cooktop_1 with storage wishlist:args args.components.minecraft:custom_data.wishlist_cookware

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"translate":"better_recipes.error"}
execute unless score done wishlist.vars matches 0 \
        run item modify entity @s weapon.mainhand wishlist:consume_one



# better_recipes:cooktop_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
$data modify entity @s {} merge from storage wishlist:cooktop_variants $(id).interaction
$execute on passengers run data modify entity @s {} merge from storage wishlist:cooktop_variants $(id).display
scoreboard players set done wishlist.vars 1

# better_recipes:interact
advancement revoke @s only better_recipes:interact
data modify storage wishlist:args item set value {}
data modify storage wishlist:args item set from entity @s SelectedItem

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function better_recipes:interact_1

# better_recipes:interact_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run function better_recipes:interact_2

# better_recipes:interact_2
execute if function better_recipes:interact_3 \
        run return run function better_recipes:empty_2
execute if data storage wishlist:args item.components.minecraft:custom_data.wishlist_recipe \
        run return run function better_recipes:fill_2
execute on vehicle on target unless predicate better_recipes:holding_ingredient \
        run return fail
return run function better_recipes:push_item_2

# better_recipes:interact_3
execute on vehicle on target if predicate better_recipes:holding_nothing run return 1
execute on vehicle on target if predicate better_recipes:holding_bowl run return 1
execute on vehicle on target if predicate better_recipes:holding_bottle run return 1
return fail

# better_recipes:empty_2
execute unless score @s wishlist.load matches 1.. \
        on vehicle on target run return run title @s actionbar {"translate":"better_recipes.empty"}
execute store result score now wishlist.vars run time query gametime
execute if score now wishlist.vars > @s wishlist.messTick \
        run data modify entity @s Tags set value ["mess"]
execute if score now wishlist.vars > @s wishlist.doneTick \
        run function better_recipes:empty_3
scoreboard players set @s wishlist.doneTick 2000000000
scoreboard players set @s wishlist.messTick 2000000000
data modify storage wishlist:args item set value {}
data modify storage wishlist:args item.id set from entity @s Tags[0]
execute on vehicle on target run function better_recipes:fetch with storage wishlist:args item
execute if score done wishlist.vars matches 0 run return fail
scoreboard players remove @s wishlist.load 1
execute if score @s wishlist.load matches 0 run data modify entity @s Tags set value []
playsound minecraft:item.bundle.remove_one player @a ~ ~ ~

# better_recipes:empty_3
data modify storage wishlist:args args set value {}
execute on vehicle if entity @s[tag=wishlist.oven] \
        run data modify storage wishlist:args args.where set value "oven"
execute on vehicle if entity @s[tag=wishlist.pan] \
        run data modify storage wishlist:args args.where set value "pan"
execute on vehicle if entity @s[tag=wishlist.pot] \
        run data modify storage wishlist:args args.where set value "pot"
execute on vehicle if entity @s[tag=wishlist.steamer] \
        run data modify storage wishlist:args args.where set value "steamer"
execute on vehicle if entity @s[tag=wishlist.rack] \
        run data modify storage wishlist:args args.where set value "rack"
execute on vehicle if entity @s[tag=wishlist.press] \
        run data modify storage wishlist:args args.where set value "press"
execute on vehicle if entity @s[tag=wishlist.still] \
        run data modify storage wishlist:args args.where set value "still"
execute on vehicle if entity @s[tag=wishlist.cask] \
        run data modify storage wishlist:args args.where set value "cask"
data modify storage wishlist:args args.what set from entity @s Tags[0]
function better_recipes:empty_4 with storage wishlist:args args

# better_recipes:empty_4
data modify entity @s Tags[0] set value "mess"
$data modify entity @s Tags[0] set from storage wishlist:better_recipes $(where).$(what)

# better_recipes:fill_2
scoreboard players add @s wishlist.load 1
execute on vehicle if entity @s[tag=wishlist.oven] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.pan] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.pot] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.steamer] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.rack] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.press] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.still] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.cask] \
        run scoreboard players set ttl wishlist.vars 1000
execute store result score @s wishlist.doneTick run time query gametime
scoreboard players operation @s wishlist.doneTick += ttl wishlist.vars
scoreboard players operation @s wishlist.messTick = @s wishlist.doneTick
scoreboard players operation @s wishlist.messTick += ttl wishlist.vars
data modify entity @s Tags append from storage wishlist:args item.components.minecraft:custom_data.wishlist_recipe
execute if data entity @s Tags[1] run data modify entity @s Tags set value ["mess"]
execute on vehicle on target run function better_recipes:fill_3
playsound minecraft:item.bundle.insert player @a ~ ~ ~

# better_recipes:fill_3
execute if predicate better_recipes:holding_suspicious_stew \
        run return run item replace entity @s weapon.mainhand with minecraft:bowl
execute if predicate better_recipes:holding_potion \
        run return run item replace entity @s weapon.mainhand with minecraft:glass_bottle
item modify entity @s weapon.mainhand wishlist:consume_one

# better_recipes:push_item_2
scoreboard players add @s wishlist.load 1
execute store result score @s wishlist.doneTick run time query gametime
scoreboard players add @s wishlist.doneTick 200
scoreboard players operation @s wishlist.messTick = @s wishlist.doneTick
scoreboard players add @s wishlist.messTick 200
data modify entity @s Tags append string storage wishlist:args item.id 10
execute if data entity @s Tags[1] run data modify entity @s Tags set value ["mess"]
execute on vehicle on target run item modify entity @s weapon.mainhand wishlist:consume_one
playsound minecraft:item.bundle.insert player @a ~ ~ ~

# better_recipes:empty_bucket
advancement revoke @s only better_recipes:empty_bucket
item modify entity @s weapon.mainhand wishlist:consume_one
playsound minecraft:item.bucket.empty player @a ~ ~ ~
give @s minecraft:bucket

# better_recipes:fill_bucket
advancement revoke @s only better_recipes:fill_bucket
item modify entity @s weapon.mainhand wishlist:consume_one
playsound minecraft:item.bucket.fill player @a ~ ~ ~
give @s minecraft:water_bucket


# better_recipes:fetch
scoreboard players set done wishlist.vars 0
data modify storage wishlist:args args set value {tool:"hand"}
$data modify storage wishlist:args args.tool set from storage wishlist:better_recipes fetch.$(id)
function better_recipes:fetch_1 with storage wishlist:args args
execute if score done wishlist.vars matches 0 run return fail
item modify entity @s weapon.mainhand wishlist:consume_one
$execute positioned ~ ~1 ~ run function better_recipes:give {id:$(id)}

# better_recipes:fetch_1
$execute unless predicate better_recipes:holding_$(tool) \
        run return run title @s actionbar { \
          "translate":"better_recipes.bad_tool", \
          "with":["$(tool)"] \
          }
scoreboard players set done wishlist.vars 1

# better_recipes:give
$loot spawn ~ ~ ~ loot better_recipes:food/$(id)
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[cooking] spawned %s", \
        "with":["$(id)"], \
        "color":"yellow" \
        }

# better_recipes:merge
advancement revoke @s only better_recipes:merge
clear @s minecraft:barrier
function better_recipes:give {id:pizza}


# better_recipes:take_pan
advancement revoke @s only better_recipes:take_pan

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s attack \
        run function better_recipes:take_pan_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"translate":"better_recipes.error"}




# better_recipes:take_pan_1
execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run data modify entity @s item.components.minecraft:custom_model_data set value 271851
execute on passengers run data modify entity @s Tags set value []
execute on passengers run scoreboard players set @s wishlist.load 0
data modify entity @s {} merge from storage custom_blocks:conf cooktop.interaction
tag @s add wishlist.custom_block
loot spawn ~ ~0.5 ~ loot better_recipes:pan
scoreboard players set done wishlist.vars 1

# better_recipes:take_pot
advancement revoke @s only better_recipes:take_pot

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s attack \
        run function better_recipes:take_pot_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"translate":"better_recipes.error"}





# better_recipes:take_pot_1
execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run data modify entity @s item.components.minecraft:custom_model_data set value 271851
execute on passengers run data modify entity @s Tags set value []
execute on passengers run scoreboard players set @s wishlist.load 0
data modify entity @s {} merge from storage custom_blocks:conf cooktop.interaction
tag @s add wishlist.custom_block
loot spawn ~ ~0.5 ~ loot better_recipes:pot
scoreboard players set done wishlist.vars 1

# better_recipes:take_steamer
advancement revoke @s only better_recipes:take_steamer

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s attack \
        run function better_recipes:take_steamer_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"translate":"better_recipes.error"}

# better_recipes:take_steamer_1
execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run data modify entity @s item.components.minecraft:custom_model_data set value 271851
execute on passengers run data modify entity @s Tags set value []
execute on passengers run scoreboard players set @s wishlist.load 0
data modify entity @s {} merge from storage custom_blocks:conf cooktop.interaction
tag @s add wishlist.custom_block
loot spawn ~ ~0.5 ~ loot better_recipes:steamer
scoreboard players set done wishlist.vars 1

# better_recipes:trash
advancement revoke @s only better_recipes:trash
item modify entity @s weapon.mainhand wishlist:consume_one
playsound minecraft:block.composter.fill player @a ~ ~ ~
give @s minecraft:bowl

