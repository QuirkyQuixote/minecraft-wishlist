Better Recipes
==============

Rewrite of the [Cooking Stations](../cooking_stations) datapack looking to make
it work after 1.20.5

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Custom Blocks](../custom_blocks) for custom blocks
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The datapack provides a number of custom recipes that result into custom food
items all of which are retextured and data-tagged suspicious stew items.

The datapack also provides a number of special blind recipes (because vanilla
minecraft can't handle custom data tags in recipe inputs) that allow combining
two custom food items into something else (e.g.: vegetable broth + cooked
noodles = vegetable noodles)

The datapack provides workstations that can transform both vanilla and custom
food items placed inside:

* Clicking on them with the `use/place` button while holding a food item will
  add the food item to the stack of ingredients being processed by the station;
  you can only stack items of the same type in the station; pushing a different
  type of item than the one already inside turns the whole stack into "mess".
* Clicking on them with the `use/place` button while holding an appropriate
  tool (a bowl, a glass bottle or nothing at all, depending on the items in the
  station) will remove one of the items in the stack and give it to the player;
  if the player was holding one item it will remove it from their hand.
* Clicking on them with the `attack/destroy` button will remove the station and
  drop it as an item.

Each station has a somewhat different way of being worked, but all of them will
signal their progress with smoke particles:

* Small smoke particles signal a recipe in progress; at this point fetching an
  item will retrieve the same item that was input originally.
* Big white particles signal a recipe is complete; at this point fetching an
  item will retrieve a transformed item.
* Black particles signal the recipe overcooked; at this point fetching an
  item will retrieve a bowl of "mess".

Each cooking station has a different function and somewhat different ways of
working:

* **Oven**: used for closed cooking of items. When the first item is placed,
  the station will start emitting smoke and completion time will automatically
  start counting down. Every time you add an item to the station completion
  time will reset. Takes 200 ticks to cook a recipe.

* **Cooktop**: this is not technically a cooking station by itself; it must be
  "completed" by using a **pan**, **pot** or **steamer**, turning it into that
  particular station

* **Pan**, **Pot**, **Steamer**: these work the same as the oven does, each one
  with their own collection of transformations; the difference is that punching
  the station will drop the item that was used on the cooktop and turn it back
  into a plain cooktop.

* **Food Press**: used to extract juices out of otherwise solid foods, such as
  oil from seeds or juices from fruits.

* **Cooling Rack**: used to let items cool or rest for a while, such as letting
  bread rise or cheese curds form. Takes 1000 ticks (one minecraft hour, 50
  real time seconds) to proceed.

* **Cask**: used to let items age or ripe, such as making bread stale, or
  ferment juice into wine, and then into vinegar. Takes 24000 ticks (a full
  minecraft day, 20 real time minutes) to proceed.

* **Still**: used to distill alcoholic beverages into stronger forms.

* **Sink**: not specifically a kitchen station: when an empty bucket is used on
  it, it becames a water bucket; when a water bucket is used on it, it becomes
  an empty bucket.

* **Trash**: not specifically a kitchen station: when a food item (anything
  whose base item is a suspicious stew) is used on it, it becomes an empty
  bowl.

Overhead
--------

Uses a number of advancements to check if players interacted with the custom
blocks in the right way.

Updates every second and searches every entity in a 4-block radius around a
player in search of a work station to update its progress.

Known Issues
------------

The "merge" crafting recipes that combine two items, at least one of which is
custom, into a new custom item, are handled with some advancement/command magic
because minecraft does not support components in recipe inputs; this means that
the output for all of those recipes is a mess item, unless you combine the
right ones, in which case the mess will become the expected item.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:


