
import json
import re

# All possible custom food items
# * key is the item name
# * value is an array containing:
#   * the base item
#   * the base name of the texture file
#   * the number used as CustomModelData

results = {
    "mess": [ 'minecraft:suspicious_stew', 'bowl_of_mess', 300000 ],
    "raw_fish_broth": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300001 ],
    "raw_meat_broth": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300002 ],
    "raw_vegetable_broth": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300003 ],
    "raw_fish_stew": [ 'minecraft:suspicious_stew', 'bowl_of_pale_chunks', 300004 ],
    "raw_meat_stew": [ 'minecraft:suspicious_stew', 'bowl_of_red_chunks', 300005 ],
    "raw_vegetable_stew": [ 'minecraft:suspicious_stew', 'bowl_of_pale_chunks', 300006 ],
    "raw_fish_roast": [ 'minecraft:suspicious_stew', 'bowl_of_pale_chunks', 300007 ],
    "raw_meat_roast": [ 'minecraft:suspicious_stew', 'bowl_of_red_chunks', 300008 ],
    "raw_vegetable_roast": [ 'minecraft:suspicious_stew', 'bowl_of_pale_chunks', 300009 ],
    "raw_custard": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300010 ],
    "cookie_dough": [ 'minecraft:bread', 'dough', 300011 ],
    "raw_meat_patty": [ 'minecraft:bread', 'meat_patty', 300012 ],
    "raw_meatballs": [ 'minecraft:suspicious_stew', 'bowl_of_red_chunks', 300013 ],
    "dough": [ 'minecraft:bread', 'dough', 300014 ],
    "risen_dough": [ 'minecraft:bread', 'risen_dough', 300015 ],
    "boiled_dough": [ 'minecraft:bread', 'risen_dough', 300016 ],
    "milk_dough": [ 'minecraft:bread', 'dough', 300017 ],
    "risen_milk_dough": [ 'minecraft:bread', 'risen_dough', 300018 ],
    "batter": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300019 ],
    "noodles": [ 'minecraft:bread', 'noodles', 300020 ],
    "cooked_noodles": [ 'minecraft:suspicious_stew', 'bowl_of_noodles', 300021 ],
    "mash": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300022 ],
    "wort": [ 'minecraft:suspicious_stew', 'bowl_of_brown_liquid', 300023 ],
    "beer": [ 'minecraft:suspicious_stew', 'bowl_of_brown_liquid', 300024 ],
    "whisky": [ 'minecraft:suspicious_stew', 'bowl_of_brown_liquid', 300025 ],
    "fish_broth": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300026 ],
    "meat_broth": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300027 ],
    "vegetable_broth": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300028 ],
    "fish_stew": [ 'minecraft:suspicious_stew', 'bowl_of_orange_chunks', 300029 ],
    "meat_stew": [ 'minecraft:suspicious_stew', 'bowl_of_brown_chunks', 300030 ],
    "vegetable_stew": [ 'minecraft:suspicious_stew', 'bowl_of_orange_chunks', 300031 ],
    "fish_roast": [ 'minecraft:suspicious_stew', 'bowl_of_orange_chunks', 300032 ],
    "meat_roast": [ 'minecraft:suspicious_stew', 'bowl_of_brown_chunks', 300033 ],
    "vegetable_roast": [ 'minecraft:suspicious_stew', 'bowl_of_orange_chunks', 300034 ],
    "custard": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300035 ],
    "cookies": [ 'minecraft:bread', 'cookies', 300036 ],
    "meat_patty": [ 'minecraft:bread', 'cooked_meat_patty', 300037 ],
    "meatballs": [ 'minecraft:suspicious_stew', 'bowl_of_brown_chunks', 300038 ],
    "hardtack": [ 'minecraft:bread', 'hardtack', 300039 ],
    "baguette": [ 'minecraft:bread', 'baguette', 300040 ],
    "bagel": [ 'minecraft:bread', 'bagel', 300041 ],
    "mantou": [ 'minecraft:bread', 'mantou', 300042 ],
    "flatbread": [ 'minecraft:bread', 'flatbread', 300043 ],
    "milk_bread": [ 'minecraft:bread', 'milk_bun', 300044 ],
    "green_tea_leaves": [ 'minecraft:brown_dye', 'green_tea_leaves', 300045 ],
    "dried_tea_leaves": [ 'minecraft:brown_dye', 'green_tea_leaves', 300046 ],
    "black_tea_leaves": [ 'minecraft:brown_dye', 'black_tea_leaves', 300047 ],
    "green_tea": [ 'minecraft:suspicious_stew', 'green_tea', 300048 ],
    "black_tea": [ 'minecraft:suspicious_stew', 'black_tea', 300049 ],
    "potato_juice": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300050 ],
    "carrot_juice": [ 'minecraft:suspicious_stew', 'bowl_of_orange_liquid', 300051 ],
    "beetroot_juice": [ 'minecraft:suspicious_stew', 'bowl_of_red_liquid', 300052 ],
    "apple_juice": [ 'minecraft:suspicious_stew', 'bowl_of_pale_liquid', 300053 ],
    "melon_juice": [ 'minecraft:suspicious_stew', 'bowl_of_red_liquid', 300054 ],
    "pumpkin_juice": [ 'minecraft:suspicious_stew', 'bowl_of_orange_liquid', 300055 ],
    "sweet_berry_juice": [ 'minecraft:suspicious_stew', 'bowl_of_red_liquid', 300056 ],
    "glow_berry_juice": [ 'minecraft:suspicious_stew', 'bowl_of_orange_liquid', 300057 ],
    "wine": [ 'minecraft:suspicious_stew', 'bowl_of_red_liquid', 300058 ],
    "liquor": [ 'minecraft:suspicious_stew', 'bowl_of_red_liquid', 300059 ],
    "curdled_milk": [ 'minecraft:suspicious_stew', 'bowl_of_white_liquid', 300060 ],
    "cheese_curds": [ 'minecraft:suspicious_stew', 'bowl_of_pale_chunks', 300061 ],
    "cheese": [ 'minecraft:bread', 'cheese', 300062 ],
    "aged_cheese": [ 'minecraft:bread', 'cheese', 300063 ],
    "raw_pizza": [ 'minecraft:bread', 'pizza_slice', 300064 ],
    "pizza": [ 'minecraft:bread', 'pizza_slice', 300065 ],
    "burger": [ 'minecraft:bread', 'burger', 300066 ],
    "fish_ramen": [ 'minecraft:suspicious_stew', 'bowl_of_noodles', 300067 ],
    "meat_ramen": [ 'minecraft:suspicious_stew', 'bowl_of_noodles', 300068 ],
    "vegetable_ramen": [ 'minecraft:suspicious_stew', 'bowl_of_noodles', 300069 ],
    "prunes": [ 'minecraft:suspicious_stew', 'bowl_of_black_chunks', 300070 ],
    "pancake": [ 'minecraft:bread', 'flatbread', 300071 ],
    "bulgur": [ 'minecraft:bread', 'bulgur', 300072 ],
    "nigirizushi": [ 'minecraft:bread', 'nigirizushi', 300073 ],
    "makizushi": [ 'minecraft:bread', 'makizushi', 300074 ],
    "waffle": [ 'minecraft:bread', 'waffle', 300075 ],
}

# All vanilla items that can be fetched from a station

vanilla_results = [
    "potato",
    "carrot",
    "beetroot",
    "brown_mushroom",
    "red_mushroom",
    "porkchop",
    "beef",
    "mutton",
    "chicken",
    "rabbit",
    "cod",
    "salmon",
    "baked_potato",
    "cooked_porkchop",
    "cooked_beef",
    "cooked_mutton",
    "cooked_chicken",
    "cooked_rabbit",
    "cooked_cod",
    "cooked_salmon",
    "apple",
    "melon_slice",
    "pumpkin",
    "sweet_berries",
    "glow_berries",
    "wheat_seeds",
    "melon_seeds",
    "pumpkin_seeds",
    "beetroot_seeds",
    "allium",
    "azure_bluet",
    "blue_orchid",
    "cornflower",
    "dandelion",
    "lily_of_the_valley",
    "oxeye_daisy",
    "poppy",
    "orange_tulip",
    "pink_tulip",
    "red_tulip",
    "white_tulip",
    "lilac",
    "peony",
    "rose_bush",
    "sunflower",
    "dried_kelp",
    ]

# Data to generate all recipes for crafting tables that use vanilla ingredients

recipes = {
        "raw_fish_broth": [
            "minecraft:bowl",
            "#better_recipes:fish",
            "minecraft:water_bucket"
            ],
        "raw_meat_broth": [
            "minecraft:bowl",
            "#better_recipes:meat",
            "minecraft:water_bucket"
            ],
        "raw_vegetable_broth": [
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "minecraft:water_bucket"
            ],
        "raw_fish_stew":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:fish",
            "minecraft:water_bucket"
            ],
        "raw_meat_stew":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:meat",
            "minecraft:water_bucket"
            ],
        "raw_vegetable_stew":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:vegetables",
            "minecraft:water_bucket"
            ],
        "raw_fish_roast":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:fish",
            ],
        "raw_meat_roast":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:meat",
            ],
        "raw_vegetable_roast":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:vegetables",
            ],
        "raw_custard":[
            "minecraft:bowl",
            "minecraft:egg",
            "minecraft:sugar",
            "minecraft:milk_bucket"
            ],
        "cookie_dough":[
            "minecraft:wheat",
            "minecraft:sugar",
            "minecraft:egg"
            ],
        "dough":[
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:water_bucket"
            ],
        "milk_dough":[
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:milk_bucket"
            ],
        "batter":[
            "minecraft:bowl",
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:water_bucket"
            ],
        "mash":[
            "minecraft:bowl",
            "minecraft:wheat",
            "minecraft:water_bucket"
            ],
        "noodles":[
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:egg",
            "minecraft:water_bucket"
            ],
        "raw_meat_patty":[
            "#better_recipes:meat",
            ],
        "raw_meatballs":[
            "minecraft:bowl",
            "#better_recipes:meat",
            "minecraft:egg",
            "minecraft:allium",
            ],
        "curdled_milk":[
            "minecraft:bowl",
            "minecraft:milk_bucket",
            "minecraft:chorus_fruit",
            ]
    }

# data to generate all recipes for crafting tables that merge two custom items

merge = {
        "raw_pizza": [ "risen_dough","cheese" ],
        "burger": [ "milk_bread","meat_patty" ],
        "fish_ramen": [ "fish_broth","cooked_noodles" ],
        "meat_ramen": [ "meat_broth","cooked_noodles" ],
        "vegetable_ramen": [ "vegetable_broth","cooked_noodles" ],
        "nigirizushi": [ "bulgur","salmon" ],
        "makizushi": [ "bulgur","dried_kelp" ],
}

# Transformation recipes in each station

trans_oven = {
        "raw_fish_roast":"fish_roast",
        "raw_meat_roast":"meat_roast",
        "raw_vegetable_roast":"vegetable_roast",
        "cookie_dough":"cookies",
        "dough":"hardtack",
        "boiled_dough":"bagel",
        "risen_dough":"baguette",
        "risen_milk_dough":"milk_bread",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        "raw_pizza":"pizza",
        }

trans_pan = {
        "raw_meat_patty":"meat_patty",
        "raw_meatballs":"meatballs",
        "dough":"flatbread",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        "batter":"pancake",
        "dried_tea_leaves":"green_tea_leaves",
        }

trans_pot = {
        "raw_fish_broth":"fish_broth",
        "raw_meat_broth":"meat_broth",
        "raw_vegetable_broth":"vegetable_broth",
        "raw_fish_stew":"fish_stew",
        "raw_meat_stew":"meat_stew",
        "raw_vegetable_stew":"vegetable_stew",
        "raw_custard":"custard",
        "risen_dough":"boiled_dough",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        "green_tea_leaves":"green_tea",
        "black_tea_leaves":"black_tea",
        "mash":"wort",
        "curdled_milk":"cheese_curds",
        "noodles":"cooked_noodles",
        "wheat":"bulgur",
        }

trans_steamer = {
        "risen_dough":"mantou",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        "dried_tea_leaves":"green_tea_leaves",
        }

trans_rack = {
        "dough":"risen_dough",
        "milk_dough":"risen_milk_dough",
        "azure_bluet":"dried_tea_leaves",
        "blue_orchid":"dried_tea_leaves",
        "cornflower":"dried_tea_leaves",
        "dandelion":"dried_tea_leaves",
        "lily_of_the_valley":"dried_tea_leaves",
        "oxeye_daisy":"dried_tea_leaves",
        "poppy":"dried_tea_leaves",
        "orange_tulip":"dried_tea_leaves",
        "pink_tulip":"dried_tea_leaves",
        "red_tulip":"dried_tea_leaves",
        "white_tulip":"dried_tea_leaves",
        "lilac":"dried_tea_leaves",
        "peony":"dried_tea_leaves",
        "rose_bush":"dried_tea_leaves",
        "sunflower":"dried_tea_leaves",
        }

trans_press = {
        "potato":"potato_juice",
        "carrot":"carrot_juice",
        "beetroot":"beetroot_juice",
        "apple":"apple_juice",
        "melon_slice":"melon_juice",
        "pumpkin":"pumpkin_juice",
        "sweet_berries":"sweet_berry_juice",
        "glow_berries":"glow_berry_juice",
}

trans_cask = {
        "potato_juice":"wine",
        "carrot_juice":"wine",
        "beetroot_juice":"wine",
        "apple_juice":"wine",
        "melon_juice":"wine",
        "pumpkin_juice":"wine",
        "sweet_berry_juice":"wine",
        "glow_berry_juice":"wine",
        "wort":"beer",
        "cheese_curds":"cheese",
        "cheese":"aged_cheese",
        "sweet_berries":"prunes",
        "glow_berries":"prunes",
        "dried_tea_leaves":"black_tea_leaves",
}

trans_still = {
        "wine":"liquor",
        "beer":"whisky",
}

# Generate loot tables for all items that can be fetched from a station
# This includes custom items and vanilla items.

for r in results:
    with open(f'data/better_recipes/loot_table/food/{r}.json', 'w') as f:
        f.write(json.dumps({
            "pools": [
                {
                    "rolls": 1,
                    "entries": [
                        {
                            "type": "minecraft:item",
                            "name": results[r][0],
                            "functions": [
                                {
                                    "function": "minecraft:set_name",
                                    "name": {
                                        "translate":f"item.wishlist.{r}",
                                        "italic": False
                                        }
                                    },
                                {
                                    "function": "minecraft:set_components",
                                    "components": {
                                        "minecraft:custom_model_data":{"floats":[results[r][2]]},
                                        "minecraft:custom_data":{"wishlist_recipe":r}
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },indent=2))

for r in vanilla_results:
    with open(f'data/better_recipes/loot_table/food/{r}.json', 'w') as f:
        f.write(json.dumps({
            "pools": [
                {
                    "rolls": 1,
                    "entries": [
                        {
                            "type": "minecraft:item",
                            "name": f'minecraft:{r}',
                            }
                        ]
                    }
                ]
            },indent=2))

# Generate model files for all results

for r in results:
    with open(f'assets/better_recipes/models/item/food/{r}.json','w') as f:
        f.write(json.dumps({
            "parent": "minecraft:item/generated",
            "textures": {
                "layer0": f"better_recipes:item/{results[r][1]}"
                }
            },indent=2))

# Generate language file for item names

def title(r):
    return re.sub("_"," ",r).title()

def name(r):
    if r in results:
        return f'<a href="#{r}">{re.sub("_"," ",r)}</a>'
    return re.sub("_", " ", re.sub("^.*:", "", r))

lang = {
        'block.wishlist.oven':'Oven',
        'block.wishlist.cooktop':'Cooktop',
        'block.wishlist.rack':'Rack',
        'block.wishlist.press':'Press',
        'block.wishlist.sink':'Sink',
        'block.wishlist.trash':'Trash',
        'block.wishlist.cask':'Cask',
        'block.wishlist.still':'Still',
        'item.wishlist.pan':'Pan',
        'item.wishlist.pot':'Pot',
        'item.wishlist.steamer':'Steamer',
        'better_recipes.empty':'Empty!',
        'better_recipes.bad_tool':'Hold %s to fetch that',
        'better_recipes.error':'Unknown error',
        }
for r in results:
    lang[f'item.wishlist.{r}'] = title(r)

with open('assets/better_recipes/lang/en_us.json', 'w') as f:
    f.write(json.dumps(lang, indent=2))

# Generate data tables for all possible transitions in a cooking station

with open("data/better_recipes/function/transitions.mcfunction", 'w') as f:
    f.write("data modify storage wishlist:better_recipes oven set value { ")
    for t in trans_oven:
        f.write("{}:{},".format(t, trans_oven[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes pan set value { ")
    for t in trans_pan:
        f.write("{}:{},".format(t, trans_pan[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes pot set value { ")
    for t in trans_pot:
        f.write("{}:{},".format(t, trans_pot[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes steamer set value { ")
    for t in trans_steamer:
        f.write("{}:{},".format(t, trans_steamer[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes press set value { ")
    for t in trans_press:
        f.write("{}:{},".format(t, trans_press[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes rack set value { ")
    for t in trans_rack:
        f.write("{}:{},".format(t, trans_rack[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes cask set value { ")
    for t in trans_cask:
        f.write("{}:{},".format(t, trans_cask[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes still set value { ")
    for t in trans_still:
        f.write("{}:{},".format(t, trans_still[t]))
    f.write("}\n\n")

    f.write('data modify storage wishlist:better_recipes fetch set value { ')
    for r in results:
        if (results[r][0] == 'minecraft:suspicious_stew'):
            f.write(f'{r}:"bowl",')
        elif (results[r][0] == 'minecraft:potion'):
            f.write(f'{r}:"bottle",')
        else:
            f.write(f'{r}:"nothing",')
    for r in vanilla_results:
        f.write(f'{r}:"nothing",')
    f.write("}\n\n")

# Generate vanilla recipes for custom data items
# These are the cooked items that can be constructed on a crafting table from
# vanilla minecraft objects

for r in recipes:
    result = results[r]

    with open("data/better_recipes/recipe/craft/{}.json".format(r), 'w') as f:
        f.write(json.dumps({
            "type": "minecraft:crafting_shapeless",
            "category": "food",
            "ingredients": recipes[r],
            "result":{
                "id":results[r][0],
                "count":1,
                "components":{
                    "minecraft:custom_model_data":{"floats":[results[r][2]]},
                    "minecraft:custom_name":f'{{"translate":"item.wishlist.{r}","italic":false}}',
                    "minecraft:custom_data":{"wishlist_recipe":r}
                    }
                }
            }, indent=2))

# Generate custom recipes for merging two custom items
# Even after 1.20.5, minecraft vanilla recipes can't handle custom data on
# inputs, so we have to use an advancement to see when the general recipe to
# merge two custom foods is used with specific data, and call a function as
# advancement reword.

for m in merge:
    ingredients = []

    a = merge[m][0]
    if a in results:
        ingredients.append({
            "components":{
                "minecraft:custom_data": {
                    "wishlist_recipe":a
                    }
                }
            })
        a = results[a][0][10:]

    b = merge[m][1]
    if b in results:
        ingredients.append({
            "components":{
                "minecraft:custom_data": {
                    "wishlist_recipe":b
                    }
                }
            })
        b = results[b][0][10:]

    bowls = 0
    if a == 'suspicious_stew': bowls = bowls + 1
    if b == 'suspicious_stew': bowls = bowls + 1
    if results[m][0] == 'minecraft:suspicious_stew': bowls = bowls - 1

    with open(f"data/better_recipes/recipe/merge/{a}__{b}.json", 'w') as f:
        f.write(json.dumps({
            "type":"minecraft:crafting_shapeless",
            "ingredients": [
                "minecraft:"+a,
                "minecraft:"+b
                ],
            "result" :{
                "id":"minecraft:suspicious_stew",
                "components": {
                    "minecraft:custom_model_data":{"floats":[300000]},
                    "minecraft:custom_name":json.dumps({
                        "translate":"item.wishlist.mess",
                        "italic":False
                        }),
                    "minecraft:custom_data":{ "wishlist_recipe":"mess" }
                    }
                }
            }, indent=2))

    with open(f"data/better_recipes/advancement/merge/{m}.json", 'w') as f:
        f.write(json.dumps({
            "criteria": {
                "example": {
                    "trigger": "minecraft:recipe_crafted",
                    "conditions": {
                        "recipe_id": f"better_recipes:merge/{a}__{b}",
                        "ingredients": ingredients
                        }
                    }
                },
            "rewards": {
                "function": f"better_recipes:merge/{m}"
                }
            }, indent=2))

    with open(f"data/better_recipes/function/merge/{m}.mcfunction", 'w') as f:
        f.write(f"advancement revoke @s only better_recipes:merge/{m}\n")
        f.write("clear @s minecraft:suspicious_stew[minecraft:custom_data={wishlist_recipe:mess}]\n")
        f.write(f"loot give @s loot better_recipes:food/{m}\n")
        if bowls > 0:
            f.write(f"give @s minecraft:bowl {bowls}\n")

# Generate knowledge book holding all the vanilla recipes

recipe_names = []
for r in recipes:
    recipe_names.append('better_recipes:craft/'+r)

with open("data/better_recipes/loot_table/book.json", 'w') as f:
    f.write(json.dumps({
  "pools": [
    {
      "rolls": 1,
      "entries": [
        {
          "type": "minecraft:item",
          "name": "minecraft:knowledge_book",
          "functions": [
            {
              "function": "minecraft:set_name",
              "name": {
                "translate":"Recipe Book",
                "italic": False
              }
            },
            {
              "function": "minecraft:set_components",
              "components": {
                  "minecraft:recipes":recipe_names,
              }
            }
          ]
        }
      ]
    }
  ]
}, indent=2))

# Generate documentation for all possible results

with open("results.html", 'w') as f:
    f.write("""<html>
<head>
<style>
ul {
  list-style-type:none;
}
</style>
</head>
<body>
<h1>Wishlist Cookbook</h1>
<h2>Cooking Methods</h2>
<ul>
<li><a name="craft">Crafting: combine items in a regular crafting table.</li>
<li><a name="bake">Baking: transform items in an oven.</li>
<li><a name="grill">Grilling: transform items in a cooktop with a pan.</li>
<li><a name="boil">Boiling: transform items in a cooktop with a pot.</li>
<li><a name="steam">Steaming: transform items in a cooktop with a steamer.</li>
<li><a name="rest">Curing: transform items in a resting surface.</li>
<li><a name="press">Pressing: transform items in a press.</li>
<li><a name="age">Aging: transform items in a cask.</li>
<li><a name="distill">Distilling: transform items in a still.</li>
</ul>
<h2>Recipes</h2>
<ul>
""")

    for r in sorted(results.keys()):
        f.write('<li>')
        f.write(f'<a name="{r}"/>')
        f.write(f'<img src="assets/better_recipes/textures/item/{results[r][1]}.png"/>')
        f.write(title(r))
        f.write('<ul>')
        if r in recipes:
            f.write('<li><a href="#craft">Crafting</a> recipe: {}</li>'.format(" + ".join(name(x) for x in recipes[r])))
        if r in merge:
            f.write('<li><a href="#craft">Crafting</a> recipe: {}</li>'.format(" + ".join(name(x) for x in merge[r])))
        for t in trans_oven:
            if trans_oven[t] == r:
                f.write(f'<li><a href="#bake">Baked</a> from: {name(t)}</li>')
        for t in trans_pan:
            if trans_pan[t] == r:
                f.write(f'<li><a href="#grill">Grilled</a> from: {name(t)}</li>')
        for t in trans_pot:
            if trans_pot[t] == r:
                f.write(f'<li><a href="#boil">Boiled</a> from: {name(t)}</li>')
        for t in trans_steamer:
            if trans_steamer[t] == r:
                f.write(f'<li><a href="#steam">Steamed</a> from: {name(t)}</li>')
        for t in trans_rack:
            if trans_rack[t] == r:
                f.write(f'<li><a href="#rest">Cured</a> from: {name(t)}</li>')
        for t in trans_press:
            if trans_press[t] == r:
                f.write(f'<li><a href="#press">Pressed</a> from: {name(t)}</li>')
        for t in trans_cask:
            if trans_cask[t] == r:
                f.write(f'<li><a href="#age">Aged</a> from: {name(t)}</li>')
        for t in trans_still:
            if trans_still[t] == r:
                f.write(f'<li><a href="#distill">Distilled</a> from: {name(t)}</li>')
        f.write('</ul></li>')

    f.write("</ul></body></html>")
