Millinery
=========

Provides entities that allow players to wear any item on their heads.

Any entity with the `wishlist.milliner` tag will replace the head armor of a
player with any item that player throws to them.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Overhead
--------

Updates every second:

* searches every entity in a 4-block radius around a player in search of a
  milliner:
    * searches every entity in a 2-block radius around a milliner in search of
      an item thrown by a player.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Ministry of Silly Hats: use a milliner to swap your head armor
