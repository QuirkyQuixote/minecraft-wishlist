# millinery:load
data modify storage wishlist:args all_chatter append value \
        "What is a ministry, and what do they do with silly hats?"
data modify storage wishlist:args all_chatter append value \
        "I tried to actually wear a silly hat and I forgot something"

function millinery:tick

# millinery:tick
schedule function millinery:tick 1s

execute at @a \
        as @e[tag=wishlist.milliner,distance=..4] \
        run function millinery:update

# millinery:update
execute at @s \
        as @e[type=item,distance=..2] \
        if data entity @s Thrower \
        run function millinery:swap_hat

# millinery:swap_hat
setblock 0 0 0 minecraft:barrel
data modify block 0 0 0 Items append from entity @s Item
data modify block 0 0 0 Items[0].Count set value 1b
execute on origin run function millinery:swap_hat_2
execute on origin run function millinery:swap_hat_1
data modify entity @s Thrower set value [I;0,0,0,0]
execute unless score done wishlist.vars matches 0 \
        run item modify entity @s container.0 wishlist:consume_one
setblock 0 0 0 minecraft:air

# millinery:swap_hat_1
scoreboard players set done wishlist.vars 0
execute if items entity @s armor.head * run return fail
item replace entity @s armor.head from block 0 0 0 container.0
advancement grant @s only wishlist:swap_hat
particle minecraft:happy_villager ~ ~1 ~ 0.2 0.6 0.2 1 10 normal
scoreboard players set done wishlist.vars 1

# millinery:swap_hat_2
execute unless items entity @s armor.head * run return fail
title @s actionbar {"translate":"wishlist.millinery.already_hatted"}
particle minecraft:angry_villager ~ ~1.5 ~ 0.2 0.2 0.2 1 1 normal

