# try_begin_barter.mcfunction - attempt to start bartering

# to barter, there must be an emerald under the pillager
execute at @s \
        if entity @e[type=minecraft:item,nbt={Item:{id:"minecraft:emerald"}},distance=..1.5] \
        run function pillager_bartering:begin_barter
