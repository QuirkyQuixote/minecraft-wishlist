# begin_barter.mcfunction - take emerald from stack and hold it in hand

execute as @e[type=item,nbt={Item:{id:"minecraft:emerald"}},sort=nearest,limit=1] \
        run function wishlist:decrement_item_entity_count
item replace entity @s weapon.mainhand with minecraft:emerald 1
scoreboard players set @s pb_appraise 6

