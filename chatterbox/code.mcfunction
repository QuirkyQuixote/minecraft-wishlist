# chatterbox:load
scoreboard objectives add wishlist.chatterbox.classic_combat trigger
scoreboard objectives add wishlist.fake dummy
function chatterbox:tick_1s
function chatterbox:tick_1t

# chatterbox:tick_1t
schedule function chatterbox:tick_1t 1t
scoreboard players reset * wishlist.fake
tag @e[tag=wishlist.fake] add wishlist.fake.unused
tag @a remove wishlist.chatty
tag @a[tag=wishlist.sneaking,predicate=!wishlist:armed] add wishlist.chatty
tag @a[predicate=chatterbox:chatty] add wishlist.chatty
execute as @a[tag=wishlist.chatty] \
        at @s anchored eyes \
        positioned ^ ^ ^ \
        run function chatterbox:fake
execute at @a run tp @e[tag=wishlist.fake.unused,distance=..8] 0 1 0

# chatterbox:fake
tag @s add wishlist.match
scoreboard players operation player wishlist.vars = @s wishlist.playerId
scoreboard players set steps wishlist.vars 45
scoreboard players set go wishlist.vars 0
function chatterbox:fake_1
execute unless score go wishlist.vars matches 0 run function chatterbox:show_fake
tag @s remove wishlist.match

# chatterbox:fake_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ #wishlist:non_full run return fail
execute as @e[type=#wishlist:alive,tag=!wishlist.match,dx=0,dy=0,dz=0] \
        unless entity @s[gamemode=spectator] \
        if function chatterbox:fake_2 \
        run return 1
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run return run function chatterbox:fake_1

# chatterbox:fake_2
execute positioned ~-1 ~-1 ~-1 unless entity @s[dx=0,dy=0,dz=0] run return fail
scoreboard players operation @s wishlist.fake = player wishlist.vars
function wishlist:get_entity_type {dst:"storage wishlist:args target_type"}
particle minecraft:dust{color:[1f,1f,1f],scale:1f} ~ ~ ~ 0 0 0 0.1 1 normal @a[tag=wishlist.match]
advancement grant @a[tag=wishlist.match] only wishlist:meet_chatterbox
scoreboard players set go wishlist.vars 1
return 1

# chatterbox:show_fake
execute unless entity @e[tag=wishlist.fake.unused] \
        run summon minecraft:interaction 0 0 0 {Tags:[wishlist.fake.unused]}
execute as @e[tag=wishlist.fake.unused,limit=1] run return run function chatterbox:show_fake_1

# chatterbox:show_fake_1
data merge entity @s {Tags:[wishlist.fake],width:0.1,height:0.1}
data modify entity @s Tags append from storage wishlist:args target_type
execute positioned ^ ^ ^0.1 run tp @s ~ ~-0.05 ~

# chatterbox:tick_1s
schedule function chatterbox:tick_1s 1s
execute at @s run kill @e[type=minecraft:interaction,tag=wishlist.chatterbox,distance=..8]

# chatterbox:configure
tag @s add wishlist.match
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
data modify storage wishlist:args chatter set from entity @s \
        SelectedItem.components.minecraft:writable_book_content.pages[0].raw
execute positioned ^ ^ ^2 \
        as @e[type=#wishlist:alive,distance=..4] \
        if score @s wishlist.fake = playerId wishlist.vars \
        run function chatterbox:configure_1
tag @s remove wishlist.match

# chatterbox:configure_1
function wishlist:recall
execute store result score employer wishlist.vars run data get storage wishlist:args memories.employer
execute if score employer wishlist.vars = playerId wishlist.vars run function chatterbox:configure_2
execute unless score employer wishlist.vars = playerId wishlist.vars run title @a[tag=wishlist.match] actionbar { "translate":"chatterbox.bad_employer", "with":[{"selector":"@s"}] }

# chatterbox:configure_2
data modify storage wishlist:args memories.chatter set from storage wishlist:args chatter
function wishlist:memorize
title @a[tag=wishlist.match] actionbar { "translate":"chatterbox.chatter_updated", "with":[{"selector":"@s"}] }

# chatterbox:interact
advancement revoke @s only chatterbox:interact
execute if entity @s[nbt={SelectedItem:{id:"minecraft:writable_book"}}] \
        run return run function chatterbox:configure
tag @s add wishlist.match
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
execute positioned ^ ^ ^2 \
        as @e[type=#wishlist:alive,distance=..4] \
        if score @s wishlist.fake = playerId wishlist.vars \
        run function chatterbox:interact_1
data modify storage wishlist:args reason set value talk_with_npc
function better_gossip:do_good
advancement grant @s only wishlist:chat_with_mob
tag @s remove wishlist.match
tellraw @a[tag=wishlist.debug] { \
        "translate":"[chatterbox] override interaction", \
        "color":"yellow" \
        }

# chatterbox:interact_1
execute if entity @s[type=player] run return run function chatterbox:interact_2
function wishlist:recall
execute unless data storage wishlist:args memories.chatter \
        run data modify storage wishlist:args memories.chatter set value '{"text":"..."}'
title @a[tag=wishlist.match] actionbar { \
        "translate":"chatterbox.chat", \
        "with":[ \
                {"selector":"@s"}, \
                {"nbt":"memories.chatter","storage":"wishlist:args","interpret":true} \
                ] \
        }

# chatterbox:interact_2
title @s actionbar {"translate":"chatterbox.be_booped","with":[{"selector":"@a[tag=wishlist.match]"}]}
title @a[tag=wishlist.match] actionbar {"translate":"chatterbox.boop","with":[{"selector":"@s"}]}

# chatterbox:pet
advancement revoke @s only chatterbox:pet
tag @s add wishlist.match
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=#wishlist:alive,distance=..4] \
        if score @s wishlist.fake = playerId wishlist.vars \
        run function chatterbox:pet_1
data modify storage wishlist:args reason set value headpat_npc
function better_gossip:do_good
advancement grant @s only wishlist:pet_mob
tag @s remove wishlist.match
tellraw @a[tag=wishlist.debug] { \
        "translate":"[chatterbox] override attack", \
        "color":"yellow" \
        }

# chatterbox:pet_1
execute at @s anchored eyes positioned ^ ^ ^ run particle minecraft:heart ~ ~ ~ 0.2 0.2 0.2 1 3 normal

execute if entity @s[type=armadillo] run playsound minecraft:entity.armadillo.ambient neutral @a
execute if entity @s[type=axolotl] run playsound minecraft:entity.axolotl.ambient neutral @a
execute if entity @s[type=bat] run playsound minecraft:entity.bat.ambient neutral @a
execute if entity @s[type=cat] run playsound minecraft:entity.cat.ambient neutral @a
execute if entity @s[type=chicken] run playsound minecraft:entity.chicken.ambient neutral @a
execute if entity @s[type=cod] run playsound minecraft:entity.cod.ambient neutral @a
execute if entity @s[type=cow] run playsound minecraft:entity.cow.ambient neutral @a
execute if entity @s[type=donkey] run playsound minecraft:entity.donkey.ambient neutral @a
execute if entity @s[type=fox] run playsound minecraft:entity.fox.ambient neutral @a
execute if entity @s[type=glow_squid] run playsound minecraft:entity.glow_squid.ambient neutral @a
execute if entity @s[type=horse] run playsound minecraft:entity.horse.ambient neutral @a
execute if entity @s[type=mooshroom] run playsound minecraft:entity.mooshroom.ambient neutral @a
execute if entity @s[type=mule] run playsound minecraft:entity.mule.ambient neutral @a
execute if entity @s[type=ocelot] run playsound minecraft:entity.ocelot.ambient neutral @a
execute if entity @s[type=parrot] run playsound minecraft:entity.parrot.ambient neutral @a
execute if entity @s[type=pig] run playsound minecraft:entity.pig.ambient neutral @a
execute if entity @s[type=pufferfish] run playsound minecraft:entity.pufferfish.ambient neutral @a
execute if entity @s[type=rabbit] run playsound minecraft:entity.rabbit.ambient neutral @a
execute if entity @s[type=salmon] run playsound minecraft:entity.salmon.ambient neutral @a
execute if entity @s[type=sheep] run playsound minecraft:entity.sheep.ambient neutral @a
execute if entity @s[type=skeleton_horse] run playsound minecraft:entity.skeleton_horse.ambient neutral @a
execute if entity @s[type=snow_golem] run playsound minecraft:entity.snow_golem.ambient neutral @a
execute if entity @s[type=squid] run playsound minecraft:entity.squid.ambient neutral @a
execute if entity @s[type=strider] run playsound minecraft:entity.strider.ambient neutral @a
execute if entity @s[type=tropical_fish] run playsound minecraft:entity.tropical_fish.ambient neutral @a
execute if entity @s[type=turtle] run playsound minecraft:entity.turtle.ambient neutral @a
execute if entity @s[type=villager] run playsound minecraft:entity.villager.ambient neutral @a
execute if entity @s[type=wandering_trader] run playsound minecraft:entity.wandering_trader.ambient neutral @a

execute if entity @s[type=bee] run playsound minecraft:entity.bee.ambient neutral @a
execute if entity @s[type=cave_spider] run playsound minecraft:entity.cave_spider.ambient neutral @a
execute if entity @s[type=dolphin] run playsound minecraft:entity.dolphin.ambient neutral @a
execute if entity @s[type=enderman] run playsound minecraft:entity.enderman.ambient neutral @a
execute if entity @s[type=goat] run playsound minecraft:entity.goat.ambient neutral @a
execute if entity @s[type=iron_golem] run playsound minecraft:entity.iron_golem.ambient neutral @a
execute if entity @s[type=llama] run playsound minecraft:entity.llama.ambient neutral @a
execute if entity @s[type=panda] run playsound minecraft:entity.panda.ambient neutral @a
execute if entity @s[type=piglin] run playsound minecraft:entity.piglin.ambient neutral @a
execute if entity @s[type=polar_bear] run playsound minecraft:entity.polar_bear.ambient neutral @a
execute if entity @s[type=spider] run playsound minecraft:entity.spider.ambient neutral @a
execute if entity @s[type=trader_llama] run playsound minecraft:entity.trader_llama.ambient neutral @a
execute if entity @s[type=wolf] run playsound minecraft:entity.wolf.ambient neutral @a
execute if entity @s[type=zombified_piglin] run playsound minecraft:entity.zombified_piglin.ambient neutral @a

execute if entity @s[type=blaze] run playsound minecraft:entity.blaze.ambient neutral @a
execute if entity @s[type=creeper] run playsound minecraft:entity.creeper.ambient neutral @a
execute if entity @s[type=drowned] run playsound minecraft:entity.drowned.ambient neutral @a
execute if entity @s[type=elder_guardian] run playsound minecraft:entity.elder_guardian.ambient neutral @a
execute if entity @s[type=endermite] run playsound minecraft:entity.endermite.ambient neutral @a
execute if entity @s[type=evoker] run playsound minecraft:entity.evoker.ambient neutral @a
execute if entity @s[type=ghast] run playsound minecraft:entity.ghast.ambient neutral @a
execute if entity @s[type=guardian] run playsound minecraft:entity.guardian.ambient neutral @a
execute if entity @s[type=hoglin] run playsound minecraft:entity.hoglin.ambient neutral @a
execute if entity @s[type=husk] run playsound minecraft:entity.husk.ambient neutral @a
execute if entity @s[type=magma_cube] run playsound minecraft:entity.magma_cube.ambient neutral @a
execute if entity @s[type=phantom] run playsound minecraft:entity.phantom.ambient neutral @a
execute if entity @s[type=piglin_brute] run playsound minecraft:entity.piglin_brute.ambient neutral @a
execute if entity @s[type=pillager] run playsound minecraft:entity.pillager.ambient neutral @a
execute if entity @s[type=ravager] run playsound minecraft:entity.ravager.ambient neutral @a
execute if entity @s[type=shulker] run playsound minecraft:entity.shulker.ambient neutral @a
execute if entity @s[type=silverfish] run playsound minecraft:entity.silverfish.ambient neutral @a
execute if entity @s[type=skeleton] run playsound minecraft:entity.skeleton.ambient neutral @a
execute if entity @s[type=slime] run playsound minecraft:entity.slime.ambient neutral @a
execute if entity @s[type=stray] run playsound minecraft:entity.stray.ambient neutral @a
execute if entity @s[type=vex] run playsound minecraft:entity.vex.ambient neutral @a
execute if entity @s[type=vindicator] run playsound minecraft:entity.vindicator.ambient neutral @a
execute if entity @s[type=witch] run playsound minecraft:entity.witch.ambient neutral @a
execute if entity @s[type=wither_skeleton] run playsound minecraft:entity.wither_skeleton.ambient neutral @a
execute if entity @s[type=zoglin] run playsound minecraft:entity.zoglin.ambient neutral @a
execute if entity @s[type=zombie] run playsound minecraft:entity.zombie.ambient neutral @a
execute if entity @s[type=zombie_villager] run playsound minecraft:entity.zombie_villager.ambient neutral @a

execute if entity @s[type=player] run title @s actionbar { \
        "translate":"chatterbox.be_patted", \
        "with":[{"selector":"@a[tag=wishlist.match]"}] \
        }

# chatterbox:prune_chunk
kill @e[type=minecraft:interaction,tag=wishlist.chatterbox,dx=16,dy=256,dz=16]

