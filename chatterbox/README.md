Chatterbox
==========

Provide more ways to interact with mobs of all types, beyond the default
hit/feed/trade.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Crouching while not holding a weapon, then looking at any mob generates a small
particle effect where the cursor touches the entity; at that point, the default
interaction is overriden by chatterbox.

* right mouse button will print a mob-specific chatter line or just "..."
* left mouse click will headpat the mob

### Mob chatter

Mob chatter is stored in the `wishlist_chatter` tag of whichever item they wear
on their head and shown in the actionbar as:

```
<NPC name> chatter
```

The chatter can be changed by the employer of the NPC by using an unsigned
book on them. The entire text of the first page will be set as the NPC chatter.
Note that the page must contain valid JSON text. For simple sentences you only
have to enclose the text in quotes.

When left-clicked the NPC will emit a generic sound that
depends on their type and show three floating hearts above their head.

Overhead
--------

This one is a little heavy; every tick:

Searches the server for interactions tagged `wishlist.fake` and tags them
`wishlist.fake.unused`

For every crouching unarmed player if they are looking at an entity of type
`#wishlist:alive`, takes one `wishlist.fake.unused` interaction and places it
in front of the player to override their actions, then removes
`wishlist.fake.unused`; if no such interaction exists one is
created.

Moves every interaction tagged `wishlist.fake.unused` to 0 0 0.

Uses an advancement to check if a player interacts with `wishlist.fake`, then
proceeds from there.

Known Issues
------------

Moving at high speeds and clicking on entities can trigger their default
behavior before the fake interaction can be set up.

The raycast algorithm that determines if an entity is being looked at can fail
around weirdly shaped blocks; use the particle effect to actually determine if
you can chatter/headpat with the mob.

Server/client sync issues may result in the fake entity being misplaced enough
that the player's line of sight does not pass through it.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Voiceless no More: crouch while looking at a chatty mob
    * Stop and Listen: right-click on a chatty mob to read their chatter
        * Seasoned Diplomat: chat with all sentient NPCs
    * Headpats!: left-click on a chatty mob to pet the it
        * Friend of all Living Things: pet every mob in the game.

