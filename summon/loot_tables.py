#!/bin/python3

# generate loot tables to obtain npcs

import json
import re

pattern = re.compile(r'\s+')

villager_types = {
        "nitwit":"Nitwit",
        "armorer":"Armorer",
        "butcher":"Butcher",
        "cartographer":"Cartographer",
        "cleric":"Cleric",
        "farmer":"Farmer",
        "fisherman":"Fisher",
        "fletcher":"Fletcher",
        "leatherworker":"Leatherworker",
        "librarian":"Librarian",
        "mason":"Mason",
        "shepherd":"Shepherd",
        "toolsmith":"Toolsmith",
        "weaponsmith":"Weaponsmith"
        }

illager_types = {
        "pillager":"Pillager",
        "vindicator":"Vindicator",
        "evoker":"Evoker",
        "witch":"Witch",
        "illusioner":"Illusioner",
        }

piglin_types = {
        "piglin":"Piglin",
        "piglin_brute":"Piglin Brute",
        }

cat_types = {
        "white":"White",
        "black":"Tuxedo",
        "red":"Ginger",
        "siamese":"Siamese",
        "british_shorthair":"British Shorthair",
        "calico":"Calico",
        "persian":"Persian",
        "ragdoll":"Ragdoll",
        "tabby":"Tabby",
        "all_black":"Black",
        "jellie":"Jellie",
        }

wolf_types = {
        "pale":"Pale",
        "ashen":"Ashen",
        "black":"Black",
        "chestnut":"Chestnut",
        "rusty":"Rusty",
        "snowy":"Snowy",
        "spotted":"Spotted",
        "striped":"Striped",
        "woods":"Woods",
        }

rabbit_types = {
        "brown":["Brown",0],
        "white":["White",1],
        "black":["Black",2],
        "white_splotched":["Black and White",3],
        "gold":["Gold",4],
        "salt":["Salt and Pepper",5],
        "evil":["The Killer",99],
        }

def make_loot_table(fname, id, dname, data):
    nbt = {
        "minecraft:custom_model_data":{"floats":[271870]},
        "minecraft:entity_data":{
            "id":"minecraft:item_frame",
            "Tags":["wishlist.summon"],
            "Item":{
                "id":"minecraft:item_frame",
                "count":1,
                "components":{
                    "minecraft:custom_data":{
                        "id":f"minecraft:{id}",
                        "data":{
                            "NoAI":True,
                            "Invulnerable":True,
                            "Tags":["wishlist.look_alive"],
                            "ArmorItems":[
                                {},
                                {},
                                {},
                                {
                                    "id":"minecraft:stone_button",
                                    "count":1,
                                    "components":{
                                        "minecraft:custom_model_data":{"floats":[999999]}
                                        }
                                    }
                                ],
                            "ArmorDropChances":[0.0,0.0,0.0,0.0]
                            } | data
                        }
                    }
                }
            }
        }

    with open('data/summon/loot_table/summon_'+fname+'.json', 'w') as f:
        f.write(json.dumps({
            "pools": [
                {
                    "rolls": 1,
                    "entries": [
                        {
                            "type": "minecraft:item",
                            "name": "minecraft:item_frame",
                            "functions": [
                                {
                                    "function": "minecraft:set_name",
                                    "name": {
                                        "translate":"Summon "+dname,
                                        "italic": False
                                        }
                                    },
                                {
                                    "function": "minecraft:set_lore",
                                    "mode": "replace_all",
                                    "lore": [
                                        {
                                            "text":"minecraft:"+id,
                                            "italic":False,
                                            "color":"gray"
                                            }
                                        ]
                                    },
                                {
                                    "function": "minecraft:set_components",
                                    "components":nbt
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }, indent=2))

for i in villager_types:
    make_loot_table(i, "villager", villager_types[i], {
        "VillagerData":{
            "Offers":{
                "Recipes":[]
                },
            "profession":f"minecraft:{i}"
            }
        })

for i in illager_types:
    make_loot_table(i, i, illager_types[i], {
        "PersistenceRequired":True,
        "CanJoinRaids":False
        })

for i in piglin_types:
    make_loot_table(i, i, piglin_types[i], {
        "PersistenceRequired":True,
        "IsImmuneToZombification":True
        })

make_loot_table("villager", "villager", "Villager", {
    "VillagerData":{
        "Offers":{
            "Recipes":[]
            },
        "profession":"minecraft:none"
        }
    })

make_loot_table("enderman", "enderman", "Enderfolk", {
    "PersistenceRequired":True,
    })

make_loot_table("villager_child", "villager", "Villager Child", {
    "Age":-2000000000
    })

make_loot_table("piglin_child", "piglin", "Piglin Child", {
    "PersistenceRequired":True,
    "IsImmuneToZombification":True,
    "IsBaby":True
    })

for i in cat_types:
    make_loot_table(i + "_cat", "cat", cat_types[i] + " Cat", {
        "PersistenceRequired":True,
        "variant":"minecraft:" + i
        })

for i in wolf_types:
    make_loot_table(i + "_wolf", "wolf", wolf_types[i] + " Wolf", {
        "PersistenceRequired":True,
        "variant":"minecraft:" + i
        })

for i in rabbit_types:
    make_loot_table(i + "_rabbit", "rabbit", rabbit_types[i][0] + " Rabbit", {
        "PersistenceRequired":True,
        "RabbitType":rabbit_types[i][1]
        })

make_loot_table("camel", "camel", "Camel", {})
make_loot_table("chicken", "chicken", "Chicken", {})
make_loot_table("cow", "cow", "Cow", {})
make_loot_table("donkey", "donkey", "Donkey", {})
make_loot_table("horse", "horse", "Horse", {})
make_loot_table("llama", "llama", "Llama", {})
make_loot_table("mule", "mule", "Mule", {})
make_loot_table("pig", "pig", "Pig", {})

pools = []
for i in cat_types:
    pools.append({
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:loot_table",
                    "value": f"summon:summon_{i}_cat"
                    }
                ]
            })
    with open('data/summon/loot_table/summon_all_cats.json', 'w') as f:
        f.write(json.dumps({
            "pools": pools
            }, indent=2))

pools = []
for i in wolf_types:
    pools.append({
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:loot_table",
                    "value": f"summon:summon_{i}_wolf"
                    }
                ]
            })
    with open('data/summon/loot_table/summon_all_wolves.json', 'w') as f:
        f.write(json.dumps({
            "pools": pools
            }, indent=2))

pools = []
for i in rabbit_types:
    pools.append({
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:loot_table",
                    "value": f"summon:summon_{i}_rabbit"
                    }
                ]
            })
    with open('data/summon/loot_table/summon_all_rabbits.json', 'w') as f:
        f.write(json.dumps({
            "pools": pools
            }, indent=2))
