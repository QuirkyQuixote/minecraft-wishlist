Summon
======

Adds a framework to summon arbitrary entities that can only be dismissed by
their summoner (employer).

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for player UI
* Requires [Wishlist Resources](../resources) for media and text translations.
* Recommends [Look Alive](../look_alive) for mob animation

Usage
-----

The datapack provides two custom items:

### ![contract] Summon Hire

This is as similar to a spawn egg as possible, with some differences:

* it can spawn *any* entity type in the game,
* it will not change the entity type spawned by a spawner,
* for live entities (that can wear a helmet) it will attempt to give them a
  pseudo-brain and store the id of the summoner there.

There are two ways to obtain these contracts:

* as as result of dismissing a previously summoned hire
* as a prewritten loot table from either this datapack or some other

This datapack in particular provides the following hiring contracts that will
summon persistent, invulnerable, AI-less mobs tagged
[`wishlist.look_alive`](../look_alive):

* `summon:summon_armorer`
* `summon:summon_butcher`
* `summon:summon_cartographer`
* `summon:summon_cleric`
* `summon:summon_enderman`
* `summon:summon_evoker`
* `summon:summon_farmer`
* `summon:summon_fisherman`
* `summon:summon_fletcher`
* `summon:summon_illusioner`
* `summon:summon_leatherworker`
* `summon:summon_librarian`
* `summon:summon_mason`
* `summon:summon_nitwit`
* `summon:summon_pillager`
* `summon:summon_piglin`
* `summon:summon_piglin_brute`
* `summon:summon_piglin_child`
* `summon:summon_shepherd`
* `summon:summon_toolsmith`
* `summon:summon_villager`
* `summon:summon_villager_child`
* `summon:summon_vindicator`
* `summon:summon_weaponsmith`
* `summon:summon_witch`

### ![wand] Dismiss Hire

This wand will check the entities in front of the player and try to find the
closest one that belongs to them, then will dismiss that entity and give the
player back their hiring contract.

Overhead
--------

Uses an advancement to check if a player placed an item frame with the right
data.

Uses [Magic](../magic) to detect if the player used a wand to trigger the
associated function.

Known Issues
------------

If a summoned entity can't be bound to their employer, no player will be able
to use ![wand] Dismiss Hire on them.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

[contract]: summon/assets/summon/textures/item/contract.png ""
[wand]: magic/assets/magic/textures/item/wand.png ""
