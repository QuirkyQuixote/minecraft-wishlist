
# summon:dismiss
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
scoreboard players set done wishlist.vars -1
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=!minecraft:player,type=!minecraft:item,distance=..2,sort=nearest] \
        run function summon:dismiss_1
execute if score done wishlist.vars matches -1 \
        run title @s actionbar {"translate":"summon.no_entities"}
execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"translate":"summon.bad_employer"}

# summon:dismiss_1
execute if score done wishlist.vars matches 1 run return fail
scoreboard players set done wishlist.vars 0

function wishlist:recall
scoreboard players set employer wishlist.vars -1
execute store result score employer wishlist.vars \
        run data get storage wishlist:args memories.employer
execute unless score employer wishlist.vars = playerId wishlist.vars \
        run return fail

data modify storage wishlist:args summon set value {}
execute at @s run function wishlist:get_entity_type {dst:"storage wishlist:args summon.id"}
data modify storage wishlist:args summon.data set from entity @s {}
data remove storage wishlist:args summon.data.UUID
data remove storage wishlist:args summon.data.Pos
data remove storage wishlist:args summon.data.Motion
data remove storage wishlist:args summon.data.Rotation
function summon:give with storage wishlist:args summon

function wishlist:dismiss

scoreboard players set done wishlist.vars 1

# summon:give
$summon minecraft:item ~ ~ ~ { \
  Item: { \
    id:"minecraft:item_frame", \
    count:1, \
    components: { \
      "minecraft:custom_name": '{"text":"Summon Hire","italic":false}', \
      "minecraft:lore": ['{"text":"$(id)","italic":false,"color":"gray"}'], \
      "minecraft:custom_model_data":{"floats":[271870]}, \
      "minecraft:entity_data":{ \
        id:"minecraft:item_frame", \
        Tags:[wishlist.summon], \
        Item: { \
          id:"minecraft:stick", \
          components: { \
            "minecraft:custom_data": { \
              "id": "$(id)", \
              "data": $(data) \
            } \
          } \
        } \
      } \
    } \
  } \
}

# summon:lore
data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.id set from storage wishlist:args summon.id
data modify storage wishlist:args args.name set from storage wishlist:args summon.data.CustomName
function summon:lore_1 with storage wishlist:args args

# summon:lore_1
$data modify storage wishlist:args lore set value [ \
        '{"text":"$(id) named ","extra":[$(name)],"color":"gray","italic":false}' \
]

# summon:summon
advancement revoke @s only summon:summon
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
execute as @e[type=item_frame,tag=wishlist.summon,distance=..8] \
        at @s \
        align xyz \
        positioned ~0.5 ~ ~0.5 \
        run function summon:summon_1

# summon:summon_1
data modify storage wishlist:args args set from entity @s Item.components.minecraft:custom_data
execute unless data storage wishlist:args args.data.ArmorItems \
        run data modify storage wishlist:args args.data.ArmorItems \
        set value [{},{},{},{}]
execute unless data storage wishlist:args args.data.ArmorItems[3].id \
        run data modify storage wishlist:args args.data.ArmorItems[3] \
        set value {id:"minecraft:stone_button",count:1b,components:{"minecraft:custom_model_data":{"floats":[999999]}}}
execute store result storage wishlist:args args.data.ArmorItems[3].components.minecraft:custom_data.employer int 1 \
        run scoreboard players get playerId wishlist.vars
function summon:summon_2 with storage wishlist:args args
kill @s

# summon:summon_2
$summon $(id) ~ ~ ~ $(data)
