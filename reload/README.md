Reload
======

Adds just one function "wishlist:reload" that disables and enables the included
datapack, but because how minecraft handles datapack, that reloads *all* loaded
datapacks instead.

Requirements
------------

Usage
-----

run ``function wishlist:reload``

Overhead
--------

None

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

