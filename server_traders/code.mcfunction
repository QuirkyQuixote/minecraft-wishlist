
# server_traders:load

data modify storage wishlist:args all_chatter append value \
        "All those traders selling from the Server's Vault got it made"
data modify storage wishlist:args all_chatter append value \
        "Nah, I think that every single thing should be on a limited supply"

data modify storage wishlist:args empty_recipe set value { \
        maxUses:2147483647, \
        xp:0, \
        rewardExp:0b, \
        "Paper.IgnoreDiscounts":1b \
        }

# server_traders:sync_money

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[0]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[0].uses set value 0
execute store result entity @s Offers.Recipes[0].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[1]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[1].uses set value 0
execute store result entity @s Offers.Recipes[1].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[2]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[2].uses set value 0
execute store result entity @s Offers.Recipes[2].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[3]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[3].uses set value 0
execute store result entity @s Offers.Recipes[3].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[4]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[4].uses set value 0
execute store result entity @s Offers.Recipes[4].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[5]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[5].uses set value 0
execute store result entity @s Offers.Recipes[5].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[6]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[6].uses set value 0
execute store result entity @s Offers.Recipes[6].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[7]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[7].uses set value 0
execute store result entity @s Offers.Recipes[7].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars

data modify storage wishlist:args recipe set from entity @s Offers.Recipes[8]
function server_traders:sync_money_1
data modify entity @s Offers.Recipes[8].uses set value 0
execute store result entity @s Offers.Recipes[8].maxUses int 1 \
        run scoreboard players get maxUses wishlist.vars


# server_traders:sync_money_1

scoreboard players set maxUses wishlist.vars 2000000000

execute store result score uses wishlist.vars \
        run data get storage wishlist:args recipe.uses
execute unless score uses wishlist.vars matches 0 \
        run function server_traders:sync_money_2
execute if data storage wishlist:args recipe.sell.tag.wishlist.currency \
        run function server_traders:sync_money_6


# server_traders:sync_money_2

execute if data storage wishlist:args recipe.buy.tag.wishlist.currency \
        run function server_traders:sync_money_3
execute if data storage wishlist:args recipe.buyB.tag.wishlist.currency \
        run function server_traders:sync_money_4
execute if data storage wishlist:args recipe.sell.tag.wishlist.currency \
        run function server_traders:sync_money_5


# server_traders:sync_money_3

execute store result score amount wishlist.vars \
        run data get storage wishlist:args recipe.buy.Count
execute store result score worth wishlist.vars \
        run data get storage wishlist:args recipe.buy.tag.wishlist.currency
scoreboard players operation amount wishlist.vars *= uses wishlist.vars
scoreboard players operation amount wishlist.vars *= worth wishlist.vars
scoreboard players operation vault wishlist.vars += amount wishlist.vars

# server_traders:sync_money_4

execute store result score amount wishlist.vars \
        run data get storage wishlist:args recipe.buyB.Count
execute store result score worth wishlist.vars \
        run data get storage wishlist:args recipe.buyB.tag.wishlist.currency
scoreboard players operation amount wishlist.vars *= uses wishlist.vars
scoreboard players operation amount wishlist.vars *= worth wishlist.vars
scoreboard players operation vault wishlist.vars += amount wishlist.vars

# server_traders:sync_money_5

execute store result score amount wishlist.vars \
        run data get storage wishlist:args recipe.sell.Count
execute store result score worth wishlist.vars \
        run data get storage wishlist:args recipe.sell.tag.wishlist.currency
scoreboard players operation amount wishlist.vars *= uses wishlist.vars
scoreboard players operation amount wishlist.vars *= worth wishlist.vars
scoreboard players operation vault wishlist.vars -= amount wishlist.vars

# server_traders:sync_money_6

execute store result score count wishlist.vars \
        run data get storage wishlist:args recipe.sell.Count
execute store result score worth wishlist.vars \
        run data get storage wishlist:args recipe.sell.tag.wishlist.currency
scoreboard players operation maxUses wishlist.vars = vault wishlist.vars
scoreboard players operation maxUses wishlist.vars /= count wishlist.vars
scoreboard players operation maxUses wishlist.vars /= worth wishlist.vars


# server_traders:tick

execute if entity @s[gamemode=creative] \
        as @e[type=#wishlist:trader,tag=wishlist.server_trader,distance=..4] \
        at @s \
        if block ~ ~-1 ~ minecraft:barrel \
        run function server_traders:update_trader


# server_traders:trade_with_server

execute as @e[type=#wishlist:trader,tag=wishlist.server_trader,distance=..6] \
        run function server_traders:sync_money

advancement revoke @s only server_traders:trade_with_server

# server_traders:update_trader

function server_traders:sync_money

data modify storage wishlist:args recipes set value []

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:0b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:9b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:18b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:1b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:10b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:19b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:2b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:11b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:20b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:3b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:12b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:21b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:4b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:13b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:22b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:5b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:14b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:23b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:6b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:15b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:24b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:7b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:16b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:25b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy set from block ~ ~-1 ~ Items[{Slot:8b}]
data modify storage wishlist:args recipe.buyB set from block ~ ~-1 ~ Items[{Slot:17b}]
data modify storage wishlist:args recipe.sell set from block ~ ~-1 ~ Items[{Slot:26b}]
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify entity @s Offers.Recipes set from storage wishlist:args recipes

particle minecraft:happy_villager ~ ~1 ~ 0.2 1 0.2 0.5 10

function server_traders:sync_money
