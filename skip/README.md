Skip
====

Utilities to "skip" (teleport) across the world.

Unlike other datapacks and mods that achieve this through commands, Skip adds
actual useable items to the game that do the teleporting in different ways.

Skipping players produce particles and sound just like enderfolk.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for magic tool framework
* Requires [Custom Blocks](../custom_blocks) for the map core custom block
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The skip datapack provides four new items for the magic datapack and one custom
block:

### ![wand] Skip Home

* Teleports[\[1\]](#safe-skip-locations) the player to their current respawn point.
* Can be obtained with the loot table `skip:skip_home`.

### ![wand] Skip Back

* Teleports[\[1\]](#safe-skip-locations) the player to the last place they
  teleported from or the last place
  they died on, whichever was most recent.
* Can be obtained with the loot table `skip:skip_back`.

### ![wand] Skip Here

* Generates a ![ticket] ticket that will teleport[\[1\]](#safe-skip-locations)
  any player to the place where it was generated.
* Can be obtained with the loot table `skip:skip_coords`.

### ![wand] Skip to Me

* Generates a ![ticket] ticket that will teleport[\[1\]](#safe-skip-locations)
  any player to the player who generated it.
* Can be obtained with the loot table `skip:skip_player`.
* Skip will fail if the player is offline or [hidden](../noise_box).

### Map Core

* Custom block obtainable with `function custom_blocks:give {id:map_core}`
* When placed, it makes the surrounding area into a map hall.
  * Default radius of the map hall is 16 blocks from the core
    * This is not configurable (yet).
  * Default zoom of the maps expected by the hall is level 2
    * You can configure the actual zoom by using the map core block
* The map corresponding to the region where the core is should be in the same
  xz coordinates as the map core.
* Crouching and jumping on the map (landing on it while crouching, actually)
  skips the player to the place on the world corresponding to the map location.

### Safe Skip Locations

For [Skip Home](#skip-home), [Skip Back](#skip-back), [Skip Here](#skip-here),
and [Skip to Me](#skip-to-me) the system will first check if the target
location is valid (i.e.: it has a solid block under it, and an empty block over
it).

If not, the code will attempt to use the `spreadplayers` command to spawn
the player close to the desired position. Several consecutive attemps will be
made to spawn the player at a distance of 1, 2, 4, 8, 16, 32, and 64 blocks of
the desired place before giving up.

Overhead
--------

Uses the hooks created by [Magic](../magic) and causes no further overhead.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Yippee Skippy!: skip through the world for the first time
  * A Place To Call Home: print a ticket to skip to a place
  * Where The Heart Is: print a ticket to skip to a player
  * Citizen Of The World: create a map hall

[wand]: magic/assets/magic/textures/item/wand.png ""
[ticket]: magic/assets/magic/textures/item/ticket.png ""
