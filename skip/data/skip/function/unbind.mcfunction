# skip:unbind
execute unless data entity @s Item.tag.wishlist.grimoire[0] run return fail
data modify storage wishlist:args book set from entity @s Item.tag.wishlist.grimoire
data modify storage wishlist:args args set value {}
function skip:unbind_1
data modify entity @s Item.tag.wishlist.grimoire set value []
function skip:rewrite
particle minecraft:instant_effect ~ ~ ~ 0.1 0.1 0.1 0.005 10
playsound minecraft:block.grindstone.use player @a ~ ~ ~

