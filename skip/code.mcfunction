
# skip:load
scoreboard objectives add wishlist.just_skipped dummy
scoreboard objectives add wishlist.dead deathCount

execute unless data storage wishlist:skip back \
        run data modify storage wishlist:skip back set value []

data modify storage wishlist:skip maps set value [ \
        { scale:128, off1:64, off2:0, }, \
        { scale:256, off1:64, off2:64, }, \
        { scale:512, off1:64, off2:192, }, \
        { scale:1024, off1:64, off2:448, }, \
        { scale:2048, off1:64, off2:960, }, \
]

data modify storage custom_blocks:conf map_core set value { \
  interaction: { \
    Tags: [wishlist.globe], \
    width: 0.8, \
    height: 0.8 \
  }, \
  display: { \
    transformation: [1f,0f,0f,0f,0f,1f,0f,-0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
  }, \
  item: { \
    custom_model_data:{floats:[271845]}, \
    custom_name:'{"text":"Map Core","italic":false}' \
  }, \
  block: "minecraft:air" \
}

function skip:tick_1t

# skip:back
data remove storage wishlist:args skip
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:back_1 with storage wishlist:args args
execute unless data storage wishlist:args skip \
        run return run title @s actionbar {"translate":"skip.no_back"}
function skip:skip with storage wishlist:args skip

# skip:back_1
$data modify storage wishlist:args skip set from storage wishlist:skip back[{id:$(id)}]

# skip:configure_map_core
advancement revoke @s only skip:configure_map_core

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function skip:configure_map_core_1

execute if score done wishlist.vars matches 0 \
        run return run title @s actionbar {"translate":"skip.error"}

title @s actionbar { \
        "translate": "skip.set_zoom", \
        "with": [{"score":{"name":"zoom","objective":"wishlist.vars"}}] \
}




# skip:configure_map_core_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail

execute on passengers \
        run data modify storage wishlist:args globe \
        set from entity @s item.components.minecraft:custom_data.wishlist_globe

execute store result score zoom wishlist.vars run data get storage wishlist:args globe.zoom
scoreboard players add zoom wishlist.vars 1
scoreboard players operation zoom wishlist.vars %= 6 wishlist.vars
execute store result storage wishlist:args globe.zoom int 1 run scoreboard players get zoom wishlist.vars

execute unless data storage wishlist:args globe.radius \
        run data modify storage wishlist:args globe.radius set value 12

execute on passengers \
        run data modify entity @s item.components.minecraft:custom_data.wishlist_globe \
        set from storage wishlist:args globe

scoreboard players set done wishlist.vars 1

# skip:coords
$data modify storage wishlist:args skip set value {dimension:"$(dimension)",x:$(x),y:$(y),z:$(z)}
execute store result storage wishlist:args skip.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:skip with storage wishlist:args skip


# skip:die
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:die_1 with storage wishlist:args args


# skip:die_1
function skip:record
$data modify storage wishlist:skip back[{id:$(id)}] merge from storage wishlist:args skip

# skip:effect
particle minecraft:portal ~ ~0.5 ~ 0.35 0.45 0.35 0.5 20
playsound minecraft:entity.enderman.teleport player @a ~ ~ ~
scoreboard players set @s wishlist.just_skipped 20

# skip:home
data modify storage wishlist:args skip set value {}
execute store result storage wishlist:args skip.id int 1 \
        run scoreboard players get @s wishlist.playerId
data modify storage wishlist:args skip.x set from entity @s SpawnX
data modify storage wishlist:args skip.y set from entity @s SpawnY
data modify storage wishlist:args skip.z set from entity @s SpawnZ
data modify storage wishlist:args skip.dimension set from entity @s SpawnDimension
function skip:skip with storage wishlist:args skip

# skip:make_coords
execute unless function wishlist:consume_level \
        run return run title @s actionbar {"translate":"skip.not_enough_levels"}
function skip:record
function skip:make_coords_1 with storage wishlist:args skip



# skip:make_coords_1
$summon minecraft:item ~ ~ ~ { \
  Item: { \
    id:"minecraft:carrot_on_a_stick", \
    count:1, \
    components: { \
      "minecraft:custom_name": '{"translate":"item.wishlist.skip_to","italic":false}', \
      "minecraft:lore": ['{"text":"$(dimension) $(x) $(y) $(z)","italic":false,"color":"gray"}'], \
      "minecraft:custom_model_data":{"floats":[271840]}, \
      "minecraft:unbreakable":{"show_in_tooltip":0b}, \
      "minecraft:custom_data": { \
        wishlist_magic: { \
          id:"skip:coords", \
          args:{dimension:"$(dimension)",x:$(x),y:$(y),z:$(z)} \
          }, \
        wishlist_one_use:1b \
        } \
      } \
    } \
  }

advancement grant @s only wishlist:use_skip_coords

# skip:make_player
execute unless function wishlist:consume_level \
        run return run title @s actionbar {"translate":"skip.not_enough_levels"}
data modify storage wishlist:args skip set value {}
execute store result storage wishlist:args skip.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:make_player_1 with storage wishlist:args skip



# skip:make_player_1
$summon minecraft:item ~ ~ ~ { \
  Item: { \
    id:"minecraft:carrot_on_a_stick", \
    count:1, \
    components: { \
      "minecraft:custom_name": '{"translate":"item.wishlist.skip_to","italic":false}', \
      "minecraft:lore": ['{"text":"player #$(id)","italic":false,"color":"gray"}'], \
      "minecraft:custom_model_data":{"floats":[271840]}, \
      "minecraft:unbreakable":{"show_in_tooltip":0b}, \
      "minecraft:custom_data": { \
        wishlist_magic: { \
          id:"skip:player", \
          args:{id:$(id)} \
          }, \
        wishlist_one_use:1b \
        } \
      } \
    } \
  }

advancement grant @s only wishlist:use_skip_player

# skip:map
scoreboard players set go wishlist.vars 0
execute as @e[tag=wishlist.globe,sort=nearest,limit=1] \
        run function skip:map_1 with entity @s Passengers[0].item.components.minecraft:custom_data.wishlist_globe
execute if score go wishlist.vars matches 0 \
        run return fail
function skip:map_2 with storage wishlist:args args




# skip:map_1
$execute unless entity @s[distance=..$(radius)] run return fail
$data modify storage wishlist:args args set from storage wishlist:skip maps[$(zoom)]
data modify storage wishlist:args args.pos set from entity @s Pos
scoreboard players set go wishlist.vars 1



# skip:map_2
# calculate the coordinates of the northwest corner of the map
$scoreboard players set scale wishlist.vars $(scale)

execute store result score dx wishlist.vars run data get storage wishlist:args args.pos[0] 1
$scoreboard players add dx wishlist.vars $(off1)
scoreboard players operation dx wishlist.vars /= scale wishlist.vars
scoreboard players operation dx wishlist.vars *= scale wishlist.vars
$scoreboard players add dx wishlist.vars $(off2)

execute store result score dz wishlist.vars run data get storage wishlist:args args.pos[2] 1
$scoreboard players add dz wishlist.vars $(off1)
scoreboard players operation dz wishlist.vars /= scale wishlist.vars
scoreboard players operation dz wishlist.vars *= scale wishlist.vars
$scoreboard players add dz wishlist.vars $(off2)

$execute store result score x0 wishlist.vars run data get storage wishlist:args args.pos[0] $(scale)
$execute store result score z0 wishlist.vars run data get storage wishlist:args args.pos[2] $(scale)

$execute store result score x1 wishlist.vars run data get entity @s Pos[0] $(scale)
$execute store result score z1 wishlist.vars run data get entity @s Pos[2] $(scale)

scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars
execute store result storage wishlist:args args.x int 1 \
        run scoreboard players operation x1 wishlist.vars += dx wishlist.vars
execute store result storage wishlist:args args.z int 1 \
        run scoreboard players operation z1 wishlist.vars += dz wishlist.vars
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:map_3 with storage wishlist:args args


# skip:map_3
function skip:record
execute at @s run function skip:effect
$execute store success score done wishlist.vars run spreadplayers $(x) $(z) 1 1 false @s
execute at @s run function skip:effect
execute if score done wishlist.vars matches 0 \
        run return run title @p actionbar {"translate":"skip.bad_map_point"}
$data modify storage wishlist:skip back[{id:$(id)}] merge from storage wishlist:args skip
advancement grant @s only wishlist:use_skip

# skip:player
$execute unless entity @a[scores={wishlist.playerId=$(id)}] \
        run return run title @s actionbar {"translate":"skip.player_offline"}
$execute unless entity @a[scores={wishlist.playerId=$(id)},tag=!wishlist.hidden] \
        run return run title @s actionbar {"translate":"skip.player_hidden"}
data modify storage wishlist:args skip set value {}
execute store result storage wishlist:args skip.id int 1 \
        run scoreboard players get @s wishlist.playerId
$execute store result storage wishlist:args skip.x int 1 \
        run scoreboard players get @a[scores={wishlist.playerId=$(id)},limit=1] wishlist.x
$execute store result storage wishlist:args skip.y int 1 \
        run scoreboard players get @a[scores={wishlist.playerId=$(id)},limit=1] wishlist.y
$execute store result storage wishlist:args skip.z int 1 \
        run scoreboard players get @a[scores={wishlist.playerId=$(id)},limit=1] wishlist.z
$data modify storage wishlist:args skip.dimension \
        set from entity @a[scores={wishlist.playerId=$(id)},limit=1] Dimension
function skip:skip with storage wishlist:args skip

# skip:record
data modify storage wishlist:args skip set value {}
execute store result storage wishlist:args skip.x int 1 run data get entity @s Pos[0]
execute store result storage wishlist:args skip.y int 1 run data get entity @s Pos[1]
execute store result storage wishlist:args skip.z int 1 run data get entity @s Pos[2]
data modify storage wishlist:args skip.dimension set from entity @p Dimension

# skip:skip
$tellraw @a[tag=wishlist.debug] { \
        "text": "[skip] $(id) to $(dimension) $(x) $(y) $(z)", \
        "color": "yellow" \
}
function skip:record
$data modify storage wishlist:skip back[{id:$(id)}] merge from storage wishlist:args skip
execute at @s run function skip:effect
$execute in $(dimension) positioned $(x) $(y) $(z) run function skip:skip_1
execute at @s run function skip:effect
advancement grant @s only wishlist:use_skip

# skip:skip_1
execute if loaded ~ ~ ~ run return run function skip:skip_2
forceload add ~ ~
function skip:skip_2
forceload remove ~ ~

# skip:skip_2
execute if block ~ ~ ~ #wishlist:non_full \
        unless block ~ ~-1 ~ #wishlist:non_full \
        unless block ~ ~-1 ~ #minecraft:trapdoors[open=true] \
        unless block ~ ~-1 ~ #minecraft:fences \
        unless block ~ ~-1 ~ #minecraft:fence_gates \
        run return run teleport @s ~ ~ ~
execute if block ~ ~1 ~ #wishlist:non_full \
        unless block ~ ~ ~ #wishlist:non_full \
        unless block ~ ~ ~ #minecraft:trapdoors[open=true] \
        unless block ~ ~ ~ #minecraft:fences \
        unless block ~ ~ ~ #minecraft:fence_gates \
        run return run teleport @s ~ ~1 ~
execute store success score done wishlist.vars run spreadplayers ~ ~ 0 1 true @s
execute unless score done wishlist.vars matches 0 run return 1
execute store success score done wishlist.vars run spreadplayers ~ ~ 0 2 true @s
execute unless score done wishlist.vars matches 0 run return 1
execute store success score done wishlist.vars run spreadplayers ~ ~ 0 4 true @s
execute unless score done wishlist.vars matches 0 run return 1
execute store success score done wishlist.vars run spreadplayers ~ ~ 0 8 true @s
execute unless score done wishlist.vars matches 0 run return 1
execute store success score done wishlist.vars run spreadplayers ~ ~ 0 16 true @s
execute unless score done wishlist.vars matches 0 run return 1
execute store success score done wishlist.vars run spreadplayers ~ ~ 0 32 true @s
execute unless score done wishlist.vars matches 0 run return 1
execute store success score done wishlist.vars run spreadplayers ~ ~ 0 64 true @s

# skip:tick_1t
schedule function skip:tick_1t 1t

execute as @a[scores={wishlist.dead=1..}] run function skip:die
scoreboard players reset * wishlist.dead

execute as @a[tag=wishlist.sneaking,tag=wishlist.land,scores={wishlist.just_skipped=0}] at @s run function skip:map

scoreboard players remove @a[scores={wishlist.just_skipped=1..}] wishlist.just_skipped 1

