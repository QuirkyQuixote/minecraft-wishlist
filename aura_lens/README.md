Aura Lens
=========

Adds an ![aura_lens] aura lens item that highlights surrounding mobs and gives
hints to their status while being held.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

An ![aura_lens] aura_lens can be aquired in two ways:

* By crafting the `aura_lens:aura_lens` recipe, that requires four copper ingots
  surrounding an eye of ender.
* Through the `aura_lens:aura_lens` loot table.

Holding the ![aura_lens] aura lens item in any hand gives the glowing status to
surrounding mobs; in addition, the mobs may emit particles hinting to their
status:

* Red particles indicate that a mob is not at full health
* Green particles indicate that a villager is homeless
* Blue particles indicate that a villager is jobless

Overhead
--------

Updates players holding an aura lens every tick.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Prosthetic Third Eye: Wield an aura lens

[aura_lens]: aura_lens/assets/aura_lens/textures/item/aura_lens.png ""
