# aura_lens:load
data modify storage wishlist:args all_chatter append value \
        "Ever used a Lens? I always get the colors mixed up"

function aura_lens:tick_1s

# aura_lens:tick_1s
schedule function aura_lens:tick_1s 1s
execute as @a if predicate aura_lens:holding_aura_lens at @s run function aura_lens:update

# aura_lens:update
advancement grant @s only wishlist:use_aura_lens
execute as @e[tag=!wishlist.hidden,distance=..16] run function aura_lens:update_tags
execute as @e[type=!armor_stand,tag=!wishlist.hidden,distance=..16] run function aura_lens:update_glow

# aura_lens:update_glow
effect give @s minecraft:glowing 2 0 true

execute if entity @s[tag=wishlist.hurt] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:dust{color:[1f,.4f,.4f],scale:1f} ~ ~ ~ 0.2 0.2 0.2 0 5

execute if entity @s[tag=wishlist.homeless] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:dust{color:[.4f,1f,.4f],scale:1f} ~ ~ ~ 0.2 0.2 0.2 0 5

execute if entity @s[tag=wishlist.jobless] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:dust{color:[.4f,.4f,1f],scale:1f} ~ ~ ~ 0.2 0.2 0.2 0 5

# aura_lens:update_tags
tag @s remove wishlist.hurt
execute store result score cur wishlist.vars run data get entity @s Health
execute store result score max wishlist.vars run attribute @s minecraft:max_health get
execute if score cur wishlist.vars < max wishlist.vars run tag @s add wishlist.hurt
execute if entity @s[type=villager,nbt=!{NoAI:1b}] run function aura_lens:update_villager_tags

# aura_lens:update_villager_tags
tag @s remove wishlist.homeless
tag @s remove wishlist.jobless
execute unless data entity @s Brain.memories."minecraft:job_site" run tag @s add wishlist.jobless
execute unless data entity @s Brain.memories."minecraft:home" run tag @s add wishlist.homeless


