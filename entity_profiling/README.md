Entity Profiling
================

Provides a number of tools to count entities by area, sometimes categorizing
them.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Random Tick](../random_tick) for update optimizations.
* Requires [Wishlist Resources](../resources) for media and text translations.

`/trigger cprof`
----------------

Gives a count of the number of entities loaded in the chunk where the player is
standing.

`/trigger rprof`
----------------

Gives a count of the number of entities loaded in the region where the player
is standing.

`/trigger pprof`
----------------

Gives a count of the number of entities loaded in a 128-block radius around the
player.

`/trigger gprof`
----------------

Updates a scoreboard with the number of entities of different types loaded in
the whole server.  Entities are grouped as the following types:

* item
* decorative (armor stands, item frames, paintings)
* villagers (split between those with AI and those without)
* passive (split between those with AI and those without)
* neutral (split between those with AI and those without)
* hostile (split between those with AI and those without)

Groups with a count of zero will not appear in the list.

Overhead
--------

Updates every second:

* checks every player to see if they have used the Entity Profiling book.

