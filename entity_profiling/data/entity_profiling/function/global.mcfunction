# Calculate amount of different entity tipes

# All entities
scoreboard players set entities ep_scores 0
execute as @e run scoreboard players add entities ep_scores 1
execute if score entities ep_scores matches 0 \
        run scoreboard players reset entities ep_scores

# Markers
scoreboard players set markers ep_scores 0
execute as @e[type=marker] run scoreboard players add markers ep_scores 1
execute if score markers ep_scores matches 0 \
        run scoreboard players reset markers ep_scores

# NPCs
scoreboard players set npcs ep_scores 0
execute as @e[tag=wishlist.npc] run scoreboard players add npcs ep_scores 1
execute if score npcs ep_scores matches 0 \
        run scoreboard players reset npcs ep_scores

# Bound
scoreboard players set bound ep_scores 0
execute as @e[tag=wishlist.player_bound] \
        run scoreboard players add bound ep_scores 1
execute if score bound ep_scores matches 0 \
        run scoreboard players reset bound ep_scores

# Items
scoreboard players set items ep_scores 0
execute as @e[type=item] run scoreboard players add items ep_scores 1
execute if score items ep_scores matches 0 \
        run scoreboard players reset items ep_scores

# Decorative items; they mostly cause fps lag
scoreboard players set decorative ep_scores 0
execute as @e[type=#wishlist:decorative] \
        run scoreboard players add decorative ep_scores 1
execute if score decorative ep_scores matches 0 \
        run scoreboard players reset decorative ep_scores

# Villagers are separate because they are their own lag source
scoreboard players set villagers_noai ep_scores 0
execute as @e[type=villager,nbt={NoAI:1b}] \
        run scoreboard players add villagers_noai ep_scores 1
execute if score villagers_noai ep_scores matches 0 \
        run scoreboard players reset villagers_noai ep_scores
scoreboard players set villagers_ai ep_scores 0
execute as @e[type=villager,nbt=!{NoAI:1b}] \
        run scoreboard players add villagers_ai ep_scores 1
execute if score villagers_ai ep_scores matches 0 \
        run scoreboard players reset villagers_ai ep_scores

# These two categories are "extra"; they count the number of villagers that
# will consume server resources by searching continuously for a bed or a job
# site block.
scoreboard players set villagers_jobless ep_scores 0
execute as @e[type=villager,nbt=!{NoAI:1b},nbt=!{VillagerData:{profession:"minecraft:nitwit"}},nbt=!{Brain:{memories:{"minecraft:job_site":{}}}}] \
        run scoreboard players add villagers_jobless ep_scores 1
execute if score villagers_jobless ep_scores matches 0 \
        run scoreboard players reset villagers_jobless ep_scores
scoreboard players set villagers_homeless ep_scores 0
execute as @e[type=villager,nbt=!{NoAI:1b},nbt=!{Brain:{memories:{"minecraft:home":{}}}}] \
        run scoreboard players add villagers_homeless ep_scores 1
execute if score villagers_homeless ep_scores matches 0 \
        run scoreboard players reset villagers_homeless ep_scores

# All passive mobs except villagers; note that you have to split the nbt and
# type selectors because those don't mix when type is a tag, for some reason
scoreboard players set passive_ai ep_scores 0
execute as @e[type=#wishlist:passive,nbt=!{NoAI:1b}] \
        run scoreboard players add passive_ai ep_scores 1
execute if score passive_ai ep_scores matches 0 \
        run scoreboard players reset passive_ai ep_scores
scoreboard players set passive_noai ep_scores 0
execute as @e[type=#wishlist:passive,nbt={NoAI:1b}] \
        run scoreboard players add passive_noai ep_scores 1
execute if score passive_noai ep_scores matches 0 \
        run scoreboard players reset passive_noai ep_scores

# All neutral mobs
scoreboard players set neutral_ai ep_scores 0
execute as @e[type=#wishlist:neutral,nbt=!{NoAI:1b}] \
        run scoreboard players add neutral_ai ep_scores 1
execute if score neutral_ai ep_scores matches 0 \
        run scoreboard players reset neutral_ai ep_scores
scoreboard players set neutral_noai ep_scores 0
execute as @e[type=#wishlist:neutral,nbt={NoAI:1b}] \
        run scoreboard players add neutral_noai ep_scores 1
execute if score neutral_noai ep_scores matches 0 \
        run scoreboard players reset neutral_noai ep_scores

# All hostile mobs
scoreboard players set hostile_ai ep_scores 0
execute as @e[type=#wishlist:hostile,nbt=!{NoAI:1b}] \
        run scoreboard players add hostile_ai ep_scores 1
execute if score hostile_ai ep_scores matches 0 \
        run scoreboard players reset hostile_ai ep_scores
scoreboard players set hostile_noai ep_scores 0
execute as @e[type=#wishlist:hostile,nbt={NoAI:1b}] \
        run scoreboard players add hostile_noai ep_scores 1
execute if score hostile_noai ep_scores matches 0 \
        run scoreboard players reset hostile_noai ep_scores

