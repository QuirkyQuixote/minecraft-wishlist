# prof.mcfunction - count entities around player and print distribution

tellraw @s ["§r§e===================="]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§eentities.................................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=minecraft:item] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§eitems.......................................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:decorative] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§edecorative........................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:display] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§edisplay.................................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=minecraft:villager,nbt=!{NoAI:1b},nbt={Brain:{memories:{"minecraft:job_site":{}}}}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§evillagers_ai......................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=minecraft:villager,nbt={NoAI:1b}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§evillagers_noai................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=minecraft:villager,nbt=!{NoAI:1b},nbt=!{Brain:{memories:{"minecraft:home":{}}}}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§evillagers_homeless...",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=minecraft:villager,nbt=!{NoAI:1b},nbt=!{VillagerData:{profession:"minecraft:nitwit"}},nbt=!{Brain:{memories:{"minecraft:job_site":{}}}}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§evillagers_jobless......",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:passive,nbt=!{NoAI:1b}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§epassive_ai.........................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:passive,nbt={NoAI:1b}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§epassive_noai...................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:neutral,nbt=!{NoAI:1b}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§eneutral_ai..........................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:neutral,nbt={NoAI:1b}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§eneutral_noai....................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:hostile,nbt=!{NoAI:1b}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§ehostile_ai............................",{"score":{"name":"tmp","objective":"ep_scores"}}]

scoreboard players set tmp ep_scores 0
execute positioned ~-160 ~-160 ~-160 \
        as @e[dx=320,dy=320,dz=320,type=#wishlist:hostile,nbt={NoAI:1b}] \
        run scoreboard players add tmp ep_scores 1
tellraw @s ["§r§ehostile_noai......................",{"score":{"name":"tmp","objective":"ep_scores"}}]

tellraw @s ["§r§e===================="]

scoreboard players reset tmp ep_scores
