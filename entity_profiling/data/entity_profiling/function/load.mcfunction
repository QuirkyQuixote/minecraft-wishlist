# load.mcfunction - Configure profiling

scoreboard objectives add ep_scores dummy "Profiling"
scoreboard objectives add prof trigger "Prof"

scoreboard objectives add prof trigger "Profile"
scoreboard objectives add gprof trigger "Profile Server"
scoreboard objectives add cprof trigger "Profile Chunk"
scoreboard objectives add rprof trigger "Profile Region"
scoreboard objectives add pprof trigger "Profile Player"
scoreboard objectives add wishlist.entity_profiling.trigger trigger
scoreboard objectives add wishlist.entity_profiling.filter trigger

scoreboard players set watch_threshold wishlist.vars 1000

function entity_profiling:tick

