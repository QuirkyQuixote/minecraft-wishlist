# watch_1.mcfunction

tellraw @a \
  [ \
    {"text":"[WARNING] too many entities at chunk "}, \
    {"score":{"name":"x","objective":"wishlist.vars"}}, \
    {"text":","}, \
    {"score":{"name":"z","objective":"wishlist.vars"}} \
  ]

execute positioned ~-128 ~ ~-128 \
        as @a[dx=256,dy=256,dz=256] \
        run tellraw @a \
          [ \
            {"text":"[WARNING] * near "}, \
            {"selector":"@s"} \
          ]

