# region_2.mcfunction

execute if score distance wishlist.vars matches 1 \
        as @e[dx=512,dy=512,dz=512] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 2 \
        as @e[dx=1024,dy=512,dz=1024] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 3 \
        as @e[dx=2048,dy=512,dz=2048] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 4 \
        as @e[dx=4096,dy=512,dz=4096] \
        run scoreboard players add count wishlist.vars 1


