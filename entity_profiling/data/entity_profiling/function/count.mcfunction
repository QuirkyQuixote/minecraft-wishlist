# chunk_2.mcfunction

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=16,dy=512,dz=16] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=32,dy=512,dz=32] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=64,dy=512,dz=64] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=128,dy=512,dz=128] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=256,dy=512,dz=256] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=512,dy=512,dz=512] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=1024,dy=512,dz=1024] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=2048,dy=512,dz=2048] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 0 \
        as @e[dx=4096,dy=512,dz=4096] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 1 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:marker] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 2 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:text_display] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 3 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:item_display] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 4 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:block_display] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 5 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:item] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 6 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:item_frame] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 7 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:glow_item_frame] \
        run scoreboard players add count wishlist.vars 1

execute if score distance wishlist.vars matches 1 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=16,dy=512,dz=16,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=32,dy=512,dz=32,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 3 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=64,dy=512,dz=64,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 4 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=128,dy=512,dz=128,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 5 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=256,dy=512,dz=256,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 6 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=512,dy=512,dz=512,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=1024,dy=512,dz=1024,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 8 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=2048,dy=512,dz=2048,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1
execute if score distance wishlist.vars matches 9 \
        if score filter wishlist.vars matches 8 \
        as @e[dx=4096,dy=512,dz=4096,type=minecraft:armor_stand] \
        run scoreboard players add count wishlist.vars 1

