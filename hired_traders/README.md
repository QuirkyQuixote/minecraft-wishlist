Hired Traders
=============

Provides villagers that will sell up to nine kinds of items at the price
configured by their employer, get their stock from their employer, and give
them the money they earn.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Summon](../summon) for summon contract
* Requires [Wishlist Resources](../resources) for media and text translations.
* Recommends [Look Alive](../look_alive) to make the traders look more lively.
* Recommends [Chatterbox](../chatterbox) to make the traders look more lively.

Usage
-----

Hired Traders provides two new items:

### ![contract] Summon Hired Trader

Use this to [summon](../summon) a hired trader.

* Obtainable with the `hired_traders:contract` loot table.
* The hired trader should be summoned on top of a barrel, whose contents are
  used to configure their trades.
* By default the hired trader looks like a vanilla villager wearing a green
  eyeshade.

### ![clipboard] Clipboard

Hold this while updating a hired trader to obtain a full inventory of their
stock.

* Can be crafted with three paper and one planks; recipe is unlocked when the
  player holds paper.
* Obtainable with the `hired_traders:inventory` loot table.

### Configuring Trades

Hired traders are configured by setting the inventory of the barrel under them
as follows:

```
buy1 buy1 buy1 buy1 buy1 buy1 buy1 buy1 buy1
buy2 buy2 buy2 buy2 buy2 buy2 buy2 buy2 buy2 
sell sell sell sell sell sell sell sell sell 
```

Every column is a trade: first and second item are the stacks bought by the
trader (one can be empty) and third column is the stack sold by the trader for
those items.

The trader will update their trades to reflect this when their employer
crouches in front of them. At this point, several things will happen:

* the trader will rebuild their trade list
* the trader will spill all items in their inventory that are not sold in any
  of their trades. This can include payment received, unless the trader has a
  trade that sells the same things that other trade buys.
* the trader pick any items on the floor around them that they are supposed to
  sell.
* the trader will update the maximum amount of times each of their trades can
  be used according to the amount of items in their inventory.

Overhead
--------

Updates every tick:

* checks every player to see if they have crouched this frame
    * checks every entity in a 5-block radius around the player to see if it is
      a hired trader to be updated.

Uses an advancement to check if a player traded with a hired trader

* if so, the trader's stock and trades are updated to reflect inventory changes

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Reasonable Hiring Practices: hire an employee
    * Grand Opening: restock an employee's trade
        * Inventory Management: destock employee's trade

[contract]: summon/assets/summon/textures/item/contract.png ""
[clipboard]: hired_traders/assets/hired_traders/textures/item/clipboard.png ""
