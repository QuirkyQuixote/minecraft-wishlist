
# hired_traders:load
scoreboard players set -1 wishlist.vars -1

# hired_traders:update
tag @s add wishlist.match
scoreboard players operation player wishlist.vars = @s wishlist.playerId
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[tag=wishlist.hired_trader,distance=..2,sort=furthest] \
        at @s \
        run function hired_traders:update_1
tag @s remove wishlist.match

# hired_traders:update_1
function wishlist:recall
execute store result score employer wishlist.vars run data get storage wishlist:args memories.employer
execute unless score player wishlist.vars = employer wishlist.vars run return fail
execute positioned ~ ~-1 ~ run function hired_traders:update_trades
function hired_traders:drop_stock
execute as @e[type=item,dx=1,dy=1,dz=1] run function hired_traders:pick_up_stock
function hired_traders:update_stock
function hired_traders:update_uses
function hired_traders:update_inventory
function wishlist:memorize
data modify entity @s Offers.Recipes set from storage hired_traders:vars recipes
advancement grant @a[tag=wishlist.match] only wishlist:hire_employee

title @a[tag=wishlist.match] actionbar { \
        "translate":"hired_traders.updated", \
        "with":[{"selector":"@s"}] \
        }

tellraw @a[tag=wishlist.debug] { \
        "translate":"[hired traders] %s updated", \
        "with":[{"selector":"@s"}], \
        "color":"yellow" \
        }

# hired_traders:trade
advancement revoke @s only hired_traders:trade
execute anchored eyes \
        positioned ^ ^ ^4 \
        as @e[tag=wishlist.hired_trader,distance=..4] \
        run function hired_traders:trade_1

# hired_traders:trade_1
function wishlist:recall
data modify storage hired_traders:vars recipes set from entity @s Offers.Recipes
function hired_traders:update_stock
function hired_traders:update_uses
function wishlist:memorize
data modify entity @s Offers.Recipes set from storage hired_traders:vars recipes

# hired_traders:drop_stock
data modify storage hired_traders:vars drop set value []
data modify storage hired_traders:vars drop append from storage wishlist:args memories.stock[{unused:1b}]
function hired_traders:drop_stock_1
data remove storage wishlist:args memories.stock[{unused:1b}]
data remove storage wishlist:args memories.stock[{}].unused

# hired_traders:drop_stock_1
execute unless data storage hired_traders:vars drop[0] run return 0
data modify storage hired_traders:vars args set value {}
data modify storage hired_traders:vars args.item set from storage hired_traders:vars drop[0]
function hired_traders:drop_stock_2 with storage hired_traders:vars args
data remove storage hired_traders:vars drop[0]
function hired_traders:drop_stock_1
execute as @a if score @s wishlist.playerId = employer wishlist.vars \
        run advancement grant @s only wishlist:destock_employee

# hired_traders:drop_stock_2
$summon minecraft:item ~ ~1 ~ {Item:$(item)}

# hired_traders:pick_up_stock
tellraw @a[tag=wishlist.debug] { \
        "translate":"[hired traders] pick up %s?", \
        "with":[{"selector":"@s"}], \
        "color":"yellow" \
        }

data modify storage hired_traders:vars args set value {}
data modify storage hired_traders:vars args.item set from entity @s Item
data modify storage hired_traders:vars args.count set from storage hired_traders:vars args.item.count
data remove storage hired_traders:vars args.item.count
function hired_traders:pick_up_stock_1 with storage hired_traders:vars args

# hired_traders:pick_up_stock_1
$execute unless data storage wishlist:args memories.stock[$(item)] run return fail
$execute store result score n wishlist.vars \
        run data get storage wishlist:args memories.stock[$(item)].count
$execute store result storage wishlist:args memories.stock[$(item)].count int 1 \
        run scoreboard players add n wishlist.vars $(count)
execute as @a if score @s wishlist.playerId = employer wishlist.vars \
        run advancement grant @s only wishlist:restock_employee
kill @s

# hired_traders:update_trades
execute unless block ~ ~ ~ minecraft:barrel run return fail

data modify storage wishlist:args memories.stock[{}].unused set value 1b
data modify storage hired_traders:vars recipes set value []
function hired_traders:update_trade {buy:0, buyB:9, sell:18}
function hired_traders:update_trade {buy:1, buyB:10, sell:19}
function hired_traders:update_trade {buy:2, buyB:11, sell:20}
function hired_traders:update_trade {buy:3, buyB:12, sell:21}
function hired_traders:update_trade {buy:4, buyB:13, sell:22}
function hired_traders:update_trade {buy:5, buyB:14, sell:23}
function hired_traders:update_trade {buy:6, buyB:15, sell:24}
function hired_traders:update_trade {buy:7, buyB:16, sell:25}
function hired_traders:update_trade {buy:8, buyB:17, sell:26}
data modify entity @s Offers.Recipes set from storage hired_traders:vars recipes

data modify storage hired_traders:vars recipes set from entity @s Offers.Recipes
function hired_traders:update_uses
data modify entity @s Offers.Recipes set from storage hired_traders:vars recipes

# hired_traders:update_trade 
data modify storage hired_traders:vars recipe set value { \
        demand:0, \
        maxUses:0, \
        priceMultiplier:0.0f, \
        rewardExp:0b, \
        specialPrice:0, \
        uses:0, \
        xp:0, \
        "Paper.IgnoreDiscounts":1b \
        }
$data modify storage hired_traders:vars recipe.buy set from block ~ ~ ~ Items[{Slot:$(buy)b}]
$data modify storage hired_traders:vars recipe.buyB set from block ~ ~ ~ Items[{Slot:$(buyB)b}]
$data modify storage hired_traders:vars recipe.sell set from block ~ ~ ~ Items[{Slot:$(sell)b}]
data remove storage hired_traders:vars recipe.buy.Slot
data remove storage hired_traders:vars recipe.buyB.Slot
data remove storage hired_traders:vars recipe.sell.Slot
data modify storage hired_traders:vars recipes append from storage hired_traders:vars recipe
data modify storage hired_traders:vars args set value {}
data modify storage hired_traders:vars args.item.id \
        set from storage hired_traders:vars recipe.sell.id
data modify storage hired_traders:vars args.item.components \
        set from storage hired_traders:vars recipe.sell.components
function hired_traders:mark_stock with storage hired_traders:vars args

# hired_traders:mark_stock
$data modify storage wishlist:args memories.stock[$(item)].unused set value 0b

# hired_traders:update_uses
function hired_traders:update_uses_1 {id:0}
function hired_traders:update_uses_1 {id:1}
function hired_traders:update_uses_1 {id:2}
function hired_traders:update_uses_1 {id:3}
function hired_traders:update_uses_1 {id:4}
function hired_traders:update_uses_1 {id:5}
function hired_traders:update_uses_1 {id:6}
function hired_traders:update_uses_1 {id:7}
function hired_traders:update_uses_1 {id:8}

# hired_traders:update_uses_1
$execute unless data storage hired_traders:vars recipes[$(id)] run return fail
data modify storage hired_traders:vars args set value {}
$data modify storage hired_traders:vars args.item set from storage hired_traders:vars recipes[$(id)].sell
data modify storage hired_traders:vars args.count set from storage hired_traders:vars args.item.count
data remove storage hired_traders:vars args.item.count
$execute store result storage hired_traders:vars recipes[$(id)].maxUses int 1 \
        run function hired_traders:update_uses_2 with storage hired_traders:vars args

# hired_traders:update_uses_2
$execute unless data storage wishlist:args memories.stock[$(item)] run return 0
$execute store result score n wishlist.vars \
        run data get storage wishlist:args memories.stock[$(item)].count 1
$scoreboard players set m wishlist.vars $(count)
return run scoreboard players operation n wishlist.vars /= m wishlist.vars

# hired_traders:update_stock
function hired_traders:update_stock_1 {id:0}
function hired_traders:update_stock_1 {id:1}
function hired_traders:update_stock_1 {id:2}
function hired_traders:update_stock_1 {id:3}
function hired_traders:update_stock_1 {id:4}
function hired_traders:update_stock_1 {id:5}
function hired_traders:update_stock_1 {id:6}
function hired_traders:update_stock_1 {id:7}
function hired_traders:update_stock_1 {id:8}

# hired_traders:update_stock_1
$execute unless data storage hired_traders:vars recipes[$(id)] run return fail
$execute store result score uses wishlist.vars run data get storage hired_traders:vars recipes[$(id)].uses
execute if score uses wishlist.vars matches 0 run return fail

data modify storage hired_traders:vars args set value {}
$data modify storage hired_traders:vars args.item set from storage hired_traders:vars recipes[$(id)].buy
data modify storage hired_traders:vars args.count set from storage hired_traders:vars args.item.count
data remove storage hired_traders:vars args.item.count
function hired_traders:update_stock_2 with storage hired_traders:vars args

data modify storage hired_traders:vars args set value {}
$data modify storage hired_traders:vars args.item set from storage hired_traders:vars recipes[$(id)].buyB
data modify storage hired_traders:vars args.count set from storage hired_traders:vars args.item.count
data remove storage hired_traders:vars args.item.count
function hired_traders:update_stock_2 with storage hired_traders:vars args

scoreboard players operation uses wishlist.vars *= -1 wishlist.vars
data modify storage hired_traders:vars args set value {}
$data modify storage hired_traders:vars args.item set from storage hired_traders:vars recipes[$(id)].sell
data modify storage hired_traders:vars args.count set from storage hired_traders:vars args.item.count
data remove storage hired_traders:vars args.item.count
function hired_traders:update_stock_2 with storage hired_traders:vars args

$data modify storage hired_traders:vars recipes[$(id)].uses set value 0

# hired_traders:update_stock_2
$scoreboard players set n wishlist.vars $(count)
scoreboard players operation n wishlist.vars *= uses wishlist.vars
$execute store result score m wishlist.vars \
        run data get storage wishlist:args memories.stock[$(item)].count
scoreboard players operation m wishlist.vars += n wishlist.vars
$execute store result storage wishlist:args memories.stock[$(item)].count int 1 \
        run scoreboard players get m wishlist.vars

# hired_traders:update_inventory
execute as @a[tag=wishlist.match] unless predicate hired_traders:holding_clipboard run return fail

data modify storage hired_traders:vars args set value {author:'"Anonymous"'}
data modify storage hired_traders:vars args.author set from entity @s CustomName

function hired_traders:update_inventory_1 {n:0}
function hired_traders:update_inventory_1 {n:1}
function hired_traders:update_inventory_1 {n:2}
function hired_traders:update_inventory_1 {n:3}
function hired_traders:update_inventory_1 {n:4}
function hired_traders:update_inventory_1 {n:5}
function hired_traders:update_inventory_1 {n:6}
function hired_traders:update_inventory_1 {n:7}
function hired_traders:update_inventory_1 {n:8}

execute as @a[tag=wishlist.match] \
        run function hired_traders:update_inventory_2 with storage hired_traders:vars args

# hired_traders:update_inventory_1
$data modify storage hired_traders:vars args.id$(n) set value '""'
$data modify storage hired_traders:vars args.id$(n) \
        set string storage wishlist:args memories.stock[$(n)].id 10
$data modify storage hired_traders:vars args.id$(n) \
        set from storage wishlist:args memories.stock[$(n)].components.minecraft:custom_name
$data modify storage hired_traders:vars args.count$(n) set value ''
$data modify storage hired_traders:vars args.count$(n) \
        set from storage wishlist:args memories.stock[$(n)].count

# hired_traders:update_inventory_2
$item replace entity @s weapon.mainhand with minecraft:written_book[ \
        minecraft:custom_model_data=271833, \
        minecraft:enchantment_glint_override=0, \
        minecraft:custom_data={"wishlist_clipboard":true}, \
        minecraft:written_book_content={ \
          "title":"Inventory", \
          "author":$(author), \
          "pages":[ \
            '[ $(id0), " ", "$(count0)", "\\n", $(id1), " ", "$(count1)", "\\n", $(id2), " ", "$(count2)", "\\n", $(id3), " ", "$(count3)", "\\n", $(id4), " ", "$(count4)", "\\n", $(id5), " ", "$(count5)", "\\n", $(id6), " ", "$(count6)", "\\n", $(id7), " ", "$(count7)", "\\n", $(id8), " ", "$(count8)", "\\n", ""]' \
            ] \
          } \
        ]

