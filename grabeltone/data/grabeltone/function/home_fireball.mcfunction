# home_fireball.mcfunction

execute on origin run tag @s add grabeltone.fireball_thrower

execute store result score x0 wishlist.vars run data get entity @s Pos[0] 16
execute store result score y0 wishlist.vars run data get entity @s Pos[1] 16
execute store result score z0 wishlist.vars run data get entity @s Pos[2] 16

scoreboard players operation x1 wishlist.vars = x0 wishlist.vars
scoreboard players operation y1 wishlist.vars = y0 wishlist.vars
scoreboard players operation z1 wishlist.vars = z0 wishlist.vars

execute as @e[type=#wishlist:alive,tag=!grabeltone.fireball_thrower,distance=..16,sort=nearest,limit=1] \
        run function grabeltone:home_fireball_1

scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
scoreboard players operation y1 wishlist.vars -= y0 wishlist.vars
scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars

execute store result entity @s power[0] double 0.000625 \
        run scoreboard players get x1 wishlist.vars
execute store result entity @s power[2] double 0.000625 \
        run scoreboard players get z1 wishlist.vars

data modify entity @s power[1] set value 0.0
data modify entity @s Motion[1] set value 0.0

tag @a remove grabeltone.fireball_thrower
