
# grabeltone:load

forceload add -16 -16 15 15
forceload add -48 -80 -18 -49

scoreboard objectives add grabeltone.die minecraft.custom:minecraft.deaths
scoreboard objectives add grabeltone.gamemode dummy

data modify storage wishlist:args all_chatter append value \
        "Welcome to Grabeltone!"
data modify storage wishlist:args all_chatter append value \
        "Have you found the Eight Bits yet?"
data modify storage wishlist:args all_chatter append value \
        "There's something weird under Ixuwata's well"
data modify storage wishlist:args all_chatter append value \
        "There's a strange tree east of L`ong Sông"
data modify storage wishlist:args all_chatter append value \
        "There's a temple on the jungle west from Spawn that goes deeper than people think"
data modify storage wishlist:args all_chatter append value \
        "Electrum Factory was built on top of something older and stranger"
data modify storage wishlist:args all_chatter append value \
        "The grandparent of all monuments is under some sandy islands not far from the Medieval Road"
data modify storage wishlist:args all_chatter append value \
        "Some of the ruins under the Western Desert may not be entirely dead"
data modify storage wishlist:args all_chatter append value \
        "The darkest of ruins is said to be under White Cliffs"
data modify storage wishlist:args all_chatter append value \
        "Somewhere under the Winter Road, there's a very cold hidden fortress"
data modify storage wishlist:args all_chatter append value \
        "Rumor says that there's a very deep well no one dares to enter in a forest southeast"
data modify storage wishlist:args all_chatter append value \
        "Ever got to hold an impossible thing?"
data modify storage wishlist:args all_chatter append value \
        "Who you gonna call? check the advancement"
data modify storage wishlist:args all_chatter append value \
        "A blacksmith at Black Bastion can produce incredible tools"
data modify storage wishlist:args all_chatter append value \
        "There's this little shop at Narshe that sells everlasting lighters and shears"
data modify storage wishlist:args all_chatter append value \
        "You can buy magical arrows at the Alcázar where the Medieval Road starts"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of lifetime stuff? It will last your entire life. One of them, at least"
data modify storage wishlist:args all_chatter append value \
        "Dying is not the end; we have an agreement with the conductors of the Train"
data modify storage wishlist:args all_chatter append value \
        "Do not linger in train tunnels; you never know when it's coming"
data modify storage wishlist:args all_chatter append value \
        "The great libraries at Spawn and Bastion have everything. For a price"
data modify storage wishlist:args all_chatter append value \
        "There are five bus lines around the world, and one each of train, airship and ferry"
data modify storage wishlist:args all_chatter append value \
        "I don't use the transports that much since we started having map halls"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of Vonturbola and Rallyim? They have been everywhere"

function grabeltone:tick
function grabeltone:tick_1s
function grabeltone:tick_3s

# grabeltone:tick

schedule function grabeltone:tick 1t

execute as @e[type=player,scores={grabeltone.die=1..}] \
        run function grabeltone:phantom_train

execute as @a[scores={wishlist.x=-40..40,wishlist.y=32..40,wishlist.z=-16..16}] \
        at @s \
        positioned ~-32 32 -16 \
        run function grabeltone:phantom_train_particles
execute as @a[scores={wishlist.x=-40..40,wishlist.y=32..40,wishlist.z=-16..16}] \
        at @s \
        positioned ~-32 32 8 \
        run function grabeltone:phantom_train_particles

execute at @a as @e[tag=wishlist.disguised,distance=..8] \
        run function grabeltone:adjust_disguise

execute as @a[tag=!grabeltone.in_sketch] at @s if dimension wishlist:sketch \
        run function grabeltone:enter_sketch
execute as @a[tag=grabeltone.in_sketch] at @s unless dimension wishlist:sketch \
        run function grabeltone:exit_sketch

# grabeltone:tick_1s

schedule function grabeltone:tick_1s 1s

execute as alphayix at @s run function grabeltone:front_alphayix
execute as EssenciaSyst at @s run function grabeltone:front_alphayix
execute as NoOneWasFoundEXE at @s run function grabeltone:front_cosmo
execute as deathisacatgirl at @s run function grabeltone:front_hazel
execute as CoolGirlAuby3 at @s run function grabeltone:front_auby

execute at @a \
        as @e[type=minecraft:zombie,tag=!grabeltone.custom_drop.done,distance=..8] \
        run function grabeltone:custom_drops

# grabeltone:tick_3s

schedule function grabeltone:tick_3s 3s

execute at @a \
        as @e[type=#wishlist:trader,distance=..16] \
        run function grabeltone:sync_trades

execute at @a run tag @e[type=villager,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=pillager,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=piglin,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=piglin_brute,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=wandering_trader,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=witch,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=vindicator,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=illusioner,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=evoker,distance=..8] add wishlist.gossipy

# grabeltone:adjust_disguise

data modify storage wishlist:args Rotation set from entity @s Rotation
execute on passengers run data modify entity @s Rotation set from storage wishlist:args Rotation

# grabeltone:enter_sketch
function wishlist:stash_inventory
execute if entity @s[gamemode=creative] run scoreboard players set @s grabeltone.gamemode 0
execute if entity @s[gamemode=survival] run scoreboard players set @s grabeltone.gamemode 1
execute if entity @s[gamemode=adventure] run scoreboard players set @s grabeltone.gamemode 2
execute if entity @s[gamemode=spectator] run scoreboard players set @s grabeltone.gamemode 3
clear @s
gamemode creative @s
tag @s add grabeltone.in_sketch

tellraw @a[tag=wishlist.debug] { \
        "translate":"[grabeltone] %s entered sketch", \
        "with":[{"selector":"@s"}], \
        "color":"yellow" \
        }

# grabeltone:exit_sketch
function wishlist:restore_inventory
execute if score @s grabeltone.gamemode matches 0 run gamemode creative @s
execute if score @s grabeltone.gamemode matches 1 run gamemode survival @s
execute if score @s grabeltone.gamemode matches 2 run gamemode adventure @s
execute if score @s grabeltone.gamemode matches 3 run gamemode spectator @s
tag @s remove grabeltone.in_sketch

tellraw @a[tag=wishlist.debug] { \
        "translate":"[grabeltone] %s exited sketch", \
        "with":[{"selector":"@s"}], \
        "color":"yellow" \
        }


# grabeltone:front_alphayix

execute if score @s fronting matches 0 \
        run team modify player.alphayix prefix {"text":"Ziyah ","color":"gold"}
execute if score @s fronting matches 0 \
        run team modify player.alphayix suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 1 \
        run team modify player.alphayix prefix {"text":"Rain ","color":"dark_purple"}
execute if score @s fronting matches 1 \
        run team modify player.alphayix suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 2 \
        run team modify player.alphayix prefix {"text":"Helen ","color":"dark_red"}
execute if score @s fronting matches 2 \
        run team modify player.alphayix suffix {"text":" (she/they)","color":"gray"}

execute if score @s fronting matches 3 \
        run team modify player.alphayix prefix {"text":"Ursa ","color":"dark_purple"}
execute if score @s fronting matches 3 \
        run team modify player.alphayix suffix {"text":" (any/all)","color":"gray"}

execute if score @s fronting matches 4 \
        run team modify player.alphayix prefix {"text":"Vesper ","color":"dark_purple"}
execute if score @s fronting matches 4 \
        run team modify player.alphayix suffix {"text":" (they/them)","color":"gray"}

execute if score @s fronting matches 5 \
        run team modify player.alphayix prefix {"text":"Harmon ","color":"dark_purple"}
execute if score @s fronting matches 5 \
        run team modify player.alphayix suffix {"text":" (he/him)","color":"gray"}

execute if score @s fronting matches 6 \
        run team modify player.alphayix prefix {"text":"Patches ","color":"dark_purple"}
execute if score @s fronting matches 6 \
        run team modify player.alphayix suffix {"text":" (she/fae)","color":"gray"}

execute if score @s fronting matches 7 \
        run team modify player.alphayix prefix {"text":"Maple ","color":"dark_purple"}
execute if score @s fronting matches 7 \
        run team modify player.alphayix suffix {"text":" (she/it)","color":"gray"}

execute if score @s fronting matches 8 \
        run team modify player.alphayix prefix {"text":"Wonder ","color":"dark_purple"}
execute if score @s fronting matches 8 \
        run team modify player.alphayix suffix {"text":" (no pronouns)","color":"gray"}

scoreboard players enable @s fronting

# grabeltone:front_auby

execute if score @s fronting matches 0 \
        run team modify player.auby prefix {"text":"Monika ","color":"light_purple"}
execute if score @s fronting matches 0 \
        run team modify player.auby suffix {"text":" (they/them)","color":"gray"}

execute if score @s fronting matches 1 \
        run team modify player.auby prefix {"text":"Bonnie ","color":"yellow"}
execute if score @s fronting matches 1 \
        run team modify player.auby suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 2 \
        run team modify player.auby prefix {"text":"Natsuki ","color":"gold"}
execute if score @s fronting matches 2 \
        run team modify player.auby suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 3 \
        run team modify player.auby prefix {"text":"Grimm ","color":"gold"}
execute if score @s fronting matches 3 \
        run team modify player.auby suffix {"text":" (they/he)","color":"gray"}

execute if score @s fronting matches 4 \
        run team modify player.auby prefix {"text":"Ziyah ","color":"gold"}
execute if score @s fronting matches 4 \
        run team modify player.auby suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 5 \
        run team modify player.auby prefix {"text":"Giratina ","color":"gold"}
execute if score @s fronting matches 5 \
        run team modify player.auby suffix {"text":" (he/they)","color":"gray"}

execute if score @s fronting matches 6 \
        run team modify player.auby prefix {"text":"Scarlett ","color":"gold"}
execute if score @s fronting matches 6 \
        run team modify player.auby suffix {"text":" (fae/she)","color":"gray"}

execute if score @s fronting matches 7 \
        run team modify player.auby prefix {"text":"Michael ","color":"gold"}
execute if score @s fronting matches 7 \
        run team modify player.auby suffix {"text":" (he/him)","color":"gray"}

scoreboard players enable @s fronting

# grabeltone:front_cosmo

execute if score @s fronting matches 0 \
        run team modify player.cosmo prefix {"text":"Alya ","color":"dark_aqua"}
execute if score @s fronting matches 0 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 1 \
        run team modify player.cosmo prefix {"text":"Red ","color":"dark_aqua"}
execute if score @s fronting matches 1 \
        run team modify player.cosmo suffix {"text":" (he/him)","color":"gray"}

execute if score @s fronting matches 2 \
        run team modify player.cosmo prefix {"text":"Alex ","color":"dark_aqua"}
execute if score @s fronting matches 2 \
        run team modify player.cosmo suffix {"text":" (they/them)","color":"gray"}

execute if score @s fronting matches 3 \
        run team modify player.cosmo prefix {"text":"Emerald ","color":"dark_aqua"}
execute if score @s fronting matches 3 \
        run team modify player.cosmo suffix {"text":" (she/they)","color":"gray"}

execute if score @s fronting matches 4 \
        run team modify player.cosmo prefix {"text":"Ariana ","color":"dark_aqua"}
execute if score @s fronting matches 4 \
        run team modify player.cosmo suffix {"text":" (she/they)","color":"gray"}

execute if score @s fronting matches 5 \
        run team modify player.cosmo prefix {"text":"Helena ","color":"dark_red"}
execute if score @s fronting matches 5 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 6 \
        run team modify player.cosmo prefix {"text":"Unknown ","color":"dark_aqua"}
execute if score @s fronting matches 6 \
        run team modify player.cosmo suffix {"text":" (any/all)","color":"gray"}

execute if score @s fronting matches 7 \
        run team modify player.cosmo prefix {"text":"Rowan ","color":"dark_aqua"}
execute if score @s fronting matches 7 \
        run team modify player.cosmo suffix {"text":" (he/they)","color":"gray"}

execute if score @s fronting matches 8 \
        run team modify player.cosmo prefix {"text":"Aurelia ","color":"dark_aqua"}
execute if score @s fronting matches 8 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 9 \
        run team modify player.cosmo prefix {"text":"Emily ","color":"dark_aqua"}
execute if score @s fronting matches 9 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 10 \
        run team modify player.cosmo prefix {"text":"Virgil ","color":"dark_aqua"}
execute if score @s fronting matches 10 \
        run team modify player.cosmo suffix {"text":" (she/Faer)","color":"gray"}

execute if score @s fronting matches 11 \
        run team modify player.cosmo prefix {"text":"Eleanor ","color":"light_purple"}
execute if score @s fronting matches 11 \
        run team modify player.cosmo suffix {"text":" (fae/Her)","color":"gray"}

scoreboard players enable @s fronting

# grabeltone:front_hazel

execute if score @s fronting matches 0 \
        run team modify player.catgirl suffix {"text":" [hazel] (she/her)","color":"gray"}
execute if score @s fronting matches 1 \
        run team modify player.catgirl suffix {"text":" [void] (she/her)","color":"gray"}
execute if score @s fronting matches 2 \
        run team modify player.catgirl suffix {"text":" [joy] (they/she)","color":"gray"}
execute if score @s fronting matches 3 \
        run team modify player.catgirl suffix {"text":" [helena] (she/her)","color":"gray"}
execute if score @s fronting matches 4 \
        run team modify player.catgirl suffix {"text":" [jackie] (she/her)","color":"gray"}
execute if score @s fronting matches 5 \
        run team modify player.catgirl suffix {"text":" [???] (she/her)","color":"gray"}
scoreboard players enable @s fronting

# grabeltone:phantom_train

execute in minecraft:overworld run tp @s 35 34 0 90 0
loot give @s loot grabeltone:coin
scoreboard players reset @s grabeltone.die
advancement grant @s only grabeltone:ride_phantom_train


# grabeltone:phantom_train_particles
scoreboard players set 640 wishlist.vars 640
scoreboard players set 80 wishlist.vars 80

data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.x double 0.1 run random value 0..640
execute store result storage wishlist:args args.y double 0.1 run random value 0..80
execute store result storage wishlist:args args.z double 0.1 run random value 0..80
function grabeltone:phantom_train_particles_1 with storage wishlist:args args


# grabeltone:phantom_train_particles_1
$particle minecraft:soul_fire_flame ~$(x) ~$(y) ~$(z) 1 0 0 1 0 normal @s


# grabeltone:sync_trades

data modify entity b5dccb72-d3e0-4fe2-b7e4-d9751664fe5c Offers.Recipes[{}]."Paper.IgnoreDiscounts" set value 1b

data modify entity @s[tag=magic_egg_trader] Offers \
        set from entity bfb6f6e9-36d5-4727-b427-6114668963c8 Offers
data modify entity @s[tag=banknote_trader] Offers \
        set from entity b5dccb72-d3e0-4fe2-b7e4-d9751664fe5c Offers
data modify entity @s[tag=bus_driver] Offers \
        set from entity 838d2205-821e-45d6-bf77-970f873030a2 Offers
data modify entity @s[tag=intercity_driver] Offers \
        set from entity b88729cf-aefd-458e-b624-95cd7fcc27ae Offers
data modify entity @s[tag=train_driver] Offers \
        set from entity 42b84071-67be-48fe-acd7-4d52c68a89d0 Offers
data modify entity @s[tag=ferry_driver] Offers \
        set from entity 2c715576-b85a-4dfc-87cf-608f90bf234f Offers
data modify entity @s[tag=box_trader] Offers \
        set from entity 0b098631-f2a2-48ce-a8de-c220dab0fd67 Offers
data modify entity @s[tag=drink_trader] Offers \
        set from entity c107a988-7faf-4696-bc39-fb50ec31405b Offers
data modify entity @s[tag=sushi_trader] Offers \
        set from entity 1df45c69-8d43-4b69-8af7-5783c5798a20 Offers
data modify entity @s[tag=intercity2_driver] Offers \
        set from entity f7d2654b-ff0b-4df8-85cb-914f22539116 Offers
data modify entity @s[tag=server_tools_trader] Offers \
        set from entity 3a1d47d2-1579-42d3-aaf0-60e66767cbec Offers
data modify entity @s[tag=map_tools_trader] Offers \
        set from entity c082feab-1cea-4233-be03-91a32bc1d2e9 Offers
data modify entity @s[tag=bread_trader] Offers \
        set from entity bb3a6920-0d07-4112-93de-5efb87feee71 Offers
data modify entity @s[tag=airship_pilot] Offers \
        set from entity 511ce5b4-b91f-46df-a25b-d8d3db970cb1 Offers
data modify entity @s[tag=intercity3_driver] Offers \
        set from entity ae354d82-985d-44f1-8951-143d17f7ab2a Offers
data modify entity @s[tag=intercity4_driver] Offers \
        set from entity b928b173-5b22-433d-886b-576a1c934e0f Offers
data modify entity @s[tag=grabeltone.quip_trader] Offers \
        set from entity dcd0019d-51bb-481d-af39-bf5ce13aad84 Offers
data modify entity @s[tag=grabeltone.emerald_trader] Offers \
        set from entity b5dccb72-d3e0-4fe2-b7e4-d9751664fe5c Offers
data modify entity @s[tag=grabeltone.ferry_driver_2] Offers.Recipes \
        set from storage grabeltone:args ferry_trades_2

tag @s[tag=magic_egg_trader] add wishlist.server_trader
tag @s[tag=banknote_trader] add wishlist.server_trader
tag @s[tag=bus_driver] add wishlist.server_trader
tag @s[tag=intercity_driver] add wishlist.server_trader
tag @s[tag=train_driver] add wishlist.server_trader
tag @s[tag=ferry_driver] add wishlist.server_trader
tag @s[tag=box_trader] add wishlist.server_trader
tag @s[tag=drink_trader] add wishlist.server_trader
tag @s[tag=sushi_trader] add wishlist.server_trader
tag @s[tag=intercity2_driver] add wishlist.server_trader
tag @s[tag=server_tools_trader] add wishlist.server_trader
tag @s[tag=map_tools_trader] add wishlist.server_trader
tag @s[tag=bread_trader] add wishlist.server_trader
tag @s[tag=airship_pilot] add wishlist.server_trader
tag @s[tag=intercity3_driver] add wishlist.server_trader
tag @s[tag=intercity4_driver] add wishlist.server_trader

data modify entity @s[tag=bus_driver] \
        ArmorItems[3].components.minecraft:custom_model_data set value 271830
data modify entity @s[tag=intercity_driver] \
        ArmorItems[3].components.minecraft:custom_model_data set value 271830
data modify entity @s[tag=intercity2_driver] \
        ArmorItems[3].components.minecraft:custom_model_data set value 271830
data modify entity @s[tag=intercity3_driver] \
        ArmorItems[3].components.minecraft:custom_model_data set value 271830
data modify entity @s[tag=intercity4_driver] \
        ArmorItems[3].components.minecraft:custom_model_data set value 271830

# grabeltone:custom_drops
tag @s add grabeltone.custom_drop.done
execute store result score r wishlist.vars run random value 0..64
execute if score r wishlist.vars matches 0 \
        run data modify entity @s DeathLootTable set value "skip:skip_back"
execute if score r wishlist.vars matches 1 \
        run data modify entity @s DeathLootTable set value "skip:skip_home"
execute if score r wishlist.vars matches 2 \
        run data modify entity @s DeathLootTable set value "skip:skip_player"
execute if score r wishlist.vars matches 3 \
        run data modify entity @s DeathLootTable set value "skip:skip_coords"

# grabeltone:use_ender_eye_in_sketch
advancement revoke @s only grabeltone:use_ender_eye_in_sketch
function skip:back
