" Vim syntax file
" Language:     String Named Binary Tag

if exists("b:current_syntax")
  finish
endif


syn region snbtList matchgroup=snbtBracket start=/\v\[([BIL];)@!/ end=/]/ containedin=snbtList,snbtCompound
syn region snbtByteArray matchgroup=snbtBracket start=/\[B;/ end=/]/ containedin=snbtList,snbtCompound
syn region snbtIntArray matchgroup=snbtBracket start=/\[I;/ end=/]/ containedin=snbtList,snbtCompound
syn region snbtLongArray matchgroup=snbtBracket start=/\[L;/ end=/]/ containedin=snbtList,snbtCompound
syn region snbtCompound matchgroup=snbtBracket start=/{/ end=/}/ containedin=snbtList,snbtCompound

syn match snbtKeyMatch /\v"([^"]|\\\")*"([[:blank:]\r\n]*:)@=/ contains=snbtKey containedin=snbtCompound
syn region snbtKey matchgroup=snbtQuote start=/"/ end=/\v\\@<!"/ contained

syn match snbtKey2Match /\v'([^']|\\\')*'([[:blank:]\r\n]*:)@=/ contains=snbtKey2 containedin=snbtCompound
syn region snbtKey2 matchgroup=snbtQuote start=/'/ end=/\v\\@<!'/ contained

syn match snbtKey3 /\v[[:alnum:]_\.\+\-]+([[:blank:]\r\n]*:)@=/ containedin=snbtCompound

syn match snbtStrMatch /\v"([^"]|\\\")*"([[:blank:]\r\n]*[,}\]])@=/ contains=snbtStr containedin=snbtCompound,snbtList
syn region snbtStr matchgroup=snbtQuote start=/"/ end=/\v\\@<!"/ contained contains=snbtEscape

syn match snbtStr2Match /\v'([^']|\\\')*'([[:blank:]\r\n]*[,}\]])@=/ contains=snbtStr2 containedin=snbtCompound,snbtList
syn region snbtStr2 matchgroup=snbtQuote start=/"/ end=/\v\\@<!'/ contained contains=snbtEscape

syn match snbtStr3 /\v[[:alnum:]_\.\+\-]+([[:blank:]\r\n]*[,}\]])@=/ containedin=snbtCompound,snbtList

syn match snbtByte /\v[+-]?(0|[1-9][0-9]*)[bB]/ containedin=snbtByteArray
syn match snbtInt /\v[+-]?(0|[1-9][0-9]*)/ containedin=snbtIntArray
syn match snbtLong /\v[+-]?(0|[1-9][0-9]*)[lL]/ containedin=snbtLongArray

syn match snbtEscape /\\["\\/bfnrt]/ contained

syn match snbtCutString /\v([:,{[][[:blank:]\r\n]*)@<="([^"]|\\\")*$/ containedin=snbtCompound,snbtList
syn match snbtCutString2 /\v([:,{[][[:blank:]\r\n]*)@<='([^']|\\\')*$/ containedin=snbtCompound,snbtList
syn match snbtTrailingComma /,\_s*[}\]]/ containedin=snbtCompound,snbtList

hi def link snbtKey Identifier
hi def link snbtKey2 Identifier
hi def link snbtKey3 Identifier
hi def link snbtStr String
hi def link snbtStr2 String
hi def link snbtStr3 String
hi def link snbtByte Number
hi def link snbtInt Number
hi def link snbtLong Number

hi def link snbtBracket Delimiter

hi def link snbtEscape Special

hi def link snbtCutString Error
hi def link snbtCutString2 Error
hi def link snbtTrailingComma Error


