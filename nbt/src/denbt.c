// denbt: NBT to SNBT converter

// Copyright (C) 2024 L. Sanz <luis.sanz@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "nbt.h"

// NBT parser
//
// Reads data in NBT format from sdtin and outputs SNBT to stdout
//
struct nbt_parser {
        // streams
        FILE *in;
        FILE *out;

        // indentation control
        int depth;
};

static struct nbt_parser _ct = { 0 };

int8_t nbt_byte(void);
int16_t nbt_short(void);
int32_t nbt_int(void);
int64_t nbt_long(void);
float nbt_float(void);
double nbt_double(void);
void nbt_byte_array(void);
void nbt_int_array(void);
void nbt_long_array(void);
void nbt_string(void);
void nbt_list(void);
void nbt_compound(void);
void nbt_value(int8_t type);

void indent(void)
{
        fprintf(_ct.out, "\n");
        for (int i = 0; i != _ct.depth; ++i)
                fprintf(_ct.out, "  ");
}

void nbt_read(void *p, size_t n)
{
        if (fread(p, n, 1, _ct.in) != 1) {
                //if (feof(_ct.in)) error(-1, 0, "%s: unexpected end of file", stdin_path);
                if (ferror(_ct.in)) error(-1, errno, "%s", stdin_path);
        }
}

int8_t nbt_byte(void)
{
        int8_t r;
        nbt_read(&r, sizeof(r));
        return r;
}

int16_t nbt_short(void)
{
        int16_t r;
        nbt_read(&r, sizeof(r));
        return be16toh(r);
}

int32_t nbt_int(void)
{
        int32_t r;
        nbt_read(&r, sizeof(r));
        return be32toh(r);
}

int64_t nbt_long(void)
{
        int64_t r;
        nbt_read(&r, sizeof(r));
        return be64toh(r);
}

float nbt_float(void)
{
        union { float f; int32_t i; } r;
        nbt_read(&r, sizeof(r));
        r.i = be32toh(r.i);
        return r.f;
}

double nbt_double(void)
{
        union { double f; int64_t i; } r;
        nbt_read(&r, sizeof(r));
        r.i = be64toh(r.i);
        return r.f;
}

void nbt_byte_array(void)
{
        int32_t len = nbt_int();
        fprintf(_ct.out, "[B;");
        while (len--) {
                fprintf(_ct.out, "%"PRId8"b", nbt_byte());
                if (len > 0) fputc(',', _ct.out);
        }
        fputc(']', _ct.out);
}

void nbt_int_array(void)
{
        int32_t len = nbt_int();
        fprintf(_ct.out, "[I;");
        while (len--) {
                fprintf(_ct.out, "%"PRId32, nbt_int());
                if (len > 0) fputc(',', _ct.out);
        }
        fputc(']', _ct.out);
}

void nbt_long_array(void)
{
        int32_t len = nbt_int();
        fprintf(_ct.out, "[L;");
        while (len--) {
                fprintf(_ct.out, "%"PRId64"l", nbt_long());
                if (len > 0) fputc(',', _ct.out);
        }
        fputc(']', _ct.out);
}

void nbt_string(void)
{
        int16_t len = nbt_short();
        fputc('"', _ct.out);
        while (len--) {
                int8_t b = nbt_byte();
                if (b == '"' || b == '\\') fputc('\\', _ct.out);
                fputc(b, _ct.out);
        }
        fputc('"', _ct.out);
}

void nbt_list(void)
{
        int8_t type = nbt_byte();
        int32_t len = nbt_int();
        fputc('[', _ct.out);
        ++_ct.depth;
        for (int32_t i = 0; i != len; ++i) {
                if (i > 0) fputc(',', _ct.out);
                indent();
                nbt_value(type);
        }
        --_ct.depth;
        if (len) indent();
        fputc(']', _ct.out);
}

void nbt_compound(void)
{
        int32_t len = 0;
        fputc('{', _ct.out);
        ++_ct.depth;
        for (;;) {
                int8_t type = nbt_byte();
                if (type == NBT_End) break;
                if (len > 0) fputc(',', _ct.out);
                indent();
                nbt_string();
                fprintf(_ct.out, ":");
                nbt_value(type);
                ++len;
        }
        --_ct.depth;
        if (len) indent();
        fputc('}', _ct.out);
}

void nbt_value(int8_t type)
{
        switch (type) {
         case NBT_Byte: fprintf(_ct.out, "%"PRId8"b", nbt_byte()); return;
         case NBT_Short: fprintf(_ct.out, "%"PRId16"s", nbt_short()); return;
         case NBT_Int: fprintf(_ct.out, "%"PRId32, nbt_int()); return;
         case NBT_Long: fprintf(_ct.out, "%"PRId64"l", nbt_long()); return;
         case NBT_Float: fprintf(_ct.out, "%gf", nbt_float()); return;
         case NBT_Double: fprintf(_ct.out, "%lgd", nbt_double()); return;
         case NBT_Byte_Array: nbt_byte_array(); return;
         case NBT_String: nbt_string(); return;
         case NBT_List: nbt_list(); return;
         case NBT_Compound: nbt_compound(); return;
         case NBT_Int_Array: nbt_int_array(); return;
         case NBT_Long_Array: nbt_long_array(); return;
         default: error(-1, 0, "%s: unrecognized NBT type (%d)", stdin_path, type);
        }
}

// Application entrypoint
//
int main(int argc, char* argv[])
{
        parse_args(argc, argv, &_ct.in, &_ct.out);
        nbt_compound();
        return EXIT_SUCCESS;
}

