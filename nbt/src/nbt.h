
#ifndef WISHLIST_NBT_H_
#define WISHLIST_NBT_H_

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <inttypes.h>

#if defined(unix)
#include <endian.h>
#include <error.h>
#include <unistd.h>
#elif defined(_WIN32)
#if __BIG_ENDIAN__
#define htobe16(x) (x)
#define htobe32(x) (x)
#define htobe64(x) (x)
#else
static inline int16_t htobe16(int16_t x)
{
        return ((x & 0xff) << 8) +
                ((x & 0xff00) >> 8);
}
static inline int32_t htobe32(int32_t x)
{
        return ((x & 0xff) << 24) +
                ((x & 0xff00) << 8) +
                ((x & 0xff0000) >> 8) +
                ((x & 0xff000000) >> 24);
}
static inline int64_t htobe64(int64_t x)
{
        return ((x & 0xff) << 56) +
                ((x & 0xff00) << 40) +
                ((x & 0xff0000) << 24) +
                ((x & 0xff000000) << 8) +
                ((x & 0xff00000000) >> 8) +
                ((x & 0xff0000000000) >> 24) +
                ((x & 0xff000000000000) >> 40) +
                ((x & 0xff00000000000000) >> 56);
}
#endif
#define be16toh(x) htobe16(x)
#define be32toh(x) htobe32(x)
#define be64toh(x) htobe64(x)

static inline void error(int status, int errnum, const char *format, ...)
{
        fflush(stdout);
        fprintf(stderr, "foo: ");
        va_list ap;
        va_start(ap, format);
        vfprintf(stderr, format, ap);
        va_end(ap);
        if (errnum)
                fprintf(stderr, ": %s", strerror(errnum));
        if (status)
                exit(status);
}

#define isspace(x) (strchr(" \t\r\n\v", x) == NULL)

#endif

enum {
        NBT_End = 0,
        NBT_Byte = 1,
        NBT_Short = 2,
        NBT_Int = 3,
        NBT_Long = 4,
        NBT_Float = 5,
        NBT_Double = 6,
        NBT_Byte_Array = 7,
        NBT_String = 8,
        NBT_List = 9,
        NBT_Compound = 10,
        NBT_Int_Array = 11,
        NBT_Long_Array = 12,
};

const char *stdin_path = "<stdin>";
const char *stdout_path = "<stdout>";

void parse_args(int argc, char *argv[], FILE **in, FILE **out)
{
        if (argc > 1 && strcmp(argv[1], "-") != 0) {
                stdin_path = argv[1];
                *in = fopen(stdin_path, "r");
                if (*in == NULL) error(-1, errno, "'%s'", stdin_path);
        } else {
                *in = stdin;
        }

        if (argc > 2) {
                stdout_path = argv[2];
                *out = fopen(stdout_path, "w");
                if (*out == NULL) error(-1, errno, "'%s'", stdout_path);
        } else {
                *out = stdout;
        }
}

#endif // WISHLIST_NBT_H_
