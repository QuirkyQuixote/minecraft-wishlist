// renbt: SNBT to NBT converter

// Copyright (C) 2024 L. Sanz <luis.sanz@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <ctype.h>
#include <stdlib.h>
#include <stdnoreturn.h>

#include "nbt.h"

// SNBT parser
//
// Reads data in SNBT format from stdin and output NBT to stdout
//
enum {
        NBT_List_End = -1,
        NBT_Compound_End = -2,
        NBT_Comma = -3,
        NBT_Colon = -4,
};

static char snbt_tok_false[] = "0b";
static char snbt_tok_true[] = "1b";

struct snbt_parser {
        // streams
        FILE *in;
        FILE *out;

        // The text for the token being read
        char *tok;
        char *tok_end;

        // Read buffer because we want the full file for stuff
        char *rbuf;
        char *rpos;
        char *rend;
        char *rcap;

        // Write buffer because we need to sometimes go back and update data
        char *wbuf;
        char *wpos;
        char *wend;
};

static struct snbt_parser _ct = { NULL };

noreturn void snbt_error(const char *format, ...);
void snbt_write(const void *buf, size_t len);
int snbt_scan_symbol(void);
int snbt_scan_string(void);
int snbt_scan_list(void);
int snbt_scan(void);
void snbt_byte(const char *i, const char *s);
void snbt_short(const char *i, const char *s);
void snbt_int(const char *i, const char *s);
void snbt_long(const char *i, const char *s);
void snbt_float(const char *i, const char *s);
void snbt_double(const char *i, const char *s);
void snbt_byte_array(const char *i, const char *s);
void snbt_string(const char *i, const char *s);
void snbt_list(const char *i, const char *s);
void snbt_compound(const char *i, const char *s);
void snbt_int_array(const char *i, const char *s);
void snbt_long_array(const char *i, const char *s);
void snbt_parse(void);

static void(* snbt_value[])(const char *, const char *) = {
        NULL,
        snbt_byte,
        snbt_short,
        snbt_int,
        snbt_long,
        snbt_float,
        snbt_double,
        snbt_byte_array,
        snbt_string,
        snbt_list,
        snbt_compound,
        snbt_int_array,
        snbt_long_array,
};

// Print an error related to the current (unterminated) token and exit
//
// The token starts at the `_ct.tok` pointer and ends at `_ct.rpos`
// `format` is the printf format for the rest of the arguments
// The final message looks as follows:
//
// <stdin_path>:<line>:<column>: <formatted message>
//   | code before the TOKEN and after too
//   |                 ^^^^^

static inline char *min(char *a, char *b) { return a < b ? a : b; }
static inline char *max(char *a, char *b) { return a > b ? a : b; }

noreturn void snbt_error(const char *format, ...)
{
        static const char whitespace[] =
                "                                "
                "                                "
                "                                "
                "                                ";
        static const char underline[] =
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^";

        int lineno = 1;
        char *line = _ct.rbuf;
        for (char *p = _ct.rbuf; p != _ct.rpos; ++p) {
                if (*p == '\n') {
                        ++lineno;
                        line = p + 1;
                }
        }

        char *line_end = _ct.rpos;
        while (line_end != _ct.rend && *line_end != '\n')
                ++line_end;

        if (line_end - line > 80) {
                line = max(line, _ct.tok - 40);
                line_end = min(line_end, _ct.rpos + 40);
        }

        fprintf(stderr, "%s:%d:%d: ", stdin_path, lineno, (int)(_ct.tok - line));
        va_list ap;
        va_start(ap, format);
        vfprintf(stderr, format, ap);
        va_end(ap);
        fprintf(stderr, "\n");
        fprintf(stderr, "  | %.*s\n", (int)(line_end - line), line);
        fprintf(stderr, "  | %.*s%.*s\n",
                        (int)(_ct.tok - line), whitespace,
                        (int)(_ct.rpos - _ct.tok), underline);
        exit(EXIT_FAILURE);
}

// Duplicate the size of a buffer, or allocate a couple bytes if empty

void snbt_grow(char **buf, char **pos, char **end)
{
        size_t off = *pos - *buf;
        size_t len = *end - *buf;
        len = len ? len * 2 : 2;
        *buf = realloc(*buf, len);
        *pos = *buf + off;
        *end = *buf + len;
}

// Slurp the entire input into a buffer so we can do whatever we want with it.

void snbt_read(void)
{
        for (;;) {
                snbt_grow(&_ct.rbuf, &_ct.rend, &_ct.rcap);
                _ct.rend += fread(_ct.rend, 1, _ct.rcap - _ct.rend, _ct.in);
                if (feof(_ct.in)) break;
                if (ferror(_ct.in)) error(-1, errno, "'%s'", stdin_path);
        }
        _ct.rpos = _ct.rbuf;
}

// Can't directly write to _ct.out because the list size is not know
// until after all elements have been parsed, so we will generate the data in a
// local buffer, then dump it all at once.

void snbt_write(const void *buf, size_t len)
{
        const char *p = buf;
        while (len--) {
                if (_ct.wpos == _ct.wend)
                        snbt_grow(&_ct.wbuf, &_ct.wpos, &_ct.wend);
                *_ct.wpos++ = *p++;
        }
}

// SNBT parsing is split into scanner and parser stages; the scanner splits the
// input into tokens and stores the string in _ct.wbuf, returning the type of
// the parsed token as return value.

int snbt_scan_string(void)
{
        char quote = *_ct.rpos;
        _ct.tok = _ct.rpos;
        for (;;) {
                ++_ct.rpos;
                if (*_ct.rpos == '\n' || *_ct.rpos == '\r')
                        snbt_error("missing quote at end of string");
                if (*_ct.rpos == '\\') {
                        ++_ct.rpos;
                } else if (*_ct.rpos == quote) {
                        _ct.tok_end = ++_ct.rpos;
                        return NBT_String;
                }
        }
}

int snbt_scan_symbol(void)
{
        static const int group[] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 4, 0,
                2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0,
                0,15, 5,10, 9,11, 8,10,10,10,10,10, 7,10,10,10,
               10,10,13, 6,12,14,10,10,10,10,10, 0, 0, 0, 0,10,
                0,15, 5,10, 9,11, 8,10,10,10,10,10, 7,10,10,10,
               10,10,13, 6,12,14,10,10,10,10,10, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        };

        static const int trans[][16] = {
                // ! +-  0 1-9 .  b  s  l  f  d  *  e  t  r  u  a
                { -1, 1, 2, 3, 4,11,11,11,19,11,11,11,15,11,11,11 },
                { -8,11, 2, 3,11,11,11,11,11,11,11,11,11,11,11,11 }, // [+-]
                { -4,11,11,11, 5, 6, 7, 8, 9,10,11,12,11,11,11,11 }, // [+-]?0
                { -4,11, 3, 3, 5, 6, 7, 8, 9,10,11,12,11,11,11,11 }, // [+-]?[1-9][0-9]*
                { -8,11, 5, 5,11,11,11,11,11,11,11,12,11,11,11,11 }, // fp point
                { -7,11, 5, 5,11,11,11,11, 9,10,11,12,11,11,11,11 }, // fp 0-9
                { -2,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // Byte
                { -3,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // Short
                { -5,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // Long
                { -6,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // Float
                { -7,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // Double
                { -8,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // String
                { -8,13,14,14,11,11,11,11,11,11,11,11,11,11,11,11 }, // fp exp
                { -8,11,13,13,11,11,11,11,11,11,11,11,11,11,11,11 }, // fp exp sign
                { -7,11,14,14,11,11,11,11, 9,10,11,11,11,11,11,11 }, // fp exp num
                { -8,11,11,11,11,11,11,11,11,11,11,11,11,16,11,11 }, // t
                { -8,11,11,11,11,11,11,11,11,11,11,11,11,11,17,11 }, // r
                { -8,11,11,11,11,11,11,11,11,11,11,18,11,11,11,11 }, // u
                { -9,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // True
                { -8,11,11,11,11,11,11,11,11,11,11,11,11,11,11,20 }, // f
                { -8,11,11,11,11,11,11,21,11,11,11,11,11,11,11,11 }, // a
                { -8,11,11,11,11,11,22,11,11,11,11,11,11,11,11,11 }, // l
                { -8,11,11,11,11,11,11,11,11,11,11,23,11,11,11,11 }, // s
                {-10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11 }, // False
        };

        _ct.tok = _ct.rpos;
        int state = 0;
        for (;;) {
                switch ((state = trans[state][group[(int)*_ct.rpos]])) {
                 case -1: _ct.tok_end = _ct.rpos; snbt_error("expected symbol");
                 case -2: _ct.tok_end = _ct.rpos; return NBT_Byte;
                 case -3: _ct.tok_end = _ct.rpos; return NBT_Short;
                 case -4: _ct.tok_end = _ct.rpos; return NBT_Int;
                 case -5: _ct.tok_end = _ct.rpos; return NBT_Long;
                 case -6: _ct.tok_end = _ct.rpos; return NBT_Float;
                 case -7: _ct.tok_end = _ct.rpos; return NBT_Double;
                 case -8: _ct.tok_end = _ct.rpos; return NBT_String;
                 case -9: _ct.tok_end = (_ct.tok = snbt_tok_true) + 2; return NBT_Byte;
                 case -10: _ct.tok_end = (_ct.tok = snbt_tok_false) + 2; return NBT_Byte;
                }
                ++_ct.rpos;
        }
        snbt_error("unexpected end of text");
}

int snbt_scan_list(void)
{
        _ct.tok = _ct.rpos;
        if (_ct.tok[2] == ';') {
                switch (_ct.tok[1]) {
                 case 'B':
                        _ct.tok_end = (_ct.rpos += 3);
                        return NBT_Byte_Array;
                 case 'I':
                        _ct.tok_end = (_ct.rpos += 3);
                        return NBT_Int_Array;
                 case 'L':
                        _ct.tok_end = (_ct.rpos += 3);
                        return NBT_Long_Array;
                 default:
                        _ct.tok_end = (_ct.rpos += 3);
                        snbt_error("invalid array type", _ct.tok[1]);
                }
        }
        _ct.tok_end = ++_ct.rpos;
        return NBT_List;
}

int snbt_scan(void)
{
        while (isspace(*_ct.rpos))
                ++_ct.rpos;
        switch (*_ct.rpos) {
         case '{': ++_ct.rpos; return NBT_Compound;
         case '}': ++_ct.rpos; return NBT_Compound_End;
         case '[': return snbt_scan_list();
         case ']': ++_ct.rpos; return NBT_List_End;
         case ',': ++_ct.rpos; return NBT_Comma;
         case ':': ++_ct.rpos; return NBT_Colon;
         case '"':
         case '\'': return snbt_scan_string();
         case '0' ... '9':
         case 'a' ... 'z':
         case 'A' ... 'Z':
         case '-':
         case '.':
         case '+':
         case '_': return snbt_scan_symbol();
         default: break;
        }
        _ct.tok = _ct.rpos++;
        snbt_error("unexpected character");
}

// The actual SNBT parser begins here; there's a different function to parse
// the *value* of each item.
//
// Despite what the NBT documentation seems to imply, the full tag definition,
// with typeid and name is managed in different ways for lists and compounds,
// so those functions handle that logic.

void snbt_byte(const char *i, const char *s)
{
        int8_t r;
        sscanf(i, "%"SCNd8, &r);
        snbt_write(&r, sizeof(r));
}

void snbt_short(const char *i, const char *s)
{
        int16_t r;
        sscanf(i, "%"SCNd16, &r);
        r = htobe16(r);
        snbt_write(&r, sizeof(r));
}

void snbt_int(const char *i, const char *s)
{
        int32_t r;
        sscanf(i, "%"SCNd32, &r);
        r = htobe32(r);
        snbt_write(&r, sizeof(r));
}

void snbt_long(const char *i, const char *s)
{
        int64_t r;
        sscanf(i, "%"SCNd64, &r);
        r = htobe64(r);
        snbt_write(&r, sizeof(r));
}

void snbt_float(const char *i, const char *s)
{
        union { float f; int32_t i; } r;
        sscanf(i, "%g", &r.f);
        r.i = htobe32(r.i);
        snbt_write(&r, sizeof(r));
}

void snbt_double(const char *i, const char *s)
{
        union { double f; int64_t i; } r;
        sscanf(i, "%lg", &r.f);
        r.i = htobe64(r.i);
        snbt_write(&r, sizeof(r));
}

void snbt_string(const char *i, const char *s)
{
        if (*i == '"' || *i == '\'') {
                ++i;
                --s;
        }
        size_t len_off = _ct.wpos - _ct.wbuf;
        int16_t len = 0;
        snbt_write(&len, sizeof(len));
        while (i != s) {
                if (*i == '\\') ++i;
                snbt_write(i, 1);
                ++len;
                ++i;
        }
        len = htobe16(len);
        memcpy(_ct.wbuf + len_off, &len, sizeof(len));
}

void snbt_list(const char *i, const char *s)
{
        int8_t type = snbt_scan();
        int32_t len = 0;
        if (type == NBT_List_End) {
                type = NBT_End;
                snbt_write(&type, 1);
                snbt_write(&len, sizeof(len));
                return;
        }
        if (type < 0) snbt_error("invalid element at start of list (%d)", type);
        snbt_write(&type, 1);
        size_t len_off = _ct.wpos - _ct.wbuf;
        snbt_write(&len, sizeof(len)); // temporary
        for (;;) {
                snbt_value[type](_ct.tok, _ct.tok_end);
                ++len;
                switch (snbt_scan()) {
                 case NBT_List_End:
                        len = htobe32(len);
                        memcpy(_ct.wbuf + len_off, &len, sizeof(len));
                        return;
                 case NBT_Comma: break;
                 default: snbt_error("expected ',' or ']' at end of list");
                }
                if (snbt_scan() != type) snbt_error("mismatched types in list");
        }
}

void snbt_compound(const char *i, const char *s)
{
        int tok = snbt_scan();
        if (tok == NBT_Compound_End) {
                tok = NBT_End;
                snbt_write(&tok, 1);
                return;
        }
        for (;;) {
                switch (tok) {
                 case NBT_Byte:
                 case NBT_Short:
                 case NBT_Int:
                 case NBT_Long:
                 case NBT_Float:
                 case NBT_Double:
                 case NBT_String:
                        break;
                 default:
                        snbt_error("invalid compound key");
                }
                const char *name = _ct.tok;
                const char *name_end = _ct.tok_end;
                if (snbt_scan() != NBT_Colon) snbt_error("expected ':' after compound key");
                tok = snbt_scan();
                snbt_write(&tok, 1);
                snbt_string(name, name_end);
                snbt_value[tok](_ct.tok, _ct.tok_end);
                switch (snbt_scan()) {
                 case NBT_Compound_End:
                        tok = NBT_End;
                        snbt_write(&tok, 1);
                        return;
                 case NBT_Comma:
                        break;
                 default:
                        snbt_error("expected ',' or '}' at end of compound");
                }
                tok = snbt_scan();
        }
}

void snbt_byte_array(const char *i, const char *s)
{
        int8_t type = snbt_scan();
        int32_t len = 0;
        if (type == NBT_List_End) {
                type = NBT_End;
                snbt_write(&len, sizeof(len));
                return;
        }
        size_t len_off = _ct.wpos - _ct.wbuf;
        snbt_write(&len, sizeof(len)); // temporary
        for (;;) {
                if (type != NBT_Byte) snbt_error("mismatched element in byte array");
                snbt_byte(_ct.tok, _ct.tok_end);
                ++len;
                switch (snbt_scan()) {
                 case NBT_List_End:
                        len = htobe32(len);
                        memcpy(_ct.wbuf + len_off, &len, sizeof(len));
                        return;
                 case NBT_Comma: break;
                 default: snbt_error("expected ',' or ']' at end of byte array");
                }
                type = snbt_scan();
        }
}

void snbt_int_array(const char *i, const char *s)
{
        int8_t type = snbt_scan();
        int32_t len = 0;
        if (type == NBT_List_End) {
                type = NBT_End;
                snbt_write(&len, sizeof(len));
                return;
        }
        size_t len_off = _ct.wpos - _ct.wbuf;
        snbt_write(&len, sizeof(len)); // temporary
        for (;;) {
                if (type != NBT_Int) snbt_error("mismatched element in int array");
                snbt_int(_ct.tok, _ct.tok_end);
                ++len;
                switch (snbt_scan()) {
                 case NBT_List_End:
                        len = htobe32(len);
                        memcpy(_ct.wbuf + len_off, &len, sizeof(len));
                        return;
                 case NBT_Comma: break;
                 default: snbt_error("expected ',' or ']' at end of int array");
                }
                type = snbt_scan();
        }
}

void snbt_long_array(const char *i, const char *s)
{
        int8_t type = snbt_scan();
        int32_t len = 0;
        if (type == NBT_List_End) {
                type = NBT_End;
                snbt_write(&len, sizeof(len));
                return;
        }
        size_t len_off = _ct.wpos - _ct.wbuf;
        snbt_write(&len, sizeof(len)); // temporary
        for (;;) {
                if (type != NBT_Long) snbt_error("mismatched element in long array");
                snbt_long(_ct.tok, _ct.tok_end);
                ++len;
                switch (snbt_scan()) {
                 case NBT_List_End:
                        len = htobe32(len);
                        memcpy(_ct.wbuf + len_off, &len, sizeof(len));
                        return;
                 case NBT_Comma: break;
                 default: snbt_error("expected ',' or ']' at end of long array");
                }
                type = snbt_scan();
        }
}

void snbt_parse(void)
{
        snbt_read();
        int tok = snbt_scan();
        if (tok != NBT_Compound)
                snbt_error("expected compound as root element");
        snbt_compound(NULL, NULL);
        fwrite(_ct.wbuf, _ct.wpos - _ct.wbuf - 1, 1, _ct.out);
}

// Application entrypoint
//
int main(int argc, char* argv[])
{
        parse_args(argc, argv, &_ct.in, &_ct.out);
        snbt_parse();
        return EXIT_SUCCESS;
}

