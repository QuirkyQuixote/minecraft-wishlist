NBT
===

Tools to manitpulate NBT files

Requirements
------------

* C compiler

Usage
-----

Provides three applications:

### denbt

De-NBT: transforms NBT binary data into SNBT text-based data that can be edited
by humans.

```
denbt
denbt <nbt file>
denbt <nbt file> <snbt file>
denbt - <snbt file>
```

1. reads from stdin, writes to stdout
2. reads from `<nbt file>`, writes to stdout
3. reads from stdin, writes to `<snbt file>`
4. reads from stdin, writes to stdout

It will *not* decompress files; in a linux shell you can pipe the input through
`zcat` to do this:

```
zcat <nbt file> | denbt ...
```

### renbt

Re-NBT: transforms SNBT readable by humans back to NBT that can be used by
programs.

```
renbt
renbt <nbt file>
renbt <nbt file> <snbt file>
renbt - <snbt file>
```

1. reads from stdin, writes to stdout
2. reads from `<nbt file>`, writes to stdout
3. reads from stdin, writes to `<snbt file>`
4. reads from stdin, writes to stdout

It will *not* compress files; in a linux shell you can pipe the output through
`gzip` to do this:

```
denbt <snbt file> | gzip > <nbt file>
```

### nbt

Edits NBT files (compressed or not) as SNBT, then saves the changes.

```
nbt <nbt file>
```

This is a bash script that wraps around denbt and renbt to allow you editing
the files; it will use whichever editor is defined in the `$EDITOR` environment
variable.

* If the NBT file can't be parsed into SNBT the editor will fail to open.
* If the editor is closed without changing the SNBT code, the program will not
  even try to parse it and the NBT file will remain untouched.
* If the SNBT code can't be parsed into NBT the file will not be modified, and
  the changes will be lost.

### Syntax files for vim

The project provides a set of syntax files to color snbt code in vim and nvim;
those can be installed by using the Makefile:

Install in `$HOME/.vim/`:

```
make vim
```

Install in `$HOME/.config/nvim`:

```
make nvim
```

### The NBT format

NBT represents structured data in a portable binary format.

All numbers, both integers and floating-point, are in big endian.

Values can be any of the following:

```
 1. Byte:       single byte
 2. Short:      16-bit integer
 3. Int:        32-bit integer
 4. Long:       64-bit integer
 5. Float:      32-bit floating-point number
 6. Double:     64-bit floating-point number
 7. Byte Array:
    * Int: size
    * Byte[size]: values
 8. String:
    * Short: size
    * Byte[size]: data
 9. List:
    * Byte: type
    * Int: size
    * typeof(type)[size]: values
10. Compound:
    * Field[]: data
        * Byte: type
        * String: key
        * typeof(type): value
    * the sequence ends on a null Byte.
11. Int Array:
    * Int: size
    * Int[size]: values
12. Long Array:
    * Int: size
    * Long[size]: values
```

The root value of the file *must* be a Compound, and experimentation says that
it should not have a terminating null Byte.

### The SNBT format

SNBT represents NBT data in a human-readable way:

Values are translated as follows:


```
 1. Byte:       [+-]?(0|[1-9][0-9]*)[bB] (*)
 2. Short:      [+-]?(0|[1-9][0-9]*)[sS]
 3. Int:        [+-]?(0|[1-9][0-9]*)
 4. Long:       [+-]?(0|[1-9][0-9]*)[lL]
 5. Float:      [+-]?((0|[1-9][0-9]*)(\.[0-9]*)?|\.[0-9]+)([eE][+-]?[0-9]+)?[fF]
 6. Double:     [+-]?((0|[1-9][0-9]*)(\.[0-9]*)?|\.[0-9]+)([eE][+-]?[0-9]+)?[dD]?
 7. Byte Array: [B;<Byte>,<Byte>,...]
 8. String:     "(\.|[^\"])*"|'(\.|[^\'])*'|[0-9a-zA-Z_.+-]+ (**)
 9. List:       [<value>,<value>,...] (***)
10. Compound:   {<key>:<value1>,<key>:<value2>,...} (****)
11. Int Array:  [I;<Int>,<Int>,...]
12. Long Array: [L;<Long>,<Long>,...]

   * The literals "true" and "false" (with any capitalization) are aliases for
     the values 1b and 0b respectively.
  ** Unquoted strings are only strings if they are *not* numbers.
 *** All values must be of the same type
**** Keys can be any of Byte, Short, Int, Long, Float, Double, String, but they
     will be cast to string in the compound; each value can be of any type.
```

Known Issues
------------

I don't know what minecraft is supposed to be doing to parse numbers, but there
are some weird particular cases:

* As far as I can ascertain, numbers like "1e1", "1e+1", "1e-1" do not parse as
  actual floating point numbers. To make them actually parse you have to either
  add a decimal point before the 'e' ("1.e1") or a float/double precision tag
  ("1e1d", "1e1f"); this tool does *not* implement that limitation.

