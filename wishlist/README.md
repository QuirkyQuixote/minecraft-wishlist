Quirky's Minecraft Wishlist - Core
==================================

The core datapack for Quirky's Minecraft Wishlist. It provides a number of
elements used by the rest of the datapacks in the collection.

Functions
---------

* `wishlist:decrement_item_entity_count`: substract one from the count field of
  `@s` that must be of type `minecraft:item`.

* `wishlist:find_entity_by_uuid`: finds the entity whose UUID matches the value
  stored in the storage `wishlist:args` at `uuid` and tags it with
  `wishlist.match`.

* `wishlist:find_player_by_uuid`: finds the player whose UUID matches the value
  stored in the storage `wishlist:args` at `uuid` and tags it with
  `wishlist.match`.

* `wishlist:throw_item`: summons an item at entity the target position with
  type set from the storage `wishlist:args` at `Item`.

* `wishlist:throw_stack`: summons an item entity at the target position with
  type set from the storage `wishlist:args` at `Item`. The amount of items at
  the resulting stack is set to the value in the score `ssize wishlist.`; if
  `ssize` is bigger than then maximum size of an stack of `Item`, it is clamped
  to the maximum stack size of it.

* `wishlist:rand`: generates the next number in a pseudorandom sequence using a
  simple linear congruent generator. State of the rng is at score `rand
  wishlist.` that can be used both to provide a seed before calling the
  function and to read the resulting number after it.

* `wishlist:rotate`: rotates the list at the storage `wishlist:args` at `elems`
  by the amounts of spaces specified in the score `wishlist.` with the name
  `pivot`.

* `wishlist:rotate_random`: rotates the list at the storage `wishlist:args` at
  `elems` by the a random amount between 0 and its own size minus 1.

* `wishlist:recall`: fills the contents of `wishlist:args` at `memory` from the
  tags of a stone button worn on the head armor slot of the target entity,
  allowing for any entity to store additional data.  Will create the button if
  it does not exist.

* `wishlist:memorize`: stores the contents of `wishlist:args` at `memory` into
  the tags of a stone button worn on the head armor slot of the target entity,
  allowing for any entity to store additional data.  It will not create the
  button if if does not exist; use `wishlist:recall` first to ensure its
  existence.

* `wishlist:skin_villager_by_biome`: Updates a villager's skin to match the one
  that it would have if it had naturally spawned in the biome it stands on.

* `wishlist:dismiss`: dismisses as silently as possible any targeted entity
  alongside every one of its passengers, recursively.

* `wishlist:stash_inventory`: copies a player's inventory into the playerId
  field in the `wishlist:inventory` storage; does not modify the actual
  player's inventory.

* `wishlist:restore_inventory`: restores a player's inventory from the playerId
  fieled in the `wishlist:inventory` storage; will destroy every single item
  currently held by the player.

Function Tags
-------------

* `#wishlist:crouch`: all functions tagged thus will be called whenever a
  player crouches, with the player as executor.

* `#wishlist:login`: all functions tagged thus will be called whenever a player
  logs in to the server, with the player as executor.

Advancements
------------

Adding this datapack provides the root wishlist advancement and some others
hanging from it:

* Wishlist: log into a server using Quirky's Wishlist
    * Wake Up: become a contributor to Quirky's Wishlist
