# wishlist:load
scoreboard objectives add wishlist.vars dummy
scoreboard objectives add wishlist.playerId dummy
scoreboard objectives add wishlist.employer dummy
scoreboard objectives add wishlist.source trigger
scoreboard objectives add wishlist.join minecraft.custom:minecraft.leave_game
scoreboard objectives add wishlist.sneak_time minecraft.custom:minecraft.sneak_time
scoreboard objectives add wishlist.last_sneak_time dummy
scoreboard objectives add wishlist.x dummy
scoreboard objectives add wishlist.y dummy
scoreboard objectives add wishlist.z dummy
scoreboard objectives add wishlist.yaw dummy
scoreboard objectives add wishlist.pitch dummy
scoreboard objectives add wishlist.facing dummy
scoreboard objectives add wishlist.dimension dummy
scoreboard objectives add wishlist.rx dummy
scoreboard objectives add wishlist.rz dummy
scoreboard objectives add wishlist.cx dummy
scoreboard objectives add wishlist.cy dummy
scoreboard objectives add wishlist.cz dummy
scoreboard objectives add wishlist.used_carrot_on_a_stick minecraft.used:minecraft.carrot_on_a_stick

execute unless score $next wishlist.playerId matches 0.. \
        run scoreboard players set $next wishlist.playerId 1

scoreboard players set rand_multiplier wishlist.vars 1103515245
scoreboard players set rand_increment wishlist.vars 12345
scoreboard players set rand_modulus wishlist.vars 1073741824

scoreboard players set 0 wishlist.vars 0
scoreboard players set 1 wishlist.vars 1
scoreboard players set 2 wishlist.vars 2
scoreboard players set 3 wishlist.vars 3
scoreboard players set 4 wishlist.vars 4
scoreboard players set 5 wishlist.vars 5
scoreboard players set 6 wishlist.vars 6
scoreboard players set 7 wishlist.vars 7
scoreboard players set 8 wishlist.vars 8
scoreboard players set 9 wishlist.vars 9
scoreboard players set 10 wishlist.vars 10
scoreboard players set 16 wishlist.vars 16
scoreboard players set 32 wishlist.vars 32
scoreboard players set 64 wishlist.vars 64
scoreboard players set 100 wishlist.vars 100

data modify storage wishlist:args all_chatter set value [ \
        "Hi!", \
        "Howdy?", \
        "Did you break the weather in the upworld?", \
        "Oh, sorry", \
        "Never mind me", \
        "I told them but they don't listen!", \
        "Step carefully", \
        "Nice duds!", \
        "Ohh, I like your face; very chic", \
        "Hm?", \
        "You know? I actually like this weather", \
        "Ready?", \
        "They're here, they're there, they're everywhere", \
        "I always come back here; it's nice", \
        "Good judgement comes from bad experiences", \
        "Bad experiences come from bad judgement", \
        "I could do this all day", \
        "Do you think even the worst person can change?", \
        "It's a beautiful day outside", \
        "Chickens are clucking", \
        "Parrots are talking", \
        "Flowers are blooming", \
        "Would you rather fight a hundred chicken-sized zombies or a zombie-sized chicken?", \
        "This is the way; dunno where it goes…", \
        "I hate sand: you touch it and it falls on you and you suffocate", \
        "My feet are sore", \
        "So what's a 'pesky bird', anyway?", \
        "I heard it was the person in the chicken costume", \
        "Trans rights!" \
        ]

function wishlist:tick_1t
function wishlist:tick_1s

# wishlist:tick_1s

schedule function wishlist:tick_1s 1s
execute at @a as @e[tag=wishlist.cash_register,distance=..8] \
        run function wishlist:cash_register/tick
execute at @a as @e[tag=wishlist.globe,distance=..8] \
        run function wishlist:globe/tick

tag b51be5e1-572c-3a8e-9802-4d2b53d1970a add wishlist.contributor
tag 3abea40c-657f-34b8-9163-618844710e72 add wishlist.contributor
tag 7ac5a819-98d0-3a41-96c1-38ad0643ff7b add wishlist.contributor
tag pipar_ add wishlist.contributor
tag Stigstille add wishlist.contributor

advancement grant @a[tag=wishlist.contributor] only wishlist:wake_up

# wishlist:tick_1t

schedule function wishlist:tick_1t 1t

execute as @a[scores={wishlist.source=1..}] \
        run function wishlist:link_source
scoreboard players reset @a wishlist.source
scoreboard players enable @a wishlist.source

execute as @a unless score @s wishlist.join matches 0 \
        run function #wishlist:login
scoreboard players set @a wishlist.join 0

execute as @a run function wishlist:sneak
execute as @a at @s run function wishlist:store_position

execute as @a[scores={wishlist.used_carrot_on_a_stick=1..}] \
        at @s \
        run function #wishlist:used_carrot_on_a_stick
scoreboard players reset * wishlist.used_carrot_on_a_stick

# wishlist:consume_level
execute if entity @s[gamemode=creative] run return 1
execute store result score level wishlist.vars run xp query @s levels
execute if score level wishlist.vars matches 0 run return fail
xp add @s -1 levels
return 1

# wishlist:damage_held_item
execute if entity @a[gamemode=creative] run return 0
execute if predicate wishlist:holding_unbreaking_3 \
        unless predicate wishlist:random_25 run return fail
execute if predicate wishlist:holding_unbreaking_2 \
        unless predicate wishlist:random_33 run return fail
execute if predicate wishlist:holding_unbreaking_1 \
        unless predicate wishlist:random_50 run return fail

scoreboard players set dmg wishlist.vars -1000000
execute store result score max_uses wishlist.vars \
        run data get entity @s SelectedItem.components.minecraft:max_damage
scoreboard players operation dmg wishlist.vars /= max_uses wishlist.vars
execute store result storage wishlist:vars damage double 0.000001 \
        run scoreboard players get dmg wishlist.vars
item modify entity @s weapon.mainhand wishlist:damage

execute store result score uses wishlist.vars \
        run data get entity @s SelectedItem.components.minecraft:damage
execute unless score uses wishlist.vars >= max_uses wishlist.vars run return 0
item replace entity @s weapon.mainhand with minecraft:air
playsound minecraft:entity.item.break player @a ~ ~ ~

# wishlist:dismiss
execute on passengers run function wishlist:dismiss
ride @s dismount
data merge entity @s {CustomName:'""',DeathLootTable:"minecraft:empty"}
data remove entity @s CustomName
tp @s 0 0 0
kill @s

# wishlist:get_entity_type
function wishlist:get_entity_type_1
$data modify $(dst) set from storage wishlist:args entity_type

# wishlist:give_player_id
execute if score @s wishlist.playerId matches 1.. run return fail
scoreboard players operation @s wishlist.playerId = $next wishlist.playerId
scoreboard players add $next wishlist.playerId 1

# wishlist:link_source
tellraw @s [ \
        { \
          "translate":"This server uses %s", \
          "with":[ \
            { \
              "text":"Quirky's Wishlist", \
              "color":"blue", \
              "underlined":true, \
              "clickEvent":{ \
                "action":"open_url", \
                "value":"https://gitlab.com/QuirkyQuixote/minecraft-wishlist" \
                }, \
              "hoverEvent":{ \
                "action":"show_text", \
                "value":"https://gitlab.com/QuirkyQuixote/minecraft-wishlist" \
                } \
              } \
            ] \
          } \
        ]

# wishlist:max_stack_size
setblock 0 0 0 minecraft:barrel
data modify block 0 0 0 Items append from storage wishlist:args Item
item modify block 0 0 0 container.0 wishlist:full_stack
execute store result score msize wishlist.vars \
        run data get block 0 0 0 Items[0].Count
execute unless data block 0 0 0 Items[0].Count \
        run scoreboard players set msize wishlist.vars 999999
setblock 0 0 0 minecraft:air

# wishlist:memorize
data modify entity @s ArmorItems[3].components.minecraft:custom_data set from storage wishlist:args memories

# wishlist:nth_element
$data modify $(dst) set from $(src)[$(idx)]

# wishlist:rand
scoreboard players operation rand_seed wishlist.vars *= rand_multiplier wishlist.vars
scoreboard players operation rand_seed wishlist.vars += rand_increment wishlist.vars
scoreboard players operation rand_seed wishlist.vars %= rand_modulus wishlist.vars
scoreboard players operation rand wishlist.vars = rand_seed wishlist.vars

# wishlist:random_element
$execute store result score wre_len wishlist.vars run data get $(src)
execute store result score wre_idx wishlist.vars run random value 0..2000000000
scoreboard players operation wre_idx wishlist.vars %= wre_len wishlist.vars
$data modify storage wishlist:args wre_args set value {dst:"$(dst)",src:"$(src)"}
execute store result storage wishlist:args wre_args.idx int 1 \
        run scoreboard players get wre_idx wishlist.vars
function wishlist:nth_element with storage wishlist:args wre_args

# wishlist:raycast
scoreboard players set steps wishlist.vars 45
execute anchored eyes positioned ^ ^ ^ run return run function wishlist:raycast_1

# wishlist:raycast_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ #wishlist:non_full \
        summon minecraft:marker \
        run return run function wishlist:raycast_2
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run return run function wishlist:raycast_1

# wishlist:raycast_2
execute store result storage wishlist:raycast x1 int 1 run data get entity @s Pos[0] 1
execute store result storage wishlist:raycast y1 int 1 run data get entity @s Pos[1] 1
execute store result storage wishlist:raycast z1 int 1 run data get entity @s Pos[2] 1
tp @s ^ ^ ^-0.1
execute store result storage wishlist:raycast x0 int 1 run data get entity @s Pos[0] 1
execute store result storage wishlist:raycast y0 int 1 run data get entity @s Pos[1] 1
execute store result storage wishlist:raycast z0 int 1 run data get entity @s Pos[2] 1
kill @s
return 1

# wishlist:recall
data remove storage wishlist:args memories
execute unless data entity @s ArmorItems[3].components.minecraft:custom_data \
        run function wishlist:recall_1
data modify storage wishlist:args memories \
        set from entity @s ArmorItems[3].components.minecraft:custom_data

# wishlist:recall_1
item replace entity @s armor.head with minecraft:stone_button[ \
        minecraft:custom_model_data={floats:[999999]} \
        ]
data modify entity @s ArmorDropChances[3] set value 0.0f

# wishlist:restore_inventory
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 run scoreboard players get @s wishlist.playerId
data modify storage wishlist:args items set from entity @s Inventory
setblock 0 0 0 minecraft:barrel
function wishlist:restore_inventory_1 with storage wishlist:args args
setblock 0 0 0 minecraft:air

# wishlist:restore_inventory_1
$function wishlist:restore_inventory_2 {id:$(id),src:0,dst:"hotbar.0"}
$function wishlist:restore_inventory_2 {id:$(id),src:1,dst:"hotbar.1"}
$function wishlist:restore_inventory_2 {id:$(id),src:2,dst:"hotbar.2"}
$function wishlist:restore_inventory_2 {id:$(id),src:3,dst:"hotbar.3"}
$function wishlist:restore_inventory_2 {id:$(id),src:4,dst:"hotbar.4"}
$function wishlist:restore_inventory_2 {id:$(id),src:5,dst:"hotbar.5"}
$function wishlist:restore_inventory_2 {id:$(id),src:6,dst:"hotbar.6"}
$function wishlist:restore_inventory_2 {id:$(id),src:7,dst:"hotbar.7"}
$function wishlist:restore_inventory_2 {id:$(id),src:8,dst:"hotbar.8"}

$function wishlist:restore_inventory_2 {id:$(id),src:9,dst:"inventory.0"}
$function wishlist:restore_inventory_2 {id:$(id),src:10,dst:"inventory.1"}
$function wishlist:restore_inventory_2 {id:$(id),src:11,dst:"inventory.2"}
$function wishlist:restore_inventory_2 {id:$(id),src:12,dst:"inventory.3"}
$function wishlist:restore_inventory_2 {id:$(id),src:13,dst:"inventory.4"}
$function wishlist:restore_inventory_2 {id:$(id),src:14,dst:"inventory.5"}
$function wishlist:restore_inventory_2 {id:$(id),src:15,dst:"inventory.6"}
$function wishlist:restore_inventory_2 {id:$(id),src:16,dst:"inventory.7"}
$function wishlist:restore_inventory_2 {id:$(id),src:17,dst:"inventory.8"}
$function wishlist:restore_inventory_2 {id:$(id),src:18,dst:"inventory.9"}
$function wishlist:restore_inventory_2 {id:$(id),src:19,dst:"inventory.10"}
$function wishlist:restore_inventory_2 {id:$(id),src:20,dst:"inventory.11"}
$function wishlist:restore_inventory_2 {id:$(id),src:21,dst:"inventory.12"}
$function wishlist:restore_inventory_2 {id:$(id),src:22,dst:"inventory.13"}
$function wishlist:restore_inventory_2 {id:$(id),src:23,dst:"inventory.14"}
$function wishlist:restore_inventory_2 {id:$(id),src:24,dst:"inventory.15"}
$function wishlist:restore_inventory_2 {id:$(id),src:25,dst:"inventory.16"}
$function wishlist:restore_inventory_2 {id:$(id),src:26,dst:"inventory.17"}
$function wishlist:restore_inventory_2 {id:$(id),src:27,dst:"inventory.18"}
$function wishlist:restore_inventory_2 {id:$(id),src:28,dst:"inventory.19"}
$function wishlist:restore_inventory_2 {id:$(id),src:29,dst:"inventory.20"}
$function wishlist:restore_inventory_2 {id:$(id),src:30,dst:"inventory.21"}
$function wishlist:restore_inventory_2 {id:$(id),src:31,dst:"inventory.22"}
$function wishlist:restore_inventory_2 {id:$(id),src:32,dst:"inventory.23"}
$function wishlist:restore_inventory_2 {id:$(id),src:33,dst:"inventory.24"}
$function wishlist:restore_inventory_2 {id:$(id),src:34,dst:"inventory.25"}
$function wishlist:restore_inventory_2 {id:$(id),src:35,dst:"inventory.26"}

$function wishlist:restore_inventory_2 {id:$(id),src:100,dst:"armor.feet"}
$function wishlist:restore_inventory_2 {id:$(id),src:101,dst:"armor.legs"}
$function wishlist:restore_inventory_2 {id:$(id),src:102,dst:"armor.chest"}
$function wishlist:restore_inventory_2 {id:$(id),src:103,dst:"armor.head"}
$function wishlist:restore_inventory_2 {id:$(id),src:-106,dst:"weapon.offhand"}

# wishlist:restore_inventory_2
$execute unless data storage wishlist:inventory $(id)[{Slot:$(src)b}] \
        run return run item replace entity @s $(dst) with minecraft:air
$data modify storage wishlist:args args set from storage wishlist:inventory $(id)[{Slot:$(src)b}]
data remove storage wishlist:args args.Slot
data modify block 0 0 0 Items append from storage wishlist:args args
$item replace entity @s $(dst) from block 0 0 0 container.0
data modify block 0 0 0 Items set value []

# wishlist:rotate
execute if score pivot wishlist.vars matches 1.. run function wishlist:rotate_1

# wishlist:rotate_1
data modify storage wishlist:args elems append from storage wishlist:args elems[0]
data remove storage wishlist:args elems[0]
scoreboard players remove pivot wishlist.vars 1
function wishlist:rotate

# wishlist:rotate_random
function wishlist:rand
execute store result score length wishlist.vars \
        run data get storage wishlist:args elems
scoreboard players operation pivot wishlist.vars = rand wishlist.vars
scoreboard players operation pivot wishlist.vars %= length wishlist.vars
function wishlist:rotate

# wishlist:skin_villager_by_biome
execute if biome ~ ~ ~ #wishlist:spawns_desert_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:desert"}}
execute if biome ~ ~ ~ #wishlist:spawns_jungle_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:jungle"}}
execute if biome ~ ~ ~ #wishlist:spawns_savanna_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:savanna"}}
execute if biome ~ ~ ~ #wishlist:spawns_snow_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:snow"}}
execute if biome ~ ~ ~ #wishlist:spawns_swamp_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:swamp"}}
execute if biome ~ ~ ~ #wishlist:spawns_taiga_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:taiga"}}
execute if biome ~ ~ ~ #wishlist:spawns_plains_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:plains"}}

# wishlist:sneak
execute if entity @s[tag=!wishlist.sneaking] \
        unless score @s wishlist.last_sneak_time = @s wishlist.sneak_time \
        at @s \
        run function #wishlist:crouch
execute if score @s wishlist.last_sneak_time = @s wishlist.sneak_time \
        run tag @s remove wishlist.sneaking
execute unless score @s wishlist.last_sneak_time = @s wishlist.sneak_time \
        run tag @s add wishlist.sneaking
scoreboard players operation @s wishlist.last_sneak_time = @s wishlist.sneak_time

# wishlist:stash_inventory
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 run scoreboard players get @s wishlist.playerId
data modify storage wishlist:args items set from entity @s Inventory
function wishlist:stash_inventory_1 with storage wishlist:args args

# wishlist:stash_inventory_1
$data modify storage wishlist:inventory $(id) set from storage wishlist:args items

# wishlist:store_position
data modify storage wishlist:args p set from entity @s {}

execute store result score @s wishlist.x run data get storage wishlist:args p.Pos[0]
execute store result score @s wishlist.y run data get storage wishlist:args p.Pos[1]
execute store result score @s wishlist.z run data get storage wishlist:args p.Pos[2]

execute store result score @s wishlist.yaw run data get storage wishlist:args p.Rotation[0]
execute store result score @s wishlist.pitch run data get storage wishlist:args p.Rotation[1]

execute store result score x wishlist.vars run data get storage wishlist:args p.Motion[0] 100
execute store result score y wishlist.vars run data get storage wishlist:args p.Motion[1] 100
execute store result score z wishlist.vars run data get storage wishlist:args p.Motion[2] 100

tag @s remove wishlist.still
execute if score x wishlist.vars matches 0 \
        if score y wishlist.vars matches -8 \
        if score z wishlist.vars matches 0 \
        run tag @s add wishlist.still

scoreboard players set @s wishlist.facing 5
execute if score @s wishlist.yaw matches -135..-45 \
        run scoreboard players set @s wishlist.facing 0
execute if score @s wishlist.yaw matches -45..45 \
        run scoreboard players set @s wishlist.facing 4
execute if score @s wishlist.yaw matches 45..135 \
        run scoreboard players set @s wishlist.facing 1
execute if score @s wishlist.pitch matches ..-45 \
        run scoreboard players set @s wishlist.facing 2
execute if score @s wishlist.pitch matches 45.. \
        run scoreboard players set @s wishlist.facing 3

scoreboard players set @s wishlist.dimension -1
execute if dimension minecraft:overworld \
        run scoreboard players set @s wishlist.dimension 0
execute if dimension minecraft:the_nether \
        run scoreboard players set @s wishlist.dimension 1
execute if dimension minecraft:the_end \
        run scoreboard players set @s wishlist.dimension 2
execute if dimension wishlist:sketch \
        run scoreboard players set @s wishlist.dimension 3
execute if dimension wishlist:ocean \
        run scoreboard players set @s wishlist.dimension 4
execute if dimension wishlist:sky \
        run scoreboard players set @s wishlist.dimension 5

scoreboard players set 512 wishlist.vars 512
scoreboard players operation @s wishlist.rx = @s wishlist.x
scoreboard players operation @s wishlist.rx /= 512 wishlist.vars
scoreboard players operation @s wishlist.rz = @s wishlist.z
scoreboard players operation @s wishlist.rz /= 512 wishlist.vars

scoreboard players set 16 wishlist.vars 16
scoreboard players operation @s wishlist.cx = @s wishlist.x
scoreboard players operation @s wishlist.cx /= 16 wishlist.vars
scoreboard players operation @s wishlist.cy = @s wishlist.y
scoreboard players operation @s wishlist.cy /= 16 wishlist.vars
scoreboard players operation @s wishlist.cz = @s wishlist.z
scoreboard players operation @s wishlist.cz /= 16 wishlist.vars

execute store success score ground0 wishlist.vars if entity @s[tag=wishlist.ground]
execute store result score ground1 wishlist.vars run data get storage wishlist:args p.OnGround

tag @s remove wishlist.ground
tag @s remove wishlist.jump
tag @s remove wishlist.land

execute if score ground1 wishlist.vars matches 1 \
        run tag @s add wishlist.ground

execute if score ground0 wishlist.vars matches 1 \
        if score ground1 wishlist.vars matches 0 \
        run tag @s add wishlist.jump

execute if score ground0 wishlist.vars matches 0 \
        if score ground1 wishlist.vars matches 1 \
        run tag @s add wishlist.land

# wishlist:throw
execute positioned ^ ^ ^0 summon marker run function wishlist:throw_1
execute positioned ^ ^ ^1 summon marker run function wishlist:throw_2

data modify storage wishlist:args args.data.Owner set from entity @s UUID
data modify storage wishlist:args args.data.Motion set value [0.0,0.0,0.0]
$execute store result storage wishlist:args args.data.Motion[0] double $(speed) \
        run scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
$execute store result storage wishlist:args args.data.Motion[1] double $(speed) \
        run scoreboard players operation y1 wishlist.vars -= y0 wishlist.vars
$execute store result storage wishlist:args args.data.Motion[2] double $(speed) \
        run scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars

function wishlist:throw_3 with storage wishlist:args args

# wishlist:throw_1
execute store result score x0 wishlist.vars run data get entity @s Pos[0] 1000
execute store result score y0 wishlist.vars run data get entity @s Pos[1] 1000
execute store result score z0 wishlist.vars run data get entity @s Pos[2] 1000
kill @s

# wishlist:throw_2
execute store result score x1 wishlist.vars run data get entity @s Pos[0] 1000
execute store result score y1 wishlist.vars run data get entity @s Pos[1] 1000
execute store result score z1 wishlist.vars run data get entity @s Pos[2] 1000
kill @s

# wishlist:throw_3
$summon $(id) ~ ~ ~ $(data)

# wishlist:throw_item
summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:stick",Count:1},Tags:[wishlist.thrown_item]}
data modify entity @e[tag=wishlist.thrown_item,limit=1] Item set from storage wishlist:args Item
tag @e[tag=wishlist.thrown_item] remove wishlist.thrown_item

# wishlist:throw_stack
function wishlist:max_stack_size
scoreboard players operation ssize wishlist.vars < msize wishlist.vars
execute store result storage wishlist:args Item.Count byte 1 \
        run scoreboard players get ssize wishlist.vars
function wishlist:throw_item

# wishlist:wake_up
tag b51be5e1-572c-3a8e-9802-4d2b53d1970a add wishlist.contributor
tag 3abea40c-657f-34b8-9163-618844710e72 add wishlist.contributor
tag 7ac5a819-98d0-3a41-96c1-38ad0643ff7b add wishlist.contributor
tag pipar_ add wishlist.contributor

advancement grant @s[tag=wishlist.contributor] only wishlist:wake_up

# wishlist:build_trade
data modify storage wishlist:args tmp set value {maxUses:2000000000}
data modify storage wishlist:args tmp.buy set from entity @s Inventory[{Slot:0b}]
data remove storage wishlist:args tmp.buy.Slot
data modify storage wishlist:args tmp.buyB set from entity @s Inventory[{Slot:1b}]
data remove storage wishlist:args tmp.buyB.Slot
data modify storage wishlist:args tmp.sell set from entity @s Inventory[{Slot:2b}]
data remove storage wishlist:args tmp.sell.Slot

# wishlist:append_trade
function wishlist:build_trade
execute positioned ^ ^1 ^ run data modify entity \
        @e[type=villager,distance=..2,sort=nearest,limit=1] Offers.Recipes \
        append from storage wishlist:args tmp

# wishlist:prepend_trade
function wishlist:build_trade
execute positioned ^ ^1 ^ run data modify entity \
        @e[type=villager,distance=..2,sort=nearest,limit=1] Offers.Recipes \
        prepend from storage wishlist:args tmp

# wishlist:insert_trade
function wishlist:build_trade
$execute positioned ^ ^1 ^ run data modify entity \
        @e[type=villager,distance=..2,sort=nearest,limit=1] Offers.Recipes \
        insert $(id) from storage wishlist:args tmp
