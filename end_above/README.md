End Above
=========

Teleports above the Overworld every player who falls to the void under The End.

Players keep their x and z coordinates when teleported to the overworld, but
their y coordinate is set to 480.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Overhead
--------

Uses advancements to check if a player in `minecraft:the_end` or `wishlist:sky`
is below y = 0.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Did it Hurt?: fall from the End into the Overworld's sky
