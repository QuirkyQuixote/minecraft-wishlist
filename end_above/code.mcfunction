
# end_above:load

data modify storage wishlist:args all_chatter append value \
        "Didja know that there was a time no one knew where the End was?"
data modify storage wishlist:args all_chatter append value \
        "So… did you ever fall from heaven? and did it hurt? No, for real"

# end_above:teleport
advancement revoke @s only end_above:fall_from_the_end
advancement revoke @s only end_above:fall_from_sky
execute in minecraft:overworld run tp @s ~ 480 ~
effect give @s minecraft:blindness 5 1 true
advancement grant @s only wishlist:fall_from_the_end

