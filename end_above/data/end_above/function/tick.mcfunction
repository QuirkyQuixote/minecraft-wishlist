# end_above:tick
schedule function end_above:tick 1t
execute as @a \
        at @s \
        in minecraft:the_end \
        if entity @a[distance=0] \
        if score @s minecraft.y < 0 \
        run function end_above:teleport

execute as @a \
        at @s \
        in wishlist:sky \
        if entity @a[distance=0] \
        if score @s minecraft.y < 0 \
        run function end_above:teleport

