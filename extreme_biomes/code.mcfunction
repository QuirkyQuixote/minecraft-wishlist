
# extreme_biomes:damage

$damage @s $(amount) $(type)


# extreme_biomes:load

function extreme_biomes:tick_1s

# extreme_biomes:supercool

scoreboard players set amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_boots \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_chestplate \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_helmet \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_leggings \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_boots \
        run scoreboard players remove amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_chestplate \
        run scoreboard players remove amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_helmet \
        run scoreboard players remove amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_leggings \
        run scoreboard players remove amount wishlist.vars 1
data modify storage wishlist:args args set value {type:"minecraft:freeze"}
execute store result storage wishlist:args args.amount int 1 \
        run scoreboard players get amount wishlist.vars
function extreme_biomes:damage with storage wishlist:args args

execute if score amount wishlist.vars matches 1.. \
        run effect give @s minecraft:slowness 10 2 true

execute if block ~ ~ ~ minecraft:water run damage @s 1 minecraft:freeze



# extreme_biomes:superhot

scoreboard players set amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_boots \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_chestplate \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_helmet \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_leggings \
        run scoreboard players add amount wishlist.vars 1
data modify storage wishlist:args args set value {type:"minecraft:on_fire"}
execute store result storage wishlist:args args.amount int 1 \
        run scoreboard players get amount wishlist.vars
function extreme_biomes:damage with storage wishlist:args args


# extreme_biomes:superhot_lava

advancement revoke @s only extreme_biomes:superhot_lava
damage @s 1000 minecraft:on_fire

# extreme_biomes:tick_1s

schedule function extreme_biomes:tick_1s 1s

execute as @a[gamemode=!creative,gamemode=!spectator] \
        at @s \
        if biome ~ ~ ~ wishlist:supercool \
        run function extreme_biomes:supercool

execute as @a[gamemode=!creative,gamemode=!spectator] \
        at @s \
        if biome ~ ~ ~ wishlist:superhot \
        run function extreme_biomes:superhot

