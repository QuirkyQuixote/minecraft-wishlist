Extreme Biomes
==============

Provides biomes that will cause harm to the player just by being in them

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The biomes are not automatically placed in any generated world; they are
intended to be used in custom constructions to create hazardous areas

### Superhot Biome

* Resource name is `wishlist:superhot`
* Causes `1 + armor pieces worn` damage of `minecraft:on_fire` type per second
  to any player standing in it.
* Causes `1000` damage of `minecraft:on_fire` type per tick to any player that
  touches lava

### Supercool Blome

* Resource name is `wishlist:supercool`
* Causes `1 + cold armor pieces worn - warm armor pieces worn` damage of
  `minecraft:freeze` type per second to any player standing in it.
  * Only leather armor is considered warm for the supercool rooms
* Causes `1` damage of `minecraft:freeze` type per second to any player that
  touches water.
  * Leather armor does not prevent this damage.
* If you get cold damage in this biome you also get slowness 2.

Overhead
--------

Checks every player every second to determine if they are in a hazardous biome

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:


