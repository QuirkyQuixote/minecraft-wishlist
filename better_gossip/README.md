Better Gossip
=============

Provides a number of global reputation scores for players according to
different mob types.

Currently, only the villager reputation system is complete. The illager
reputation system is halfway done, although it can be used as-is.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Random Tick](../random_tick) for update optimizations.
* Requires [Custom Blocks](../custom_blocks) for pseudoblocks.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Gossip is stored in two lists of "good" and "bad" gossip, with up to three
pieces of gossip, each of which is of the form:

```
{ reason:<string>, player:<playerid> }
```

There are a bunch of actions that a player may perform that will be remembered
by gossipy entities if they witness them:

### Sources of Good Rep

* give an item to a mob,
* trade with a villager,
* bartering with a piglin,
* talking to a npc from [Chatterbox](../chatterbox).

When a mob is randomly queried, some other conditions may become positive
gossip:

* for villagers, having worked recently (rep goes to the nearest player);
* for villagers, having gossiped recently (rep goes to the nearest player);
* for piglins, wearing gold.

### Sources of Bad Rep

* hurting a villager,
* hurting an unarmed pillager,
* hurting an iron golem,
* hurting an unangered piglin,
* hurting a cat,
* placing lava,
* placing fire,
* hurting a npc from [Chatterbox](../chatterbox).

When a mob is randomly queried, some other conditions may become negative
gossip:

* for villagers, being crammed (rep goes to the nearest player);
* for villagers, not having a bed (rep goes to the nearest player);
* for villagers, not having maxed health (rep goes to the nearest player);
* for villagers, having not slept in some time (rep goes to the nearest player);
* for piglins and piglin brutes, being angry at a player.

### Propagation

Every tick, a random chunk is searched for a gossippy entity for it to update
their current gossip:

Every gossiper in the chunk will take one random piece of gossip from the
global pool and add it to their own gossip lists.

Then, one gossiper from the chunk will randomly take one of their own pieces of
gossip and put it in the global pool.

### Listening to Global Gossip

When gossip propagation happens, players that are near radio items as provided
by the core wishlist package will hear their gossip propagating, as one of the
following lines:

```
wishlist.better_gossip.radio.hurt_villager
wishlist.better_gossip.radio.hurt_pillager
wishlist.better_gossip.radio.hurt_iron_golem
wishlist.better_gossip.radio.place_lava
wishlist.better_gossip.radio.place_fire
wishlist.better_gossip.radio.give_item
wishlist.better_gossip.radio.trade_with_villager
wishlist.better_gossip.radio.crammed_villager
wishlist.better_gossip.radio.gossiping_villager
wishlist.better_gossip.radio.homeless_villager
wishlist.better_gossip.radio.sick_villager
wishlist.better_gossip.radio.sleepless_villager
wishlist.better_gossip.radio.working_villager
```

This can be used to find which gossipers have issues in your settlements to
solve their problems.

### Reputation Decay

When a player hasn't been talked about for at least 1200 ticks, their
reputation will start to decay, and they lose one reputation point per minute
in all their scores.

### Luck and Unluck

Players who maintain a high good rep and low bad rep for a time are granted the
`luck` effect. Alternatively, players who maintain a low good rep and high bad
rep for a time are given the `unluck` effect.

Every game minute, a check is made of the value resulting from `vg_positive -
vg_negative` for every player:

* if it's greater than 180 the `vg_good_luck` score is increased by 1,
* if it's less than 180 `vg_good_luck` score is reset to zero,
* if it's less than -180 the `vg_bad_luck` score is increased by 1,
* if it's greater than -180 `vg_bad_luck` score is reset to zero.

The `vg_good_luck` and `vg_bad_luck` scores give the players the `luck` and
`unluck` effects respectively at a level depending on the score value:

* ..10: nothing
* 10..20: level 1
* 20..40: level 2
* 40..80: level 3
* 80..: level 4

Overhead
--------

Adds dummy advancements:

* checks if a player has placed a radio item (tagged oak sign),
* checks if a player interacted with a radio block,
* checks if a player attacked a radio block.

Queries a random chunk per tick:

* searches every entity in the chunk for an gossiper.

Updates once per second

* checks every entity in a 4-block radius around each player for a radio.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Radio Amateur: place a radio
    * Rumor Has It: hear your name mentioned in the radio
        * The Price of Fame: have one of your rep scores reach 200
            * Karma-riffic: have your luck or unluck reach level 4
