# villager/propagate_sadness

execute store result score @s wishlist.better_gossip.last_gossiped \
        run time query gametime
scoreboard players operation @s vg_negative += sadness wishlist.vars
execute if entity @e[tag=wishlist.better_gossip.radio,distance=..4] \
        run tellraw @s \
          { \
            "translate":"wishlist.better_gossip.bad_news", \
            "with":[{"selector":"@e[tag=wishlist.better_gossip.self]"}], \
            "color":"gray", \
            "italic":true \
          }
execute if entity @e[tag=wishlist.better_gossip.radio,distance=..4] \
        run advancement grant @s only wishlist:rumor_has_it
execute at @e[tag=wishlist.better_gossip.radio,distance=..4] \
        run playsound wishlist:radio.voices block @s ~ ~ ~

