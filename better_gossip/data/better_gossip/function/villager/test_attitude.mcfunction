# test_attitude.mcfunction - propagate villager attitude to player reputation

# Reset attitude values
scoreboard players set sadness wishlist.vars 0
scoreboard players set happiness wishlist.vars 0

# Check if the villager is sad. if not, test if they're happy
function better_gossip:villager/test_sadness
execute if score sadness wishlist.vars matches 0 \
        run function better_gossip:villager/test_happiness

execute if score sadness wishlist.vars matches 1.. \
        run tellraw @a[distance=..100,tag=vg_radio] \
          { \
            "translate":"wishlist.better_gossip.bad_news", \
            "with":[{"selector":"@s"}], \
            "color":"gray", \
            "italic":true \
          }
execute if score happiness wishlist.vars matches 1.. \
        run tellraw @a[distance=..100,tag=vg_radio] \
          { \
            "translate":"wishlist.better_gossip.good_news", \
            "with":[{"selector":"@s"}], \
            "color":"gray", \
            "italic":true \
          }
execute if score sadness wishlist.vars matches 1.. \
        run advancement grant @a[distance=..100,tag=vg_radio] only wishlist:rumor_has_it
execute if score happiness wishlist.vars matches 1.. \
        run advancement grant @a[distance=..100,tag=vg_radio] only wishlist:rumor_has_it

# Neighbouring players get the praise and the blame
scoreboard players operation @a[distance=..100] vg_negative += sadness wishlist.vars
scoreboard players operation @a[distance=..100] vg_positive += happiness wishlist.vars

