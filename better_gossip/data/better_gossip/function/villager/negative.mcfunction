# negative.mcfunction - propagate negative gossip

data modify storage gossip:tmp Type set value "minor_negative"
execute store result storage gossip:tmp Value int 1 \
        run scoreboard players get @s vg_negative
data modify storage gossip:tmp Target set from entity @s UUID
execute as @e[type=villager,tag=wishlist.gossipy,dx=16,dy=256,dz=16] \
        run data modify entity @s Gossips append from storage gossip:tmp

