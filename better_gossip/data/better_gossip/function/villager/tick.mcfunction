# villager/tick.mcfunction

execute store result score now wishlist.vars run time query daytime
execute if score now wishlist.vars matches 13000.. \
        store result storage wishlist:args memories.last_night_seen int 1 \
        run scoreboard players get now wishlist.vars

scoreboard players set sadness wishlist.vars 0
scoreboard players set happiness wishlist.vars 0

function better_gossip:villager/test_sadness
function better_gossip:villager/test_happiness

