# test_sadness.mcfunction - calculate villager sadness

# Any villagers with at least two others in the same block feels bad.
scoreboard players set cram wishlist.vars 0
execute as @e[type=villager,distance=..0.5] \
        run scoreboard players add cram wishlist.vars 1
execute if score cram wishlist.vars matches 3.. \
        run function better_gossip:villager/test_cramming

# If the villager does not have a home, they feel bad.
execute unless data entity @s Brain.memories."minecraft:home" \
        run function better_gossip:villager/test_homeless

# If the villager is sick, they feel bad.
execute if entity @s[nbt=!{Health:20.0f}] \
        run function better_gossip:villager/test_sickness

# If the villager has not slept in some time, they feel bad
execute store result score t1 wishlist.vars \
        run data get storage wishlist:args memories.last_night_seen
execute store result score t2 wishlist.vars \
        run data get entity @s Brain.memories."minecraft:last_slept".value
scoreboard players operation t1 wishlist.vars -= t2 wishlist.vars
execute if score t1 wishlist.vars matches 92000.. \
        run function better_gossip:villager/test_sleepless

