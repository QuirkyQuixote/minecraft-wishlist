# test_happiness.mcfunction - calculate villager happiness

# If the villager has worked recently, they feel happy
execute store result score t1 wishlist.vars run time query gametime
execute store result score t2 wishlist.vars \
        run data get entity @s Brain.memories."minecraft:last_worked_at_poi".value
scoreboard players operation t1 wishlist.vars -= t2 wishlist.vars
execute if score t1 wishlist.vars matches ..6000 \
        run function better_gossip:villager/test_work

# If the villager has gossipped recently, they feel happy
execute store result score t1 wishlist.vars run time query gametime
execute store result score t2 wishlist.vars run data get entity @s LastGossipDecay
scoreboard players operation t1 wishlist.vars -= t2 wishlist.vars
execute if score t1 wishlist.vars matches ..6000 \
        run function better_gossip:villager/test_gossip

