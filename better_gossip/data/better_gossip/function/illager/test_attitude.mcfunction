# test_attitude.mcfunction - update happiness and sadness

# Reset attitude values
scoreboard players set sadness wishlist.vars 0
scoreboard players set happiness wishlist.vars 0

# Check if the illager is sad. if not, test if they're happy
function better_gossip:illager/test_sadness
execute if score sadness wishlist.vars matches 0 \
        run function better_gossip:illager/test_happiness

# Neighbouring players get the praise and the blame
scoreboard players operation @a[distance=..100,sort=random,limit=1] vg_negative += sadness wishlist.vars
scoreboard players operation @a[distance=..100,sort=random,limit=1] vg_positive += happiness wishlist.vars

