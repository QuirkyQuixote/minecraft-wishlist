# test_sadness.mcfunction - calculate sadness for entity

# Any illagers with at least two others in the same block feels bad.
scoreboard players set cram wishlist.vars 0
execute as @e[type=pillager,distance=..1] \
        run scoreboard players add cram wishlist.vars 1
execute if score cram wishlist.vars matches 3.. \
        run scoreboard players add sadness wishlist.vars 1

# If the illager does not have a home, they feel bad.
execute unless data entity @s Brain.memories."minecraft:home" \
        run scoreboard players add sadness wishlist.vars 1

# If the illager is sick, they feel bad.
execute if entity @s[nbt=!{Health:20.0f}] \
        run scoreboard players add sadness wishlist.vars 1

# If the illager has not slept in some time, they feel bad
execute store result score t1 wishlist.vars run time query gametime
execute store result score t2 wishlist.vars \
        run data get entity @s Brain.memories."minecraft:last_slept".value
scoreboard players operation t1 wishlist.vars -= t2 wishlist.vars
execute if score t1 wishlist.vars matches 92000.. \
        run scoreboard players add sadness wishlist.vars 1

# If there are hostile entities near the illager, they feel bad
execute if entity @e[type=#wishlist:hostile,distance=..16] \
        run scoreboard players add sadness wishlist.vars 1

