# test_happiness.mcfunction - calculate happiness for illager

# If the illager has a job site block, they feel happy
#execute if data entity @s Brain.memories."minecraft:job_site" run scoreboard players add happiness wishlist.vars 1

# If the illager has worked recently, they feel happy
execute store result score t1 wishlist.vars run time query gametime
execute store result score t2 wishlist.vars \
        run data get entity @s Brain.memories."minecraft:last_worked_at_poi".value
scoreboard players operation t1 wishlist.vars -= t2 wishlist.vars
execute if score t1 wishlist.vars matches ..6000 \
        run scoreboard players add happiness wishlist.vars 1

# If the illager has gossipped recently, they feel happy
execute store result score t1 wishlist.vars run time query gametime
execute store result score t2 wishlist.vars run data get entity @s LastGossipDecay
scoreboard players operation t1 wishlist.vars -= t2 wishlist.vars
execute if score t1 wishlist.vars matches ..6000 \
        run scoreboard players add happiness wishlist.vars 1

