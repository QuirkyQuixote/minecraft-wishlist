# chunk.mcfunction

execute store result score tick_now wishlist.vars run time query gametime
scoreboard players operation tick_now wishlist.vars %= gossip_frequency wishlist.vars

execute if score tick_now wishlist.vars matches 0 \
        as @e[tag=wishlist.gossipy,dx=16,dy=256,dz=16,sort=random,limit=1] \
        at @s \
        run function better_gossip:chunk_1

execute if score tick_now wishlist.vars matches 0 \
        as @e[tag=wishlist.gossipy,dx=16,dy=256,dz=16] \
        at @s \
        run function better_gossip:chunk_4

execute if score tick_now wishlist.vars matches 0 \
        as @a \
        run function better_gossip:clamp_reputation
