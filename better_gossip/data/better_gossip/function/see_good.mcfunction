# see_good.mcfunction

function wishlist:recall
data modify storage wishlist:args memories.gossip.good \
        append from storage wishlist:args gossip
execute if data storage wishlist:args memories.gossip.good[3] \
        run data remove storage wishlist:args memories.gossip.good[0]
execute anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:happy_villager ~ ~ ~ 0.2 0.2 0.2 0 2
function wishlist:memorize

