# chunk_1.mcfunction

function wishlist:recall

execute if entity @s[type=villager,nbt=!{NoAI:1b}] \
        run function better_gossip:villager/tick
execute if entity @s[type=piglin] \
        run function better_gossip:piglin/tick
execute if entity @s[type=piglin_brute] \
        run function better_gossip:piglin/tick

function wishlist:rand
scoreboard players operation rand wishlist.vars %= 8 wishlist.vars

data modify storage wishlist:args gossip set value {}
execute if score rand wishlist.vars matches 0 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args memories.gossip.good[0]
execute if score rand wishlist.vars matches 1 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args memories.gossip.good[1]
execute if score rand wishlist.vars matches 2 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args memories.gossip.good[2]
execute if score rand wishlist.vars matches 4 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args memories.gossip.bad[0]
execute if score rand wishlist.vars matches 5 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args memories.gossip.bad[1]
execute if score rand wishlist.vars matches 6 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args memories.gossip.bad[2]

execute if score rand wishlist.vars matches 0..3 \
        run data modify storage wishlist:args gossip_pool.good \
        append from storage wishlist:args gossip
execute if score rand wishlist.vars matches 4..7 \
        run data modify storage wishlist:args gossip_pool.bad \
        append from storage wishlist:args gossip

execute if data storage wishlist:args gossip_pool.good[3] \
        run data remove storage wishlist:args gossip_pool.good[0]
execute if data storage wishlist:args gossip_pool.bad[3] \
        run data remove storage wishlist:args gossip_pool.bad[0]

execute if data storage wishlist:args gossip \
        run function better_gossip:chunk_2

function wishlist:memorize
