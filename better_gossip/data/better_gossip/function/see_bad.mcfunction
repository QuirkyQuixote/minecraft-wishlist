# see_bad.mcfunction

function wishlist:recall
data modify storage wishlist:args memories.gossip.bad \
        append from storage wishlist:args gossip
execute if data storage wishlist:args memories.gossip.bad[3] \
        run data remove storage wishlist:args memories.gossip.bad[0]
execute anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:angry_villager ~ ~ ~ 0.2 0.2 0.2 0 2
function wishlist:memorize

