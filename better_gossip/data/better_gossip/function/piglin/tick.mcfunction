# piglin/tick.mcfunction

data modify storage wishlist:args brain set from entity @s Brain

execute if data storage wishlist:args brain.memories."minecraft:angry_at" \
        run function better_gossip:piglin/anger
execute as @a[distance=..16] if predicate better_gossip:wear_gold \
        run function better_gossip:piglin/wear_gold

