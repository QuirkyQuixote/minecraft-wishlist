# do_bad.mcfunction

data modify storage wishlist:args gossip set value {}
data modify storage wishlist:args gossip.reason \
        set from storage wishlist:args reason
execute store result storage wishlist:args gossip.player int 1 \
        run scoreboard players get @s wishlist.playerId
execute as @e[tag=wishlist.gossipy,distance=..8] \
        at @s \
        run function better_gossip:see_bad

