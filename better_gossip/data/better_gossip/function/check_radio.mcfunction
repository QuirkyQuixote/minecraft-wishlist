# check_radio.mcfunction - check if entity is near a radio

tag @s remove vg_radio

execute if entity @e[type=minecraft:interaction,tag=wishlist.better_gossip.radio,distance=..4] \
        run tag @s add vg_radio

execute at @e[type=minecraft:interaction,tag=wishlist.better_gossip.radio,distance=..4] \
        run playsound better_gossip:radio.voices voice @s ~ ~ ~

