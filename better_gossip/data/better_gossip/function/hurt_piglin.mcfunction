# hurt_piglin.mcfunction

scoreboard players set angry wishlist.vars 0
execute on target \
        if data entity @s Brain.memories."minecraft:angry_at" \
        run scoreboard players set angry wishlist.vars 1
data modify storage wishlist:args reason set value hurt_piglin
execute if score angry wishlist.vars matches 0 \
        run function better_gossip:do_bad
advancement revoke @s only better_gossip:hurt_piglin


