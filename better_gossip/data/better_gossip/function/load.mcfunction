# load.mcfunction - Setup gossip

scoreboard objectives add vg_positive dummy "Positive Villager Rep"
scoreboard objectives add vg_negative dummy "Negative Villager Rep"
scoreboard objectives add vg_positive_min dummy "Minimum Positive Villager Rep"
scoreboard objectives add vg_negative_min dummy "Minimum Negative Villager Rep"
scoreboard objectives add vg_good_luck dummy
scoreboard objectives add vg_bad_luck dummy

scoreboard objectives add ig_positive dummy "Good Rep (Illagers)"
scoreboard objectives add ig_negative dummy "Bad Rep (Illagers)"
scoreboard objectives add ig_killed deathCount
scoreboard objectives add ig_pillagers_killed minecraft.killed:minecraft.pillager "Pillagers Killed"
scoreboard objectives add ig_vindicators_killed minecraft.killed:minecraft.vindicator "Vindicators Killed"
scoreboard objectives add ig_evokers_killed minecraft.killed:minecraft.evoker "Evokers Killed"
scoreboard objectives add ig_illusioners_killed minecraft.killed:minecraft.illusioner "Illusioners Killed"
scoreboard objectives add ig_ravagers_killed minecraft.killed:minecraft.ravager "Ravagers Killed"
scoreboard objectives add ig_witches_killed minecraft.killed:minecraft.witch "Witches Killed"

scoreboard objectives add wishlist.better_gossip.last_gossiped dummy

scoreboard players set death_rep_hit wishlist.vars 50
scoreboard players set gossip_frequency wishlist.vars 20
scoreboard players set 8 wishlist.vars 8

data modify storage wishlist:args all_chatter append value \
        "Radio news were *savage* today. I wasn't expecting that"
data modify storage wishlist:args all_chatter append value \
        "I missed the last broadcast, so I'm running behind; you know what's up?"

data modify storage wishlist:custom_blocks radio set value { \
        interaction: { \
                Tags: [wishlist.better_gossip.radio], \
                width: 0.5, \
                height: 0.5 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.0f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271828, \
                      display:{Name:'{"text":"Radio","italic":false}'} \
              } \
        }, \
        block: "minecraft:air" \
}

function better_gossip:tick_1s
