# chunk_3.mcfunction

data modify storage wishlist:args tags set from entity @s Tags
data modify entity @s Tags append from storage wishlist:args gossip.reason

data modify storage wishlist:args chatter \
        set from storage wishlist:args gossip.reason

execute if entity @s[tag=hurt_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.hurt_villager"}'

execute if entity @s[tag=hurt_pillager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.hurt_pillager"}'

execute if entity @s[tag=hurt_pillager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.hurt_pillager"}'

execute if entity @s[tag=hurt_iron_golem] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.hurt_iron_golem"}'

execute if entity @s[tag=place_lava] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.place_lava"}'

execute if entity @s[tag=place_fire] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.place_fire"}'

execute if entity @s[tag=give_item] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.give_item"}'

execute if entity @s[tag=trade_with_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.trade_with_villager"}'

execute if entity @s[tag=homeless_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.homeless_villager"}'

execute if entity @s[tag=sick_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.sick_villager"}'

execute if entity @s[tag=sleepless_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.sleepless_villager"}'

execute if entity @s[tag=crammed_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.crammed_villager"}'

execute if entity @s[tag=working_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.working_villager"}'

execute if entity @s[tag=gossiping_villager] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.gossiping_villager"}'

execute if entity @s[tag=talk_with_npc] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.talk_with_npc"}'

execute if entity @s[tag=hurt_npc] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.hurt_npc"}'

execute if entity @s[tag=anger_piglin] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.anger_piglin"}'

execute if entity @s[tag=hurt_piglin] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.hurt_piglin"}'

execute if entity @s[tag=hurt_cat] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.hurt_cat"}'

execute if entity @s[tag=barter_with_piglin] \
        run data modify storage wishlist:args chatter \
        set value '{"translate":"wishlist.better_gossip.radio.barter_with_piglin"}'

tellraw @a[tag=wishlist.better_gossip.target] \
  { \
     "translate":"wishlist.better_gossip.radio.chatter", \
     "with":[ \
       {"selector":"@s"}, \
       {"storage":"wishlist:args","nbt":"chatter","interpret":true} \
     ], \
     "color":"gray", \
     "italic":true \
  }

execute as @a[tag=wishlist.better_gossip.target] \
        at @s \
        run playsound better_gossip:radio.voices block @s ~ ~ ~

advancement grant @a[tag=wishlist.better_gossip.target] only wishlist:rumor_has_it

data modify entity @s Tags set from storage wishlist:args tags
tag @a remove wishlist.better_gossip.target
