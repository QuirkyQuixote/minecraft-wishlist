
execute as @e[tag=wishlist.better_gossip.radio,distance=..8] \
        run tag @s add wishlist.custom_block

execute as @e[tag=wishlist.better_gossip.radio,distance=..8] \
        on passengers \
        run data modify entity @s item set value \
        { \
                id: "minecraft:item_frame", \
                tag: { \
                        CustomModelData: 271828, \
                        display: {Name: '{"text":"Radio","italic":false}'}, \
                        EntityTag: {Item: {id: "minecraft:item_frame", tag: {wishlist: {custom_block: {id: "radio"}}}, Count: 1b}} \
                }, \
                Count: 1b \
        }

