

States
------

### Reset

* Every room enters this state when no players are in the dungeon

### Ready

* Reset rooms enter this state when at least one player in the dungeon is close
  enough to them that the chunks will be loaded.
* All pots are placed
* All blocks are restored

### Open

* Ready rooms enter this state when at least one player in the dungeon is
  inside them
* All hostile and passive mobs are spawned

### Clear

* Open rooms enter this state when no hostile mobs associated with them remain

Locks
-----

### Mob Locks

* associated to a particular room
* stored in `dungeons:conf rooms[].locks`
* activate when the room clears

```
{
   type:1b,
   d:string,
   x:int,
   y:int,
   z:int,
   state:byte,
   room:int
}
```

### Cracks

* no room association
* stored in `dungeons:conf locks`
* activate when dynamite is used

```
{
   type:2b,
   d:string,
   x:int,
   y:int,
   z:int,
   state:byte,
}
```

### Key Locks

* no room association
* stored in `dungeons:conf locks`
* lock entity is created if it does not exist when dungeon resets

```
{
   type:4b,
   d:string,
   x:int,
   y:int,
   z:int,
   state:byte
}
```

### Big Locks

* no room association
* stored in `dungeons:conf locks`
* lock entity is created if it does not exist when dungeon resets

```
{
   type:8b,
   d:string,
   x:int,
   y:int,
   z:int,
   state:byte,
}
```

