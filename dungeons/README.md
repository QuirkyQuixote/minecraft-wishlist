Dungeons 2.0
============

Provides tools to build Zelda-like dungeons

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Looping Music](../looping_music) for looping music.
* Requires [Magic](../magic) for custom tools.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The UI to construct dungeons is a book with links to commands; it can be
spawned with the `dungeons:book` loot table.

Dungeon Mechanics
-----------------

The smallest unit for a dungeon is a chunk: each chunk is a 16x16x16 cube
aligned to a 16-block grid in all dimensions (that includes vertically).

One or more chunks can form a room: a room is the behavioral unit of a dungeon,
and contains a number of mobs and loot that will spawn in predetermined places.

A dungeon room can be in the following states:

* Empty, if the dungeon has not been initialized.
* Ready, if a player has come close enough to the room center.
* Open, if one or more players entered it this run, but there are still hostile mobs on it.
* Clear, if it was opened and no hostile mobs spawned in it remain.

### Empty Rooms

When there are no players inside the dungeon it resets:

* all rooms go back the the empty status,
* the id for the current run is increased by one,
* the blocks that were changed during the last run are restored to their
  previous state, temporarilty forceloading the chunk if necessary.

### Ready Rooms

When a player comes close to an empty room, the rooms enter ready status

### Open Rooms

When a player enters a ready room, it enters open status:

* All loot and mobs (hostile and passive) are spawned.
* The attributes for hostile mobs are adjusted by the amount of enchantments
  the players in the dungeon carry.
* The inventory for hostile and passive mobs is checked for keys, and their
  run id is updated to the present one.
* If the room has a bossbar, the max health of all the hostile mobs in it is
  added up to calculate the bossbar mas value.
* If the room is a boss room, the music changes to boss music for all players
  in the dungeon.

### Clear Rooms

When an open room can't find hostile mobs associated with it, the room is
considered clear:

* all mob doors in the room open.

Dungeon Features
----------------

### Mobs

Dungeon rooms may contain hostile mobs, passive mobs, and loot.

* Hostile mobs are spawned when the room opens, and must be killed to clear
  the room.
* Passive mobs are spawned when the room opens, but do not have to be killed
  to clear the room.
* Loot is spawned inside pots (custom ones) when the room opens.

### Doors

Dungeon rooms may be connected by doors of several types.
Doors are separate from rooms, since they actually connect two together.

* Mob doors open when the door clears.
* Small key doors open with any key retrieved on the same dungeon run.
* Big key doors open only with a big key retrieved on the same dungeon run.
* Cracks open only when dynamite is detonated next to them.

### Bossbars

A dungeon room may have an associated bossbar with the name `dungeons:<room
number>`:

* The bossbar will be shown to all players inside the room, and none outside.
* The max health of the bossbar will be the combined health of all hostile mobs
  in the room when it opens.
* The current health of the bossbar will the the combined health of all hostile
  mobs in the room at the time.

### Custom Items

The following custom items are added as part of the dungeon:

* ![small_key] Small keys can be used to open basic locked doors in the same dungeon run
  they were spawned, but are consumed when doing so.
* ![big_key] Big keys can be used to open basic or big locked doors in the same dungeon
  run they were spawned, and are not consumed when doing so.
* ![dynamite] Dynamite is a non-stackable throwable weapon that explodes approximately
  three seconds after being thrown causing massive damage to mobs and
  destroying cracked walls.
* The custom pot is not a block, but a non-stackable throwable weapon that
  causes 16 damage of type `minecraft:falling_block` and extreme knockback to
  the mobs it hits. Pots can't be placed down by players, only by the dungeon:
  attacking one will break it and using it will pick it up as a throwable item;
  in both cases, the item inside the pot will drop where the pot used to be.

### Custom Music

The dungeon overrides vanilla minecraft music:

* When a player enters the dungeon, vanilla music is stopped and a dungeon
  music track is played.
* When the boss room is opened in the dungeon, the dungeon track stops, and
  boss music track is played for every player in the dungeon, regardless of if
  they were the ones to open the room.
* When the boss room is cleared the boss track stops.
* Both musics stop and vanilla music is re-enabled for any player who exits the
  dungeon.

### Difficulty Scaling

Hostile mobs in a dungeon are scaled according to how many players come in and
how prepared they are.

* When a room is opened, the number of total enchants carried by every player
  in the dungeon is added.
* Every mob in the new room gets a 10% health and strength increase per enchant

Room Commands
-------------

### Add Room

* creates a whole new room with the chunk the player is standing on, at the y
  coordinate the player is standing on, and selects the room.
* Fails if the chunk is already in use by a different room.

### Select Room

* Selects the room that contains the chunk the player is standing on.
* Fails if the player is not standing on a room.

### Move Room

* Repositions the center of the room the player is standing on to the current
  position of the player.
* Fails if the player is not standing on a room.

Chunk Commands
--------------

### Add Chunk

* Adds the chunk the player is standing on to the selected room.
* Fails if the chunk is already in use by a different room.

### Remove Chunk

* Removes the chunk the player is standing on from its room.
* If the chunk list of the room empties, the room itself and all its data is removed too
* Fails if the player is not standing on a room.

Hostile Mob Commands
--------------------

### Stash Hostile (add)

* Stashes the closest entity in front of the player as a hostile entity that
  will spawn when the room opens and must be killed to clear the room
* Fails if no entity can be found or the player is not standing on a room.
* Note that not *all* entity data is stashed; a hostile entity will keep only
  the following tags:
  * `id` (it's entity type tag)
  * `Pos`
  * `Tags`
  * `ArmorItems`
  * `HandItems`
  * `ArmorDropChances`
  * `HandDropChances`
  * `Attributes`
    * Except for "Random spawn bonus" modifiers.
    * Note that that the mob attributes may be further modified by the dungeon
      to adjust difficulty to the kind of weapons/armor the players brought for
      that run.

### Pop Hostiles (pop)

* Pops all hostiles stashed in the current room.
* Fails if the player is not standing on a room.

### Restash Hostiles (sto)

* Restashes all popped hostiles in the current room.
* Fails if the player is not standing on a room.

### Discard Hostiles (trs)

* Discards all popped hostiles in the current room.
* Fails if the player is not standing on a room.

Passive Mob Commands
--------------------

### Stash Passive (add)

* Stashes the closest entity in front of the player as a passive entity that
  will spawn when the room opens and be removed when the dungeon resets
* Fails if no entity can be found or the player is not standing on a room.
* Passive mobs will keep *all* data, so the one summoned will be a clone of the
  original one.

### Pop Passives (pop)

* Pops all passives stashed in the current room
* Fails if the player is not standing on a room.

### Restash Passives (sto)

* Restashes all popped passives in the current room.
* Fails if the player is not standing on a room.

### Discard Passives (trs)

* Discards all popped passives in the current room.
* Fails if the player is not standing on a room.

Loot Commands
-------------

* Stashed loot spawns in the form of custom pot blocks that can be broken or
  picked up to retrieve their contents.

### Stash Loot

* Stashes loot at the player's position
* Contents are specified with a loot table:
  * nothing
  * apple
  * dynamite
  * small key
  * big key
* Fails if the player is not standing on a room.

### Pop Loot

* Pops all loot stashed in the current room
* Fails if the player is not standing on a room.

Lock Commands
-------------

Doors can be locked by the dungeon to be automatically opened when a condition
is satisfied; of course, vanilla minecraft allows opening non-iron doors by
hand so you should either use iron doors or a plugin that prevents the doors
from opening.

### Add Mob Lock

* Mob doors open when all hostile mobs in the room have been killed
* The commands will fail if the player is not in a room or looking at a door,
  or if a lock already exists on the door

### Add Small Key Lock

* Small key doors open when any key is used on them
* Small keys will be consumed when a door is opened
* Big keys will not be consumed, so they can be reused in other doors
* The commands will fail if the player is not in a room or looking at a door,
  or if a lock already exists on the door

### Add Big Key Door

* Big doors open when a big key is used on them
* The commands will fail if the player is not in a room or looking at a door,
  or if a lock already exists on the door

### Remove Lock

* Removes the lock on the door the player is looking at
* The commands will fail if the player is not in a room or looking at a door,
  or if there is no lock in the door

Crack Commands
--------------

Cracks are very similar to doors but they are placed in walls. When dynamite
detonates at most four blocks away from the placed crack, a 2x2x2 cube of
blocks around the placed crack will be turned to air.

### Add Crack

* Adds a crack on the wall the player is looking at
* The commands will fail if the player is not in a room or looking at a wall,
  or if there is already a crack on that wall.

### Remove Crack

* Removes a crack from the wall the player is looking at
* The commands will fail if the player is not in a room or looking at a wall,
  or if there is no crack on that wall.

Overhead
--------

Checks every player every second to determine if they are in a dungeon; if at
least one player is inside, further checks are performed

Checks every player every tick to see if they have used a carrot on a stick.

Known Issues
------------

Regardless of how many dungeon rooms there are in the entire world, all of them
are considered a single dungeon, so every player must exit every room for the
rooms to reset.

The boss music keeps playing after a player dies until they click the restart
button.

Dynamite sets up the *closest* player as damage source.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:


[small_key]: dungeons/assets/dungeons/textures/item/small_key.png ""
[big_key]: dungeons/assets/dungeons/textures/item/big_key.png ""
[dynamite]: dungeons/assets/dungeons/textures/item/dynamite.png ""
