
# dungeons:load

scoreboard objectives add wishlist.currentRun dummy
scoreboard objectives add wishlist.selectedRoom dummy
scoreboard objectives add wishlist.currentRoom dummy
scoreboard objectives add wishlist.lastRoom dummy

execute unless data storage dungeons:conf chunks \
        run data modify storage dungeons:conf chunks set value {}
execute unless data storage dungeons:conf rooms \
        run data modify storage dungeons:conf rooms set value []
execute unless data storage dungeons:conf locks \
        run data modify storage dungeons:conf locks set value []
execute unless data storage dungeons:conf nextRoom \
        run data modify storage dungeons:conf nextRoom set value 0

data modify storage wishlist:args all_chatter append value \
        "It's mildly inconvenient to go alone"
data modify storage wishlist:args all_chatter append value \
        "Dungeon maintenance is hard: they change the locks when no one's looking"
data modify storage wishlist:args all_chatter append value \
        "A small key will open one door; a big one will open all of them"
data modify storage wishlist:args all_chatter append value \
        "The dynamite thing is scary, but also scarce"

data modify storage dungeons:conf music_track set value \
        {track:"dungeons:music.dungeon",ttl:242,volume:0.25,pitch:1,minVolume:0}

data modify storage dungeons:conf boss_track set value \
        {track:"dungeons:music.boss",ttl:97,volume:0.25,pitch:1,minVolume:0}

function dungeons:tick_1s
function dungeons:tick_5t
function dungeons:tick_1t

# dungeons:tick_1s

schedule function dungeons:tick_1s 1s

execute unless entity @a[gamemode=!spectator,scores={wishlist.currentRoom=0..}] \
        if score emptyDungeon wishlist.vars matches 0 \
        run function dungeons:clear_rooms

data modify storage dungeons:vars buf set from storage dungeons:conf rooms
function dungeons:ready_rooms

data modify storage dungeons:vars locks set value []
data modify storage dungeons:vars locks append from storage dungeons:conf locks[{state:0b}]
function dungeons:setup_locks

execute if entity @a[gamemode=!spectator,scores={wishlist.currentRoom=0..}] \
        run function dungeons:update_rooms

execute at @a \
        as @e[tag=wishlist.bomb,distance=..32] \
        at @s \
        run function dungeons:update_bomb

execute as @a[scores={wishlist.currentRoom=-1}] run function looping_music:stop

execute as @e[tag=wishlist.dungeon.hostile] \
        unless score @s wishlist.currentRun = currentRun wishlist.vars \
        run function wishlist:dismiss
execute as @e[tag=wishlist.dungeon.passive] \
        unless score @s wishlist.currentRun = currentRun wishlist.vars \
        run function wishlist:dismiss

# dungeons:tick_1t

schedule function dungeons:tick_1t 1t
scoreboard players set updateRooms wishlist.vars 0
execute as @a at @s run function dungeons:update_player
execute unless score updateRooms wishlist.vars matches 0 \
        run function dungeons:update_rooms

# dungeons:tick_5t

schedule function dungeons:tick_5t 5t
execute at @a \
        at @e[tag=wishlist.dungeon.treasure,distance=..8] \
        run particle minecraft:wax_off ~ ~0.5 ~ 0.1 0.1 0.1 0 1 normal


# dungeons:add_chunk

function dungeons:room_args
execute if data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] Chunk is already in a room", \
                "color":"yellow" \
        }
execute store result storage dungeons:vars args.room int 1 \
        run scoreboard players get @s wishlist.selectedRoom
function dungeons:add_chunk_1 with storage dungeons:vars args

# dungeons:add_chunk_1

$data modify storage dungeons:conf chunks."$(chunk)" set value $(room)
$data modify storage dungeons:conf rooms[{id:$(room)}].chunks."$(chunk)" set value {}
$tellraw @s { \
        "translate":"[dungeons] Added chunk %s to room %s", \
        "with": ["$(chunk)","$(room)"], \
        "color":"yellow" \
}

# dungeons:add_room

function dungeons:room_args
execute if data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] Room already exists", \
                "color":"yellow" \
        }
execute store result storage dungeons:vars args.x int 1 \
        run scoreboard players get @s wishlist.x
execute store result storage dungeons:vars args.y int 1 \
        run scoreboard players get @s wishlist.y
execute store result storage dungeons:vars args.z int 1 \
        run scoreboard players get @s wishlist.z
data modify storage dungeons:vars args.d set from entity @s Dimension
data modify storage dungeons:vars args.room set from storage dungeons:conf nextRoom
function dungeons:add_room_1 with storage dungeons:vars args
function dungeons:add_chunk_1 with storage dungeons:vars args


# dungeons:add_room_1

$scoreboard players set n wishlist.vars $(room)
execute store result storage dungeons:conf nextRoom int 1 \
        run scoreboard players add n wishlist.vars 1

$data modify storage dungeons:conf rooms append value {id:$(room),d:"$(d)",x:$(x),y:$(y),z:$(z)}
$scoreboard players set @s wishlist.selectedRoom $(room)
$tellraw @s { \
        "translate":"[dungeons] Created room %s", \
        "with": ["$(room)"], \
        "color":"yellow" \
}


# dungeons:break_pot

advancement revoke @s only dungeons:break_pot

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s attack \
        run function dungeons:break_pot_1



# dungeons:break_pot_1

execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
function dungeons:empty_pot
execute at @s run particle minecraft:item{ \
        item:{ \
                id:"minecraft:carrot_on_a_stick", \
                components:{"minecraft:custom_model_data":{"floats":[271838]}} \
                } \
        } \
        ~ ~ ~ 0.3 0.3 0.3 0.05 40
execute at @s run playsound minecraft:block.decorated_pot.shatter block @a ~ ~ ~
function wishlist:dismiss




# dungeons:clear_rooms

execute if data storage dungeons:conf rooms[{}] \
        run data modify storage dungeons:conf rooms[{}].state set value 0

execute if data storage dungeons:conf locks[{state:2b}] \
        run data modify storage dungeons:conf locks[{state:2b}].state set value 0b

execute store result storage dungeons:conf currentRun int 1 \
        run scoreboard players add currentRun wishlist.vars 1
scoreboard players set emptyDungeon wishlist.vars 1

scoreboard players set dungeonBoss wishlist.vars 0

function dungeons:pop_blocks

tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Dungeon reset!", \
        "color":"yellow" \
}

# dungeons:empty_pot

execute if data entity @s Passengers[0].item.components.minecraft:custom_data.table \
        run function dungeons:empty_pot_1 with entity @s \
        Passengers[0].item.components.minecraft:custom_data
execute if data entity @s Passengers[0].item.components.minecraft:custom_data.item \
        run function dungeons:empty_pot_2 with entity @s \
        Passengers[0].item.components.minecraft:custom_data



# dungeons:empty_pot_1

$execute at @s run loot spawn ~ ~ ~ loot $(table)

# dungeons:empty_pot_2

$execute at @s run summon minecraft:item ~ ~1 ~ { \
        Item:$(item), \
        Motion: [0.0,0.2,0.0], \
        PickupDelay:40s, \
        Tags:[wishlist.dungeon.treasure] \
}

execute as @a[scores={wishlist.currentRoom=0..}] \
        at @s \
        run playsound dungeons:sfx.find_treasure block @s ~ ~ ~ 0.5

# dungeons:hit_mob_with_pot

advancement revoke @s only dungeons:hit_mob_with_pot
tag @s add wishlist.match
execute at @s positioned ^ ^ ^16 as @e[distance=..16] run function dungeons:hit_mob_with_pot_1
tag @s remove wishlist.match

# dungeons:hit_mob_with_pot_1

execute on attacker unless entity @s[tag=wishlist.match] run return fail
execute store result score tmp wishlist.vars run data get entity @s HurtTime
execute if score tmp wishlist.vars matches 0 run return fail

execute facing entity @a[tag=wishlist.match,limit=1] feet \
        positioned ^ ^ ^0 \
        summon marker \
        run function wishlist:throw_1
execute facing entity @a[tag=wishlist.match,limit=1] feet \
        positioned ^ ^ ^-1 \
        summon marker \
        run function wishlist:throw_2

execute store result entity @s Motion[0] double 0.0025 \
        run scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
data modify entity @s Motion[1] set value 0.2
execute store result entity @s Motion[2] double 0.0025 \
        run scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars

damage @s 16 minecraft:falling_block

# dungeons:move_room

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to move", \
                "color":"yellow" \
        }
execute store result storage dungeons:vars args.x int 1 \
        run scoreboard players get @s wishlist.x
execute store result storage dungeons:vars args.y int 1 \
        run scoreboard players get @s wishlist.y
execute store result storage dungeons:vars args.z int 1 \
        run scoreboard players get @s wishlist.z
data modify storage dungeons:vars args.d set from entity @s Dimension
function dungeons:move_room_1 with storage dungeons:vars args


# dungeons:move_room_1

$data modify storage dungeons:conf rooms[{id:$(room)}] merge value {d:"$(d)",x:$(x),y:$(y),z:$(z)}
$tellraw @s { \
        "translate":"[dungeons] Moved room %s", \
        "with": ["$(room)"], \
        "color":"yellow" \
}


# dungeons:pick_up_pot

advancement revoke @s only dungeons:pick_up_pot

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function dungeons:pick_up_pot_1


# dungeons:pick_up_pot_1

execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
function dungeons:empty_pot
execute at @s run loot spawn ~ ~ ~ loot dungeons:pot
execute at @s run playsound minecraft:block.decorated_pot.break block @a ~ ~ ~
function wishlist:dismiss



# dungeons:place_pot_1

$execute in $(d) \
        positioned $(x) $(y) $(z) \
        run summon minecraft:interaction ~ ~ ~ { \
                Tags:[wishlist.dungeon.pot,wishlist.dungeon.passive], \
                Passengers: [ \
                        { \
                                id: "minecraft:item_display", \
                                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                                item: { \
                                        id: "minecraft:carrot_on_a_stick", \
                                        count: 1, \
                                        components:{ \
                                                "minecraft:custom_model_data":{"floats":[271838]}, \
                                                "minecraft:custom_data":{table: "$(table)"} \
                                        } \
                                } \
                        } \
                ] \
        }

# dungeons:place_pot_2

$execute in $(d) \
        positioned $(x) $(y) $(z) \
        run summon minecraft:interaction ~ ~ ~ { \
                Tags:[wishlist.dungeon.pot,wishlist.dungeon.passive], \
                Passengers: [ \
                        { \
                                id: "minecraft:item_display", \
                                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                                item: { \
                                        id: "minecraft:carrot_on_a_stick", \
                                        count: 1, \
                                        components:{ \
                                                "minecraft:custom_model_data":{"floats":[271838]}, \
                                                "minecraft:custom_data":{item: $(item)} \
                                        } \
                                } \
                        } \
                ] \
        }

# dungeons:place_pots

execute unless data storage dungeons:vars pop[0] run return 0
execute if data storage dungeons:vars pop[0].table \
        run function dungeons:place_pot_1 with storage dungeons:vars pop[0]
execute if data storage dungeons:vars pop[0].item \
        run function dungeons:place_pot_2 with storage dungeons:vars pop[0]
data remove storage dungeons:vars pop[0]
scoreboard players add count wishlist.vars 1
function dungeons:place_pots

# dungeons:pop

scoreboard players set count wishlist.vars 0
execute summon minecraft:marker run function dungeons:pop_1

# dungeons:pop_1

execute unless data storage dungeons:vars pop[0] run return run kill @s
data modify storage wishlist:args stash set from storage dungeons:vars pop[0]
function stash:pop2
data remove storage dungeons:vars pop[0]
scoreboard players add count wishlist.vars 1
function dungeons:pop_1

# dungeons:pop_blocks

execute unless data storage dungeons:conf blocks[0] run return 0
function dungeons:pop_blocks_1 with storage dungeons:conf blocks[0]
data remove storage dungeons:conf blocks[0]
function dungeons:pop_blocks


# dungeons:pop_blocks_1

$execute in $(d) \
        positioned $(x) $(y) $(z) \
        run function dungeons:pop_blocks_2 {id:"$(id)"}


# dungeons:pop_blocks_2

$execute if loaded ~ ~ ~ run return run setblock ~ ~ ~ $(id)
forceload add ~ ~
$setblock ~ ~ ~ $(id)
forceload remove ~ ~

# dungeons:pop_hostile

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to pop", \
                "color":"yellow" \
        }
function dungeons:pop_hostile_1 with storage dungeons:vars args


# dungeons:pop_hostile_1

$data modify storage dungeons:vars pop set from storage dungeons:conf rooms[{id:$(room)}].hostiles
execute unless data storage dungeons:vars pop[0] \
        run return run tellraw @s { \
                "translate":"[dungeons] No hostile mobs to pop", \
                "color":"yellow" \
        }
data modify storage dungeons:vars pop[{}].NoAI set value 1b
data modify storage dungeons:vars pop[{}].Tags append value wishlist.dungeon.hostile.debug
function dungeons:pop
$execute as @e[tag=wishlist.dungeon.hostile.debug] \
        unless score @s wishlist.currentRoom matches 0.. \
        run scoreboard players set @s wishlist.currentRoom $(room)
tellraw @s { \
        "translate":"[dungeons] Popped %s hostile mobs", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}
$data modify storage dungeons:conf rooms[{id:$(room)}].hostiles set value []

# dungeons:pop_loot

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to pop", \
                "color":"yellow" \
        }
function dungeons:pop_loot_1 with storage dungeons:vars args
tellraw @s { \
        "translate":"[dungeons] Popped %s loot", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}



# dungeons:pop_loot_1

$data modify storage dungeons:vars pop set from storage dungeons:conf rooms[{id:$(room)}].loot
$data modify storage dungeons:conf rooms[{id:$(room)}].loot set value []
function dungeons:pop


# dungeons:pop_passive

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to pop", \
                "color":"yellow" \
        }
function dungeons:pop_passive_1 with storage dungeons:vars args

# dungeons:pop_passive_1

$data modify storage dungeons:vars pop set from storage dungeons:conf rooms[{id:$(room)}].passives
execute unless data storage dungeons:vars pop[0] \
        run return run tellraw @s { \
                "translate":"[dungeons] No passive mobs to pop", \
                "color":"yellow" \
        }
data modify storage dungeons:vars pop[{}].Tags append value wishlist.dungeon.passive.debug
function dungeons:pop
$execute as @e[tag=wishlist.dungeon.passive.debug] \
        unless score @s wishlist.currentRoom matches 0.. \
        run scoreboard players set @s wishlist.currentRoom $(room)
tellraw @s { \
        "translate":"[dungeons] Popped %s passive mobs", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}
$data modify storage dungeons:conf rooms[{id:$(room)}].passives set value []

# dungeons:ready_rooms

execute unless data storage dungeons:vars buf[0] run return 0
execute store result score state wishlist.vars \
        run data get storage dungeons:vars buf[0].state
execute if score state wishlist.vars matches 0 \
        run function dungeons:update_empty with storage dungeons:vars buf[0]
data remove storage dungeons:vars buf[0]
function dungeons:ready_rooms

# dungeons:remove_chunk

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No chunk to remove", \
                "color":"yellow" \
        }
execute store result storage dungeons:vars args.room int 1 \
        run scoreboard players get @s wishlist.selectedRoom
function dungeons:remove_chunk_1 with storage dungeons:vars args

# dungeons:remove_chunk_1

$data remove storage dungeons:conf chunks."$(chunk)"
$tellraw @s { \
        "translate":"[dungeons] Removed chunk %s from room %s", \
        "with": ["$(chunk)","$(room)"], \
        "color":"yellow" \
}

$data remove storage dungeons:conf rooms[{id:$(room)}].chunks.$(chunk)
$execute store result score n wishlist.vars \
        run data get storage dungeons:conf rooms[{id:$(room)}].chunks
execute unless score n wishlist.vars matches 0 run return fail
$data remove storage dungeons:conf rooms[{id:$(room)}]
$tellraw @s { \
        "translate":"[dungeons] Removed room %s", \
        "with": ["$(room)"], \
        "color":"yellow" \
}

# dungeons:room_args

data modify storage dungeons:vars args set value {}
data modify storage dungeons:vars args.d set from entity @s Dimension
execute store result storage dungeons:vars args.x int 1 run scoreboard players get @s wishlist.cx
execute store result storage dungeons:vars args.y int 1 run scoreboard players get @s wishlist.cy
execute store result storage dungeons:vars args.z int 1 run scoreboard players get @s wishlist.cz
function dungeons:room_args_1 with storage dungeons:vars args


# dungeons:room_args_1

$data modify storage dungeons:vars args set value {chunk:"$(d).$(x).$(y).$(z)"}
$data modify storage dungeons:vars args.room set from storage dungeons:conf chunks."$(d).$(x).$(y).$(z)"

# dungeons:select_room

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to select", \
                "color":"yellow" \
        }
execute store result score @s wishlist.selectedRoom run data get storage dungeons:vars args.room
tellraw @s { \
        "translate":"[dungeons] Selected room %s", \
        "with":[{"score":{"name":"@s","objective":"wishlist.selectedRoom"}}], \
        "color":"yellow" \
}

# dungeons:setdown_boss

execute as @a run function looping_music:stop
scoreboard players set dungeonBoss wishlist.vars 0

# dungeons:setup_boss

execute as @a[scores={wishlist.currentRoom=0..}] at @s \
        run function looping_music:play with storage dungeons:conf boss_track
scoreboard players set dungeonBoss wishlist.vars 1

# dungeons:setup_hostile

scoreboard players operation @s wishlist.currentRun = currentRun wishlist.vars

data modify storage wishlist:args args set value []

scoreboard players operation n wishlist.vars = enchants wishlist.vars
scoreboard players operation n wishlist.vars *= 10 wishlist.vars
scoreboard players add n wishlist.vars 100

data modify storage wishlist:args args set value \
        {attr: "minecraft:max_health"}
execute store result score base wishlist.vars \
        run attribute @s minecraft:max_health base get
scoreboard players operation base wishlist.vars *= n wishlist.vars
execute store result storage wishlist:args args.base int 0.01 \
        run scoreboard players get base wishlist.vars
function dungeons:setup_hostile_1 with storage wishlist:args args
execute store result entity @s Health float 1 \
        run attribute @s minecraft:max_health get

data modify storage wishlist:args args set value \
        {attr: "minecraft:attack_damage"}
execute store result score base wishlist.vars \
        run attribute @s minecraft:attack_damage base get
scoreboard players operation base wishlist.vars *= n wishlist.vars
execute store result storage wishlist:args args.base int 0.01 \
        run scoreboard players get base wishlist.vars
function dungeons:setup_hostile_1 with storage wishlist:args args

execute if data entity @s HandItems[0].components.minecraft:custom_data.wishlist_dungeon_run \
        run data modify entity @s HandItems[0].components.minecraft:custom_data.wishlist_dungeon_run \
        set from storage dungeons:conf currentRun
execute if data entity @s HandItems[1].components.minecraft:custom_data.wishlist_dungeon_run \
        run data modify entity @s HandItems[1].components.minecraft:custom_data.wishlist_dungeon_run \
        set from storage dungeons:conf currentRun

attribute @s minecraft:spawn_reinforcements base set 0

# dungeons:setup_hostile_1

$attribute @s $(attr) base set $(base)

$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] %s %s = %s", \
        "with": [{"selector":"@s"},"$(attr)","$(base)"], \
        "color": "yellow" \
}

# dungeons:setup_passive

scoreboard players operation @s wishlist.currentRun = currentRun wishlist.vars

data modify entity @s \
        Offers.Recipes[{sell:{tag:{wishlist:{small_key:{}}}}}].sell.components.minecraft:custom_data.wishlist_dungeon_run \
        set from storage dungeons:conf currentRun
data modify entity @s \
        Offers.Recipes[{sell:{tag:{wishlist:{big_key:{}}}}}].sell.components.minecraft:custom_data.wishlist_dungeon_run \
        set from storage dungeons:conf currentRun


# dungeons:stash_block

data modify storage dungeons:conf blocks append value {}
setblock 0 0 0 barrel
loot insert 0 0 0 mine ~ ~ ~ minecraft:netherite_pickaxe
data modify storage dungeons:conf blocks[-1].id set from block 0 0 0 Items[0].id
setblock 0 0 0 air
execute summon marker run function dungeons:stash_block_1



# dungeons:stash_block_1

data modify storage dungeons:conf blocks[-1].d \
        set from entity @p Dimension
execute store result storage dungeons:conf blocks[-1].x int 1 \
        run data get entity @s Pos[0]
execute store result storage dungeons:conf blocks[-1].y int 1 \
        run data get entity @s Pos[1]
execute store result storage dungeons:conf blocks[-1].z int 1 \
        run data get entity @s Pos[2]
kill @s

# dungeons:stash_custom_loot

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
scoreboard players set done wishlist.vars 0
data modify storage dungeons:vars loot set value {}
data modify storage dungeons:vars loot.item set from entity @s Inventory[{Slot:-106b}]
data remove storage dungeons:vars loot.item.Slot
data modify storage dungeons:vars loot.d set from entity @p Dimension
execute store result storage dungeons:vars loot.x int 1 \
        run scoreboard players get @s wishlist.x
execute store result storage dungeons:vars loot.y int 1 \
        run scoreboard players get @s wishlist.y
execute store result storage dungeons:vars loot.z int 1 \
        run scoreboard players get @s wishlist.z
function dungeons:stash_custom_loot_1 with storage dungeons:vars args
execute unless score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Stashed %s", \
                "with": [{"nbt":"loot.item.id","storage":"dungeons:vars"}], \
                "color":"yellow" \
        }
execute if score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Nothing to stash", \
                "color":"yellow" \
        }




# dungeons:stash_custom_loot_1

$data modify storage dungeons:conf rooms[{id:$(room)}].loot append from storage dungeons:vars loot
scoreboard players set done wishlist.vars 1


# dungeons:stash_all_hostiles
function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
function dungeons:stash_all_hostiles_1 with storage dungeons:vars args

# dungeons:stash_all_hostiles_1
scoreboard players set count wishlist.vars 0
$execute as @e[tag=wishlist.dungeon.hostile.debug,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:stash_hostile_1 {room:$(room)}
tellraw @s { \
        "translate":"[dungeons] Stashed %s hostile mobs", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

# dungeons:discard_all_hostiles
function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to discard", \
                "color":"yellow" \
        }
function dungeons:discard_all_hostiles_1 with storage dungeons:vars args

# dungeons:discard_all_hostiles_1
$execute as @e[tag=wishlist.dungeon.hostile.debug,scores={wishlist.currentRoom=$(room)}] \
        run function wishlist:discard
tellraw @s { \
        "translate":"[dungeons] Hostiles discarded", \
        "color":"yellow" \
        }

# dungeons:stash_all_passives
function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
function dungeons:stash_all_passives_1 with storage dungeons:vars args

# dungeons:stash_all_passives_1
scoreboard players set count wishlist.vars 0
$execute as @e[tag=wishlist.dungeon.passive.debug,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:stash_passive_1 {room:$(room)}
tellraw @s { \
        "translate":"[dungeons] Stashed %s passive mobs", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

# dungeons:discard_all_passives
function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to discard", \
                "color":"yellow" \
        }
function dungeons:discard_all_passives_1 with storage dungeons:vars args

# dungeons:discard_all_passives_1
$execute as @e[tag=wishlist.dungeon.passive.debug,scores={wishlist.currentRoom=$(room)}] \
        run function wishlist:discard
tellraw @s { \
        "translate":"[dungeons] Passives discarded", \
        "color":"yellow" \
        }

# dungeons:stash_hostile

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
scoreboard players set count wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=!player,distance=..4,sort=nearest,limit=1] \
        run function dungeons:stash_hostile_1 with storage dungeons:vars args
execute unless score count wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Stashed %s as mob", \
                "with": [{"nbt":"mob.id","storage":"dungeons:vars"}], \
                "color":"yellow" \
        }
execute if score count wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Nothing to stash", \
                "color":"yellow" \
        }


# dungeons:stash_hostile_1

tag @s remove wishlist.dungeon.hostile.debug
data remove entity @s Attributes[{}].Modifiers[{Name:"Random spawn bonus"}]
data modify storage dungeons:vars mob set value {}
function wishlist:get_entity_type {dst:"storage dungeons:vars mob.id"}
data modify storage dungeons:vars mob.Pos set from entity @s Pos
data modify storage dungeons:vars mob.ArmorItems set from entity @s ArmorItems
data modify storage dungeons:vars mob.HandItems set from entity @s HandItems
data modify storage dungeons:vars mob.ArmorDropChances set from entity @s ArmorDropChances
data modify storage dungeons:vars mob.HandDropChances set from entity @s HandDropChances
data modify storage dungeons:vars mob.Attributes set from entity @s Attributes
data modify storage dungeons:vars mob.Tags set from entity @s Tags
$data modify storage dungeons:conf rooms[{id:$(room)}].hostiles append from storage dungeons:vars mob
function wishlist:dismiss
scoreboard players add count wishlist.vars 1


# dungeons:stash_loot

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
scoreboard players set done wishlist.vars 0
$data modify storage dungeons:vars loot set value {table:"$(table)"}
data modify storage dungeons:vars loot.d set from entity @p Dimension
execute store result storage dungeons:vars loot.x int 1 \
        run scoreboard players get @s wishlist.x
execute store result storage dungeons:vars loot.y int 1 \
        run scoreboard players get @s wishlist.y
execute store result storage dungeons:vars loot.z int 1 \
        run scoreboard players get @s wishlist.z
function dungeons:stash_loot_1 with storage dungeons:vars args
execute unless score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Stashed %s", \
                "with": [{"nbt":"loot.table","storage":"dungeons:vars"}], \
                "color":"yellow" \
        }
execute if score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Nothing to stash", \
                "color":"yellow" \
        }



# dungeons:stash_loot_1


$data modify storage dungeons:conf rooms[{id:$(room)}].loot append from storage dungeons:vars loot
scoreboard players set done wishlist.vars 1


# dungeons:stash_passive

function dungeons:room_args
execute unless data storage dungeons:vars args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
scoreboard players set count wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=!player,distance=..4,sort=nearest,limit=1] \
        run function dungeons:stash_passive_1 with storage dungeons:vars args
execute unless score count wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Stashed %s as passive", \
                "with": [{"nbt":"stash.id","storage":"wishlist:args"}], \
                "color":"yellow" \
        }
execute if score count wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Nothing to stash", \
                "color":"yellow" \
        }



# dungeons:stash_passive_1

tag @s remove wishlist.dungeon.passive.debug
function stash:stash
$data modify storage dungeons:conf rooms[{id:$(room)}].passives append from storage wishlist:args stash
function wishlist:dismiss
scoreboard players add count wishlist.vars 1


# dungeons:throw_bomb
execute unless predicate dungeons:holding_dynamite run return fail

data modify storage wishlist:args args set value { \
        id: "minecraft:item", \
        data: { \
                Item:{ \
                        id:"minecraft:carrot_on_a_stick", \
                        count:1b, \
                        components:{"minecraft:custom_model_data":{"floats":[271837]}} \
                }, \
                PickupDelay: 10000, \
                Tags:[wishlist.bomb], \
        } \
}

execute unless entity @s[tag=wishlist.sneaking] \
        anchored eyes \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0006}
execute if entity @s[tag=wishlist.sneaking] \
        anchored feet \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0006}

playsound minecraft:entity.tnt.primed player @s ~ ~ ~

item modify entity @s weapon.mainhand wishlist:consume_one

# dungeons:throw_pot
execute unless predicate dungeons:holding_pot run return fail

data modify storage wishlist:args args set value { \
        id: "minecraft:snowball", \
        data: { \
                Item:{ \
                        id:"minecraft:carrot_on_a_stick", \
                        count:1, \
                        components:{"minecraft:custom_model_data":{"floats":[271839]}} \
                }, \
                PickupDelay: 10000, \
                Tags:[wishlist.dungeon.pot], \
        } \
}

execute unless entity @s[tag=wishlist.sneaking] \
        anchored eyes \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0010}
execute if entity @s[tag=wishlist.sneaking] \
        anchored feet \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0010}

item modify entity @s weapon.mainhand wishlist:consume_one


# dungeons:update_bomb

execute store result score age wishlist.vars run data get entity @s Age
execute if score age wishlist.vars matches ..40 run return fail
particle minecraft:explosion_emitter ~ ~ ~
playsound minecraft:entity.generic.explode block @a ~ ~ ~
execute as @e[distance=0..1] run damage @s 128 minecraft:explosion by @p
execute as @e[distance=1..2] run damage @s 96 minecraft:explosion by @p
execute as @e[distance=2..3] run damage @s 64 minecraft:explosion by @p
execute as @e[distance=3..4] run damage @s 32 minecraft:explosion by @p

data modify storage dungeons:vars locks set value []
data modify storage dungeons:vars locks \
        append from storage dungeons:conf locks[{type:"crack",state:1b}]
function dungeons:open_cracks

kill @s

# dungeons:update_bossbar

$bossbar set dungeons:$(room) players @a[scores={wishlist.currentRoom=$(room)}]
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Update bossbar for room %s", \
        "with": ["$(room)"], \
        "color": "yellow" \
}

# dungeons:update_clear


# dungeons:update_debug

scoreboard players operation room wishlist.vars = @s wishlist.currentRoom
execute as @e[tag=wishlist.dungeon.hostile] \
        if score @s wishlist.currentRoom = room wishlist.vars \
        run function wishlist:dismiss
execute as @e[tag=wishlist.dungeon.passive] \
        if score @s wishlist.currentRoom = room wishlist.vars \
        run function wishlist:dismiss


# dungeons:update_empty

$execute in $(d) positioned $(x) $(y) $(z) unless entity @a[distance=..64] \
        run return fail

$data modify storage dungeons:conf rooms[{id:$(id)}].state set value 1
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Room %s ready!", \
        "with": ["$(id)"], \
        "color": "yellow" \
}


# dungeons:update_health

execute store result score cur wishlist.vars \
        run data get entity @s Health
scoreboard players operation acc wishlist.vars += cur wishlist.vars

# dungeons:update_max_health

execute store result score cur wishlist.vars \
        run attribute @s minecraft:max_health get
scoreboard players operation acc wishlist.vars += cur wishlist.vars

# dungeons:update_open

scoreboard players set acc wishlist.vars 0
$execute as @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:update_health
$execute store result bossbar dungeons:$(room) value \
        run scoreboard players get acc wishlist.vars

$execute if entity @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run return fail

data modify storage dungeons:vars locks set value []
$data modify storage dungeons:vars locks \
        append from storage dungeons:conf locks[{type:"mob",room:$(room)}]
function dungeons:open_locks

$execute if data storage dungeons:conf rooms[{id:$(room)}].boss \
        run function dungeons:setdown_boss

$data modify storage dungeons:conf rooms[{id:$(room)}].state set value 3
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Room %s clear!", \
        "with": ["$(room)"], \
        "color": "yellow" \
}


# dungeons:update_player

scoreboard players operation @s wishlist.lastRoom = @s wishlist.currentRoom
scoreboard players set @s wishlist.currentRoom -1
function dungeons:room_args
execute if data storage dungeons:vars args.room \
        store result score @s wishlist.currentRoom \
        run data get storage dungeons:vars args.room
execute if score @s wishlist.lastRoom = @s wishlist.currentRoom run return 0

execute if entity @s[gamemode=!spectator] \
        run scoreboard players set updateRooms wishlist.vars 1

execute if score @s wishlist.currentRoom matches 0.. \
        if score @s wishlist.lastRoom matches -1 \
        unless score dungeonBoss wishlist.vars matches 1 \
        run function looping_music:play with storage dungeons:conf music_track
execute if score @s wishlist.currentRoom matches 0.. \
        if score @s wishlist.lastRoom matches -1 \
        if score dungeonBoss wishlist.vars matches 1 \
        run function looping_music:play with storage dungeons:conf boss_track

data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.room int 1 \
        run scoreboard players get @s wishlist.currentRoom
function dungeons:update_bossbar with storage wishlist:args args
execute store result storage wishlist:args args.room int 1 \
        run scoreboard players get @s wishlist.lastRoom
function dungeons:update_bossbar with storage wishlist:args args


# dungeons:update_ready

$data modify storage dungeons:conf rooms[{id:$(room)}].state set value 2
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Room %s open!", \
        "with": ["$(room)"], \
        "color": "yellow" \
}

$data modify storage dungeons:vars pop set from storage dungeons:conf rooms[{id:$(room)}].hostiles
execute if data storage dungeons:vars pop[0] \
        run data modify storage dungeons:vars pop[] merge value { \
                NoAI:0b, \
                DeathLootTable:"dungeons:mob_drop", \
                PersistenceRequired:1b \
        }
execute if data storage dungeons:vars pop[0] \
        run data modify storage dungeons:vars pop[].Tags append value wishlist.dungeon.hostile
function dungeons:pop
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Placed %s hostiles", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

$data modify storage dungeons:vars pop set from storage dungeons:conf rooms[{id:$(room)}].passives
execute if data storage dungeons:vars pop[0] \
        run data modify storage dungeons:vars pop[] merge value { \
                DeathLootTable:"minecraft:empty", \
                PersistenceRequired:1b \
        }
execute if data storage dungeons:vars pop[0] \
        run data modify storage dungeons:vars pop[].Tags append value wishlist.dungeon.passive
function dungeons:pop
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Placed %s passives", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

$data modify storage dungeons:vars pop set from storage dungeons:conf rooms[{id:$(room)}].loot
scoreboard players set count wishlist.vars 0
function dungeons:place_pots
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Placed %s pots", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

$execute as @e[tag=wishlist.dungeon.hostile] \
        unless score @s wishlist.currentRoom matches 0.. \
        run scoreboard players set @s wishlist.currentRoom $(room)
$execute as @e[tag=wishlist.dungeon.passive] \
        unless score @s wishlist.currentRoom matches 0.. \
        run scoreboard players set @s wishlist.currentRoom $(room)

data modify storage wishlist:args enchants set value []
execute as @a[scores={wishlist.currentRoom=0..}] \
        run data modify storage wishlist:args enchants \
        append from entity @s Inventory[{}].components.minecraft:custom_data.Enchantments[].lvl
scoreboard players set enchants wishlist.vars 1
function dungeons:accumulate_enchants
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Adjusting hostiles by %s", \
        "with": [{"score":{"name":"enchants","objective":"wishlist.vars"}}], \
        "color": "yellow" \
}
$execute as @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:setup_hostile

$execute as @e[tag=wishlist.dungeon.passive,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:setup_passive

scoreboard players set acc wishlist.vars 0
$execute as @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:update_max_health
$execute store result bossbar dungeons:$(room) max \
        run scoreboard players get acc wishlist.vars

$execute if data storage dungeons:conf rooms[{id:$(room)}].boss \
        run function dungeons:setup_boss


# dungeons:accumulate_enchants
execute unless data storage wishlist:args enchants[0] run return 0
execute store result score tmp wishlist.vars run data get storage wishlist:args enchants[0]
scoreboard players operation enchants wishlist.vars += tmp wishlist.vars
data remove storage wishlist:args enchants[0]
function dungeons:accumulate_enchants

# dungeons:update_room

data modify storage dungeons:vars args set value {}
execute store result storage dungeons:vars args.room int 1 \
        run scoreboard players get @s wishlist.currentRoom
function dungeons:update_room_1 with storage dungeons:vars args

# dungeons:update_room_1

$execute if entity @a[scores={wishlist.currentRoom=$(room)},predicate=dungeons:editing] \
        run return run function dungeons:update_debug {room:$(room)}
$execute store result score state wishlist.vars \
        run data get storage dungeons:conf rooms[{id:$(room)}].state
$execute if score state wishlist.vars matches 1 \
        run return run function dungeons:update_ready {room:$(room)}
$execute if score state wishlist.vars matches 2 \
        run return run function dungeons:update_open {room:$(room)}
$execute if score state wishlist.vars matches 3 \
        run return run function dungeons:update_clear {room:$(room)}

# dungeons:update_rooms

execute as @a[gamemode=!spectator,scores={wishlist.currentRoom=0..}] \
        at @s \
        run function dungeons:update_room
scoreboard players set emptyDungeon wishlist.vars 0

# dungeons:create_mob_lock
data modify storage dungeons:vars args set value {type:"mob"}
execute store result storage dungeons:vars args.room int 1 run \
        scoreboard players get @s wishlist.currentRoom
function dungeons:create_lock

# dungeons:create_small_key_lock
data modify storage dungeons:vars args set value {type:"small_key"}
function dungeons:create_lock

# dungeons:create_big_key_lock
data modify storage dungeons:vars args set value {type:"big_key"}
function dungeons:create_lock

# dungeons:create_crack
data modify storage dungeons:vars args set value {type:"crack"}
function dungeons:raycast_wall
execute unless data storage dungeons:vars args.d \
        run return run tellraw @s { \
                "translate":"[dungeons] No wall to crack", \
                "color":"yellow" \
                }
function dungeons:create_lock_1 with storage dungeons:vars args

# dungeons:create_lock
function dungeons:raycast_door
execute unless data storage dungeons:vars args.d \
        run return run tellraw @s { \
                "translate":"[dungeons] No door to lock", \
                "color":"yellow" \
                }
function dungeons:create_lock_1 with storage dungeons:vars args

# dungeons:create_lock_1
$execute if data storage dungeons:conf locks[{d:"$(d)",x:$(x),y:$(y),z:$(z)}] \
        run return run tellraw @s { \
                "translate":"[dungeons] Lock already exists", \
                "color":"yellow" \
                }
data modify storage dungeons:conf locks append from storage dungeons:vars args
tellraw @s { \
        "translate":"[dungeons] Lock created", \
        "color":"yellow" \
        }

# dungeons:remove_lock
function dungeons:raycast_door
execute unless data storage dungeons:vars args.d \
        run return run tellraw @s { \
                "translate":"[dungeons] No door to unlock", \
                "color":"yellow" \
                }
function dungeons:remove_lock_1 with storage dungeons:vars args

# dungeons:remove_lock_1
$execute store success score done wishlist.vars \
        run data remove storage dungeons:conf locks[{d:"$(d)",x:$(x),y:$(y),z:$(z)}]
execute if score done wishlist.vars matches 0 \
        run return run tellraw @s { \
                "translate":"[dungeons] No lock to remove", \
                "color":"yellow" \
                }
$execute in $(d) positioned $(x) $(y) $(z) run kill @e[tag=wishlist.dungeon.lock,distance=..2]
tellraw @s { \
        "translate":"[dungeons] Lock removed", \
        "color":"yellow" \
        }

# dungeons:remove_crack
function dungeons:raycast_wall
execute unless data storage dungeons:vars args.d \
        run return run tellraw @s { \
                "translate":"[dungeons] No wall to fix", \
                "color":"yellow" \
                }
function dungeons:remove_lock_1 with storage dungeons:vars args

# dungeons:remove_crack_1
$execute store success score done wishlist.vars \
        run data remove storage dungeons:conf locks[{d:"$(d)",x:$(x),y:$(y),z:$(z)}]
execute if score done wishlist.vars matches 0 \
        run return run tellraw @s { \
                "translate":"[dungeons] No crack to remove", \
                "color":"yellow" \
                }
tellraw @s { \
        "translate":"[dungeons] Crack removed", \
        "color":"yellow" \
        }

# dungeons:raycast_door
scoreboard players set steps wishlist.vars 45
execute anchored eyes positioned ^ ^ ^ run function dungeons:raycast_door_1

# dungeons:raycast_door_1
execute if score steps wishlist.vars matches 0 run return fail

execute if block ~ ~ ~ #minecraft:doors[facing=east,half=lower,hinge=left] \
        positioned ~ ~ ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=east,half=lower,hinge=right] \
        positioned ~ ~ ~-1 align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=east,half=upper,hinge=left] \
        positioned ~ ~-1 ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=east,half=upper,hinge=right] \
        positioned ~ ~-1 ~-1 align xyz summon marker \
        run return run function dungeons:raycast_door_2

execute if block ~ ~ ~ #minecraft:doors[facing=north,half=lower,hinge=left] \
        positioned ~ ~ ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=north,half=lower,hinge=right] \
        positioned ~-1 ~ ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=north,half=upper,hinge=left] \
        positioned ~ ~-1 ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=north,half=upper,hinge=right] \
        positioned ~-1 ~-1 ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2

execute if block ~ ~ ~ #minecraft:doors[facing=west,half=lower,hinge=left] \
        positioned ~ ~ ~-1 align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=west,half=lower,hinge=right] \
        positioned ~ ~ ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=west,half=upper,hinge=left] \
        positioned ~ ~-1 ~-1 align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=west,half=upper,hinge=right] \
        positioned ~ ~-1 ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2

execute if block ~ ~ ~ #minecraft:doors[facing=south,half=lower,hinge=left] \
        positioned ~-1 ~ ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=south,half=lower,hinge=right] \
        positioned ~ ~ ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=south,half=upper,hinge=left] \
        positioned ~-1 ~-1 ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2
execute if block ~ ~ ~ #minecraft:doors[facing=south,half=upper,hinge=right] \
        positioned ~ ~-1 ~ align xyz summon marker \
        run return run function dungeons:raycast_door_2

execute unless block ~ ~ ~ #wishlist:non_full run return fail
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run return run function dungeons:raycast_door_1

# dungeons:raycast_door_2
data modify storage dungeons:vars args.d set from entity @p Dimension
execute store result storage dungeons:vars args.x int 1 run data get entity @s Pos[0]
execute store result storage dungeons:vars args.y int 1 run data get entity @s Pos[1]
execute store result storage dungeons:vars args.z int 1 run data get entity @s Pos[2]
data modify storage dungeons:vars args.state set value 0b
kill @s
return 1

# dungeons:raycast_wall
scoreboard players set steps wishlist.vars 45
execute anchored eyes positioned ^ ^ ^ run function dungeons:raycast_wall_1

# dungeons:raycast_wall_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ #wishlist:non_full \
        positioned ~-0.5 ~-0.5 ~-0.5 align xyz summon marker \
        run return run function dungeons:raycast_door_2
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run return run function dungeons:raycast_wall_1

# dungeons:setup_locks
execute unless data storage dungeons:vars locks[0] run return 0
function dungeons:setup_lock with storage dungeons:vars locks[0]
data remove storage dungeons:vars locks[0]
function dungeons:setup_locks

# dungeons:setup_lock
$execute in $(d) positioned $(x) $(y) $(z) \
        unless entity @a[gamemode=!spectator,distance=..32] \
        run return fail
$data modify storage dungeons:conf locks[{d:"$(d)",x:$(x),y:$(y),z:$(z)}].state set value 1b
$execute in $(d) positioned $(x).0 $(y).0 $(z).0 run function dungeons:setup_$(type)_lock
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Setup lock %s %s %s %s", \
        "with":["$(d)","$(x)","$(y)","$(z)"], \
        "color":"yellow" \
}

# dungeons:setup_small_key_lock
data modify storage dungeons:vars args set value { \
        display: { \
                Tags:[wishlist.dungeon.lock], \
                transformation:[1.0f,0f,0f,0f,0f,1.0f,0f,0f,0f,0f,1.0f,0f,0f,0f,0f,1f], \
                item: { \
                        id: "minecraft:item_frame", \
                        count:1, \
                        components:{"minecraft:custom_model_data":{"floats":[271868]}} \
                } \
        } \
}
function dungeons:setup_lock_overlay

# dungeons:setup_big_key_lock
data modify storage dungeons:vars args set value { \
        display: { \
                Tags:[wishlist.dungeon.lock], \
                transformation:[1.0f,0f,0f,0f,0f,1.0f,0f,0f,0f,0f,1.0f,0f,0f,0f,0f,1f], \
                item: { \
                        id: "minecraft:item_frame", \
                        count:1, \
                        components:{"minecraft:custom_model_data":{"floats":[271869]}} \
                } \
        } \
}
function dungeons:setup_lock_overlay

# dungeons:setup_lock_overlay
execute if block ~ ~ ~ #minecraft:doors[facing=east] \
        positioned ~0.093750 ~1 ~1 run function dungeons:setup_lock_overlay_x
execute if block ~ ~ ~ #minecraft:doors[facing=north] \
        positioned ~1 ~1 ~0.9062 run function dungeons:setup_lock_overlay_z
execute if block ~ ~ ~ #minecraft:doors[facing=west] \
        positioned ~0.9062 ~1 ~1 run function dungeons:setup_lock_overlay_x
execute if block ~ ~ ~ #minecraft:doors[facing=south] \
        positioned ~1 ~1 ~0.09375 run function dungeons:setup_lock_overlay_z

# dungeons:setup_lock_overlay_x
data modify storage dungeons:vars args.display.Rotation set value [90.0f,0.0f]
function dungeons:setup_lock_overlay_1 with storage dungeons:vars args

# dungeons:setup_lock_overlay_z
data modify storage dungeons:vars args.display.Rotation set value [0.0,0.0]
function dungeons:setup_lock_overlay_1 with storage dungeons:vars args

# dungeons:setup_lock_overlay_1
$summon minecraft:item_display ~ ~ ~ $(display)

# dungeons:use_small_key
execute unless predicate dungeons:holding_small_key run return fail
function dungeons:raycast_door
execute unless data storage dungeons:vars args.d run return fail
data modify storage dungeons:vars types set value {"small_key":1b}
function dungeons:try_open_lock with storage dungeons:vars args
execute if score done wishlist.vars matches 0 run return fail
item modify entity @s weapon.mainhand wishlist:consume_one

# dungeons:use_big_key
execute unless predicate dungeons:holding_big_key run return fail
function dungeons:raycast_door
execute unless data storage dungeons:vars args.d run return fail
data modify storage dungeons:vars types set value {"small_key":1b,"big_key":1b}
function dungeons:try_open_lock with storage dungeons:vars args

# dungeons:try_open_lock
scoreboard players set done wishlist.vars 0
$execute unless data storage dungeons:conf \
        locks[{d:"$(d)",x:$(x),y:$(y),z:$(z),state:1b}] run return fail

$function dungeons:check_lock_type with storage dungeons:conf \
        locks[{d:"$(d)",x:$(x),y:$(y),z:$(z),state:1b}]
execute if score go wishlist.vars matches 0 \
        run return run title @s actionbar { "translate":"dungeons.bad_key" }

data modify storage wishlist:args item set from entity @s SelectedItem
execute store result score run wishlist.vars \
        run data get storage wishlist:args item.components.minecraft:custom_data.wishlist_dungeon_run
execute unless score run wishlist.vars = currentRun wishlist.vars \
        run return run title @s actionbar { "translate":"dungeons.old_key" }

$function dungeons:open_lock {d:"$(d)",x:$(x),y:$(y),z:$(z)}
scoreboard players set done wishlist.vars 1

# dungeons:check_lock_type
$execute store success score go wishlist.vars if data storage dungeons:vars types.$(type)

# dungeons:open_locks
execute unless data storage dungeons:vars locks[0] run return fail
function dungeons:open_lock with storage dungeons:vars locks[0]
data remove storage dungeons:vars locks[0]
function dungeons:open_locks

# dungeons:open_lock
$data modify storage dungeons:conf locks[{d:"$(d)",x:$(x),y:$(y),z:$(z)}].state set value 2b
$execute in $(d) positioned $(x) $(y) $(z) run function dungeons:open_lock_1

# dungeons:open_lock_1
execute positioned ~ ~-2 ~ run function dungeons:stash_block
execute positioned ~ ~-2 ~1 run function dungeons:stash_block
execute positioned ~1 ~-2 ~ run function dungeons:stash_block
execute positioned ~1 ~-2 ~1 run function dungeons:stash_block
fill ~ ~-2 ~ ~1 ~-2 ~1 redstone_torch
kill @e[tag=wishlist.dungeon.lock,distance=..2]

tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Door opened!", \
        "color": "yellow" \
}

execute as @a[scores={wishlist.currentRoom=0..}] \
        at @s \
        run playsound dungeons:sfx.open_door block @s ~ ~ ~ 0.5

# dungeons:open_cracks
execute unless data storage dungeons:vars locks[0] run return fail
function dungeons:open_crack with storage dungeons:vars locks[0]
data remove storage dungeons:vars locks[0]
function dungeons:open_cracks

# dungeons:open_crack
$execute in $(d) positioned $(x) $(y) $(z) unless function dungeons:open_crack_1 run return fail
$data modify storage dungeons:conf locks[{d:"$(d)",x:$(x),y:$(y),z:$(z)}].state set value 2b

# dungeons:open_crack_1
execute unless entity @s[distance=..4] run return fail

execute positioned ~ ~ ~ run function dungeons:stash_block
execute positioned ~ ~ ~1 run function dungeons:stash_block
execute positioned ~ ~1 ~ run function dungeons:stash_block
execute positioned ~ ~1 ~1 run function dungeons:stash_block
execute positioned ~1 ~ ~ run function dungeons:stash_block
execute positioned ~1 ~ ~1 run function dungeons:stash_block
execute positioned ~1 ~1 ~ run function dungeons:stash_block
execute positioned ~1 ~1 ~1 run function dungeons:stash_block
fill ~ ~ ~ ~1 ~1 ~1 minecraft:air
kill @s

tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Door opened!", \
        "color": "yellow" \
}

execute as @a[scores={wishlist.currentRoom=0..}] \
        at @s \
        run playsound dungeons:sfx.open_door block @s ~ ~ ~ 0.5

return 1
