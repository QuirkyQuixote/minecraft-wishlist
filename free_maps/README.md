Free Maps
=========

Allows copying a map on an item frame by clicking a blank map on it

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Clicking an empty map on an item frame containing a filled map will remove one
empty map from the player's hand (unless the player is in creative mode) and
give them a copy of the filled map.

Overhead
--------

Uses an advancement to check for players clicking on item frames with filled
maps while holding empty maps

Known Issues
------------

If the item frame is covered by a protection plugin such as worldguard, the
code that fixes the item frame rotation after the player clicks on it will
overcorrect and the map will end rotating counterclockwise.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:
