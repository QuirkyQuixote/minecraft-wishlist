# free_maps:copy
advancement revoke @s only free_maps:copy
execute unless function wishlist:raycast run return fail
scoreboard players set done wishlist.vars 0
function free_maps:copy_1 with storage wishlist:raycast {}
execute if score done wishlist.vars matches 0 run return fail
function free_maps:copy_3 with storage wishlist:args item

# free_maps:copy_1
$execute positioned $(x0) $(y0) $(z0) \
        as @e[type=#wishlist:item_frames,distance=..0.5] \
        run function free_maps:copy_2

# free_maps:copy_2
data modify storage wishlist:args item set value {name:''}
data modify storage wishlist:args item.id set from entity @s Item.components.minecraft:map_id
data modify storage wishlist:args item.name set from entity @s Item.components.minecraft:custom_name
execute store result score rot wishlist.vars run data get entity @s ItemRotation
scoreboard players remove rot wishlist.vars 1
execute store result entity @s ItemRotation byte 1 \
        run scoreboard players operation rot wishlist.vars %= 8 wishlist.vars
scoreboard players set done wishlist.vars 1

# free_maps:copy_3
item modify entity @s[gamemode=!creative] weapon.mainhand wishlist:consume_one
$give @s minecraft:filled_map[minecraft:map_id=$(id),minecraft:custom_name='$(name)']

