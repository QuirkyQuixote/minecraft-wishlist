# Adjust status of players wearing the firefighter suit

effect give @s minecraft:fire_resistance 1
effect give @s minecraft:resistance 1
effect give @s minecraft:slowness 1
effect clear @s minecraft:poison
effect clear @s minecraft:wither
advancement grant @s only wishlist:wear_custom_suit
