# Adjust status of players wearing each suit type

schedule function custom_suits:tick_1s 1s

tag @a remove wishlist.firefighter_suit
tag @a[nbt={Inventory:[{Slot:100b,tag:{suit:"firefighter"}},{Slot:101b,tag:{suit:"firefighter"}},{Slot:102b,tag:{suit:"firefighter"}},{Slot:103b,tag:{suit:"firefighter"}}]}] add wishlist.firefighter_suit

tag @a remove wishlist.diving_suit
tag @a[nbt={Inventory:[{Slot:100b,tag:{suit:"diving"}},{Slot:101b,tag:{suit:"diving"}},{Slot:102b,tag:{suit:"diving"}},{Slot:103b,tag:{suit:"diving"}}]}] add wishlist.diving_suit

tag @a remove wishlist.running_suit
tag @a[nbt={Inventory:[{Slot:100b,tag:{suit:"running"}},{Slot:101b,tag:{suit:"running"}},{Slot:102b,tag:{suit:"running"}},{Slot:103b,tag:{suit:"running"}}]}] add wishlist.running_suit
