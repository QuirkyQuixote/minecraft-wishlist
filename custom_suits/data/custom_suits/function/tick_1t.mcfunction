# Adjust status of players wearing each suit type

schedule function custom_suits:tick_1t 1t

execute as @a[tag=wishlist.firefighter_suit] \
        run function custom_suits:firefighter_suit
execute as @a[tag=wishlist.diving_suit] run function custom_suits:diving_suit
execute as @a[tag=wishlist.running_suit] run function custom_suits:running_suit
