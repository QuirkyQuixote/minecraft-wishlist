
# looping_music:load

scoreboard objectives add wishlist.ttlMusic dummy

execute unless data storage wishlist:looping_music players \
        run data modify storage wishlist:looping_music players set value []

function looping_music:tick_1s

# looping_music:play

function looping_music:stop
$data modify storage wishlist:args args set value \
        {track:"$(track)",ttl:$(ttl),volume:$(volume),pitch:$(pitch),minVolume:$(minVolume)}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function looping_music:play_1 with storage wishlist:args args

# looping_music:play_1

$data remove storage wishlist:looping_music players[{id:$(id)}]
data modify storage wishlist:looping_music players append from storage wishlist:args args
scoreboard players set @s wishlist.ttlMusic 0

$tellraw @a[tag=wishlist.debug] { \
        "translate":"[looping_music] Play %s for %s", \
        "with": ["$(track)","$(id)"], \
        "color": "yellow" \
}

# looping_music:stop

data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function looping_music:stop_1 with storage wishlist:args args

# looping_music:stop_1

$data modify storage wishlist:args args.track \
        set from storage wishlist:looping_music players[{id:$(id)}].track
function looping_music:stop_2 with storage wishlist:args args

# looping_music:stop_2

$stopsound @s master $(track)
$data remove storage wishlist:looping_music players[{id:$(id)}].track
scoreboard players set @s wishlist.ttlMusic -1


# looping_music:tick_1s

schedule function looping_music:tick_1s 1s

execute as @a run function looping_music:update


# looping_music:update

execute if score @s wishlist.ttlMusic matches -1 \
        run return fail
stopsound @s music
execute unless score @s wishlist.ttlMusic matches 0 \
        run scoreboard players remove @s wishlist.ttlMusic 1
execute unless score @s wishlist.ttlMusic matches 0 \
        run return 0
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function looping_music:update_1 with storage wishlist:args args

# looping_music:update_1

$data modify storage wishlist:args args \
        merge from storage wishlist:looping_music players[{id:$(id)}]
function looping_music:update_2 with storage wishlist:args args

# looping_music:update_2

$execute at @s run playsound $(track) master @s ~ ~ ~ $(volume) $(pitch) $(minVolume)
$scoreboard players set @s wishlist.ttlMusic $(ttl)
