Look Alive
==========

Add some limited animations to AI-less entities so they look less creepy:

* applies to all entities with the `wishlist.look_alive` tag,
* if a player comes is four blocks or less from them, they turn to look,
* otherwise, they have a chance to look at other random entity near them.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Overhead
--------

Updates every second:

* checks every entity in a 16-block radius around each player in search of
  tagged entities to update.
