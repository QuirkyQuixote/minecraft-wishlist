# look_alive:load
scoreboard players set 160 wishlist.vars 160
function look_alive:tick

# look_alive:tick
schedule function look_alive:tick 1s
execute at @a[gamemode=!spectator] \
        as @e[type=#wishlist:mobs,tag=wishlist.look_alive,distance=..16] \
        at @s \
        run function look_alive:update_entity

# look_alive:update_entity
execute if entity @a[gamemode=!spectator,distance=..4] \
        run return run function look_alive:face_player
function look_alive:face_entity

# look_alive:face_entity
# face random entity
function wishlist:recall
scoreboard players set t1 wishlist.vars 0
execute store result score t1 wishlist.vars \
        run data get storage wishlist:args memories.focus_timeout
execute store result score t0 wishlist.vars run time query gametime
execute if score t1 wishlist.vars < t0 wishlist.vars \
        run function look_alive:face_entity_1

# look_alive:face_entity_1
tag @s add wishlist.self
execute positioned ~-4 ~-1 ~-4 \
        run tag @e[type=!marker,type=!interaction,tag=!wishlist.dull,tag=!wishlist.self,dx=8,dy=2,dz=8,sort=random,limit=1] add wishlist.match
execute if entity @e[tag=wishlist.match] \
        at @s \
        anchored eyes \
        facing entity @e[tag=wishlist.match,limit=1] eyes \
        run function look_alive:face_entity_2
tag @e[distance=..16] remove wishlist.match
tag @s remove wishlist.self

execute store result score t1 wishlist.vars run random value 160..320
scoreboard players operation t1 wishlist.vars += t0 wishlist.vars
execute store result storage wishlist:args memories.focus_timeout int 1 \
        run scoreboard players get t1 wishlist.vars
function wishlist:memorize

# look_alive:face_entity_2
execute if block ^ ^ ^1 #wishlist:non_full run rotate @s ~ ~

# look_alive:face_player
execute facing entity @p[gamemode=!spectator] feet run rotate @s ~ ~
function look_alive:clamp_angle
advancement grant @p[gamemode=!spectator] only wishlist:interact_with_npc

# look_alive:clamp_angle
# prevent mobs from looking too straight up/down
execute store result score angle wishlist.vars run data get entity @s Rotation[1]
execute if score angle wishlist.vars matches 30.. run rotate @s ~ 30
execute if score angle wishlist.vars matches ..-30 run rotate @s ~ -30

