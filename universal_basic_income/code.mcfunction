# universal_basic_income:load

scoreboard objectives add wishlist.pay_day dummy

scoreboard players set 1728000 wishlist.vars 1728000

data modify storage wishlist:args all_chatter append value \
        "So I heard that the rulers of the upworld would rather die than UBI? That's sad"
data modify storage wishlist:args all_chatter append value \
        "Give me a minute; I'm waiting for my daily money"

function universal_basic_income:tick

# universal_basic_income:tick
schedule function universal_basic_income:tick 60s
execute store result score pay_day wishlist.vars run time query gametime
scoreboard players operation pay_day wishlist.vars /= 1728000 wishlist.vars

execute as @a[gamemode=!spectator,gamemode=!creative,tag=!wishlist.hidden] \
        unless score @s wishlist.pay_day = pay_day wishlist.vars \
        if score vault wishlist.vars matches 16.. \
        if entity @s[nbt={OnGround:1b}] \
        run function universal_basic_income:tick_1


# universal_basic_income:tick_1

execute at @s \
        rotated ~ 0 \
        run summon minecraft:enderman ^ ^ ^3 { \
          Tags:[wishlist.mail_giver,wishlist.look_alive], \
          CustomName:'"Post Carrier"', \
          NoAI:1b \
          }
playsound minecraft:entity.enderman.teleport neutral @a[distance=..8] ~ ~ ~
tellraw @s { \
        "translate":"wishlist.player_mail.receive_mail", \
        "with":["Welfare Office"], \
        "color":"dark_gray" \
        }
execute at @s \
        rotated ~ 0 \
        run loot spawn ^ ^1.25 ^1 loot universal_basic_income:check
advancement grant @s only wishlist:universal_basic_income
scoreboard players operation @s wishlist.pay_day = pay_day wishlist.vars
scoreboard players remove vault wishlist.vars 16

