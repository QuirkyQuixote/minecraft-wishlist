Universal Basic Income
======================

Give some emeralds to players periodically.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Player Mail](../player_mail) for mail deliverers
* Requires [Wishlist Resources](../resources) for media and text translations.

Overhead
--------

Updates once per minute:

* checks every player to see if it's time to pay them.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Universal Basic Income: get some emeralds to spend
