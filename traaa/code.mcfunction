
# traaa:load

data modify storage wishlist:args all_chatter append value \
        "Welcome to Trans Station!"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of lifetime stuff? It will last your entire life. One of them, at least"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of Vonturbola and Rallyim? They have been everywhere"
data modify storage wishlist:args all_chatter append value \
        "You're awesome!"
data modify storage wishlist:args all_chatter append value \
        "You're valid!"
data modify storage wishlist:args all_chatter append value \
        "You deserve hugs!"

data modify storage wishlist:args all_chatter append value \
        "Today I'm gonna spend most of the day at the shops in Figaro... and it might not be enough!"
data modify storage wishlist:args all_chatter append value \
        "Did you know that you can get a free house and warehouse access in the Figaro Community?"
data modify storage wishlist:args all_chatter append value \
        "I'm flabbergasted at the amount of people who cooperated to build the city of Figaro"

data modify storage wishlist:args all_chatter append value \
        "Ever wondered how the mail gets everywhere? It's Black Bastion's Work"
data modify storage wishlist:args all_chatter append value \
        "High into the sky~ Among the swirling mists~"
data modify storage wishlist:args all_chatter append value \
        "I didn't know that the only way to reach Black Bastion was to ride a ship... on the air"

data modify storage wishlist:args all_chatter append value \
        "There are no propellers, no sails, no exhausts at Ker Amont; and yet it flies!"
data modify storage wishlist:args all_chatter append value \
        "The Ker Railway goes everywhere: good and bad… like that creepy swamp…"
data modify storage wishlist:args all_chatter append value \
        "Humming Lake? the sound is faint, but it's there; it comes from deep down"

function traaa:tick_3s
function traaa:tick_1t

# traaa:tick_1t
schedule function traaa:tick_1t 1t
execute in minecraft:overworld as @a[scores={wishlist.x=2985,wishlist.y=99..106,wishlist.z=-943}] at @s run teleport @s ~-29 ~ ~
execute in minecraft:overworld as @a[scores={wishlist.x=2957,wishlist.y=99..106,wishlist.z=-943}] at @s run teleport @s ~29 ~ ~
execute in minecraft:overworld as @a[scores={wishlist.x=2985,wishlist.y=99..106,wishlist.z=-959}] at @s run teleport @s ~-29 ~ ~
execute in minecraft:overworld as @a[scores={wishlist.x=2957,wishlist.y=99..106,wishlist.z=-959}] at @s run teleport @s ~29 ~ ~
execute in minecraft:overworld positioned 2847269 319 12843736 run teleport @a[distance=..20] 465 76 565

# traaa:tick_3s
schedule function traaa:tick_3s 3s
execute at @a as @e[tag=traaa.custom_trader,distance=..16] \
        run function traaa:custom_trader
execute at @a run tag @e[type=#traaa:gossipy,distance=..8] add wishlist.gossipy

# traaa:custom_trader

execute if entity @s[tag=traaa.map_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args map_trader
execute if entity @s[tag=traaa.ferry_driver] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args ferry_driver
execute if entity @s[tag=traaa.train_driver] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args train_driver
execute if entity @s[tag=traaa.bartender] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args bartender
execute if entity @s[tag=traaa.baker] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args baker
execute if entity @s[tag=traaa.sushi_chef] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args sushi_chef
execute if entity @s[tag=traaa.noodle_chef] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args noodle_chef
execute if entity @s[tag=traaa.waffle_chef] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args waffle_chef
execute if entity @s[tag=traaa.pizza_chef] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args pizza_chef
execute if entity @s[tag=traaa.book_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args book_trader
execute if entity @s[tag=traaa.box_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args box_trader
execute if entity @s[tag=traaa.pharmacist] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args pharmacist
execute if entity @s[tag=traaa.tea_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args tea_trader
execute if entity @s[tag=traaa.grocer] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args grocer
execute if entity @s[tag=traaa.bank_teller] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args bank_teller
execute if entity @s[tag=traaa.hiring_manager_1] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args hiring_manager_1
execute if entity @s[tag=traaa.hiring_manager_2] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args hiring_manager_2
execute if entity @s[tag=traaa.hiring_manager_3] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args hiring_manager_3
execute if entity @s[tag=traaa.pet_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args pet_trader
execute if entity @s[type=minecraft:wandering_trader] \
        run data modify entity @s DespawnDelay set value 2000000000

# traaa:new_player
#execute if entity @s[gamemode=adventure] run tellraw @s {"text":"Welcome to the server! Your current gamemode is adventure! To be able to play on the server, you have to be manually verified by a server admin"}
gamemode survival @s[gamemode=adventure]

