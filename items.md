Custom Items
============

Every custom item that can be added with the wishlist datapacks:

* ![astrolabe] [Astrolabe](astrolabe)
* ![aura_lens] [Aura Lens](aura_lens)
* ![bauble] [Bauble](magic)
* ![modern_book_yellow] [Better Nicks](better_nicks)
* ![big_key] [Big Key](dungeons)
* ![cushion] [Cushion](cushion)
* ![wand] [Dismiss Hire](summon)
* ![dynamite] [Dynamite](dungeons)
* ![modern_book_yellow] [Entity Profiling](entity_profiling)
* ![eraser] [Eraser](graffiti)
* ![grimoire] [Grimoire](magic)
* ![modern_book_yellow] [Item Frames](item_frames)
* ![lock_key] [Key](lock)
* ![lock_blank] [Key Blank](lock)
* ![large_light] [Large Light](modern_lights)
* ![medium_light] [Medium Light](modern_lights)
* ![music_disc_mindless_marionette] [Music Disc (Mindless Marienette)](custom_discs)
* ![music_disc_rusty_rain] [Music Disc (Rusty Rain)](custom_discs)
* ![noise_box] [Noise Box](noise_box)
* ![pan] [Pan](better_recipes)
* ![wand] [Place Bait](bait)
* ![modern_book_yellow] [Popular Displays](popular_displays)
* ![pot] [Pot](better_recipes)
* ![wand] [Remove Bait](bait)
* ![wand] [Skip Back](skip)
* ![wand] [Skip Home](skip)
* ![ticket] [Skip to](skip)
* ![wand] [Skip to Coords](skip)
* ![wand] [Skip to Player](skip)
* ![small_key] [Small Key](dungeons)
* ![small_light] [Small Light](modern_lights)
* ![steamer] [Steamer](better_recipes)
* ![contract] [Summon Hire](summon)
* ![contract] [Summon Armorer](summon)
* ![contract] [Summon Butcher](summon)
* ![contract] [Summon Cartographer](summon)
* ![contract] [Summon Cleric](summon)
* ![contract] [Summon Enderfolk](summon)
* ![contract] [Summon Evoker](summon)
* ![contract] [Summon Farmer](summon)
* ![contract] [Summon Fisher](summon)
* ![contract] [Summon Fletcher](summon)
* ![contract] [Summon Illusioner](summon)
* ![contract] [Summon Leatherworker](summon)
* ![contract] [Summon Librarian](summon)
* ![contract] [Summon Mason](summon)
* ![contract] [Summon Nitwit](summon)
* ![contract] [Summon Piglin](summon)
* ![contract] [Summon Piglin Brute](summon)
* ![contract] [Summon Piglin Child](summon)
* ![contract] [Summon Pillager](summon)
* ![contract] [Summon Shepherd](summon)
* ![contract] [Summon Toolsmith](summon)
* ![contract] [Summon Villager](summon)
* ![contract] [Summon Villager Child](summon)
* ![contract] [Summon Vindicator](summon)
* ![contract] [Summon Weaponsmith](summon)
* ![contract] [Summon Witch](summon)
* ![white_marker] [Thick White Marker](graffiti)
* ![light_gray_marker] [Thick Light Gray Marker](graffiti)
* ![gray_marker] [Thick Gray Marker](graffiti)
* ![black_marker] [Thick Black Marker](graffiti)
* ![brown_marker] [Thick Brown Marker](graffiti)
* ![red_marker] [Thick Red Marker](graffiti)
* ![orange_marker] [Thick Orange Marker](graffiti)
* ![yellow_marker] [Thick Yellow Marker](graffiti)
* ![lime_marker] [Thick Lime Marker](graffiti)
* ![green_marker] [Thick Green Marker](graffiti)
* ![cyan_marker] [Thick Cyan Marker](graffiti)
* ![light_blue_marker] [Thick Light Blue Marker](graffiti)
* ![blue_marker] [Thick Blue Marker](graffiti)
* ![purple_marker] [Thick Purple Marker](graffiti)
* ![magenta_marker] [Thick Magenta Marker](graffiti)
* ![pink_marker] [Thick Pink Marker](graffiti)
* ![white_marker] [Thin White Marker](graffiti)
* ![light_gray_marker] [Thin Light Gray Marker](graffiti)
* ![gray_marker] [Thin Gray Marker](graffiti)
* ![black_marker] [Thin Black Marker](graffiti)
* ![brown_marker] [Thin Brown Marker](graffiti)
* ![red_marker] [Thin Red Marker](graffiti)
* ![orange_marker] [Thin Orange Marker](graffiti)
* ![yellow_marker] [Thin Yellow Marker](graffiti)
* ![lime_marker] [Thin Lime Marker](graffiti)
* ![green_marker] [Thin Green Marker](graffiti)
* ![cyan_marker] [Thin Cyan Marker](graffiti)
* ![light_blue_marker] [Thin Light Blue Marker](graffiti)
* ![blue_marker] [Thin Blue Marker](graffiti)
* ![purple_marker] [Thin Purple Marker](graffiti)
* ![magenta_marker] [Thin Magenta Marker](graffiti)
* ![pink_marker] [Thin Pink Marker](graffiti)
* ![ticket] [Ticket](magic)
* ![wand] [Wand](magic)

Custom Food Items
-----------------

* ![bowl_of_pale_chunks] [Bowl of Allium](better_recipes)
* ![bowl_of_orange_stuff] [Bowl of Aged Cheese](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Apple](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Apple Juice](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Apple Liquor](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Apple Wine](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Azure Bluet](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Azure Bluet Tea](better_recipes)
* ![bagel] [Bowl of Bagel](better_recipes)
* ![baguette] [Bowl of Baguette](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Baked Potato](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Batter](better_recipes)
* ![bowl_of_black_liquid] [Bowl of Beer](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Beetroot](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Beetroot Juice](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Beetroot Liquor](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Beetroot Wine](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Beetroot Seeds](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Boiled Dough](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Blue Orchid](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Blue Orchid Tea](better_recipes)
* ![bowl_of_brown_chunks] [Bowl of Brown Mushroom](better_recipes)
* ![burger] [Bowl of Burger](better_recipes)
* ![bowl_of_orange_stuff] [Bowl of Carrot](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Carrot Juice](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Carrot Liquor](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Carrot Wine](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Chamomile Tea](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Cheese](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Cheese Curds](better_recipes)
* ![bowl_of_brown_stuff] [Bowl of Cooked Beef](better_recipes)
* ![bowl_of_orange_stuff] [Bowl of Cooked Chicken](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Cooked Cod](better_recipes)
* ![bowl_of_brown_stuff] [Bowl of Cooked Mutton](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Cooked Noodles](better_recipes)
* ![pizza] [Bowl of Cooked Pizza](better_recipes)
* ![bowl_of_brown_stuff] [Bowl of Cooked Porkchop](better_recipes)
* ![bowl_of_orange_stuff] [Bowl of Cooked Rabbit](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Cooked Salmon](better_recipes)
* ![bowl_of_brown_chunks] [Bowl of Cookies](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Cornflower](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Cornflower Tea](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Custard](better_recipes)
* ![bowl_of_white_liquid] [Bowl of Curdled Milk](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Dandelion](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Dandelion Tea](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Dough](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Azure Bluet](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Blue Orchid](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Chamomile](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Cornflower](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Dandelion](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Lilac](better_recipes)
* ![bowl_of_white_stuff] [Bowl of Dried Lily of the Valley](better_recipes)
* ![bowl_of_orange_stuff] [Bowl of Dried Orange Tulip](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Peony](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Dried Pink Tulip](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Dried Poppy](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Dried Red Tulip](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Dried Rose](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Dried Sunflower](better_recipes)
* ![bowl_of_white_stuff] [Bowl of Dried White Tulip](better_recipes)
* ![hardtack] [Bowl of Hardtack](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Fish Broth](better_recipes)
* ![noodles] [Bowl of Fish Ramen](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Fish Stew](better_recipes)
* ![flatbread] [Bowl of Flatbread](better_recipes)
* ![bowl_of_orange_chunks] [Bowl of Glow Berries](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Glow Berry Juice](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Glow Berry Liquor](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Glow Berry Wine](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Lilac](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Lilac Tea](better_recipes)
* ![bowl_of_white_chunks] [Bowl of Lily of the Valley](better_recipes)
* ![bowl_of_white_liquid] [Bowl of Lily of the Valley Tea](better_recipes)
* ![mantou] [Bowl of Mantou](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Mash](better_recipes)
* ![bowl_of_brown_chunks] [Bowl of Meatballs](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Meat Broth](better_recipes)
* ![bowl_of_brown_stuff] [Bowl of Meat Patty](better_recipes)
* ![noodles] [Bowl of Meat Ramen](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Meat Stew](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Melon Juice](better_recipes)
* ![bowl_of_black_stuff] [Bowl of Melon Seeds](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Melon Liquor](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Melon Slice](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Melon Wine](better_recipes)
* ![bowl_of_mess] [Bowl of Mess](better_recipes)
* ![bowl_of_brown_stuff] [Bowl of Milk Bread](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Milk Dough](better_recipes)
* ![bowl_of_orange_chunks] [Bowl of Orange Tulip](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Orange Tulip Tea](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Oxeye Daisy](better_recipes)
* ![flatbread] [Bowl of Pancake](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Peony](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Peony Tea](better_recipes)
* ![bowl_of_red_chunks] [Bowl of Pink Tulip](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Pink Tulip Tea](better_recipes)
* ![bowl_of_red_chunks] [Bowl of Poppy](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Poppy Tea](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Potato](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Potato Juice](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Potato Liquor](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Potato Wine](better_recipes)
* ![bowl_of_black_chunks] [Bowl of Prunes](better_recipes)
* ![bowl_of_orange_stuff] [Bowl of Pumpkin](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Pumpkin Juice](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Pumpkin Liquor](better_recipes)
* ![bowl_of_orange_liquid] [Bowl of Pumpkin Wine](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Pumpkin Seeds](better_recipes)
* ![bowl_of_red_chunks] [Bowl of Red Tulip](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Red Tulip Tea](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Risen Dough](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Risen Milk Dough](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Raw Beef](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Raw Chicken](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Raw Cod](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Raw Cookies](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Raw Custard](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Raw Fish Broth](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Raw Fish Stew](better_recipes)
* ![bowl_of_red_chunks] [Bowl of Raw Meatballs](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Raw Meat Broth](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Raw Meat Patty](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Raw Meat Stew](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Raw Mutton](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Raw Noodles](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Raw Pizza](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Raw Porkchop](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Raw Rabbit](better_recipes)
* ![bowl_of_red_stuff] [Bowl of Raw Salmon](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Raw Vegetable Broth](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Raw Vegetable Stew](better_recipes)
* ![bowl_of_red_chunks] [Bowl of Red Mushroom](better_recipes)
* ![bowl_of_red_chunks] [Bowl of Rose](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Rose Tea](better_recipes)
* ![bowl_of_red_chunks] [Bowl of Sweet Berries](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Sweet Berry Juice](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Sweet Berry Liquor](better_recipes)
* ![bowl_of_red_liquid] [Bowl of Sweet Berry Wine](better_recipes)
* ![bowl_of_pale_chunks] [Bowl of Sunflower](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Sunflower Tea](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Vegetable Broth](better_recipes)
* ![noodles] [Bowl of Vegetable Ramen](better_recipes)
* ![bowl_of_pale_liquid] [Bowl of Vegetable Stew](better_recipes)
* ![bowl_of_pale_stuff] [Bowl of Wheat Seeds](better_recipes)
* ![bowl_of_white_chunks] [Bowl of White Tulip](better_recipes)
* ![bowl_of_white_liquid] [Bowl of White Tulip Tea](better_recipes)
* ![bowl_of_black_liquid] [Bowl of Whisky](better_recipes)
* ![bowl_of_black_liquid] [Bowl of Wort](better_recipes)


[astrolabe]: resources/assets/wishlist/textures/item/astrolabe.png ""
[aura_lens]: resources/assets/wishlist/textures/item/aura_lens.png ""
[bauble]: resources/assets/wishlist/textures/item/bauble.png ""
[modern_book_yellow]: resources/assets/wishlist/textures/item/modern_book_yellow.png ""
[big_key]: resources/assets/wishlist/textures/item/big_key.png ""
[contract]: resources/assets/wishlist/textures/item/contract.png ""
[cushion]: resources/assets/wishlist/textures/item/cushion.png ""
[dynamite]: resources/assets/wishlist/textures/item/dynamite.png ""
[modern_book_yellow]: resources/assets/wishlist/textures/item/modern_book_yellow.png ""
[eraser]: resources/assets/wishlist/textures/item/eraser.png ""
[grimoire]: resources/assets/wishlist/textures/item/book.png ""
[modern_book_yellow]: resources/assets/wishlist/textures/item/modern_book_yellow.png ""
[lock_key]: resources/assets/wishlist/textures/item/lock_key.png ""
[lock_blank]: resources/assets/wishlist/textures/item/lock_blank.png ""
[large_light]: resources/assets/wishlist/textures/item/large_light.png ""
[medium_light]: resources/assets/wishlist/textures/item/medium_light.png ""
[music_disc_mindless_marionette]: resources/assets/wishlist/textures/item/music_disc_mindless_marionette.png ""
[music_disc_rusty_rain]: resources/assets/wishlist/textures/item/music_disc_rusty_rain.png ""
[noise_box]: resources/assets/wishlist/textures/item/noise_box_on.png ""
[pan]: resources/assets/wishlist/textures/item/pan.png ""
[modern_book_yellow]: resources/assets/wishlist/textures/item/modern_book_yellow.png ""
[pot]: resources/assets/wishlist/textures/item/pot.png ""
[small_key]: resources/assets/wishlist/textures/item/small_key.png ""
[small_light]: resources/assets/wishlist/textures/item/small_light.png ""
[steamer]: resources/assets/wishlist/textures/item/steamer.png ""
[white_marker]: resources/assets/wishlist/textures/item/white_marker.png ""
[light_gray_marker]: resources/assets/wishlist/textures/item/light_gray_marker.png ""
[gray_marker]: resources/assets/wishlist/textures/item/gray_marker.png ""
[black_marker]: resources/assets/wishlist/textures/item/black_marker.png ""
[brown_marker]: resources/assets/wishlist/textures/item/brown_marker.png ""
[red_marker]: resources/assets/wishlist/textures/item/red_marker.png ""
[orange_marker]: resources/assets/wishlist/textures/item/orange_marker.png ""
[yellow_marker]: resources/assets/wishlist/textures/item/yellow_marker.png ""
[lime_marker]: resources/assets/wishlist/textures/item/lime_marker.png ""
[green_marker]: resources/assets/wishlist/textures/item/green_marker.png ""
[cyan_marker]: resources/assets/wishlist/textures/item/cyan_marker.png ""
[light_blue_marker]: resources/assets/wishlist/textures/item/light_blue_marker.png ""
[blue_marker]: resources/assets/wishlist/textures/item/blue_marker.png ""
[purple_marker]: resources/assets/wishlist/textures/item/purple_marker.png ""
[magenta_marker]: resources/assets/wishlist/textures/item/magenta_marker.png ""
[pink_marker]: resources/assets/wishlist/textures/item/pink_marker.png ""
[ticket]: resources/assets/wishlist/textures/item/ticket.png ""
[wand]: resources/assets/wishlist/textures/item/wand.png ""
[bowl_of_mess]: resources/assets/wishlist/textures/item/bowl_of_mess.png ""
[bowl_of_white_liquid]: resources/assets/wishlist/textures/item/bowl_of_white_liquid.png ""
[bowl_of_pale_liquid]: resources/assets/wishlist/textures/item/bowl_of_pale_liquid.png ""
[bowl_of_orange_liquid]: resources/assets/wishlist/textures/item/bowl_of_orange_liquid.png ""
[bowl_of_red_liquid]: resources/assets/wishlist/textures/item/bowl_of_red_liquid.png ""
[bowl_of_brown_liquid]: resources/assets/wishlist/textures/item/bowl_of_brown_liquid.png ""
[bowl_of_black_liquid]: resources/assets/wishlist/textures/item/bowl_of_black_liquid.png ""
[bowl_of_white_stuff]: resources/assets/wishlist/textures/item/bowl_of_white_stuff.png ""
[bowl_of_pale_stuff]: resources/assets/wishlist/textures/item/bowl_of_pale_stuff.png ""
[bowl_of_orange_stuff]: resources/assets/wishlist/textures/item/bowl_of_orange_stuff.png ""
[bowl_of_red_stuff]: resources/assets/wishlist/textures/item/bowl_of_red_stuff.png ""
[bowl_of_brown_stuff]: resources/assets/wishlist/textures/item/bowl_of_brown_stuff.png ""
[bowl_of_black_stuff]: resources/assets/wishlist/textures/item/bowl_of_black_stuff.png ""
[bowl_of_white_chunks]: resources/assets/wishlist/textures/item/bowl_of_white_chunks.png ""
[bowl_of_pale_chunks]: resources/assets/wishlist/textures/item/bowl_of_pale_chunks.png ""
[bowl_of_orange_chunks]: resources/assets/wishlist/textures/item/bowl_of_orange_chunks.png ""
[bowl_of_red_chunks]: resources/assets/wishlist/textures/item/bowl_of_red_chunks.png ""
[bowl_of_brown_chunks]: resources/assets/wishlist/textures/item/bowl_of_brown_chunks.png ""
[bowl_of_black_chunks]: resources/assets/wishlist/textures/item/bowl_of_black_chunks.png ""
[pizza]: resources/assets/wishlist/textures/item/pizza_slice.png ""
[burger]: resources/assets/wishlist/textures/item/cheeseburger.png ""
[noodles]: resources/assets/grabeltone/textures/item/noodles.png ""
[flatbread]: resources/assets/grabeltone/textures/item/flatbread.png ""
[baguette]: resources/assets/grabeltone/textures/item/baguette.png ""
[bagel]: resources/assets/grabeltone/textures/item/bagel.png ""
[mantou]: resources/assets/grabeltone/textures/item/mantou.png ""
[hardtack]: resources/assets/grabeltone/textures/item/hardtack.png ""
