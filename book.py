#!/bin/python3

# book.py - Create a loot table that generates a book from its JSON pages
#
# Usage: book.py
#       [-a|--author <author>]
#       [-t|--title <title>]
#       [-x|--extra <extra>]
#       <output>
#       <pages...>

import argparse
import json
import os

parser = argparse.ArgumentParser(
        prog='book.py',
        description='Create minecraft loot table for a book from its pages',
        epilog='You must provide at least one page, and it must be valid JSON')
parser.add_argument('-a', '--author')
parser.add_argument('-t', '--title')
parser.add_argument('-x', '--extra')
parser.add_argument('output', metavar='OUTPUT', type=str, nargs=1, help='output file')
parser.add_argument('pages', metavar='PAGE', type=str, nargs='+', help='a page for the book')
args = parser.parse_args()

author = args.author if args.author else "Anonymous"
title = {"raw":args.title if args.title else "Untitled"}
pages = []
for p in args.pages:
    with open(p, 'r') as f:
        pages.append({"raw":json.dumps(json.load(f))})
extra = json.loads(args.extra) if args.extra else {}

os.makedirs(os.path.dirname(args.output[0]), exist_ok=True)
with open(args.output[0], 'w') as f:
    f.write(json.dumps({
        "pools": [
            {
                "rolls": 1,
                "entries": [
                    {
                        "type": "minecraft:item",
                        "name": "minecraft:written_book",
                        "functions": [
                            {
                                "function": "minecraft:set_components",
                                "components": {
                                    "minecraft:custom_model_data":{
                                        "floats":[271832]
                                        },
                                    "minecraft:written_book_content":{
                                        "author": author,
                                        "title": title,
                                        "pages": pages,
                                        },
                                    "minecraft:custom_data": extra
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }, indent=2))

