Copper Tools
============

Adds a craftable set of copper tools

Requirements
------------

* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Copper tools are just like golden tools, crafted in the same way, except with a
copper ingot instead of gold.

The only difference, gameplay-wise, is that they have 200 durability.

Overhead
--------

Known Issues
------------

Fixing a copper tool with an gold ingot or with another gold tool of the same
type (even if it's reskinned as a copper one) results in a golden tool.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

