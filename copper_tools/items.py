#!/bin/python3

import json
import os

items = [
        {
            "type":"shovel",
            "pattern": [
                " 1 ",
                " 2 ",
                " 2 "
                ]
            },
        {
            "type":"pickaxe",
            "pattern": [
                "111",
                " 2 ",
                " 2 "
                ]
            },
        {
            "type":"axe",
            "pattern": [
                " 11",
                " 21",
                " 2 "
                ]
            },
        {
            "type":"hoe",
            "pattern": [
                " 11",
                " 2 ",
                " 2 "
                ]
            },
        {
            "type":"sword",
            "pattern": [
                " 1 ",
                " 1 ",
                " 2 "
                ]
            },
        ]

os.makedirs('data/copper_tools/loot_table', exist_ok=True)
os.makedirs('data/copper_tools/recipe', exist_ok=True)
os.makedirs('assets/copper_tools/models/item', exist_ok=True)

for i in items:
    components = {
            "minecraft:custom_name": json.dumps({
                "translate":f"item.wishlist.copper_{i['type']}",
                "italic":False
                }),
            "minecraft:custom_model_data": {"floats":[607735]},
            "minecraft:max_damage": 200
            }

    with open(f'data/copper_tools/recipe/copper_{i["type"]}.json', 'w') as f:
        f.write(json.dumps({
            "type": "minecraft:crafting_shaped",
            "category": "equipment",
            "pattern": i['pattern'],
            "key": {
                "1": "minecraft:copper_ingot",
                "2": "minecraft:stick"
                },
            "result": {
                "id": f"minecraft:golden_{i['type']}",
                "count": 1,
                "components": components
                }
            }, indent=2))

    with open(f'data/copper_tools/loot_table/copper_{i["type"]}.json', 'w') as f:
        f.write(json.dumps({
            "pools": [
                {
                    "rolls": 1,
                    "entries": [
                        {
                            "type": "minecraft:item",
                            "name": f"minecraft:golden_{i['type']}",
                            "functions": [
                                {
                                    "function": "minecraft:set_components",
                                    "components": components
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }, indent=2))

